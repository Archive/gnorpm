/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <gnome.h>
#include "rpmquerydlg.h"
#include <string.h>
#include "misc.h"

static GtkObjectClass *parent_class = NULL;

static void rpm_query_dialog_class_init(RpmQueryDialogClass *klass);
static void rpm_query_dialog_init(RpmQueryDialog *dlg);
static void rpm_query_dialog_destroy(GtkObject *object);

guint rpm_query_dialog_get_type() {
  static int querydialog_type = 0;
  if (!querydialog_type) {
    GtkTypeInfo querydialog_info = {
      "RpmQueryDialog",
      sizeof(RpmQueryDialog),
      sizeof(RpmQueryDialogClass),
      (GtkClassInitFunc) rpm_query_dialog_class_init,
      (GtkObjectInitFunc) rpm_query_dialog_init,
      (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL
    };
    querydialog_type = gtk_type_unique(gtk_window_get_type(),
				       &querydialog_info);
  }
  return querydialog_type;
}

static void rpm_query_dialog_class_init(RpmQueryDialogClass *klass) {
  parent_class = gtk_type_class(gtk_window_get_type());
  GTK_OBJECT_CLASS(klass)->destroy = rpm_query_dialog_destroy;
}

static void rpm_query_dialog_init(RpmQueryDialog *self) {
  gtk_window_set_title(GTK_WINDOW(self), _("Package Info"));
  set_icon(GTK_WIDGET(self));

  self->notebook = gtk_notebook_new();
  gtk_container_set_border_width(GTK_CONTAINER(self->notebook), 5);
  gtk_notebook_set_scrollable(GTK_NOTEBOOK(self->notebook), TRUE);
  gtk_container_add(GTK_CONTAINER(self), self->notebook);
  gtk_widget_show(self->notebook);

  self->pages = NULL;
}

GtkWidget *rpm_query_dialog_new(DBHandle *hdl, GList *indices) {
  RpmQueryDialog *self;

  self = gtk_type_new(rpm_query_dialog_get_type());
  rpm_query_dialog_add_pages(self, hdl, indices);
  return GTK_WIDGET(self);
}

GtkWidget *rpm_query_dialog_new_from_files(DBHandle *hdl, GList *files) {
  RpmQueryDialog *self;

  self = gtk_type_new(rpm_query_dialog_get_type());
  rpm_query_dialog_add_page_files(self, hdl, files);
  return GTK_WIDGET(self);
}

void rpm_query_dialog_add_pages(RpmQueryDialog *self, DBHandle *hdl,
				GList *indices) {
  guint index;
  GtkWidget *page;

  /* this reduces db opens/closes while loading pages */
  db_handle_db_up(hdl);
  for (; indices != NULL; indices = indices->next) {
    index = GPOINTER_TO_UINT(indices->data);
    page = rpm_query_new_from_index(hdl, index);
    if (!page) continue;
    rpm_query_dialog_add_page(self, RPM_QUERY(page));
  }
  gtk_notebook_set_page(GTK_NOTEBOOK(self->notebook), 0);
  db_handle_db_down(hdl);
}

void rpm_query_dialog_add_page_files(RpmQueryDialog *self, DBHandle *hdl,
				     GList *files) {
  gchar *fname;
  GtkWidget *page;
  for (; files != NULL; files = files->next) {
    fname = files->data;
    page = rpm_query_new_from_file(hdl, fname);
    if (!page) continue;
    rpm_query_dialog_add_page(self, RPM_QUERY(page));
  }
  gtk_notebook_set_page(GTK_NOTEBOOK(self->notebook), 0);
}

void rpm_query_dialog_add_page(RpmQueryDialog *self, RpmQuery *page) {
  GtkWidget *label, *widget;
  int pos = 0;

  while ((widget = gtk_notebook_get_nth_page(GTK_NOTEBOOK(self->notebook),
	      				    pos)) != NULL) {
    if (strcmp(RPM_QUERY(widget)->pkg_name, page->pkg_name) > 0)
      break;
    ++pos;
  }
  gtk_container_set_border_width(GTK_CONTAINER(page), 5);
  label = gtk_label_new(page->pkg_name);
  gtk_notebook_insert_page(GTK_NOTEBOOK(self->notebook), GTK_WIDGET(page),
			   label, pos);
  gtk_widget_show(GTK_WIDGET(page));
  gtk_widget_show(label);
  self->pages = g_list_prepend(self->pages, page);
}
  
static void rpm_query_dialog_destroy(GtkObject *object) {
  g_list_free(RPM_QUERY_DIALOG(object)->pages);
  if (parent_class->destroy)
    (* parent_class->destroy)(object);
}

void rpm_query_dialog_set_verify_func(RpmQueryDialog *self, GtkRpmCallback cb,
				      gpointer data) {
  GList *pages;
  GtkObject *page;

  for (pages = self->pages; pages != NULL; pages = pages->next) {
    page = pages->data;
    gtk_signal_connect(page, "verify", (GtkSignalFunc)cb, data);
  }
}

void rpm_query_dialog_set_uninstall_func(RpmQueryDialog *self,
					 GtkRpmCallback cb, gpointer data) {
  GList *pages;
  GtkObject *page;

  for (pages = self->pages; pages != NULL; pages = pages->next) {
    page = pages->data;
    gtk_signal_connect(page, "uninstall", (GtkSignalFunc)cb, data);
  }
}

void rpm_query_dialog_set_install_func(RpmQueryDialog *self, GtkRpmCallback cb,
				      gpointer data) {
  GList *pages;
  GtkObject *page;

  for (pages = self->pages; pages != NULL; pages = pages->next) {
    page = pages->data;
    gtk_signal_connect(page, "install", (GtkSignalFunc)cb, data);
  }
}
void rpm_query_dialog_set_upgrade_func(RpmQueryDialog *self, GtkRpmCallback cb,
				      gpointer data) {
  GList *pages;
  GtkObject *page;

  for (pages = self->pages; pages != NULL; pages = pages->next) {
    page = pages->data;
    gtk_signal_connect(page, "upgrade", (GtkSignalFunc)cb, data);
  }
}
void rpm_query_dialog_set_checksig_func(RpmQueryDialog *self,
					GtkRpmCallback cb, gpointer data) {
  GList *pages;
  GtkObject *page;

  for (pages = self->pages; pages != NULL; pages = pages->next) {
    page = pages->data;
    gtk_signal_connect(page, "checksig", (GtkSignalFunc)cb, data);
  }
}

void rpm_query_dialog_set_close_func(RpmQueryDialog *self, GtkRpmCallback cb,
				      gpointer data) {
  GList *pages;
  GtkObject *page;

  for (pages = self->pages; pages != NULL; pages = pages->next) {
    page = pages->data;
    gtk_signal_connect(page, "close", (GtkSignalFunc)cb, data);
  }
}
