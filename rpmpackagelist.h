/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __RPM_PACKAGE_LIST_H__
#define __RPM_PACKAGE_LIST_H__

#include <gtk/gtk.h>
#include <libgnomeui/gnome-icon-list.h>
#include "gtkpathtree.h"
#include <rpmlib.h>
#include "dbhandle.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif

#define RPM_PACKAGE_LIST(obj) GTK_CHECK_CAST(obj, rpm_package_list_get_type(), RpmPackageList)
#define RPM_PACKAGE_LIST_CLASS(class) GTK_CHECK_CAST_CLASS(class, rpm_package_list_get_type(), RpmPackageListClass)
#define RPM_IS_PACKAGE_LIST(obj) GTK_CHECK_TYPE(obj, rpm_package_list_get_type())

typedef enum {
  RPM_PACKAGE_CLIST,  /* show packages in a column list */
  RPM_PACKAGE_ILIST   /* show packages in an icon list */
} RpmPackageListMode;

typedef struct _RpmPackageList RpmPackageList;
typedef struct _RpmPackageListClass RpmPackageListClass;

struct _RpmPackageList {
  GtkHPaned parent;
  GtkWidget *tree;

  /* right pane */
  GtkWidget *clist_cont;
  GtkWidget *ilist_cont;
  GtkWidget *clist;
  GtkWidget *ilist;
  DBHandle *hdl;
  gchar *cur_group;
  RpmPackageListMode mode;

  GList *selection;  /* a list of ints representing db offsets */
};

struct _RpmPackageListClass {
  GtkHPanedClass parent_class;

  void (* selection_changed)(RpmPackageList *self);
  void (* context_menu)(RpmPackageList *self, GdkEventButton *event,
			guint index);
  void (* query)(RpmPackageList *self, gpointer *index);
};

guint rpm_package_list_get_type(void);
GtkWidget *rpm_package_list_new(DBHandle *hdl);
void rpm_package_list_clear_selection(RpmPackageList *list);
void rpm_package_list_unselect(RpmPackageList *list, guint index);
void rpm_package_list_set_mode(RpmPackageList *list, RpmPackageListMode mode);
void rpm_package_list_update_pane(RpmPackageList *self);
void rpm_package_list_add_group(RpmPackageList *self, gchar *group);

#ifdef __cplusplus
}
#endif

#endif /* __RPM_PACKAGE_LIST_H__ */
