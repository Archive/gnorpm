/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <gnome.h>
#include "rpmpackagelist.h"
#include <unistd.h>
#include <fcntl.h>
#include <gtk/gtkmarshal.h>

enum {
  SELECTION_CHANGED,
  CONTEXT_MENU,
  QUERY,
  LAST_SIGNAL
};

static GtkHPanedClass *parent_class = NULL;
static guint rpmpkg_signals[LAST_SIGNAL] = { 0 };

static void rpm_package_list_class_init(RpmPackageListClass *klass);
static void rpm_package_list_init(RpmPackageList *list);
gint rpm_package_list_set_db(RpmPackageList *list, DBHandle *db);

static void rpm_package_list_destroy(GtkObject *obj);
static void rpm_package_list_update_clist(RpmPackageList *list,
					  gchar *group_name);

static void rpm_package_list_select_row(RpmPackageList *list, gint row,
					gint column, GdkEventButton *event);
static void rpm_package_list_unselect_row(RpmPackageList *list, gint row,
					  gint column, GdkEventButton *event);
static void rpm_package_list_select_icon(RpmPackageList *list, gint icon,
					 GdkEvent *event);
static void rpm_package_list_unselect_icon(RpmPackageList *list, gint icon,
					   GdkEvent *event);

static int clist_button_press(GtkCList *clist, GdkEventButton *event,
			      RpmPackageList *list);
static int ilist_button_press(GnomeIconList *ilist, GdkEventButton *event,
			      RpmPackageList *list);

guint rpm_package_list_get_type() {
  static guint packagelist_type = 0;
  if (!packagelist_type) {
    GtkTypeInfo packagelist_info = {
      "RpmPackageList",
      sizeof(RpmPackageList),
      sizeof(RpmPackageListClass),
      (GtkClassInitFunc) rpm_package_list_class_init,
      (GtkObjectInitFunc) rpm_package_list_init,
      (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };
    packagelist_type = gtk_type_unique(gtk_hpaned_get_type(),
				       &packagelist_info);
  }
  return packagelist_type;
}

static void rpm_package_list_class_init(RpmPackageListClass *klass) {
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class(gtk_hpaned_get_type());

  rpmpkg_signals[SELECTION_CHANGED] = 
    gtk_signal_new("selection_changed",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmPackageListClass, selection_changed),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);
  rpmpkg_signals[CONTEXT_MENU] =
    gtk_signal_new("context_menu",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmPackageListClass, context_menu),
		   gtk_marshal_NONE__POINTER_UINT,
		   GTK_TYPE_NONE, 2,
		   GTK_TYPE_GDK_EVENT, GTK_TYPE_UINT);
  rpmpkg_signals[QUERY] = 
    gtk_signal_new("query",
	           GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmPackageListClass, query),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);
  gtk_object_class_add_signals(object_class, rpmpkg_signals, LAST_SIGNAL);

  object_class->destroy = rpm_package_list_destroy;
}

static void rpm_package_list_init(RpmPackageList *self) {
  static char *headers[] = {N_("Package"), N_("Version"),
			    N_("Release"), N_("Summary")};
  gint i;
  GtkWidget *w;

  for (i = 0; i < sizeof(headers)/sizeof(char *); i++)
    headers[i] = _(headers[i]);

  gtk_paned_set_handle_size(GTK_PANED(self), 10);
  gtk_paned_set_gutter_size(GTK_PANED(self), 10);

  w = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(w), GTK_POLICY_AUTOMATIC,
				 GTK_POLICY_AUTOMATIC);
  gtk_widget_set_usize(w, 170, -1); /* set min width to 170 */
  gtk_paned_add1(GTK_PANED(self), w);
  gtk_widget_show(w);

  self->tree = gtk_path_tree_new(_("Packages"));
  gtk_signal_connect_object(GTK_OBJECT(self->tree), "path_selected",
			    (GtkSignalFunc)rpm_package_list_update_clist,
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(w), self->tree);
  gtk_widget_show(self->tree);

  self->clist_cont = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(self->clist_cont),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_paned_add2(GTK_PANED(self), self->clist_cont);
  gtk_widget_show(self->clist_cont);
  gtk_widget_ref(self->clist_cont);

  self->clist = gtk_clist_new_with_titles(4, headers);
  gtk_signal_connect_object(GTK_OBJECT(self->clist), "select_row",
			    (GtkSignalFunc)rpm_package_list_select_row,
			    GTK_OBJECT(self));
  gtk_signal_connect_object(GTK_OBJECT(self->clist), "unselect_row",
			    (GtkSignalFunc)rpm_package_list_unselect_row,
			    GTK_OBJECT(self));
  gtk_clist_column_titles_passive(GTK_CLIST(self->clist));
  gtk_clist_set_selection_mode(GTK_CLIST(self->clist), GTK_SELECTION_MULTIPLE);
  gtk_clist_set_column_min_width(GTK_CLIST(self->clist), 0, 120);
  gtk_clist_set_column_auto_resize(GTK_CLIST(self->clist), 0, TRUE);
  gtk_clist_set_column_min_width(GTK_CLIST(self->clist), 1, 50);
  gtk_clist_set_column_auto_resize(GTK_CLIST(self->clist), 1, TRUE);
  gtk_clist_set_column_min_width(GTK_CLIST(self->clist), 2, 40);
  gtk_clist_set_column_auto_resize(GTK_CLIST(self->clist), 2, TRUE);
  gtk_clist_set_column_auto_resize(GTK_CLIST(self->clist), 3, TRUE);
  gtk_clist_columns_autosize(GTK_CLIST(self->clist));
  gtk_container_add(GTK_CONTAINER(self->clist_cont), self->clist);
  gtk_widget_show(self->clist);

  self->ilist_cont = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(self->ilist_cont),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_widget_ref(self->ilist_cont);

  self->ilist = gnome_icon_list_new(70, NULL, FALSE);
  gnome_icon_list_set_separators(GNOME_ICON_LIST(self->ilist), " \t\n-");
  gtk_widget_ref(self->ilist);
  gnome_icon_list_set_selection_mode(GNOME_ICON_LIST(self->ilist),
				     GTK_SELECTION_MULTIPLE);
  gtk_signal_connect_object(GTK_OBJECT(self->ilist), "select_icon",
			    (GtkSignalFunc)rpm_package_list_select_icon,
			    GTK_OBJECT(self));
  gtk_signal_connect_object(GTK_OBJECT(self->ilist), "unselect_icon",
			    (GtkSignalFunc)rpm_package_list_unselect_icon,
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(self->ilist_cont), self->ilist);
  gtk_widget_show(self->ilist);

  gtk_signal_connect(GTK_OBJECT(self->clist), "button_press_event",
		     GTK_SIGNAL_FUNC(clist_button_press), self);
  gtk_signal_connect(GTK_OBJECT(self->ilist), "button_press_event",
		     GTK_SIGNAL_FUNC(ilist_button_press), self);

  self->mode = RPM_PACKAGE_CLIST;
  self->hdl = NULL;
  self->cur_group = NULL;
  self->selection = NULL;
}

GtkWidget *rpm_package_list_new(DBHandle *hdl) {
  RpmPackageList *self;

  self = gtk_type_new(rpm_package_list_get_type());
  if (rpm_package_list_set_db(self, hdl)) {
    gtk_widget_destroy(GTK_WIDGET(self));
    return NULL;
  }

  return GTK_WIDGET(self);
}

gint rpm_package_list_set_db(RpmPackageList *self, DBHandle *hdl) {
  GtkPathTree *tree;
  Header h;
#ifdef	HAVE_RPM_4_0
  rpmdbMatchIterator mi;
#else
  gint i;
#endif
  gchar *group;

  self->hdl = hdl;
  db_handle_db_up(self->hdl);

  tree = GTK_PATH_TREE(self->tree);
  gtk_path_tree_freeze(tree);

#ifdef	HAVE_RPM_4_0
  mi = rpmdbInitIterator(self->hdl->db, RPMDBI_PACKAGES, NULL, 0);
  while ((h = rpmdbNextIterator(mi)) != NULL) {
    if (headerGetEntry(h, RPMTAG_GROUP, NULL, (void **) &group, NULL))
      gtk_path_tree_add_path(tree, group);
  }
  rpmdbFreeIterator(mi);
#else	/* !HAVE_RPM_4_0 */
  for (i = rpmdbFirstRecNum(self->hdl->db); i > 0;
       i = rpmdbNextRecNum(self->hdl->db, i)) {
    h = rpmdbGetRecord(self->hdl->db, i);
    if (h) {
      if (headerGetEntry(h, RPMTAG_GROUP, NULL, (void **) &group, NULL))
	gtk_path_tree_add_path(tree, group);
      headerFree(h);
    }
  }
#endif	/* !HAVE_RPM_4_0 */

  gtk_path_tree_thaw(tree);

  db_handle_db_down(self->hdl);
  return 0;
}

void rpm_package_list_clear_selection(RpmPackageList *self) {
  /* clear out selection */
  g_list_free(self->selection);
  self->selection = NULL;
  /* clear the clist's selection */
  gtk_clist_unselect_all(GTK_CLIST(self->clist));
  /* clear the icon list's selection */
  gnome_icon_list_unselect_all(GNOME_ICON_LIST(self->ilist), NULL, NULL);
  gtk_signal_emit(GTK_OBJECT(self), rpmpkg_signals[SELECTION_CHANGED]);
}

void rpm_package_list_unselect(RpmPackageList *self, guint index) {
  gint row;

  if (g_list_find(self->selection, GUINT_TO_POINTER(index))) {
    self->selection = g_list_remove(self->selection, GUINT_TO_POINTER(index));

    row = gtk_clist_find_row_from_data(GTK_CLIST(self->clist),
				       GUINT_TO_POINTER(index));
    if (row > -1)
      gtk_clist_unselect_row(GTK_CLIST(self->clist), row, -1);
    row = gnome_icon_list_find_icon_from_data(GNOME_ICON_LIST(self->ilist),
					      GUINT_TO_POINTER(index));
    if (row > -1)
      gnome_icon_list_unselect_icon(GNOME_ICON_LIST(self->ilist), row);

    gtk_signal_emit(GTK_OBJECT(self), rpmpkg_signals[SELECTION_CHANGED]);
  }
}

void rpm_package_list_set_mode(RpmPackageList *self, RpmPackageListMode mode) {
  switch (mode) {
  case RPM_PACKAGE_CLIST:
    if (self->mode == RPM_PACKAGE_CLIST) return;
    gtk_container_remove(GTK_CONTAINER(self), self->ilist_cont);
    gtk_widget_hide(self->ilist_cont);
    gtk_paned_add2(GTK_PANED(self), self->clist_cont);
    gtk_widget_show(self->clist_cont);
    break;
  case RPM_PACKAGE_ILIST:
    if (self->mode == RPM_PACKAGE_ILIST) return;
    gtk_container_remove(GTK_CONTAINER(self), self->clist_cont);
    gtk_widget_hide(self->clist_cont);
    gtk_paned_add2(GTK_PANED(self), self->ilist_cont);
    gtk_widget_show(self->ilist_cont);
    break;
  }
  self->mode = mode;
}

void rpm_package_list_add_group(RpmPackageList *self, gchar *group) {
  gtk_path_tree_add_path(GTK_PATH_TREE(self->tree), group);
}

#include "gnome-package.xpm"

static GdkImlibImage *rpm_package_list_get_icon(RpmPackageList *self,
						Header h) {
  gchar *data;
  gint fd, len;
  GdkImlibImage *ret;
  char *workpath;
  if (headerGetEntry(h, RPMTAG_GIF, NULL, (void **)&data, &len)) {
    workpath = gnome_util_home_file("gnorpm.d/gnorpm.gif");
    fd = open(workpath, O_RDWR | O_CREAT, 0666);
    write(fd, data, len);
    close(fd);
    ret = gdk_imlib_load_image(workpath);
    gdk_imlib_changed_image(ret); /* prevent caching */
    unlink(workpath);
    g_free(workpath);
  } else if (headerGetEntry(h, RPMTAG_XPM, NULL, (void**)&data, &len)) {
    workpath = gnome_util_home_file("gnorpm.d/gnorpm.xpm");
    fd = open(workpath, O_RDWR | O_CREAT, 0666);
    write(fd, data, len);
    close(fd);
    ret = gdk_imlib_load_image(workpath);
    gdk_imlib_changed_image(ret); /* prevent caching */
    unlink(workpath);
    g_free(workpath);
  } else
    /* fallback ... */
    ret = gdk_imlib_create_image_from_xpm_data(gnome_package_xpm);
  return ret;
}

static void rpm_package_list_update_clist(RpmPackageList *self,
					  gchar *group_name) {
  self->cur_group = group_name;

  rpm_package_list_update_pane(self);
}

void rpm_package_list_update_pane(RpmPackageList *self) {
  gchar *row[4], text[50], *tmp;
#ifdef	HAVE_RPM_4_0
  rpmdbMatchIterator mi;
#else
  dbiIndexSet matches;
  gint i;
#endif
  gint row_id, icon_id;
  Header h;
  GtkCList *clist;
  GnomeIconList *ilist;
  GdkImlibImage *im;

  clist = GTK_CLIST(self->clist);
  ilist = GNOME_ICON_LIST(self->ilist);

  if (!self->cur_group) {
    gtk_clist_clear(clist);
    gnome_icon_list_clear(ilist);
    g_list_free(self->selection);
    self->selection = NULL;
    return;
  }

  db_handle_db_up(self->hdl);

#ifdef	HAVE_RPM_4_0
  mi = rpmdbInitIterator(self->hdl->db, RPMTAG_GROUP, self->cur_group, 0);

  if (rpmdbGetIteratorCount(mi) == 0) {
    /* no matches ... */
    rpmdbFreeIterator(mi);
    gtk_clist_clear(clist);
    gnome_icon_list_clear(ilist);
    db_handle_db_down(self->hdl);
    return;
  }

  gtk_clist_freeze(clist);
  gtk_clist_clear(clist);
  gnome_icon_list_freeze(ilist);
  gnome_icon_list_clear(ilist);

  while ((h = rpmdbNextIterator(mi)) != NULL) {
    headerGetEntry(h, RPMTAG_NAME,    NULL, (void**)&row[0], NULL);
    headerGetEntry(h, RPMTAG_VERSION, NULL, (void**)&row[1], NULL);
    headerGetEntry(h, RPMTAG_RELEASE, NULL, (void**)&row[2], NULL);
    headerGetEntry(h, RPMTAG_SUMMARY, NULL, (void**)&row[3], NULL);
    g_snprintf(text, 49, "%s\n%s-%s", row[0], row[1], row[2]);

    for (row_id = 0; row_id < clist->rows; row_id++) {
      gtk_clist_get_text(clist, row_id, 0, &tmp);
      if (strcmp(tmp, row[0]) > 0)
	break;
    }
    gtk_clist_insert(clist, row_id, row);
    gtk_clist_set_row_data(clist, row_id,
			   GUINT_TO_POINTER(rpmdbGetIteratorOffset(mi)));
    im = rpm_package_list_get_icon(self, h);
    gnome_icon_list_insert_imlib(ilist, row_id, im, text);
    icon_id = row_id;
    gnome_icon_list_set_icon_data(ilist, icon_id,
				  GUINT_TO_POINTER(rpmdbGetIteratorOffset(mi)));

    if (g_list_find(self->selection,
		    GUINT_TO_POINTER(rpmdbGetIteratorOffset(mi)))) {
      gtk_clist_select_row(clist, row_id, -1);
      gnome_icon_list_select_icon(ilist, icon_id);
    }
  }

  rpmdbFreeIterator(mi);

#else	/* !HAVE_RPM_4_0 */

  if (rpmdbFindByGroup(self->hdl->db, self->cur_group, &matches)) {
    /* no matches ... */
    gtk_clist_clear(clist);
    gnome_icon_list_clear(ilist);
    g_list_free(self->selection);
    self->selection = NULL;
    db_handle_db_down(self->hdl);
    return;
  }

  gtk_clist_freeze(clist);
  gtk_clist_clear(clist);
  gnome_icon_list_freeze(ilist);
  gnome_icon_list_clear(ilist);
  g_list_free(self->selection);
  self->selection = NULL;
  
  for (i = 0; i < matches.count; i++) {
    h = rpmdbGetRecord(self->hdl->db, matches.recs[i].recOffset);
    headerGetEntry(h, RPMTAG_NAME,    NULL, (void**)&row[0], NULL);
    headerGetEntry(h, RPMTAG_VERSION, NULL, (void**)&row[1], NULL);
    headerGetEntry(h, RPMTAG_RELEASE, NULL, (void**)&row[2], NULL);
    headerGetEntry(h, RPMTAG_SUMMARY, NULL, (void**)&row[3], NULL);
    g_snprintf(text, 49, "%s\n%s-%s", row[0], row[1], row[2]);

    for (row_id = 0; row_id < clist->rows; row_id++) {
      gtk_clist_get_text(clist, row_id, 0, &tmp);
      if (strcmp(tmp, row[0]) > 0)
	break;
    }
    gtk_clist_insert(clist, row_id, row);
    gtk_clist_set_row_data(clist, row_id,
			   GUINT_TO_POINTER(matches.recs[i].recOffset));
    im = rpm_package_list_get_icon(self, h);
    gnome_icon_list_insert_imlib(ilist, row_id, im, text);
    icon_id = row_id;
    gnome_icon_list_set_icon_data(ilist, icon_id,
				  GUINT_TO_POINTER(matches.recs[i].recOffset));

    if (g_list_find(self->selection,
		    GUINT_TO_POINTER(matches.recs[i].recOffset))) {
      gtk_clist_select_row(clist, row_id, -1);
      gnome_icon_list_select_icon(ilist, icon_id);
    }
    headerFree(h);
  }
  dbiFreeIndexRecord(matches);
#endif	/* !HAVE_RPM_4_0 */

  gtk_clist_thaw(clist);
  gnome_icon_list_thaw(ilist);

  db_handle_db_down(self->hdl);
}

static void rpm_package_list_select_row(RpmPackageList *self, gint row,
					gint column, GdkEventButton *event) {
  guint offset;

  /* column is set < 0 only when we redo selection of rows when we change
   * package group. */
  if (self->mode != RPM_PACKAGE_CLIST) return;
  offset =GPOINTER_TO_UINT(gtk_clist_get_row_data(GTK_CLIST(self->clist),row));
  if (g_list_find(self->selection, GUINT_TO_POINTER(offset)) == NULL)
    self->selection = g_list_prepend(self->selection,GUINT_TO_POINTER(offset));
  offset = gnome_icon_list_find_icon_from_data(GNOME_ICON_LIST(self->ilist),
					       GUINT_TO_POINTER(offset));
  if (offset >= 0)
    gnome_icon_list_select_icon(GNOME_ICON_LIST(self->ilist), offset);
  gtk_signal_emit(GTK_OBJECT(self), rpmpkg_signals[SELECTION_CHANGED]);
}
static void rpm_package_list_unselect_row(RpmPackageList *self, gint row,
					  gint column, GdkEventButton *event) {
  guint offset;
  if (self->mode != RPM_PACKAGE_CLIST) return;
  offset =GPOINTER_TO_UINT(gtk_clist_get_row_data(GTK_CLIST(self->clist),row));
  self->selection = g_list_remove(self->selection, GUINT_TO_POINTER(offset));
  offset = gnome_icon_list_find_icon_from_data(GNOME_ICON_LIST(self->ilist),
					       GUINT_TO_POINTER(offset));
  if (offset >= 0)
    gnome_icon_list_unselect_icon(GNOME_ICON_LIST(self->ilist), offset);
  gtk_signal_emit(GTK_OBJECT(self), rpmpkg_signals[SELECTION_CHANGED]);
}

static void rpm_package_list_select_icon(RpmPackageList *self, gint icon,
					 GdkEvent *event) {
  guint offset;

  /* Avoid recursive callbacks from maintaining the invisible window 
     context */
     
  if (self->mode == RPM_PACKAGE_CLIST) return;

  offset = GPOINTER_TO_UINT(gnome_icon_list_get_icon_data(
				GNOME_ICON_LIST(self->ilist), icon));
  if (g_list_find(self->selection, GUINT_TO_POINTER(offset)) == NULL)
    self->selection = g_list_prepend(self->selection,GUINT_TO_POINTER(offset));
  offset = gtk_clist_find_row_from_data(GTK_CLIST(self->clist),
					GUINT_TO_POINTER(offset));
  if (offset >= 0)
    gtk_clist_select_row(GTK_CLIST(self->clist), offset, -1);
  gtk_signal_emit(GTK_OBJECT(self), rpmpkg_signals[SELECTION_CHANGED]);
}

static void rpm_package_list_unselect_icon(RpmPackageList *self, gint icon,
					   GdkEvent *event) {
  guint offset;
  if (self->mode == RPM_PACKAGE_CLIST) return;

  offset = GPOINTER_TO_UINT(gnome_icon_list_get_icon_data(
				GNOME_ICON_LIST(self->ilist), icon));
  self->selection = g_list_remove(self->selection, GUINT_TO_POINTER(offset));
  offset = gtk_clist_find_row_from_data(GTK_CLIST(self->clist),
					GUINT_TO_POINTER(offset));
  if (offset >= 0)
    gtk_clist_unselect_row(GTK_CLIST(self->clist), offset, -1);
  gtk_signal_emit(GTK_OBJECT(self), rpmpkg_signals[SELECTION_CHANGED]);
}

static int clist_button_press(GtkCList *clist, GdkEventButton *event,
			      RpmPackageList *list) {
  if (event->type == GDK_BUTTON_PRESS || event->type == GDK_2BUTTON_PRESS) {
    gint row;
    gpointer index;

    if (!gtk_clist_get_selection_info(clist, event->x, event->y, &row, NULL))
      return FALSE;
    index = gtk_clist_get_row_data(clist, row);
    if (event->button == 3)
      gtk_signal_emit(GTK_OBJECT(list), rpmpkg_signals[CONTEXT_MENU],
		      event, GPOINTER_TO_UINT(index));
    else
      if (event->type == GDK_2BUTTON_PRESS && event->button == 1)
	gtk_signal_emit(GTK_OBJECT(list), rpmpkg_signals[QUERY], index);
    return TRUE;
  }
  return FALSE;
}
static int ilist_button_press(GnomeIconList *ilist, GdkEventButton *event,
			      RpmPackageList *list) {
  if (event->type == GDK_BUTTON_PRESS || event->type == GDK_2BUTTON_PRESS) {
    gint icon = gnome_icon_list_get_icon_at(ilist, event->x, event->y);
    gpointer index;

    if (icon < 0) return FALSE;
    index = gnome_icon_list_get_icon_data(ilist, icon);
    if (event->button == 3)
      gtk_signal_emit(GTK_OBJECT(list), rpmpkg_signals[CONTEXT_MENU],
		      event, GPOINTER_TO_UINT(index));
    else
      if (event->type == GDK_2BUTTON_PRESS && event->button == 1)
        gtk_signal_emit(GTK_OBJECT(list), rpmpkg_signals[QUERY],
	    		index);
    return TRUE;
  }
  return FALSE;
}


static void rpm_package_list_destroy(GtkObject *object) {
  RpmPackageList *self;

  g_return_if_fail(object != NULL);
  g_return_if_fail(RPM_IS_PACKAGE_LIST(object));

  self = RPM_PACKAGE_LIST(object);

  gtk_widget_unref(self->clist_cont);
  gtk_widget_unref(self->ilist_cont);

  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (* GTK_OBJECT_CLASS(parent_class)->destroy)(object);
}


