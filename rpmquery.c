/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <gnome.h>
#include "rpmquery.h"
#include <time.h>
#include <fcntl.h>
#include "pixmaps.h"
#include "rpmcompat.h"

enum {
  VERIFY,
  UNINSTALL,
  INSTALL,
  UPGRADE,
  CHECKSIG,
  CLOSE,
  LAST_SIGNAL
};

static guint query_signals[LAST_SIGNAL] = { 0 };
static GtkTableClass *parent_class = NULL;

static void rpm_query_class_init(RpmQueryClass *klass);
static void rpm_query_init(RpmQuery *query);

static void rpm_query_realize(GtkWidget *widget); /* catch this to draw text */
static void rpm_query_destroy(GtkObject *object);

static void rpm_query_chk_scroll(GtkAdjustment *adj, GtkWidget *scroll);
static void rpm_query_call_verify(RpmQuery *query);
static void rpm_query_call_uninstall(RpmQuery *query);
static void rpm_query_call_install(RpmQuery *query);
static void rpm_query_call_upgrade(RpmQuery *query);
static void rpm_query_call_checksig(RpmQuery *query);
static void rpm_query_call_close(RpmQuery *query);

static void rpm_query_set_from_header(RpmQuery *query, Header head);

guint rpm_query_get_type() {
  static guint query_type = 0;
  if (!query_type) {
    GtkTypeInfo query_info = {
      "RpmQuery",
      sizeof(RpmQuery),
      sizeof(RpmQueryClass),
      (GtkClassInitFunc) rpm_query_class_init,
      (GtkObjectInitFunc) rpm_query_init,
      (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };
    query_type = gtk_type_unique(gtk_table_get_type(), &query_info);
  }
  return query_type;
}

static void rpm_query_class_init(RpmQueryClass *klass) {
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass *)klass;
  query_signals[VERIFY] =
    gtk_signal_new("verify",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmQueryClass, verify),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);
  query_signals[UNINSTALL] =
    gtk_signal_new("uninstall",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmQueryClass, uninstall),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);
  query_signals[INSTALL] =
    gtk_signal_new("install",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmQueryClass, install),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);
  query_signals[UPGRADE] =
    gtk_signal_new("upgrade",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmQueryClass, upgrade),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);
  query_signals[CHECKSIG] =
    gtk_signal_new("checksig",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmQueryClass, checksig),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);
  query_signals[CLOSE] =
    gtk_signal_new("close",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmQueryClass, close),
		   gtk_signal_default_marshaller,
		   GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals(object_class, query_signals, LAST_SIGNAL);
  parent_class = gtk_type_class(gtk_table_get_type());
  object_class->destroy = rpm_query_destroy;
  GTK_WIDGET_CLASS(klass)->realize = rpm_query_realize;
}

static void rpm_query_init(RpmQuery *self) {
  static gchar *headers[] = { N_("D"), N_("C"), N_("S"), N_("Path") };
  GtkTable *table;
  GtkWidget *w, *box;

  table = GTK_TABLE(self);

  self->pkgname = gtk_label_new("<pkgname>");
  gtk_widget_set_name(self->pkgname, "PackageName");
  gtk_table_attach(table, self->pkgname, 0,4, 0,1, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0,0);
  gtk_widget_show(self->pkgname);

  w = gtk_label_new(_("Size:"));
  gtk_widget_set_name(w, "Heading");
  gtk_misc_set_alignment(GTK_MISC(w), 1.0, 0.5);
  gtk_table_attach(table, w, 0,1, 1,2, GTK_FILL, GTK_FILL, 0,0);
  gtk_widget_show(w);
  self->size = gtk_label_new("<size>");
  gtk_widget_set_name(self->size, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->size), 0.0, 0.5);
  gtk_table_attach(table, self->size, 1,2, 1,2, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0,0);
  gtk_widget_show(self->size);

  w = gtk_label_new(_("Install Date:"));
  gtk_widget_set_name(w, "Heading");
  gtk_misc_set_alignment(GTK_MISC(w), 1.0, 0.5);
  gtk_table_attach(table, w, 2,3, 1,2, GTK_FILL, GTK_FILL, 0,0);
  gtk_widget_show(w);
  self->install = gtk_label_new(_("not installed"));
  gtk_widget_set_name(self->install, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->install), 0.0, 0.5);
  gtk_table_attach(table, self->install, 3,4, 1,2, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0,0);
  gtk_widget_show(self->install);

  w = gtk_label_new(_("Build Host:"));
  gtk_widget_set_name(w, "Heading");
  gtk_misc_set_alignment(GTK_MISC(w), 1.0, 0.5);
  gtk_table_attach(table, w, 0,1, 2,3, GTK_FILL, GTK_FILL, 0,0);
  gtk_widget_show(w);
  self->bhost = gtk_label_new("<bhost>");
  gtk_widget_set_name(self->bhost, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->bhost), 0.0, 0.5);
  gtk_table_attach(table, self->bhost, 1,2, 2,3, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0,0);
  gtk_widget_show(self->bhost);

  w = gtk_label_new(_("Build Date:"));
  gtk_widget_set_name(w, "Heading");
  gtk_misc_set_alignment(GTK_MISC(w), 1.0, 0.5);
  gtk_table_attach(table, w, 2,3, 2,3, GTK_FILL, GTK_FILL, 0,0);
  gtk_widget_show(w);
  self->bdate = gtk_label_new("<bdate>");
  gtk_widget_set_name(self->bdate, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->bdate), 0.0, 0.5);
  gtk_table_attach(table, self->bdate, 3,4, 2,3, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0,0);
  gtk_widget_show(self->bdate);

  w = gtk_label_new(_("Distribution:"));
  gtk_widget_set_name(w, "Heading");
  gtk_misc_set_alignment(GTK_MISC(w), 1.0, 0.5);
  gtk_table_attach(table, w, 0,1, 3,4, GTK_FILL, GTK_FILL, 0,0);
  gtk_widget_show(w);
  self->dist = gtk_label_new("<dist>");
  gtk_widget_set_name(self->dist, "Information");
  gtk_label_set_line_wrap(GTK_LABEL(self->dist), TRUE);
  gtk_misc_set_alignment(GTK_MISC(self->dist), 0.0, 0.5);
  gtk_table_attach(table, self->dist, 1,2, 3,4, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0,0);
  gtk_widget_show(self->dist);

  w = gtk_label_new(_("Vendor:"));
  gtk_widget_set_name(w, "Heading");
  gtk_misc_set_alignment(GTK_MISC(w), 1.0, 0.5);
  gtk_table_attach(table, w, 2,3, 3,4, GTK_FILL, GTK_FILL, 0,0);
  gtk_widget_show(w);
  self->vend = gtk_label_new("<vend>");
  gtk_widget_set_name(self->vend, "Information");
  gtk_label_set_line_wrap(GTK_LABEL(self->vend), TRUE);
  gtk_misc_set_alignment(GTK_MISC(self->vend), 0.0, 0.5);
  gtk_table_attach(table, self->vend, 3,4, 3,4, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0,0);
  gtk_widget_show(self->vend);

  w = gtk_label_new(_("Group:"));
  gtk_widget_set_name(w, "Heading");
  gtk_misc_set_alignment(GTK_MISC(w), 1.0, 0.5);
  gtk_table_attach(table, w, 0,1, 4,5, GTK_FILL, GTK_FILL, 0,0);
  gtk_widget_show(w);
  self->group = gtk_label_new("<group>");
  gtk_widget_set_name(self->group, "Information");
  gtk_label_set_line_wrap(GTK_LABEL(self->group), TRUE);
  gtk_misc_set_alignment(GTK_MISC(self->group), 0.0, 0.5);
  gtk_table_attach(table, self->group, 1,2, 4,5, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0,0);
  gtk_widget_show(self->group);

  w = gtk_label_new(_("Packager:"));
  gtk_widget_set_name(w, "Heading");
  gtk_misc_set_alignment(GTK_MISC(w), 1.0, 0.5);
  gtk_table_attach(table, w, 2,3, 4,5, GTK_SHRINK, GTK_FILL, 0,0);
  gtk_widget_show(w);

  self->packager = gnome_href_new("<packager>", NULL);
  gtk_table_attach(table, self->packager, 3,4, 4,5, GTK_SHRINK|GTK_EXPAND,
		   GTK_FILL, 0,0);
  gtk_label_set_line_wrap(GTK_LABEL(GNOME_HREF(self->packager)->label), TRUE);
  gtk_widget_show(self->packager);
  /* tie visibility of label to visibility of url */
  gtk_signal_connect_object(GTK_OBJECT(self->packager), "show",
			    GTK_SIGNAL_FUNC(gtk_widget_show), GTK_OBJECT(w));
  gtk_signal_connect_object(GTK_OBJECT(self->packager), "hide",
			    GTK_SIGNAL_FUNC(gtk_widget_hide), GTK_OBJECT(w));

  w = gtk_label_new(_("URL:"));
  gtk_widget_set_name(w, "Heading");
  gtk_misc_set_alignment(GTK_MISC(w), 1.0, 0.5);
  gtk_table_attach(table, w, 0,1, 5,6, GTK_FILL, GTK_FILL, 0,0);
  gtk_widget_show(w);
  self->url = gnome_href_new("<url>", NULL);
  gtk_table_attach(table, self->url, 1,4, 5,6, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0,0);
  gtk_widget_show(self->url);
  /* tie visibility of label to visibility of url */
  gtk_signal_connect_object(GTK_OBJECT(self->url), "show",
			    GTK_SIGNAL_FUNC(gtk_widget_show), GTK_OBJECT(w));
  gtk_signal_connect_object(GTK_OBJECT(self->url), "hide",
			    GTK_SIGNAL_FUNC(gtk_widget_hide), GTK_OBJECT(w));

  box = gtk_hbox_new(FALSE, 3);
  gtk_table_attach(table, box, 0,4, 6,7, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0,0);
  gtk_widget_show(box);

  self->description = gtk_text_new(FALSE, FALSE);
  gtk_widget_set_name(self->description, "Description");
  gtk_widget_set_usize(self->description, -1, 65);
  gtk_text_set_editable(GTK_TEXT(self->description), FALSE);
  gtk_text_set_word_wrap(GTK_TEXT(self->description), TRUE);
  gtk_box_pack_start(GTK_BOX(box), self->description, TRUE, TRUE, 0);
  gtk_widget_show(self->description);
  w = gtk_vscrollbar_new(GTK_TEXT(self->description)->vadj);
  gtk_signal_connect(GTK_OBJECT(GTK_TEXT(self->description)->vadj), "changed",
		     (GtkSignalFunc) rpm_query_chk_scroll, w);
  gtk_box_pack_start(GTK_BOX(box), w, FALSE, TRUE, 0);
  gtk_widget_show(w);

  w = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(w),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_table_attach(table, w, 0,4, 7,8, GTK_FILL|GTK_EXPAND,
		   GTK_FILL|GTK_EXPAND, 0,0);
  gtk_widget_show(w);

  self->files = gtk_clist_new_with_titles(4, headers);
  gtk_widget_set_name(self->files, "FileList");
  gtk_widget_set_usize(self->files, -1, 75);
  gtk_clist_column_titles_passive(GTK_CLIST(self->files));
  gtk_clist_set_column_min_width(GTK_CLIST(self->files), 0, 10);
  gtk_clist_set_column_min_width(GTK_CLIST(self->files), 1, 10);
  gtk_clist_set_column_min_width(GTK_CLIST(self->files), 2, 10);
  gtk_clist_set_column_auto_resize(GTK_CLIST(self->files), 3, TRUE);
  gtk_container_add(GTK_CONTAINER(w), self->files);
  gtk_widget_show(self->files);

  self->action_area = gtk_hbutton_box_new();
  gtk_button_box_set_layout(GTK_BUTTON_BOX(self->action_area),
			    GTK_BUTTONBOX_SPREAD);
  gtk_table_attach(table, self->action_area, 0,4, 8,9, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0,0);
  gtk_widget_show(self->action_area);

  gtk_table_set_col_spacing(table, 0, 5);
  gtk_table_set_col_spacing(table, 1, 20);
  gtk_table_set_col_spacing(table, 2, 5);
  gtk_table_set_row_spacing(table, 3, 3);
  gtk_table_set_row_spacing(table, 4, 3);
  gtk_table_set_row_spacing(table, 5, 3);
  gtk_table_set_row_spacing(table, 7, 5);

  self->source = RPM_QUERY_DB;
  self->hdl = NULL;
  self->index = 0;
  self->fname = NULL;
  self->desc = NULL;
  self->pkg_name = NULL;
#ifdef WITH_RPMFIND
  self->rdfData = NULL;
#endif
}

GtkWidget *rpm_query_new(void) {
  RpmQuery *self = gtk_type_new(rpm_query_get_type());

  return GTK_WIDGET(self);
}

GtkWidget *rpm_query_new_from_index(DBHandle *hdl, guint index) {
  RpmQuery *self;

  self = gtk_type_new(rpm_query_get_type());
  if (rpm_query_set_from_db(self, hdl, index)) {
    gtk_widget_destroy(GTK_WIDGET(self));
    return NULL;
  }

  rpm_query_add_opp_buttons(self);
  rpm_query_add_close_button(self);
  return GTK_WIDGET(self);
}

GtkWidget *rpm_query_new_from_file(DBHandle *hdl, gchar *file) {
  RpmQuery *self;

  self = gtk_type_new(rpm_query_get_type());
  if (rpm_query_set_from_file(self, hdl, file)) {
    gtk_widget_destroy(GTK_WIDGET(self));
    return NULL;
  }

  rpm_query_add_install_button(self);
  db_handle_db_up(hdl);

 {
#ifdef HAVE_RPM_4_0
  rpmdbMatchIterator mi;
  mi = rpmdbInitIterator(hdl->db, RPMDBI_LABEL, self->pkg_name, 0);
  if (rpmdbGetIteratorCount(mi) > 0)
    rpm_query_add_upgrade_button(self);
  rpmdbFreeIterator(mi);
#else
  dbiIndexSet matches;

  if (!rpmdbFindByLabel(hdl->db, self->pkg_name, &matches)) {
    /* there is a package by this name in the rpm database ... */
    dbiFreeIndexRecord(matches);
    rpm_query_add_upgrade_button(self);
  }
#endif
 }

  db_handle_db_down(hdl);
  rpm_query_add_checksig_button(self);
  rpm_query_add_close_button(self);
  return GTK_WIDGET(self);
}

#ifdef WITH_RPMFIND
GtkWidget *rpm_query_new_from_rdf(DBHandle *hdl, rpmData *data) {
  RpmQuery *self;

  self = gtk_type_new(rpm_query_get_type());
  if (rpm_query_set_from_rdf(self, hdl, data)) {
    gtk_widget_destroy(GTK_WIDGET(self));
    return NULL;
  }

  rpm_query_add_install_button(self);
  rpm_query_add_close_button(self);
  return GTK_WIDGET(self);
}
#endif

void rpm_query_add_install_button(RpmQuery *self) {
  GtkWidget *w;

  w = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
				RPM_STOCK_PIXMAP_INSTALL), _("Install"));
  gtk_signal_connect_object(GTK_OBJECT(w), "clicked",
			    (GtkSignalFunc)rpm_query_call_install,
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(self->action_area), w);
  gtk_widget_show(w);
}

void rpm_query_add_upgrade_button(RpmQuery *self) {
  GtkWidget *w;

  w = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
				RPM_STOCK_PIXMAP_UPGRADE), _("Upgrade"));
  gtk_signal_connect_object(GTK_OBJECT(w), "clicked",
			    (GtkSignalFunc)rpm_query_call_upgrade,
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(self->action_area), w);
  gtk_widget_show(w);
}

void rpm_query_add_checksig_button(RpmQuery *self) {
  GtkWidget *w;

  w = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
				RPM_STOCK_PIXMAP_VERIFY), _("Check Sig"));
  gtk_signal_connect_object(GTK_OBJECT(w), "clicked",
			    (GtkSignalFunc)rpm_query_call_checksig,
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(self->action_area), w);
  gtk_widget_show(w);
}

void rpm_query_add_close_button(RpmQuery *self) {
  GtkWidget *w;

  w = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
				GNOME_STOCK_PIXMAP_CLOSE), _("Close"));
  gtk_signal_connect_object(GTK_OBJECT(w), "clicked",
			    (GtkSignalFunc) rpm_query_call_close,
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(self->action_area), w);
  gtk_widget_show(w);
}

void rpm_query_add_opp_buttons(RpmQuery *self) {
  GtkWidget *w;

  w = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
				RPM_STOCK_PIXMAP_VERIFY), _("Verify"));
  gtk_signal_connect_object(GTK_OBJECT(w), "clicked",
			    (GtkSignalFunc)rpm_query_call_verify,
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(self->action_area), w);
  gtk_widget_show(w);

  w = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
				RPM_STOCK_PIXMAP_UNINSTALL), _("Uninstall"));
  gtk_signal_connect_object(GTK_OBJECT(w), "clicked",
			    (GtkSignalFunc)rpm_query_call_uninstall,
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(self->action_area), w);
  gtk_widget_show(w);
}

void rpm_query_clear(RpmQuery *self) {
  if (self->pkg_name) g_free(self->pkg_name);
  self->pkg_name = NULL;
  gtk_label_set(GTK_LABEL(self->pkgname), "");
  gtk_label_set(GTK_LABEL(self->size), "0");
  gtk_label_set(GTK_LABEL(self->bhost), _("<none>"));
  gtk_label_set(GTK_LABEL(self->bdate), "");
  gtk_label_set(GTK_LABEL(self->dist), _("<none>"));
  gtk_label_set(GTK_LABEL(self->vend), _("<none>"));
  gtk_label_set(GTK_LABEL(self->group), _("<none>"));
  gtk_widget_hide(self->packager);
  gtk_widget_hide(self->url);
  gtk_editable_delete_text(GTK_EDITABLE(self->description), 0, -1);
  if (self->desc) g_free(self->desc);
  self->desc = NULL;
  gtk_clist_clear(GTK_CLIST(self->files));

  if (self->fname) g_free(self->fname);
  self->fname = NULL;
  self->index = 0;
#ifdef WITH_RPMFIND
  if (self->rdfData) rpmDataFree(self->rdfData);
  self->rdfData = NULL;
#endif
}

gint rpm_query_set_from_db(RpmQuery *self, DBHandle *hdl, guint index) {
  Header h;

  g_return_val_if_fail(hdl != NULL, 1);
  db_handle_db_up(hdl);

#ifdef HAVE_RPM_4_0
  { rpmdbMatchIterator mi;
    mi = rpmdbInitIterator(hdl->db, RPMDBI_PACKAGES, &index, sizeof(index));
    h = rpmdbNextIterator(mi);
    if (!h) {
      rpmdbFreeIterator(mi);
      db_handle_db_down(hdl);
      return 1;
    }
    rpm_query_set_from_header(self, h);
    rpmdbFreeIterator(mi);
  }
#else
  h = rpmdbGetRecord(hdl->db, index);

  if (!h) {
    db_handle_db_down(hdl);
    return 1;
  }
  rpm_query_set_from_header(self, h);
  headerFree(h);
#endif

  if (self->fname) g_free(self->fname);
  self->fname = NULL;
#ifdef WITH_RPMFIND
  if (self->rdfData) rpmDataFree(self->rdfData);
  self->rdfData = NULL;
#endif

  self->source = RPM_QUERY_DB;
  self->hdl = hdl;
  self->index = index;
  db_handle_db_down(self->hdl);
  return 0;
}

gint rpm_query_set_from_file(RpmQuery *self, DBHandle *hdl, gchar *file) {
  FD_t fd;
  Header h = NULL;

  fd = fdOpen(file, O_RDONLY, 0);
  if (fdFileno(fd) < 0)
    return 1;
  if (rpmReadPackageHeader(fd, &h, NULL, NULL, NULL) || !h) {
    if (!h) headerFree(h);
    fdClose(fd);
    return 1;
  }
  rpm_query_set_from_header(self, h);
  headerFree(h);

  if (self->fname) g_free(self->fname);
  self->index = 0;
#ifdef WITH_RPMFIND
  if (self->rdfData) rpmDataFree(self->rdfData);
  self->rdfData = NULL;
#endif

  self->source = RPM_QUERY_FILE;
  self->fname = g_strdup(file);
  self->hdl = hdl;
  fdClose(fd);
  return 0;
}

/* tries to get real name and email from an address string */
static void rpm_query_parse_address(gchar *addr, gchar **email, gchar **name) {
  char *pos, *pos2;

  *email = NULL;
  if ((pos = strchr(addr, '<')) == NULL) {
    pos = g_strdup(addr);
    *name = g_strdup(g_strstrip(pos));
    g_free(pos);
    if (strchr(*name, '@'))
      *email = g_strdup(*name);
  } else {
    *email = g_strdup(pos+1);
    pos2 = strchr(*email, '>');
    if ((pos2 = strchr(*email, '>')) != NULL)
      *pos2 = '\0';

    *name = g_strdup(addr);
    (*name)[pos-addr] = '\0';
    g_strchomp(*name);
  }
}

static void rpm_query_set_from_header(RpmQuery *self, Header h) {
  gchar buf[512], *s1, *s2, *s3, *row[4];
  gchar **path = NULL, **links = NULL, *state = NULL;
  gchar **dirnames=NULL;
  gint32 *dirindex=NULL;
  gint32 count, i, *flag = NULL;
  int_32 * myt = NULL;
  time_t timbuf, *tm = &timbuf;

  headerGetEntry(h, RPMTAG_NAME, NULL, (void **)&s1, NULL);
  if (self->pkg_name) g_free(self->pkg_name);
  self->pkg_name = g_strdup(s1);
  headerGetEntry(h, RPMTAG_VERSION, NULL, (void **)&s2, NULL);
  headerGetEntry(h, RPMTAG_RELEASE, NULL, (void **)&s3, NULL);
  g_snprintf(buf, 511, "%s-%s-%s", s1, s2, s3);
  gtk_label_set(GTK_LABEL(self->pkgname), buf);
  headerGetEntry(h, RPMTAG_SIZE, NULL, (void **)&flag, NULL);
  g_snprintf(buf, 511, "%d", *flag);
  gtk_label_set(GTK_LABEL(self->size), buf);
  headerGetEntry(h, RPMTAG_INSTALLTIME, NULL, (void **)&myt, NULL);
  if (myt) {
    *tm = *myt;
    strftime(buf, 511, "%a %b %d %I:%M:%S %Z %Y", gmtime(tm));
    gtk_label_set(GTK_LABEL(self->install), buf);
  }
  headerGetEntry(h, RPMTAG_BUILDHOST, NULL, (void **)&s1, NULL);
  gtk_label_set(GTK_LABEL(self->bhost), s1?s1:_("<none>"));
  headerGetEntry(h, RPMTAG_BUILDTIME, NULL, (void **)&myt, NULL);
  if (myt) {
    *tm = *myt;
    strftime(buf, 511, "%a %b %d %I:%M:%S %Z %Y", gmtime(tm));
    gtk_label_set(GTK_LABEL(self->bdate), buf);
  }
  headerGetEntry(h, RPMTAG_DISTRIBUTION, NULL, (void **)&s1, NULL);
  gtk_label_set(GTK_LABEL(self->dist), s1?s1:_("<none>"));
  headerGetEntry(h, RPMTAG_VENDOR, NULL, (void **)&s1, NULL);
  gtk_label_set(GTK_LABEL(self->vend), s1?s1:_("<none>"));

  headerGetEntry(h, RPMTAG_GROUP, NULL, (void **)&s1, NULL);
  gtk_label_set(GTK_LABEL(self->group), s1?s1:_("<none>"));

  headerGetEntry(h, RPMTAG_PACKAGER, NULL, (void **)&s1, NULL);
  if (s1) {
    rpm_query_parse_address(s1, &s2, &s3);
    if (s2) {
      s1 = g_strconcat("mailto:", s2, NULL);
      gnome_href_set_url(GNOME_HREF(self->packager), s1);
      g_free(s1);
      gtk_widget_set_sensitive(self->packager, TRUE);
    } else
      gtk_widget_set_sensitive(self->packager, FALSE);
    gnome_href_set_label(GNOME_HREF(self->packager), s3);
    if (s2) g_free(s2);
    g_free(s3);
    gtk_widget_show(self->packager);
  } else
    gtk_widget_hide(self->packager);

  headerGetEntry(h, RPMTAG_URL, NULL, (void **)&s1, NULL);
  if (s1) {
    gnome_href_set_url(GNOME_HREF(self->url), s1);
    gnome_href_set_label(GNOME_HREF(self->url), s1);
    gtk_widget_show(self->url);
  } else
    gtk_widget_hide(self->url);

  if (GTK_WIDGET_REALIZED(self)) {
    gtk_text_freeze(GTK_TEXT(self->description));
    gtk_editable_delete_text(GTK_EDITABLE(self->description), 0, -1);
    headerGetEntry(h, RPMTAG_DESCRIPTION, NULL, (void **)&s1, NULL);
    gtk_text_insert(GTK_TEXT(self->description), NULL, NULL, NULL,
		    s1?s1:_("<none>"), -1);
    gtk_text_thaw(GTK_TEXT(self->description));
    if (self->desc) g_free(self->desc);
    self->desc = NULL;
  } else {
    headerGetEntry(h, RPMTAG_DESCRIPTION, NULL, (void **)&s1, NULL);
    self->desc = g_strdup(s1);
  }


  gtk_clist_freeze(GTK_CLIST(self->files));
  gtk_clist_clear(GTK_CLIST(self->files));
  if (!headerGetEntry(h, RPMTAG_OLDFILENAMES, NULL, (void **)&path, &count)) {
    if(count==0)
    {
      gint32 len;
      headerGetEntry(h, RPMTAG_BASENAMES, NULL, (void**)&path, &count);
      headerGetEntry(h, RPMTAG_DIRNAMES, NULL, (void**)&dirnames, &len);
      headerGetEntry(h, RPMTAG_DIRINDEXES, NULL, (void**)&dirindex, &len);
    }
    if(count==0)
    {
      gtk_clist_thaw(GTK_CLIST(self->files));
      return;
     }
  }
  headerGetEntry(h, RPMTAG_FILELINKTOS, NULL, (void **)&links, NULL);
  if (!headerGetEntry(h, RPMTAG_FILESTATES, NULL, (void **)&state, NULL))
    state = NULL;
  headerGetEntry(h, RPMTAG_FILEFLAGS, NULL, (void **)&flag, NULL);
  for (i = 0; i < count; i++) {
    if ((flag[i] & RPMFILE_DOC) != 0)
      row[0] = "D";
    else
      row[0] = " ";
    if ((flag[i] & RPMFILE_CONFIG) != 0)
      row[1] = "C";
    else
      row[1] = " ";
    if (state != NULL) {
      if (state[i] == RPMFILE_STATE_REPLACED)
	row[2] = "r";
      else if (state[i] == RPMFILE_STATE_NOTINSTALLED)
	row[2] = "n";
      else if (state[i] == RPMFILE_STATE_NETSHARED)
	row[2] = "s";
      else
	row[2] = "";
    } else row[2] = "";
    if (*(links[i]) == '\0')
    {
      if(dirnames==NULL)
        row[3] = path[i];
      else
      {
        g_snprintf(buf, 511, "%s%s",dirnames[dirindex[i]], path[i]);
        row[3] = buf;
      }
    }
    else {
      if(dirnames==NULL)
        g_snprintf(buf, 511, "%s -> %s", path[i], links[i]);
      else
      	g_snprintf(buf, 511, "%s%s -> %s", dirnames[dirindex[i]], path[i], links[i]);
      row[3] = buf;
    }
    gtk_clist_append(GTK_CLIST(self->files), row);
  }
  free(path);
  free(links);
  gtk_clist_thaw(GTK_CLIST(self->files));
}

#ifdef WITH_RPMFIND
gint rpm_query_set_from_rdf(RpmQuery *self, DBHandle *hdl, rpmData *data) {
  gchar buf[512];

  self->pkg_name = g_strdup(data->name);
  g_snprintf(buf, 511, "%s-%s-%s", data->name, data->version, data->release);
  gtk_label_set(GTK_LABEL(self->pkgname), buf);
  g_snprintf(buf, 511, "%d", data->size);
  gtk_label_set(GTK_LABEL(self->size), buf);
  gtk_label_set(GTK_LABEL(self->bhost), data->host?data->host:_("<none>"));
  strftime(buf, 511, "%a %b %d %I:%M:%S %Z %Y", gmtime(&(data->date)));
  gtk_label_set(GTK_LABEL(self->bdate), buf);
  gtk_label_set(GTK_LABEL(self->dist),
		data->distribution ? data->distribution : _("<none>"));
  gtk_label_set(GTK_LABEL(self->vend), data->vendor?data->vendor:_("<none>"));
  gtk_label_set(GTK_LABEL(self->group), data->group?data->group:_("<none>"));
  if (data->packager) {
    gchar *email, *name;

    rpm_query_parse_address(data->packager, &email, &name);
    if (email) {
      char *tmp = g_strconcat("mailto:", email, NULL);
      gnome_href_set_url(GNOME_HREF(self->packager), tmp);
      g_free(tmp);
      gtk_widget_set_sensitive(self->packager, TRUE);
    } else
      gtk_widget_set_sensitive(self->packager, FALSE);
    gnome_href_set_label(GNOME_HREF(self->packager), name);
    if (email) g_free(email);
    g_free(name);
    gtk_widget_show(self->packager);
  } else
    gtk_widget_hide(self->packager);
  if (data->url) {
    gnome_href_set_url(GNOME_HREF(self->url), data->url);
    gnome_href_set_label(GNOME_HREF(self->url), data->url);
    gtk_widget_show(self->url);
  } else
    gtk_widget_hide(self->url);

  if (GTK_WIDGET_REALIZED(self)) {
    gtk_text_freeze(GTK_TEXT(self->description));
    gtk_editable_delete_text(GTK_EDITABLE(self->description), 0, -1);
    gtk_text_insert(GTK_TEXT(self->description), NULL, NULL, NULL,
                    data->description?data->description:_("<none>"), -1);
    gtk_text_thaw(GTK_TEXT(self->description));
    if (self->desc) g_free(self->desc);
    self->desc = NULL;
  } else {
    self->desc = g_strdup(data->description?data->description:_("<none>"));
  }

  gtk_clist_freeze(GTK_CLIST(self->files));
  gtk_clist_clear(GTK_CLIST(self->files));
  if (data->filelist) {
    gchar **filelist = g_strsplit(data->filelist, "\n", -1), **tmp;
    gchar *row[4];

    row[0] = row[1] = row[2] = "";
    for (tmp = filelist; *tmp; tmp++) {
      row[3] = tmp[0];
      gtk_clist_append(GTK_CLIST(self->files), row);
    }
    g_strfreev(filelist);
  }
  gtk_clist_thaw(GTK_CLIST(self->files));

  if (self->fname) g_free(self->fname);
  self->fname = NULL;
  self->index = 0;
  if (self->rdfData) rpmDataFree(self->rdfData);

  self->source = RPM_QUERY_RDF;
  self->hdl = hdl;
  self->rdfData = data;
  return 0;
}
#endif

static void rpm_query_destroy(GtkObject *object) {
  RpmQuery *self;

  g_return_if_fail(object != NULL);
  g_return_if_fail(RPM_IS_QUERY(object));

  self = RPM_QUERY(object);

  if (self->desc) g_free(self->desc);
  if (self->fname) g_free(self->fname);
  if (self->pkg_name) g_free(self->pkg_name);

#ifdef WITH_RPMFIND
  if (self->rdfData) rpmDataFree(self->rdfData);
#endif

  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (* GTK_OBJECT_CLASS(parent_class)->destroy)(object);
}

static void rpm_query_realize(GtkWidget *widget) {
  RpmQuery *self;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(RPM_IS_QUERY(widget));

  self = RPM_QUERY(widget);

  if (GTK_WIDGET_CLASS(parent_class)->realize)
    (* GTK_WIDGET_CLASS(parent_class)->realize)(widget);

  if (self->desc) {
    gtk_widget_realize(self->description);
    gtk_text_freeze(GTK_TEXT(self->description));
    gtk_editable_delete_text(GTK_EDITABLE(self->description), 0,
			     gtk_text_get_length(GTK_TEXT(self->description)));
    gtk_text_insert(GTK_TEXT(self->description), NULL, NULL, NULL,
		    self->desc?self->desc:_("<none>"), -1);
    gtk_text_thaw(GTK_TEXT(self->description));
    g_free(self->desc);
    self->desc = NULL;
  }
}

static void rpm_query_chk_scroll(GtkAdjustment *adj, GtkWidget *scroll) {
  if (adj->upper - adj->lower <= adj->page_size) {
    if (GTK_WIDGET_VISIBLE(scroll))
      gtk_widget_hide(scroll);
  } else {
    if (!GTK_WIDGET_VISIBLE(scroll))
      gtk_widget_show(scroll);
  }
}

static void rpm_query_call_verify(RpmQuery *query) {
  gtk_signal_emit(GTK_OBJECT(query), query_signals[VERIFY]);
}
static void rpm_query_call_uninstall(RpmQuery *query) {
  gtk_signal_emit(GTK_OBJECT(query), query_signals[UNINSTALL]);
}
static void rpm_query_call_install(RpmQuery *query) {
  gtk_signal_emit(GTK_OBJECT(query), query_signals[INSTALL]);
}
static void rpm_query_call_upgrade(RpmQuery *query) {
  gtk_signal_emit(GTK_OBJECT(query), query_signals[UPGRADE]);
}
static void rpm_query_call_checksig(RpmQuery *query) {
  gtk_signal_emit(GTK_OBJECT(query), query_signals[CHECKSIG]);
}
static void rpm_query_call_close(RpmQuery *query) {
  gtk_signal_emit(GTK_OBJECT(query), query_signals[CLOSE]);
}
