/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <rpmlib.h>
#include "rpmdentry.h"
#include "misc.h"

enum {
  SAVE_DENTRY,
  LAST_SIGNAL
};
static guint dentry_signals[LAST_SIGNAL];

static GtkObjectClass *parent_class = NULL;

static void rpm_dentry_edit_class_init(RpmDentryEditClass *klass);
static void rpm_dentry_edit_init(RpmDentryEdit *rde);
static void rpm_dentry_edit_marshal_signal(GtkObject *object,
					   GtkSignalFunc func,
					   gpointer data, GtkArg *args);

static void rpm_dentry_edit_destroy(GtkObject *obj);
static void rpm_dentry_edit_clicked(GnomeDialog *dialog, gint button);

static void rpm_dentry_edit_populate_menu(RpmDentryEdit *rde);

guint rpm_dentry_edit_get_type() {
  static guint dentry_type = 0;
  if (!dentry_type) {
    GtkTypeInfo dentry_info = {
      "RpmDentryEdit",
      sizeof(RpmDentryEdit),
      sizeof(RpmDentryEditClass),
      (GtkClassInitFunc) rpm_dentry_edit_class_init,
      (GtkObjectInitFunc) rpm_dentry_edit_init,
      (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };
    dentry_type = gtk_type_unique(gnome_dialog_get_type(), &dentry_info);
  }
  return dentry_type;
}

static void rpm_dentry_edit_class_init(RpmDentryEditClass *klass) {
  GtkObjectClass *object_class;

  object_class = GTK_OBJECT_CLASS(klass);
  parent_class = gtk_type_class(gnome_dialog_get_type());
  dentry_signals[SAVE_DENTRY] =
    gtk_signal_new("save_dentry",
		   GTK_RUN_FIRST,
		   GTK_OBJECT_CLASS(klass)->type,
		   GTK_SIGNAL_OFFSET(RpmDentryEditClass, save_dentry),
		   rpm_dentry_edit_marshal_signal,
		   GTK_TYPE_NONE, 1,
		   GTK_TYPE_STRING);
  gtk_object_class_add_signals(object_class, dentry_signals, LAST_SIGNAL);

  object_class->destroy = rpm_dentry_edit_destroy;
  GNOME_DIALOG_CLASS(klass)->clicked = rpm_dentry_edit_clicked;
}

static void rpm_dentry_edit_init(RpmDentryEdit *self) {
  GtkWidget *vbox;
  /* add the buttons to the dialog */
  set_icon(GTK_WIDGET(self));
  gtk_window_set_title(GTK_WINDOW(self), _("Desktop Entry Editor"));
  gnome_dialog_append_buttons(GNOME_DIALOG(self), GNOME_STOCK_PIXMAP_SAVE,
			      GNOME_STOCK_BUTTON_CLOSE, NULL);

  self->indices = NULL;
  self->hdl = NULL;

  vbox = GNOME_DIALOG(self)->vbox;

  self->menu = gtk_option_menu_new();
  gtk_box_pack_start(GTK_BOX(vbox), self->menu, FALSE, TRUE, 0);
  gtk_widget_show(self->menu);
  self->notebook = gtk_notebook_new();
  gtk_box_pack_start(GTK_BOX(vbox), self->notebook, TRUE, TRUE, 0);
  gtk_widget_show(self->notebook);
  self->dentry = GNOME_DENTRY_EDIT(gnome_dentry_edit_new_notebook(
			GTK_NOTEBOOK(self->notebook)));
}

GtkWidget *rpm_dentry_edit_new(DBHandle *hdl, GList *indices) {
  RpmDentryEdit *self = gtk_type_new(rpm_dentry_edit_get_type());
  GList *tmp;

  self->hdl = hdl;
  /* copy the indices list */
  for (tmp = indices; tmp; tmp = tmp->next)
    self->indices = g_list_prepend(self->indices, tmp->data);
  rpm_dentry_edit_populate_menu(self);
  return GTK_WIDGET(self);
}

/*
 *	Q: Should we score /opt/[garbage]/bin.
 *
 *	Someone who uses the hideous /opt stuff can figure this out and play
 *	with the heuristics
 *
 *	Currently:
 *	Prefer 'bin' paths first of all - these are all user accessible
 *	If there is no 'bin' path then the ucb path for BSD unix
 *	If there is no 'ucb' path then sbin paths
 */
 
static int score_path(const char *path)
{
	if(strncmp(path, "/usr", 4)==0)
	{
		path+=4;
		if(strncmp(path, "/local", 6)==0)
			path+=6;
		if(strncmp(path, "/ucb",4 )==0)
			return 2;
	}
	if(strncmp(path, "/sbin", 5)==0)
		return 1;
	if(strncmp(path, "/bin", 4)==0) 
		return 3;
	return 0;
}

static void rpm_dentry_edit_select_package(GtkWidget *w, RpmDentryEdit *self) {
  gint index = GPOINTER_TO_INT(gtk_object_get_user_data(GTK_OBJECT(w)));
  gint32 i, len, *sizes, max, file_num, *dirindex, binpath, n;
  gint16 *modes;
  Header h;
  GnomeDesktopEntry *dentry = gnome_dentry_get_dentry(self->dentry);
  gchar *s, *s2, buf[512], **strlist, **dirnames;

  db_handle_db_up(self->hdl);

#ifdef	HAVE_RPM_4_0
  { rpmdbMatchIterator mi;
    mi = rpmdbInitIterator(self->hdl->db, RPMDBI_PACKAGES, &index, sizeof(index));
    h = rpmdbNextIterator(mi);
    if (h)
        h = headerLink(h);
    rpmdbFreeIterator(mi);
  }
#else
  h = rpmdbGetRecord(self->hdl->db, index);
#endif

  /* name on dentry will probably be package name */
  headerGetEntry(h, RPMTAG_NAME, NULL, (void**)&s, NULL);
  if (dentry->name) g_free(dentry->name);
  dentry->name = g_strdup(s);

  /* RPM docs are stored in a common place: */
  headerGetEntry(h, RPMTAG_VERSION, NULL, (void**)&s2, NULL);
  g_snprintf(buf, 512, "/usr/doc/%s-%s", s, s2);
  if (dentry->docpath) g_free(dentry->docpath);
  if (g_file_exists(buf))
    dentry->docpath = g_strdup(buf);
  else 
  {
    g_snprintf(buf, 512, "/usr/share/doc/%s-%s", s, s2);
    if (g_file_exists(buf))
      dentry->docpath = g_strdup(buf);
    else
      dentry->docpath = g_strdup("");
  }
  
  /* dentry->comments == rpm->summary */
  headerGetEntry(h, RPMTAG_SUMMARY, NULL, (void**)&s, NULL);
  if (dentry->comment) g_free(dentry->comment);
  dentry->comment = g_strdup(s);

  /*if the package requires libX11, then it probably doesn't need a terminal*/
  headerGetEntry(h, RPMTAG_REQUIRENAME, NULL, (void**)&strlist, &len);
  if (strlist) {
    dentry->terminal = TRUE;
    for (i = 0; i < len; i++)
      if (!strncmp(strlist[i], "libX11", 6)) {
	dentry->terminal = FALSE;
	break;
      }
    g_free(strlist);
  }

  /* pick largest executable in package as the main executable
   * Take path into account as an overriding rule */
  headerGetEntry(h, RPMTAG_OLDFILENAMES, NULL, (void**)&strlist, &len);
  if(len==0)
  {
    gint32 tmp;
    headerGetEntry(h, RPMTAG_BASENAMES, NULL, (void**)&strlist, &len);
    headerGetEntry(h, RPMTAG_DIRNAMES, NULL, (void**)&dirnames, &tmp);
    headerGetEntry(h, RPMTAG_DIRINDEXES, NULL, (void**)&dirindex, &tmp);
  }
  headerGetEntry(h, RPMTAG_FILESIZES, NULL, (void**)&sizes, &len);
  headerGetEntry(h, RPMTAG_FILEMODES, NULL, (void**)&modes, &len);
  max = -1;
  file_num = -1;
  binpath = 0;

  for (i = 0; i < len; i++)
  {
    if(dirnames)
    	n = score_path(dirnames[dirindex[i]]);
    else
    	n = 0;	/* For now we dont score on old old rpm */
    	
    /* check if any of the exec bits are set */
    if (modes[i] & 0111)
    {
      if(sizes[i] >= max && n == binpath) 
      {	
   	file_num = i;
	max = sizes[i];
      }
      else if(n>binpath)
      {
   	file_num = i;
	max = sizes[i];
	binpath = n;
      }
      
      /* If we find a file with matching name to the rpm then only the
         better paths can beat it */
      if(strcmp(strlist[i], dentry->name)==0 && n >= binpath)
      {
   	file_num = i;
	max = sizes[i];
	binpath = n+1;
      }
    }
  }
  
  
  g_strfreev(dentry->exec);
  
  if (file_num >= 0) {
    dentry->exec_length = 1;
    dentry->exec = g_new(gchar *, 2);
    if(dirnames)
        dentry->exec[0] = g_strconcat(dirnames[dirindex[file_num]],  strlist[file_num], NULL);
    else
        dentry->exec[0] = g_strdup(strlist[file_num]);
    dentry->exec[1] = NULL;
  } else {
    dentry->exec_length = 0;
    dentry->exec = g_new(gchar *, 1);
    dentry->exec[0] = NULL;
  }
  g_free(strlist);
  if(dirnames)
  	g_free(dirnames);

  /* XXXX should do something about the icon here -- what ?? */
  db_handle_db_down(self->hdl);

  if (dentry->type) g_free(dentry->type);
  dentry->type = g_strdup("Application");

  gnome_dentry_edit_set_dentry(self->dentry, dentry);
  gnome_desktop_entry_free(dentry);
  headerFree(h);
}

static void rpm_dentry_edit_populate_menu(RpmDentryEdit *self) {
  GList *tmp;
  GtkWidget *menu, *item = NULL;
  Header h;
  gint index;
  gchar buf[512], *s1, *s2, *s3;

  db_handle_db_up(self->hdl);
  gtk_option_menu_remove_menu(GTK_OPTION_MENU(self->menu));
  menu = gtk_menu_new();
  for (tmp = self->indices; tmp; tmp = tmp->next) {
    index = GPOINTER_TO_INT(tmp->data);

#ifdef	HAVE_RPM_4_0
    { rpmdbMatchIterator mi;
      mi = rpmdbInitIterator(self->hdl->db, RPMDBI_PACKAGES, &index, sizeof(index));
      h = rpmdbNextIterator(mi);
      if (h)
          h = headerLink(h);
      rpmdbFreeIterator(mi);
    }
#else
    h = rpmdbGetRecord(self->hdl->db, index);
#endif

    headerGetEntry(h, RPMTAG_NAME,    NULL, (void**)&s1, NULL);
    headerGetEntry(h, RPMTAG_VERSION, NULL, (void**)&s2, NULL);
    headerGetEntry(h, RPMTAG_RELEASE, NULL, (void**)&s3, NULL);
    g_snprintf(buf, 511, "%s-%s-%s", s1, s2, s3);
    headerFree(h);

    item = gtk_menu_item_new_with_label(buf);
    gtk_object_set_user_data(GTK_OBJECT(item), tmp->data);
    gtk_signal_connect(GTK_OBJECT(item), "activate",
		       GTK_SIGNAL_FUNC(rpm_dentry_edit_select_package),
		       self);
    gtk_widget_show(item);
    gtk_menu_prepend(GTK_MENU(menu), item);
  }
  if (item)
    rpm_dentry_edit_select_package(item, self);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(self->menu), menu);
  if (!self->indices || !self->indices->next)
    gtk_widget_hide(self->menu);
  else
    gtk_widget_show(self->menu);
  db_handle_db_down(self->hdl);
}

static void rpm_dentry_edit_save_cb(GtkWidget *b, RpmDentryEdit *self) {
  GtkFileSelection *fs = gtk_object_get_user_data(GTK_OBJECT(b));
  gchar *fname = gtk_file_selection_get_filename(fs);
  GnomeDesktopEntry *dentry = gnome_dentry_get_dentry(self->dentry);

  if (dentry->location) g_free(dentry->location);
  dentry->location = g_strdup(fname);

  gnome_desktop_entry_save(dentry);
  gnome_desktop_entry_free(dentry);

  gtk_widget_destroy(GTK_WIDGET(fs));
}

static void rpm_dentry_edit_save_dentry(RpmDentryEdit *self) {
  GtkWidget *win, *button;
  gchar *appdir;

  win = gtk_file_selection_new(_("Save Desktop Entry"));
  /* set the start dir to the $(prefix)/share/apps directory */
  appdir = gnome_datadir_file("gnome/apps/");
  gtk_file_selection_set_filename(GTK_FILE_SELECTION(win), appdir);
  g_free(appdir);
  button = GTK_FILE_SELECTION(win)->ok_button;
  gtk_object_set_user_data(GTK_OBJECT(button), win);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
		     GTK_SIGNAL_FUNC(rpm_dentry_edit_save_cb),
		     self);
  gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(win)->cancel_button),
			    "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy),
			    GTK_OBJECT(win));
  gtk_widget_show(win);
}

static void rpm_dentry_edit_destroy(GtkObject *object) {
  RpmDentryEdit *self = RPM_DENTRY_EDIT(object);
  if (self->indices) g_list_free(self->indices);
  if (parent_class->destroy)
    (* parent_class->destroy)(object);
}

static void rpm_dentry_edit_clicked(GnomeDialog *dlg, gint button) {
  RpmDentryEdit *self = RPM_DENTRY_EDIT(dlg);

  if (button == 0)
    rpm_dentry_edit_save_dentry(self);
  else
    gtk_widget_destroy(GTK_WIDGET(self));
  if (GNOME_DIALOG_CLASS(parent_class)->clicked)
    (* GNOME_DIALOG_CLASS(parent_class)->clicked)(dlg, button);
}

static void rpm_dentry_edit_marshal_signal(GtkObject *object,
					   GtkSignalFunc func,
					   gpointer data, GtkArg *args) {
  typedef void (*sig_func)(GtkObject *o, gchar *name, gpointer data);
  sig_func rfunc = (sig_func)func;
  (*rfunc)(object, GTK_VALUE_STRING(args[0]), data);
}


