/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "rpmwebfind.h"
#include "rpmquery.h"
#include "rpminstalldlg.h"
#include "misc.h"

#include "package.xpm"
#include "dir-open.xpm"
#include "dir-close.xpm"

static void rpm_web_find_class_init(RpmWebFindClass *klass);
static void rpm_web_find_init(RpmWebFind *self);

guint rpm_web_find_get_type(void) {
  static guint webfind_type = 0;
  if (!webfind_type) {
    GtkTypeInfo webfind_info = {
      "RpmWebFind",
      sizeof(RpmWebFind),
      sizeof(RpmWebFindClass),
      (GtkClassInitFunc) rpm_web_find_class_init,
      (GtkObjectInitFunc) rpm_web_find_init,
      (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };
    webfind_type = gtk_type_unique(gtk_vbox_get_type(), &webfind_info);
  }
  return webfind_type;
}

static GtkObjectClass *parent_class;

static void rpm_web_find_destroy(GtkObject *object);

static void rpm_web_find_class_init(RpmWebFindClass *klass) {
 parent_class = gtk_type_class(gtk_vbox_get_type());

 GTK_OBJECT_CLASS(klass)->destroy = rpm_web_find_destroy;
}

static gint rpm_web_find_do_search(RpmWebFind *self);
static void rpm_web_find_fill_tree(RpmWebFind *self, GList *results);
static void rpm_web_find_tree_expand(RpmWebFind *self, GtkCTreeNode *node);
static void rpm_web_find_tree_select_row(RpmWebFind *self, GtkCTreeNode *node,
					 gint column);

static void rpm_web_find_install(RpmWebFind *self);
static void rpm_web_find_transfer(RpmWebFind *self);

static gint init_tree(RpmWebFind *self) {
  GList *pkgList;

  /* Make sure the window doesn't disappear without us noticing */
  gtk_object_ref(GTK_OBJECT(self));
  pkgList = getFullPackageList();
  if (!GTK_OBJECT_DESTROYED(GTK_OBJECT(self)))
    rpm_web_find_fill_tree(self, pkgList);
  gtk_object_unref(GTK_OBJECT(self));
  return FALSE;
}

static void rpm_web_find_init(RpmWebFind *self) {
  GtkWidget *hbox, *wid, *paned;
  gchar *titles[] = { N_("Name"), N_("Distribution") };

  gtk_box_set_spacing(GTK_BOX(self), GNOME_PAD);

  hbox = gtk_hbox_new(FALSE, GNOME_PAD);
  gtk_container_set_border_width(GTK_CONTAINER(hbox), GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(self), hbox, FALSE, TRUE, 0);
  gtk_widget_show(hbox);

  self->gentry = gnome_entry_new("RpmWebFindHistory");
  self->entry = gnome_entry_gtk_entry(GNOME_ENTRY(self->gentry));
  gtk_combo_disable_activate(GTK_COMBO(self->gentry));
  gtk_signal_connect_object(GTK_OBJECT(self->entry), "activate",
			    GTK_SIGNAL_FUNC(rpm_web_find_do_search),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(hbox), self->gentry, TRUE, TRUE, 0);
  gtk_widget_show(self->gentry);

  wid = gtk_button_new_with_label(_("Search"));
  gtk_signal_connect_object(GTK_OBJECT(wid), "clicked",
			    GTK_SIGNAL_FUNC(rpm_web_find_do_search),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(hbox), wid, FALSE, TRUE, 0);
  gtk_widget_show(wid);

  paned = gtk_hpaned_new();
  gtk_paned_set_handle_size(GTK_PANED(paned), 10);
  gtk_paned_set_gutter_size(GTK_PANED(paned), 10);
  gtk_box_pack_start(GTK_BOX(self), paned, TRUE, TRUE, 0);
  gtk_widget_show(paned);

  wid = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(wid),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_paned_add1(GTK_PANED(paned), wid);
  gtk_widget_show(wid);
  titles[0] = _(titles[0]);
  titles[1] = _(titles[1]);
  self->ctree = gtk_ctree_new_with_titles(2, 0, titles);
  gtk_clist_set_selection_mode(GTK_CLIST(self->ctree), GTK_SELECTION_BROWSE);
  gtk_ctree_set_indent(GTK_CTREE(self->ctree), 10);
  gtk_clist_set_column_width(GTK_CLIST(self->ctree), 0, 150);
  gtk_clist_column_titles_passive(GTK_CLIST(self->ctree));
  gtk_widget_set_usize(self->ctree, 280, -1);
  gtk_clist_set_sort_column(GTK_CLIST(self->ctree), 0);
  gtk_clist_set_sort_type(GTK_CLIST(self->ctree), GTK_SORT_ASCENDING);
  gtk_signal_connect_object(GTK_OBJECT(self->ctree), "tree_select_row",
			    GTK_SIGNAL_FUNC(rpm_web_find_tree_select_row),
			    GTK_OBJECT(self));
  gtk_signal_connect_object(GTK_OBJECT(self->ctree), "tree_expand",
			    GTK_SIGNAL_FUNC(rpm_web_find_tree_expand),
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(wid), self->ctree);
  gtk_widget_show(self->ctree);

  self->info = rpm_query_new();
  if(geteuid()==0)
	  rpm_query_add_install_button(RPM_QUERY(self->info));
  wid = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
			GNOME_STOCK_PIXMAP_CONVERT), _("Download"));
  gtk_signal_connect_object(GTK_OBJECT(wid), "clicked",
			    GTK_SIGNAL_FUNC(rpm_web_find_transfer),
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(RPM_QUERY(self->info)->action_area),
		    wid);
  gtk_widget_show(wid);

  rpm_query_add_close_button(RPM_QUERY(self->info));
  gtk_signal_connect_object(GTK_OBJECT(self->info), "install",
			    GTK_SIGNAL_FUNC(rpm_web_find_install),
			    GTK_OBJECT(self));
  /*gtk_signal_connect_object(GTK_OBJECT(self->info), "upgrade",
			    GTK_SIGNAL_FUNC(rpm_web_find_upgrade),
			    GTK_OBJECT(self));*/
  
  gtk_container_set_border_width(GTK_CONTAINER(self->info), GNOME_PAD);
  gtk_paned_add2(GTK_PANED(paned), self->info);
  gtk_widget_show(self->info);

  self->fld_p = gdk_pixmap_colormap_create_from_xpm_d(NULL,
	gtk_widget_get_default_colormap(), &(self->fld_b), NULL,
	DIRECTORY_OPEN_XPM);
  self->fldclose_p = gdk_pixmap_colormap_create_from_xpm_d(NULL,
	gtk_widget_get_default_colormap(), &(self->fldclose_b), NULL,
	DIRECTORY_CLOSE_XPM);
  self->pkg_p = gdk_pixmap_colormap_create_from_xpm_d(NULL,
	gtk_widget_get_default_colormap(), &(self->pkg_b), NULL, package_xpm);

  self->packageSelected = FALSE;
  self->alt = NULL;
  self->name = NULL;

  self->fill_tag = 0;
  self->fill_results = self->fill_pos = NULL;

  self->vc_tag = 0;
  self->vc_node = NULL;

  self->install_cb = NULL;
  self->upgrade_cb = NULL;
  self->query_cb = NULL;
  self->checksig_cb = NULL;

  self->oldPkg = NULL;
  self->newPkg = NULL;
  self->curPkg = NULL;

  gtk_idle_add((GtkFunction)init_tree, self);
}

GtkWidget *rpm_web_find_new(RpmPackageList *pl) {
  RpmWebFind *self = gtk_type_new(rpm_web_find_get_type());

  self->pl = pl;
  self->hdl = pl->hdl;
  RPM_QUERY(self->info)->hdl = pl->hdl;
  return GTK_WIDGET(self);
}

void rpm_web_find_set_callbacks(RpmWebFind *self,
				GtkSignalFunc install_cb,
				GtkSignalFunc upgrade_cb,
				GtkSignalFunc query_cb,
				GtkSignalFunc checksig_cb) {
  self->install_cb = install_cb;
  self->upgrade_cb = upgrade_cb;
  self->query_cb = query_cb;
  self->checksig_cb = checksig_cb;
}

static gint rpm_web_find_do_search(RpmWebFind *self) {
  GList *results;
  gchar *search;

  /* the version check idle is running -- kill it */
  if (self->vc_tag)
    gtk_idle_remove(self->vc_tag);
  self->vc_tag = 0;
  self->vc_node = NULL;

  /* a previous fill_tree idle is running -- kill it */
  if (self->fill_tag) {
    gtk_widget_set_sensitive(self->ctree, TRUE);
    gtk_clist_thaw(GTK_CLIST(self->ctree));
    gtk_idle_remove(self->fill_tag);
    freeApropos(self->fill_results);
  }
  self->fill_tag = 0;
  self->fill_results = self->fill_pos = NULL;

  search = gtk_entry_get_text(GTK_ENTRY(self->entry));

  gtk_object_ref(GTK_OBJECT(self));
  results = aproposSearch(search);
  if (!GTK_OBJECT_DESTROYED(GTK_OBJECT(self)))
    rpm_web_find_fill_tree(self, results);
  gtk_object_unref(GTK_OBJECT(self));
  return FALSE;
}

static gint rpm_web_find_vcheck(RpmWebFind *self) {
  gchar *name;
#ifdef	HAVE_RPM_4_0
  rpmdbMatchIterator mi;
  Header h, best = NULL;
#else
  dbiIndexSet matches;
#endif

  if (!self->vc_node || ! GTK_CTREE_ROW(self->vc_node)) {
    self->vc_tag = 0;
    self->vc_node = NULL;
    return FALSE;
  }
  gtk_ctree_get_node_info(GTK_CTREE(self->ctree), self->vc_node, &name,
			  NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  db_handle_db_up(self->hdl);

#ifdef	HAVE_RPM_4_0
  /* deduce newest version from those installed */
  mi = rpmdbInitIterator(self->hdl->db, RPMDBI_LABEL, name, 0);
  while ((h = rpmdbNextIterator(mi)) != NULL) {
    if (!best) best = headerLink(h);
    else {
      if (rpmVersionCompare(best, h) < 0) {
	headerFree(best);
	best = headerLink(h);
      }
    }
  }
  rpmdbFreeIterator(mi);

  if (best) {
    gchar *ver, *rel, buf[512];
    headerGetEntry(best, RPMTAG_VERSION, NULL, (void**)&ver, NULL);
    headerGetEntry(best, RPMTAG_RELEASE, NULL, (void**)&rel, NULL);
    g_snprintf(buf, 511, _("have %s-%s"), ver, rel);
    headerFree(best);
    gtk_ctree_node_set_text(GTK_CTREE(self->ctree), self->vc_node, 1, buf);
  }

#else	/* !HAVE_RPM_4_0 */

  if (!rpmdbFindByLabel(self->hdl->db, name, &matches)) {
    Header best = NULL;
    gint i;
    gchar *ver, *rel, buf[512];
    
    /* deduce newest version from those installed */
    for (i = 0; i < matches.count; i++) {
      gint index = matches.recs[i].recOffset;
      Header h = rpmdbGetRecord(self->hdl->db, index);

      if (!best)
	best = h;
      else {
	if (rpmVersionCompare(best, h) < 0) {
	  headerFree(best);
	  best = h;
	} else
	  headerFree(h);
      }
    }
    dbiFreeIndexRecord(matches);

    headerGetEntry(best, RPMTAG_VERSION, NULL, (void**)&ver, NULL);
    headerGetEntry(best, RPMTAG_RELEASE, NULL, (void**)&rel, NULL);
    g_snprintf(buf, 511, _("have %s-%s"), ver, rel);
    headerFree(best);
    gtk_ctree_node_set_text(GTK_CTREE(self->ctree), self->vc_node, 1, buf);
  }
#endif	/* HAVE_RPM_4_0 */

  db_handle_db_down(self->hdl);
  self->vc_node = GTK_CTREE_ROW(self->vc_node)->sibling;
  if (self->vc_node)
    return TRUE;
  self->vc_tag = 0;
  return FALSE;
}

static gint rpm_web_find_fill_idle_cb(RpmWebFind *self) {
  gchar *row[2] = { "", "" };
  gchar *name = self->fill_pos->data;
  GtkCTreeNode *child, *bogus;

  row[0] = name;
  child = gtk_ctree_insert_node(GTK_CTREE(self->ctree), NULL, NULL,
				row, 2, self->fldclose_p, self->fldclose_b,
				self->fld_p, self->fld_b, FALSE, FALSE);
  /* bogus node, so we can catch expand events, and work out nodes only
   * as needed */
  bogus = gtk_ctree_insert_node(GTK_CTREE(self->ctree), child, NULL,
				row, 2, NULL, NULL, NULL, NULL, TRUE, FALSE);

  self->fill_pos = self->fill_pos->next;
  if (self->fill_pos != NULL)
    return TRUE;  /* still more rows to process */

  /* we have finished filling the tree */
  gtk_clist_select_row(GTK_CLIST(self->ctree), 0, 0);
  gtk_widget_set_sensitive(self->ctree, TRUE);
  gtk_clist_thaw(GTK_CLIST(self->ctree));

  self->vc_node = gtk_ctree_node_nth(GTK_CTREE(self->ctree), 0);
  if (self->vc_node)
    self->vc_tag = gtk_idle_add((GtkFunction)rpm_web_find_vcheck, self);

  freeApropos(self->fill_results);
  self->fill_tag = 0;
  self->fill_results = self->fill_pos = NULL;

  return FALSE;
}

static void rpm_web_find_fill_tree(RpmWebFind *self, GList *results) {
  /* the version check idle is running -- kill it */
  if (self->vc_tag)
    gtk_idle_remove(self->vc_tag);
  self->vc_tag = 0;
  self->vc_node = NULL;

  /* a previous fill_tree idle is running -- kill it */
  if (self->fill_tag) {
    gtk_widget_set_sensitive(self->ctree, TRUE);
    gtk_clist_thaw(GTK_CLIST(self->ctree));
    gtk_idle_remove(self->fill_tag);
    freeApropos(self->fill_results);
  }
  self->fill_tag = 0;
  self->fill_results = self->fill_pos = NULL;

  self->packageSelected = FALSE;
  self->alt = NULL;
  self->name = NULL;

  self->fill_results = self->fill_pos = results;
  gtk_clist_clear(GTK_CLIST(self->ctree));
  if (!self->fill_results)
    return;

  gtk_widget_set_sensitive(self->ctree, FALSE);
  gtk_clist_freeze(GTK_CLIST(self->ctree));
  self->fill_tag = gtk_idle_add((GtkFunction)rpm_web_find_fill_idle_cb, self);
}

static void rpm_web_find_tree_expand(RpmWebFind *self, GtkCTreeNode *node) {
  GtkCTreeRow *crow = GTK_CTREE_ROW(node);
  gchar *name, buf[256];

  gtk_ctree_node_get_text(GTK_CTREE(self->ctree), node, 0, &name);
  gtk_ctree_get_node_info(GTK_CTREE(self->ctree), node, &name,
			  NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  if (crow->is_leaf == FALSE && (crow->children == NULL 
		|| GTK_CTREE_ROW(crow->children)->sibling == NULL)) {
    /* fill in alternates */
    GList *alts, *tmp;
    GtkCTreeNode *child = NULL;

    /* there was only one alternative, so don't recalculate */
    if (GTK_CTREE_ROW(crow->children) &&
	gtk_ctree_node_get_row_data(GTK_CTREE(self->ctree), crow->children))
      return;

    gtk_widget_set_sensitive(self->ctree, FALSE);
    alts = alternateSearch(name);
    if(alts==NULL)
    	goto failed;
    alts = sortAlternates(alts, NULL, NULL);
    db_handle_db_up(self->hdl);
    statAlternates(self->hdl->db, alts);
    db_handle_db_down(self->hdl);
    if (crow->children)
      gtk_ctree_remove_node(GTK_CTREE(self->ctree), crow->children);

    for (tmp = alts; tmp; tmp = tmp->next) {
      rpmPackageAlternate *alt = tmp->data;
      gchar *row[2];

      g_snprintf(buf, 255, "%s-%s", alt->version, alt->release);
      row[0] = buf;
      row[1] = distribInfoGet(alt->subdir)->name;
      child = gtk_ctree_insert_node(GTK_CTREE(self->ctree), node, NULL,
				    row, 2, self->pkg_p, self->pkg_b,
				    self->pkg_p, self->pkg_b, TRUE, FALSE);
      gtk_ctree_node_set_row_data_full(GTK_CTREE(self->ctree), child,
				       alt, (GtkDestroyNotify)
				       freeRpmPackageAlternate);
      if (alt->status == OLD_PACKAGE) {
	if (self->oldPkg == NULL) {
	  GdkColor colour;
	  gchar *colour_name = gnome_config_get_string("/gnorpm/Install/oldPkg=grey50");
	  self->oldPkg = gtk_style_copy(gtk_widget_get_style(self->ctree));
	  if (gdk_color_parse(colour_name, &colour) &&
	      gdk_color_alloc(gtk_widget_get_colormap(self->ctree), &colour))
	    self->oldPkg->fg[GTK_STATE_NORMAL] = colour;
	  g_free(colour_name);
	}
	gtk_ctree_node_set_row_style(GTK_CTREE(self->ctree), child,
				     self->oldPkg);
      } else if (alt->status == NEW_PACKAGE) {
	if (self->newPkg == NULL) {
	  GdkColor colour;
	  gchar *colour_name = gnome_config_get_string("/gnorpm/Install/newPkg=blue");
	  self->newPkg = gtk_style_copy(gtk_widget_get_style(self->ctree));
	  if (gdk_color_parse(colour_name, &colour) &&
	      gdk_color_alloc(gtk_widget_get_colormap(self->ctree), &colour))
	    self->newPkg->fg[GTK_STATE_NORMAL] = colour;
	  g_free(colour_name);
	}
	gtk_ctree_node_set_row_style(GTK_CTREE(self->ctree), child,
				     self->newPkg);
      } else if (alt->status == CURRENT_PACKAGE) {
	if (self->curPkg == NULL) {
	  GdkColor colour;
	  gchar *colour_name = gnome_config_get_string("/gnorpm/Install/curPkg=green4");
	  self->curPkg = gtk_style_copy(gtk_widget_get_style(self->ctree));
	  if (gdk_color_parse(colour_name, &colour) &&
	      gdk_color_alloc(gtk_widget_get_colormap(self->ctree), &colour))
	    self->curPkg->fg[GTK_STATE_NORMAL] = colour;
	  g_free(colour_name);
	}
	gtk_ctree_node_set_row_style(GTK_CTREE(self->ctree), child,
				     self->curPkg);
      }
    }
    g_list_free(alts);
failed:
    gtk_widget_set_sensitive(self->ctree, TRUE);
  }
}
static void rpm_web_find_tree_select_row(RpmWebFind *self, GtkCTreeNode *node,
					 gint column) {
  rpmData *rpm;
  self->alt = gtk_ctree_node_get_row_data(GTK_CTREE(self->ctree), node);

  if (!self->alt) {
    self->packageSelected = FALSE;
    gtk_ctree_get_node_info(GTK_CTREE(self->ctree), node, &(self->name),
			    NULL, NULL, NULL, NULL, NULL, NULL, NULL);

    rpm_query_clear(RPM_QUERY(self->info));
    return;
  }
  self->packageSelected = TRUE;
  self->name = FALSE;

  rpm = alternateGetInfo(self->alt);
  if(rpm)
	rpm_query_set_from_rdf(RPM_QUERY(self->info), self->hdl, rpm);
  gtk_widget_queue_resize(self->info);
}

static gboolean rpm_web_find_get_packages(RpmWebFind *self, GList **local) {
  GList *urls = NULL, *tmp, *items = NULL, *localfiles = NULL;
  GtkWidget *win, *box, *list, *w;
  gint res;

  /* No name to download yet ? */

  db_handle_db_up(self->hdl);
  gtk_object_ref(GTK_OBJECT(self));
  if (self->packageSelected)
    urls = resolveAlternate(self->alt, self->hdl->db);
  else if(self->name)
    urls = resolveName(self->name, self->hdl->db);
  else {
    db_handle_db_down(self->hdl);
    gtk_object_unref(GTK_OBJECT(self));
    return FALSE;
  }

  db_handle_db_down(self->hdl);

  /* 'Cancel' may have been clicked while downloading things from the net */
  if (GTK_OBJECT_DESTROYED(GTK_OBJECT(self))) {
    gtk_object_unref(GTK_OBJECT(self));
	return FALSE;
  }
  gtk_object_unref(GTK_OBJECT(self));

  if (!urls) {
    win = gnome_message_box_new(_("No packages to download"),
				GNOME_MESSAGE_BOX_INFO,
				GNOME_STOCK_BUTTON_OK, NULL);
    gnome_dialog_set_parent(GNOME_DIALOG(win),
		GTK_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(self))));
    set_icon(win);
    gtk_widget_show(win);
    return FALSE;
  }

  win = gnome_dialog_new(_("Download files?"), GNOME_STOCK_BUTTON_YES,
			 GNOME_STOCK_BUTTON_NO, NULL);
  gnome_dialog_set_parent(GNOME_DIALOG(win),
		GTK_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(self))));
  gtk_window_set_policy(GTK_WINDOW(win), FALSE, TRUE, TRUE);
  set_icon(win);

  gnome_dialog_close_hides(GNOME_DIALOG(win), FALSE);
  box = GNOME_DIALOG(win)->vbox;
  w = gtk_label_new(_("Download these packages?"));
  gtk_box_pack_start(GTK_BOX(box), w, FALSE, TRUE, 0);
  gtk_widget_show(w);
  for (tmp = urls; tmp; tmp = tmp->next) {
    GtkWidget *item = gtk_list_item_new_with_label(tmp->data);
    
    gtk_widget_show(item);
    items = g_list_append(items, item);
  }
  w = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_usize(w, 320, 120);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(w), GTK_POLICY_AUTOMATIC,
				 GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start(GTK_BOX(box), w, TRUE, TRUE, 0);
  gtk_widget_show(w);
  list = gtk_list_new();
  gtk_list_set_selection_mode(GTK_LIST(list), GTK_SELECTION_BROWSE);
  gtk_list_append_items(GTK_LIST(list), items);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(w), list);
  gtk_widget_show(list);
  
  gtk_widget_show(win);

  res = gnome_dialog_run_and_close(GNOME_DIALOG(win));

  if (res == 1) {
    g_list_foreach(urls, (GFunc)g_free, NULL);
    g_list_free(urls);
    return FALSE;
  }

  for (tmp = urls; tmp; tmp = tmp->next) {
    char *file = url_download_file(tmp->data);

    if (!file) {
      statusbar_msg(_("Couldn't download files"));
      g_list_foreach(urls, (GFunc)g_free, NULL);
      g_list_free(urls);
      g_list_foreach(localfiles, (GFunc)g_free, NULL);
      g_list_free(localfiles);
      return FALSE;
    }
    localfiles = g_list_append(localfiles, file);
  }
  g_list_foreach(urls, (GFunc)g_free, NULL);
  g_list_free(urls);

  if (local)
    *local = localfiles;
  else {
    g_list_foreach(localfiles, (GFunc)g_free, NULL);
    g_list_free(localfiles);
  }
  return TRUE;
}

static void rpm_web_find_install(RpmWebFind *self) {
  GtkWidget *win;
  GList *localfiles, *tmp;

  if (!rpm_web_find_get_packages(self, &localfiles))
    return;
  if(geteuid())
  {
    GtkWidget *d=gnome_error_dialog(_("You need to be the superuser to install packages."));
    gnome_dialog_run_and_close(GNOME_DIALOG(d));
  }


  win = rpm_install_dialog_new(self->hdl);
  gtk_window_set_title(GTK_WINDOW(win), _("Install"));
  if (self->install_cb)
    gtk_signal_connect(GTK_OBJECT(win), "install",
		       self->install_cb, self->pl);
  if (self->upgrade_cb)
    gtk_signal_connect(GTK_OBJECT(win), "upgrade",
		       self->upgrade_cb, self->pl);
  if (self->query_cb)
    gtk_signal_connect(GTK_OBJECT(win), "query",
		       self->query_cb, self->pl);
  if (self->checksig_cb)
    gtk_signal_connect(GTK_OBJECT(win), "checksig",
		       self->checksig_cb, self->hdl);

  for (tmp = localfiles; tmp; tmp = tmp->next)
    rpm_install_dialog_add_file(RPM_INSTALL_DIALOG(win), tmp->data, TRUE);
  g_list_foreach(localfiles, (GFunc)g_free, NULL);
  g_list_free(localfiles);
  gtk_widget_show(win);
}

static void rpm_web_find_transfer(RpmWebFind *self) {
  rpm_web_find_get_packages(self, NULL);
}

static void rpm_web_find_destroy(GtkObject *object) {
  RpmWebFind *self = RPM_WEB_FIND(object);

  if (self->oldPkg) gtk_style_unref(self->oldPkg);
  if (self->newPkg) gtk_style_unref(self->newPkg);
  if (self->curPkg) gtk_style_unref(self->curPkg);

  gdk_pixmap_unref(self->fld_p);      gdk_bitmap_unref(self->fld_b);
  gdk_pixmap_unref(self->fldclose_p); gdk_bitmap_unref(self->fldclose_b);
  gdk_pixmap_unref(self->pkg_p);      gdk_bitmap_unref(self->pkg_b);
  
  /* the version check idle is running -- kill it */
  if (self->vc_tag)
    gtk_idle_remove(self->vc_tag);

  /* a previous fill_tree idle is running -- kill it */
  if (self->fill_tag) {
    gtk_clist_thaw(GTK_CLIST(self->ctree));
    gtk_idle_remove(self->fill_tag);
    freeApropos(self->fill_results);
  }

  if (parent_class->destroy)
    (* parent_class->destroy)(object);
}
