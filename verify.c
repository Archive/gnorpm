/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "verify.h"
#include <string.h>
#include "misc.h"

GtkWidget *verify_packages(DBHandle *hdl, GList *indices, gint omitMask) {
  Header h;
  GtkWidget *win, *vbox, *pkg_name, *fname, *swin, *clist, *button;
  gchar buf1[512], buf2[512], buf3[512], *s1, *s2, *s3, **files;
  gchar *headings[] = {N_("Package"), N_("File"), N_("Problem")}, *row[3];
  guint index, i, count, failCount = 0;
  gint result;
  gint32 *dirindex=NULL;
  gchar **dirnames=NULL;
  gint32 len;

  headings[0] = _(headings[0]);
  headings[1] = _(headings[1]);
  headings[2] = _(headings[2]);
  win = gtk_dialog_new();
  set_icon(win);
  gtk_window_set_title(GTK_WINDOW(win), _("Verifying Packages"));
  vbox = gtk_vbox_new(FALSE, 5);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(win)->vbox), vbox, TRUE, TRUE, 0);
  gtk_widget_show(vbox);

  pkg_name = gtk_label_new(_("No packages selected"));
  gtk_box_pack_start(GTK_BOX(vbox), pkg_name, FALSE, TRUE, 0);
  gtk_widget_show(pkg_name);
  fname = gtk_label_new("<file>");
  gtk_box_pack_start(GTK_BOX(vbox), fname, FALSE, TRUE, 0);
  gtk_widget_show(fname);

  swin = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(swin),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start(GTK_BOX(vbox), swin, TRUE, TRUE, 0);
  gtk_widget_show(swin);

  clist = gtk_clist_new_with_titles(3, headings);
  gtk_widget_set_usize(clist, 350, 100);
  gtk_clist_column_titles_passive(GTK_CLIST(clist));
  gtk_clist_set_column_width(GTK_CLIST(clist), 0, 75);
  gtk_clist_set_column_width(GTK_CLIST(clist), 1, 130);
  gtk_clist_set_column_width(GTK_CLIST(clist), 2, 100);
  gtk_container_add(GTK_CONTAINER(swin), clist);
  gtk_widget_show(clist);

  button = gnome_stock_button(GNOME_STOCK_BUTTON_CLOSE);
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    (GtkSignalFunc)gtk_widget_destroy,
			    GTK_OBJECT(win));
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(win)->action_area), button, FALSE,
		     FALSE, 0);
  gtk_widget_grab_default(button);
  gtk_widget_set_sensitive(button, FALSE);
  gtk_widget_show(button);

  gtk_widget_show(win);

  db_handle_db_up(hdl);
  for (; indices != NULL; indices = indices->next) {
    index = GPOINTER_TO_UINT(indices->data);

#ifdef HAVE_RPM_4_0
    { rpmdbMatchIterator mi;
      mi = rpmdbInitIterator(hdl->db, RPMDBI_PACKAGES, &index, sizeof(index));
      h = rpmdbNextIterator(mi);
      if (h)
          h = headerLink(h);
      rpmdbFreeIterator(mi);
    }
#else
    h = rpmdbGetRecord(hdl->db, index);
#endif
    headerGetEntry(h, RPMTAG_NAME,    NULL, (void **)&s1, NULL);
    headerGetEntry(h, RPMTAG_VERSION, NULL, (void **)&s2, NULL);
    headerGetEntry(h, RPMTAG_RELEASE, NULL, (void **)&s3, NULL);
    g_snprintf(buf1, 511, "%s-%s-%s", s1, s2, s3);
    gtk_label_set(GTK_LABEL(pkg_name), buf1);
    headerGetEntry(h, RPMTAG_OLDFILENAMES, NULL, (void **)&files, &count);
    if (files == 0)
    {
      headerGetEntry(h, RPMTAG_BASENAMES, NULL, (void**)&files, &count);
      headerGetEntry(h, RPMTAG_DIRNAMES, NULL, (void**)&dirnames, &len);
      headerGetEntry(h, RPMTAG_DIRINDEXES, NULL, (void**)&dirindex, &len);
    }
    if (files) {
      for (i = 0; i < count; i++) {
	gtk_label_set(GTK_LABEL(fname), files[i]);
	while (gtk_events_pending())
	  gtk_main_iteration();
	if (rpmVerifyFile(hdl->root, h, i, (rpmVerifyAttrs *) &result, omitMask)
			|| result) {
	  row[0] = buf1;
	  if(dirnames)
	  {
	    snprintf(buf3, 512, "%s%s", dirnames[dirindex[i]], files[i]);
	    row[1]=buf3;
	  }
	  else
	    row[1] = files[i];
	  
	  row[2] = &buf2[2];
	  buf2[0] = '\0';
	  if (!result) row[2] = _("missing");
	  else {
	    if (result & RPMVERIFY_MD5)      strcat(buf2, _(", md5"));
	    if (result & RPMVERIFY_FILESIZE) strcat(buf2, _(", file size"));
	    if (result & RPMVERIFY_LINKTO)   strcat(buf2,
						 _(", symbolic link problem"));
	    if (result & RPMVERIFY_USER)     strcat(buf2, _(", user"));
	    if (result & RPMVERIFY_GROUP)    strcat(buf2, _(", group"));
	    if (result & RPMVERIFY_MTIME)    strcat(buf2,
						    _(", modification time"));
	    if (result & RPMVERIFY_MODE)     strcat(buf2, _(", file mode"));
	    if (result & RPMVERIFY_RDEV)     strcat(buf2,
						    _(", device file type"));
	  }
	  gtk_clist_append(GTK_CLIST(clist), row);
	  failCount++;
	}
      }
      free(files);
    }
    gtk_label_set(GTK_LABEL(fname), _("*script*"));
    while (gtk_events_pending())
      gtk_main_iteration();
    if (!gnome_config_get_bool("/gnorpm/Flags/NoScripts=false")) {
      if (rpmVerifyScript(hdl->root, h, 0)) {
	row[0] = buf1;
	row[1] = _("*script*");
	row[2] = _("script problem");
	gtk_clist_append(GTK_CLIST(clist), row);
	failCount++;
      }
    }
    headerFree(h);
  }
  db_handle_db_down(hdl);
  if (failCount) {
    g_snprintf(buf1, 511, _("%d problems found."), failCount);
    gtk_label_set(GTK_LABEL(fname), buf1);
  } else
    gtk_label_set(GTK_LABEL(fname), _("No problems found."));
  gtk_widget_set_sensitive(button, TRUE);
  return win;
}

GtkWidget *verify_one(DBHandle *hdl, guint index, gint omitMask) {
  GList *indices;
  GtkWidget *ret;
  indices = g_list_append(NULL, GUINT_TO_POINTER(index));
  ret = verify_packages(hdl, indices, omitMask);
  g_list_free(indices);
  return ret;
}
