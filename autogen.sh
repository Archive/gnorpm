#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Gnome-RPM"

(test -f $srcdir/configure.in \
  && test -f $srcdir/gnorpm.c \
  && test -d $srcdir/find) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gnorpm directory"
    exit 1
}

. $srcdir/macros/autogen.sh
