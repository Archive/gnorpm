/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <gnome.h>
#include "rpmprogress.h"

static void rpm_progress_class_init(RpmProgressClass *klass);
static void rpm_progress_init(RpmProgress *prog);

guint rpm_progress_get_type() {
  static guint progress_type = 0;
  if (!progress_type) {
    GtkTypeInfo progress_info = {
      "RpmProgress",
      sizeof(RpmProgress),
      sizeof(RpmProgressClass),
      (GtkClassInitFunc) rpm_progress_class_init,
      (GtkObjectInitFunc) rpm_progress_init,
      (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };
    progress_type = gtk_type_unique(gtk_table_get_type(), &progress_info);
  }
  return progress_type;
}

static void rpm_progress_class_init(RpmProgressClass *klass) {
}
static void rpm_progress_init(RpmProgress *self) {
  GtkTable *table;
  GtkWidget *label;

  table = GTK_TABLE(self);
  gtk_table_set_col_spacings(table, 10);

  self->pkg_name = gtk_label_new("");
  gtk_widget_set_name(self->pkg_name, "PackageName");
  gtk_table_attach(table, self->pkg_name, 0,4, 0,1, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(self->pkg_name);

  self->pkg_progress = gtk_progress_bar_new();
  gtk_widget_set_name(self->pkg_progress, "PackageProgress");
  gtk_table_attach(table, self->pkg_progress, 0,4, 1,2, GTK_FILL|GTK_EXPAND,
		   GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->pkg_progress);

  label = gtk_label_new(_("Done"));
  gtk_widget_set_name(label, "Heading");
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(table, label, 1,2, 2,3, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(label);
  label = gtk_label_new(_("Remaining"));
  gtk_widget_set_name(label, "Heading");
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(table, label, 2,3, 2,3, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(label);
  label = gtk_label_new(_("Total"));
  gtk_widget_set_name(label, "Heading");
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(table, label, 3,4, 2,3, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(label);

  label = gtk_label_new(_("Packages"));
  gtk_widget_set_name(label, "Heading");
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(table, label, 0,1, 3,4, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(label);
  self->pkgsDone = gtk_label_new("0");
  gtk_widget_set_name(self->pkgsDone, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->pkgsDone), 1.0, 0.5);
  gtk_table_attach(table, self->pkgsDone, 1,2, 3,4, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(self->pkgsDone);
  self->pkgsLeft = gtk_label_new("0");
  gtk_widget_set_name(self->pkgsLeft, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->pkgsLeft), 1.0, 0.5);
  gtk_table_attach(table, self->pkgsLeft, 2,3, 3,4, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(self->pkgsLeft);
  self->pkgsTot = gtk_label_new("0");
  gtk_widget_set_name(self->pkgsTot, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->pkgsTot), 1.0, 0.5);
  gtk_table_attach(table, self->pkgsTot, 3,4, 3,4, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(self->pkgsTot);

  label = gtk_label_new(_("Size"));
  gtk_widget_set_name(label, "Heading");
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(table, label, 0,1, 4,5, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(label);
  self->sizeDone = gtk_label_new("0");
  gtk_widget_set_name(self->sizeDone, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->sizeDone), 1.0, 0.5);
  gtk_table_attach(table, self->sizeDone, 1,2, 4,5, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(self->sizeDone);
  self->sizeLeft = gtk_label_new("0");
  gtk_widget_set_name(self->sizeLeft, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->sizeLeft), 1.0, 0.5);
  gtk_table_attach(table, self->sizeLeft, 2,3, 4,5, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(self->sizeLeft);
  self->sizeTot = gtk_label_new("0");
  gtk_widget_set_name(self->sizeTot, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->sizeTot), 1.0, 0.5);
  gtk_table_attach(table, self->sizeTot, 3,4, 4,5, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(self->sizeTot);

  label = gtk_label_new(_("Time"));
  gtk_widget_set_name(label, "Heading");
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(table, label, 0,1, 5,6, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(label);
  self->timeDone = gtk_label_new("00:00:00");
  gtk_widget_set_name(self->timeDone, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->timeDone), 1.0, 0.5);
  gtk_table_attach(table, self->timeDone, 1,2, 5,6, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(self->timeDone);
  self->timeLeft = gtk_label_new("??:??:??");
  gtk_widget_set_name(self->timeLeft, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->timeLeft), 1.0, 0.5);
  gtk_table_attach(table, self->timeLeft, 2,3, 5,6, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(self->timeLeft);
  self->timeTot = gtk_label_new("??:??:??");
  gtk_widget_set_name(self->timeTot, "Information");
  gtk_misc_set_alignment(GTK_MISC(self->timeTot), 1.0, 0.5);
  gtk_table_attach(table, self->timeTot, 3,4, 5,6, GTK_FILL|GTK_EXPAND,
		   GTK_FILL, 0, 0);
  gtk_widget_show(self->timeTot);

  self->tot_progress = gtk_progress_bar_new();
  gtk_table_attach(table, self->tot_progress, 0,4, 6,7, GTK_FILL|GTK_EXPAND,
		   GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->tot_progress);

  self->numInstalled = -1;
  self->numPackages = 1;
  self->amountInstalled = 0;
  self->curAmount = 0;
  self->curPkgSize = 0;
  self->starttime = time(NULL);
}

GtkWidget *rpm_progress_new(guint numPackages, guint totSize) {
  RpmProgress *self = gtk_type_new(rpm_progress_get_type());
  gchar buf[512];

  self->numPackages = numPackages;
  self->totSize = (totSize ? totSize : 1);
  g_snprintf(buf, 511, "%d", numPackages);
  gtk_label_set(GTK_LABEL(self->pkgsTot), buf);
  g_snprintf(buf, 511, "%d", totSize);
  gtk_label_set(GTK_LABEL(self->sizeTot), buf);
  return GTK_WIDGET(self);
}

void rpm_progress_next(RpmProgress *self, gchar *pkg_name, guint curPkgSize) {
  gchar buf[512];

  self->numInstalled++;
  self->amountInstalled += self->curPkgSize;
  g_snprintf(buf, 511, "%d", self->numInstalled);
  gtk_label_set(GTK_LABEL(self->pkgsDone), buf);
  g_snprintf(buf, 511, "%d", self->numPackages - self->numInstalled);
  gtk_label_set(GTK_LABEL(self->pkgsLeft), buf);

  gtk_progress_bar_update(GTK_PROGRESS_BAR(self->pkg_progress), 0.0);
  gtk_progress_bar_update(GTK_PROGRESS_BAR(self->tot_progress),
			  (1.0 * self->amountInstalled) / self->totSize);

  gtk_label_set(GTK_LABEL(self->pkg_name), pkg_name);

  self->curPkgSize = curPkgSize;

  while (gtk_events_pending())
    gtk_main_iteration();
}

void rpm_progress_update(RpmProgress *self, gdouble pcnt) {
  gchar buf[512];
  guint curAmount, pkgAmount;
  time_t curtime, finaltime;

  pkgAmount = pcnt * self->curPkgSize;
  curAmount = self->amountInstalled + pkgAmount;

  gtk_progress_bar_update(GTK_PROGRESS_BAR(self->pkg_progress), pcnt);
  gtk_progress_bar_update(GTK_PROGRESS_BAR(self->tot_progress),
			  (1.0 * curAmount) / self->totSize);

  g_snprintf(buf, 511, "%dK", curAmount / 1024);
  gtk_label_set(GTK_LABEL(self->sizeDone), buf);
  g_snprintf(buf, 511, "%dK", (self->totSize - curAmount) / 1024);
  gtk_label_set(GTK_LABEL(self->sizeLeft), buf);

  curtime = time(NULL) - self->starttime;
  finaltime = curtime * 1.0 * self->totSize / curAmount;
  g_snprintf(buf, 511, "%02ld:%02ld:%02ld", curtime / 3600,
	     (curtime % 3600) / 60, curtime % 60);
  gtk_label_set(GTK_LABEL(self->timeDone), buf);
  curtime = finaltime - curtime;
  g_snprintf(buf, 511, "%02ld:%02ld:%02ld", curtime / 3600,
	     (curtime % 3600) / 60, curtime % 60);
  gtk_label_set(GTK_LABEL(self->timeLeft), buf);
  g_snprintf(buf, 511, "%02ld:%02ld:%02ld", finaltime / 3600,
	     (finaltime % 3600) / 60, finaltime % 60);
  gtk_label_set(GTK_LABEL(self->timeTot), buf);
  while (gtk_events_pending())
    gtk_main_iteration();
}

