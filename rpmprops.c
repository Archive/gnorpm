/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "rpmprops.h"
#include <rpmlib.h>
#include <string.h>
#include <glib.h>
#include "misc.h"
#include "install.h"

#ifdef WITH_RPMFIND
#include "find/libfind.h"
#endif

static GnomePropertyBoxClass *parent_class;

static void rpm_props_box_destroy(GtkObject *object);
static void rpm_props_box_apply(GnomePropertyBox *self, gint page_num);

static void rpm_props_box_class_init(RpmPropsBoxClass *klass);
static void rpm_props_box_init(RpmPropsBox *box);

#ifdef WITH_RPMFIND
static gchar* adjust_proxy_text(const gchar *proxyText);
#endif

extern GnomeApp *gnorpm_app;

guint rpm_props_box_get_type(void) {
  static guint propsbox_type = 0;
  if (!propsbox_type) {
    GtkTypeInfo propsbox_info = {
      "RpmPropsBox",
      sizeof(RpmPropsBox),
      sizeof(RpmPropsBoxClass),
      (GtkClassInitFunc) rpm_props_box_class_init,
      (GtkObjectInitFunc) rpm_props_box_init,
      (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };
    propsbox_type = gtk_type_unique(gnome_property_box_get_type(),
				    &propsbox_info);
  }
  return propsbox_type;
}

static void rpm_props_box_class_init(RpmPropsBoxClass *klass) {
  parent_class = GNOME_PROPERTY_BOX_CLASS(gtk_type_class(
				gnome_property_box_get_type()));
  GTK_OBJECT_CLASS(klass)->destroy = rpm_props_box_destroy;
  GNOME_PROPERTY_BOX_CLASS(klass)->apply = rpm_props_box_apply;
}

typedef struct {
  gint32 interfaceFlags, transFlags, probFilter;
  const gchar *tag, *string, *help_string;
} RpmFlagInfo;
const static RpmFlagInfo prop_flags[] = {
    /* install options */
  {INTER_NODEPS,  0, 0, "NoDependencies", N_("No dependency checks"),
   N_("Do not check for dependencies on other rpms. Equivalent to --nodeps")},
  {INTER_NOORDER, 0, 0, "NoReorder", N_("No reordering"),
   N_("Do not reorder package installation to satisfy dependencies. Equivalent to --noorder")},
  {0, RPMTRANS_FLAG_NOSCRIPTS, 0, "NoScripts",
   N_("Don't run scripts"),
   N_("Do not run the pre and post install scripts. Equivalent to --noscripts.")}, 
  /* upgrade options */
  {0, 0, RPMPROB_FILTER_REPLACEPKG, "ReplacePkgs",
   N_("Allow replacement of packages"), 
   N_("Replace packages with a new copy of itself. Equivalent to --replacepkgs.")},
  {0, 0, RPMPROB_FILTER_REPLACEOLDFILES, "ReplaceFiles",
   N_("Allow replacement of files"),
   N_("Replace files owned by another package. Equivalent to --replacefiles.")},
  {0, 0, RPMPROB_FILTER_OLDPACKAGE, "UpgradeToOld",
   N_("Allow upgrade to old version"),
   N_("Upgrade packages to an older package if need be. Equivalent to --oldpackage")},
  {0, RPMTRANS_FLAG_KEEPOBSOLETE, 0, "KeepObsolete",
   N_("Keep packages made obsolete"),
   N_("If a package has been obsoleted, keep it anyway.")},
  /* file options */
  {0, RPMTRANS_FLAG_NODOCS, 0, "NoDocumentation",
   N_("Don't install documentation"),
   N_("Do not install any of the doc files. Equivalent to --excludedocs.")},
  {0, RPMTRANS_FLAG_ALLFILES, 0, "AllFiles",
   N_("Install all files"),
   N_("Install all files")},
  /* database options */
  {0, RPMTRANS_FLAG_JUSTDB, RPMTRANS_FLAG_JUSTDB, "JustDB",
   N_("Just update database"),
   N_("Do not change any files, just update the database as though you did. Equivalent to --justdb") },
  {0, RPMTRANS_FLAG_TEST, RPMTRANS_FLAG_TEST, "JustTest",
   N_("Just test"),
   N_("Do not actually change anything, just test.  Equivalent to --test") },
  /* architecture checks */
  {0, 0, RPMPROB_FILTER_IGNOREARCH, "NoArchCheck",
   N_("Don't check package architecture"), 
   N_("Do not check the system and rpm architectures. Equivalent to --ignorearch")},
  {0, 0, RPMPROB_FILTER_IGNOREOS, "NoOSCheck",
   N_("Don't check package OS"),
   N_("Do not check the package OS field. Equivalent to --ignoreos")},
};

const int num_prop_flags = sizeof(prop_flags) / sizeof(RpmFlagInfo);

static GtkWidget *rpm_props_box_fill_frame(RpmPropsBox *self, gchar *name,
					   gint start, gint end,
					   GtkTooltips *tooltip);
#ifdef WITH_RPMFIND
static void rpm_props_dist_select_row(GtkCList *clist, gint row, gint col,
				      GdkEvent *event, RpmPropsBox *self);
static void rpm_props_dist_changed(GtkButton *button, RpmPropsBox *self);
#endif

static void rpm_props_box_init(RpmPropsBox *self) {
  static GnomeHelpMenuEntry help_ref = { "gnorpm", "preferences-win.html" };
  GtkWidget *frame, *hbox, *vbox, *box, *table, *label, *wid;
  GtkTooltips *tooltip;
  gchar *text;
  GdkColor colour;
  gint i;
#ifdef WITH_RPMFIND
  GList *tmp, *metadataList;
  gchar *distTitles[4] = { N_("Name"), N_("ID"), N_("Rating"), N_("mirror") };
#endif

  set_icon(GTK_WIDGET(self));
  gtk_window_set_title(GTK_WINDOW(self), _("Preferences"));

  tooltip = gtk_tooltips_new();

  self->interfaceFlags = self->transFlags = self->probFilter = 0;
  self->flags = g_new(GtkWidget *, num_prop_flags);
  /* create the behviour page */
  hbox = gtk_hbox_new(FALSE, GNOME_PAD);
  gtk_container_set_border_width(GTK_CONTAINER(hbox), GNOME_PAD);
  vbox = gtk_vbox_new(FALSE, GNOME_PAD);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 0);
  gtk_widget_show(vbox);

  frame = rpm_props_box_fill_frame(self, _("Install Options"), 0, 2, tooltip);
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  frame = rpm_props_box_fill_frame(self, _("Upgrade Options"), 3, 6, tooltip);
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  vbox = gtk_vbox_new(FALSE, GNOME_PAD);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 0);
  gtk_widget_show(vbox);
  
  frame = rpm_props_box_fill_frame(self, _("Other Options"), 7, 8, tooltip);
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  frame = rpm_props_box_fill_frame(self, _("Database Options"), 9, 10,tooltip);
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  frame = rpm_props_box_fill_frame(self, _("Architecture Options"), 11, 12,
				   tooltip);
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  label = gtk_label_new(_("Behaviour"));
  gtk_widget_show(hbox);
  gtk_widget_show(label);
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(self), hbox, label);

  /* the package listing page */
  vbox = gtk_vbox_new(FALSE, GNOME_PAD);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD);
  frame = gtk_frame_new(_("Package Listing"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, TRUE, 0);
  gtk_widget_show(frame);

  box = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
  gtk_container_set_border_width(GTK_CONTAINER(box), GNOME_PAD);
  gtk_container_add(GTK_CONTAINER(frame), box);
  gtk_widget_show(box);

  self->asList = gtk_radio_button_new_with_label(NULL, _("View as list"));
  gtk_tooltips_set_tip(tooltip, self->asList,
		       _("Display packages in a list format"), NULL);
  gtk_box_pack_start(GTK_BOX(box), self->asList, TRUE, TRUE, 0);
  gtk_widget_show(self->asList);
  gtk_signal_connect_object(GTK_OBJECT(self->asList), "toggled",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  self->asIcons = gtk_radio_button_new_with_label(
		GTK_RADIO_BUTTON(self->asList)->group, _("View as icons"));
  gtk_tooltips_set_tip(tooltip, self->asIcons,
		       _("Display packages as icons in package list"),NULL);
  gtk_box_pack_start(GTK_BOX(box), self->asIcons, TRUE, TRUE, 0);
  gtk_widget_show(self->asIcons);
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(self->asIcons),
		gnome_config_get_bool("/gnorpm/Layout/asIcons=true"));

  gtk_signal_connect_object(GTK_OBJECT(self->asList), "toggled",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_signal_connect_object(GTK_OBJECT(self->asIcons), "toggled",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));

  label = gtk_label_new(_("Package Listing"));
  gtk_widget_show(vbox);
  gtk_widget_show(label);
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(self), vbox, label);

  /* the install window page */
  vbox = gtk_vbox_new(FALSE, GNOME_PAD);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD);

  frame = gtk_frame_new(_("Package Colours"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, TRUE, 0);
  gtk_widget_show(frame);
  table = gtk_table_new(3, 2, FALSE);
  gtk_table_set_row_spacings(GTK_TABLE(table), GNOME_PAD);
  gtk_table_set_col_spacings(GTK_TABLE(table), GNOME_PAD);
  gtk_container_set_border_width(GTK_CONTAINER(table), GNOME_PAD);
  gtk_container_add(GTK_CONTAINER(frame), table);
  gtk_widget_show(table);

  label = gtk_label_new(_("Older Colour:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 0,1,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);

  self->oldColour = gnome_color_picker_new();
  gtk_tooltips_set_tip(tooltip, self->oldColour,
		       _("Set the colour used to highlight packages older than the installed version"), NULL);
  gtk_table_attach(GTK_TABLE(table), self->oldColour, 1,2, 0,1,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);

  text = gnome_config_get_string("/gnorpm/Install/oldPkg=grey50");
  if (gdk_color_parse(text, &colour))
    gnome_color_picker_set_i16(GNOME_COLOR_PICKER(self->oldColour),
			       colour.red, colour.green, colour.blue, 0xffff);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(self->oldColour), "color_set",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_widget_show(self->oldColour);

  label = gtk_label_new(_("Current Colour:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 1,2,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);

  self->currentColour = gnome_color_picker_new();
  gtk_tooltips_set_tip(tooltip, self->currentColour,
		       _("Set the colour used to highlight packages that are same as the installed version"), NULL);
  gtk_table_attach(GTK_TABLE(table), self->currentColour, 1,2, 1,2,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);

  text = gnome_config_get_string("/gnorpm/Install/curPkg=green4");
  if (gdk_color_parse(text, &colour))
    gnome_color_picker_set_i16(GNOME_COLOR_PICKER(self->currentColour),
			       colour.red, colour.green, colour.blue, 0xffff);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(self->currentColour), "color_set",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_widget_show(self->currentColour);

  label = gtk_label_new(_("Newer Colour:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 2,3,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);

  self->newColour = gnome_color_picker_new();
  gtk_tooltips_set_tip(tooltip, self->newColour,
		       _("Set the colour used to highlight packages newer than the installed version"), NULL);
  gtk_table_attach(GTK_TABLE(table), self->newColour, 1,2, 2,3,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);

  text = gnome_config_get_string("/gnorpm/Install/newPkg=blue");
  if (gdk_color_parse(text, &colour))
    gnome_color_picker_set_i16(GNOME_COLOR_PICKER(self->newColour),
			       colour.red, colour.green, colour.blue, 0xffff);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(self->newColour), "color_set",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_widget_show(self->newColour);

  frame = gtk_frame_new(_("Default File Selection Dialog Path"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, TRUE, 0);
  gtk_widget_show(frame);
  box = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
  gtk_container_set_border_width(GTK_CONTAINER(box), GNOME_PAD);
  gtk_container_add(GTK_CONTAINER(frame), box);
  gtk_widget_show(box);

  self->rpmPath = gnome_file_entry_new("RpmPath", "RpmPath");
  gtk_tooltips_set_tip(tooltip, self->rpmPath,
		       _("The default directory where the file selection dialog points at"), NULL);
  gnome_file_entry_set_directory(GNOME_FILE_ENTRY(self->rpmPath), TRUE);
  text=gnome_config_get_string("/gnorpm/Paths/rpmDir=/mnt/cdrom/RedHat/RPMS");
  gtk_entry_set_text(GTK_ENTRY(gnome_file_entry_gtk_entry(
			GNOME_FILE_ENTRY(self->rpmPath))), text);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(self->rpmPath))), "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(box), self->rpmPath, TRUE, TRUE, 0);
  gtk_widget_show(self->rpmPath);

  frame = gtk_frame_new(_("RPM Directories"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  wid = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(wid),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_container_set_border_width(GTK_CONTAINER(wid), GNOME_PAD);
  gtk_container_add(GTK_CONTAINER(frame), wid);
  gtk_widget_show(wid);

  self->rpmDirs = gtk_text_new(NULL, NULL);
  gtk_tooltips_set_tip(tooltip, self->rpmDirs,
		       _("Newline separated list of directories where RPMs may be stored"), NULL);
  gtk_text_set_editable(GTK_TEXT(self->rpmDirs), TRUE);
  text = gnome_config_get_string("/gnorpm/Paths/packageLocations="
				 "/mnt/cdrom/RedHat/RPMS:"
				 "/mnt/cdrom/SRPMS:"
				 "/mnt/cdrom/RPMS:"
				 "/mnt/cdrom/i386:"
				 "/mnt/cdrom/alpha:"
				 "/mnt/cdrom/sparc:"
				 "/mnt/cdrom/noarch:"
				 "/usr/src/redhat/RPMS/i386:"
				 "/usr/src/redhat/RPMS/alpha:"
				 "/usr/src/redhat/RPMS/sparc:"
				 "/usr/src/redhat/RPMS/noarch");
  for (i = 0; text[i] != '\0'; i++)
    if (text[i] == ':') text[i] = '\n';
  i = 0;
  gtk_editable_insert_text(GTK_EDITABLE(self->rpmDirs), text, strlen(text),
			   &i);
  gtk_signal_connect_object(GTK_OBJECT(self->rpmDirs), "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(wid), self->rpmDirs);
  gtk_widget_show(self->rpmDirs);

  label = gtk_label_new(_("Install Window"));
  gtk_widget_show(vbox);
  gtk_widget_show(label);
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(self), vbox, label);

#ifdef WITH_RPMFIND
  /* the network page */
  vbox = gtk_vbox_new(FALSE, GNOME_PAD);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD);
  frame = gtk_frame_new(_("Network Settings"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, TRUE, 0);
  gtk_widget_show(frame);

  table = gtk_table_new(6, 2, FALSE);
  gtk_table_set_row_spacings(GTK_TABLE(table), GNOME_PAD);
  gtk_table_set_col_spacings(GTK_TABLE(table), GNOME_PAD);
  gtk_container_set_border_width(GTK_CONTAINER(table), GNOME_PAD);
  gtk_container_add(GTK_CONTAINER(frame), table);
  gtk_widget_show(table);

  label = gtk_label_new(_("HTTP Proxy:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 0,1,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);
  self->httpProxy = gtk_entry_new();
  gtk_tooltips_set_tip(tooltip, self->httpProxy,
		       _("Format: http://proxyhost:port/"), NULL);
  text = gnome_config_get_string("/gnorpm/rpmfind/http-proxy=");
  gtk_entry_set_text(GTK_ENTRY(self->httpProxy), text);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(self->httpProxy), "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_table_attach(GTK_TABLE(table), self->httpProxy, 1,4, 0,1,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->httpProxy);

  label = gtk_label_new(_("FTP Proxy:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 1,2,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);
  self->ftpProxy = gtk_entry_new();
  gtk_tooltips_set_tip(tooltip, self->ftpProxy,
		       _("Format: http://proxyhost:port/"), NULL);
  text = gnome_config_get_string("/gnorpm/rpmfind/ftp-proxy=");
  gtk_entry_set_text(GTK_ENTRY(self->ftpProxy), text);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(self->ftpProxy), "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_table_attach(GTK_TABLE(table), self->ftpProxy, 1,4, 1,2,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->ftpProxy);

  label = gtk_label_new(_("Proxy User:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 2,3,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);
  self->proxyUser = gtk_entry_new();
  gtk_tooltips_set_tip(tooltip, self->proxyUser,
		       _("The user name to send to the proxy server"), NULL);
  text = gnome_config_get_string("/gnorpm/rpmfind/proxy-user=");
  gtk_entry_set_text(GTK_ENTRY(self->proxyUser), text);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(self->proxyUser), "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_table_attach(GTK_TABLE(table), self->proxyUser, 1,4, 2,3,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->proxyUser);

  label = gtk_label_new(_("Proxy Password:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 3,4,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);
  self->proxyPass = gtk_entry_new();
  gtk_tooltips_set_tip(tooltip, self->proxyPass,
		       _("Your password will be not securely stored!"), NULL);
  gtk_entry_set_visibility(GTK_ENTRY(self->proxyPass), FALSE);
  text = gnome_config_get_string("/gnorpm/rpmfind/proxy-password=");
  /* simple non secure password munging */
  for (i = 0; text[i] != '\0'; i++)
    text[i] ^= 0x42;
  gtk_entry_set_text(GTK_ENTRY(self->proxyPass), text);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(self->proxyPass), "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_table_attach(GTK_TABLE(table), self->proxyPass, 1,4, 3,4,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->proxyPass);

  label = gtk_label_new(_("Cache expire:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 4,5,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);
  hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
  gtk_table_attach(GTK_TABLE(table), hbox, 1,2, 4,5,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(hbox);

  self->expires = gtk_spin_button_new(
	GTK_ADJUSTMENT(gtk_adjustment_new(0.0, 0.0, 365.0, 1.0, 1.0, 1.0)),
	1.0, 0);
  gtk_tooltips_set_tip(tooltip, self->expires,
		       _("The number of days til a downloaded file expires"),
		       NULL);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(self->expires),
			gnome_config_get_int("/gnorpm/rpmfind/expires=14"));
  gtk_signal_connect_object(GTK_OBJECT(self->expires), "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(hbox), self->expires, TRUE, TRUE, 0);
  gtk_widget_show(self->expires);
  label = gtk_label_new(_("days"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 0);
  gtk_widget_show(label);

  label = gtk_label_new(_("Local Hostname:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 5,6,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);
  self->hostname = gtk_entry_new();
  gtk_tooltips_set_tip(tooltip, self->hostname,
		       _("The hostname of the computer -- used to guess distances to mirrors"), NULL);
  text = gnome_config_get_string("/gnorpm/rpmfind/hostname");
  gtk_entry_set_text(GTK_ENTRY(self->hostname), text);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(self->hostname), "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_table_attach(GTK_TABLE(table), self->hostname, 1,2, 5,6,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->hostname);

  label = gtk_label_new(_("Network"));
  gtk_widget_show(vbox);
  gtk_widget_show(label);
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(self), vbox, label);

  /* the rpmfind page */
  vbox = gtk_vbox_new(FALSE, GNOME_PAD);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD);
  frame = gtk_frame_new(_("Rpmfind Options"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);
  table = gtk_table_new(5, 4, FALSE);
  gtk_table_set_row_spacings(GTK_TABLE(table), GNOME_PAD);
  gtk_table_set_col_spacings(GTK_TABLE(table), GNOME_PAD);
  gtk_container_set_border_width(GTK_CONTAINER(table), GNOME_PAD);
  gtk_container_add(GTK_CONTAINER(frame), table);
  gtk_widget_show(table);

  label = gtk_label_new(_("Metadata Server:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 0,1,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);

  self->metadataServer = gtk_combo_new();
  gtk_tooltips_set_tip(tooltip, self->metadataServer,
		       _("The server used to download metadata from"), NULL);
  gtk_combo_set_value_in_list(GTK_COMBO(self->metadataServer), FALSE, FALSE);
  metadataList = metadataGetList();
  if (metadataList)
    gtk_combo_set_popdown_strings(GTK_COMBO(self->metadataServer),
				  metadataList);

  if(access("/etc/redhat-release", F_OK)==0)
    text = gnome_config_get_string("/gnorpm/rpmfind/server=http://www.redhat.com/RDF");
  else
    text = gnome_config_get_string("/gnorpm/rpmfind/server=http://rufus.w3.org/linux/RDF");

  gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(self->metadataServer)->entry), text);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(GTK_COMBO(self->metadataServer)->entry),
			    "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_table_attach(GTK_TABLE(table), self->metadataServer, 1,4, 0,1,
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(self->metadataServer);
  self->metadataServer = GTK_COMBO(self->metadataServer)->entry;

  label = gtk_label_new(_("Download dir:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 1,2,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);

  self->downloadDir = gnome_file_entry_new("Foo", "Download Dir");
  gtk_tooltips_set_tip(tooltip, self->downloadDir,
		       _("The directory to place downloaded RPMs in"), NULL);
  gnome_file_entry_set_directory(GNOME_FILE_ENTRY(self->downloadDir), TRUE);
  text = gnome_config_get_string("/gnorpm/rpmfind/rpm-dir=");
  gnome_file_entry_set_default_path(GNOME_FILE_ENTRY(self->downloadDir),text);
  gtk_entry_set_text(GTK_ENTRY(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(self->downloadDir))), text);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(self->downloadDir))), "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_table_attach(GTK_TABLE(table), self->downloadDir, 1,4, 1,2,
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(self->downloadDir);

  label = gtk_label_new(_("Vendor:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 2,3,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  self->vendor = gtk_entry_new();
  gtk_tooltips_set_tip(tooltip, self->vendor,
			   _("The vendor of your distribution (used to sort package alternates)"), NULL);
  text = gnome_config_get_string("/gnorpm/rpmfind/vendor");
  gtk_entry_set_text(GTK_ENTRY(self->vendor), text);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(self->vendor), "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_table_attach(GTK_TABLE(table), self->vendor, 1,2, 2,3,
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(self->vendor);
  label = gtk_label_new(_("Distrib:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 2,3, 2,3,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  self->distrib = gtk_entry_new();
  gtk_tooltips_set_tip(tooltip, self->distrib,
			   _("The name of your distribution (used to sort package alternates)"), NULL);
  text = gnome_config_get_string("/gnorpm/rpmfind/distribution");
  gtk_entry_set_text(GTK_ENTRY(self->distrib), text);
  g_free(text);
  gtk_signal_connect_object(GTK_OBJECT(self->distrib), "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_table_attach(GTK_TABLE(table), self->distrib, 3,4, 2,3,
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(self->distrib);

  self->wantSource = gtk_check_button_new_with_label(_("Want sources"));
  gtk_tooltips_set_tip(tooltip, self->wantSource,
		       _("Check this if you want to download source rather than binary packages"), NULL);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(self->wantSource),
		gnome_config_get_bool("/gnorpm/rpmfind/wantSources=false"));
  gtk_signal_connect_object(GTK_OBJECT(self->wantSource), "toggled",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_table_attach(GTK_TABLE(table), self->wantSource, 0,2, 3,4,
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(self->wantSource);
  self->wantLatest = gtk_check_button_new_with_label(_("Want latest version"));
  gtk_tooltips_set_tip(tooltip, self->wantLatest,
		       _("Check this if you want the latest version rather than the most compatible version of a package"), NULL);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(self->wantLatest),
	gnome_config_get_bool("/gnorpm/rpmfind/wantLatestVersion=false"));
  gtk_signal_connect_object(GTK_OBJECT(self->wantLatest), "toggled",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_table_attach(GTK_TABLE(table), self->wantLatest, 2,4, 3,4,
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(self->wantLatest);

  label = gtk_label_new(_("No Upgrade List:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.0);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 4,5,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);

  wid = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(wid),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_table_attach(GTK_TABLE(table), wid, 1,4, 4,5,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(wid);

  self->noUpgrades = gtk_text_new(NULL, NULL);
  gtk_tooltips_set_tip(tooltip, self->noUpgrades,
		       _("A newline separated list of packages to never update with rpmfind"), NULL);
  gtk_text_set_editable(GTK_TEXT(self->noUpgrades), TRUE);
  i = 0;
  for (tmp = getNoUpgradeList(); tmp; tmp = tmp->next) {
    if (i != 0) /* not the first element */
      gtk_editable_insert_text(GTK_EDITABLE(self->noUpgrades), "\n", 1, &i);
    gtk_editable_insert_text(GTK_EDITABLE(self->noUpgrades), tmp->data,
			     strlen(tmp->data), &i);
  }
  gtk_signal_connect_object(GTK_OBJECT(self->noUpgrades), "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(wid), self->noUpgrades);
  gtk_widget_show(self->noUpgrades);

  label = gtk_label_new(_("Rpmfind"));
  gtk_widget_show(vbox);
  gtk_widget_show(label);
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(self), vbox, label);

  /* distributions page */
  vbox = gtk_vbox_new(FALSE, GNOME_PAD);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD);
  frame = gtk_frame_new(_("Distribution Settings"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);
  table = gtk_table_new(6, 3, FALSE);
  gtk_table_set_row_spacings(GTK_TABLE(table), GNOME_PAD);
  gtk_table_set_col_spacings(GTK_TABLE(table), GNOME_PAD);
  gtk_container_set_border_width(GTK_CONTAINER(table), GNOME_PAD);
  gtk_container_add(GTK_CONTAINER(frame), table);
  gtk_widget_show(table);

  wid = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(wid),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_table_attach(GTK_TABLE(table), wid, 0,3, 0,1,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(wid);
  distTitles[0] = _(distTitles[0]);
  distTitles[1] = _(distTitles[1]);
  distTitles[2] = _(distTitles[2]);
  distTitles[3] = _(distTitles[3]);
  self->distClist = gtk_clist_new_with_titles(4, distTitles);
  gtk_clist_column_titles_passive(GTK_CLIST(self->distClist));
  gtk_widget_set_usize(self->distClist, -1, 75);
  gtk_clist_set_selection_mode(GTK_CLIST(self->distClist),
			       GTK_SELECTION_BROWSE);
  gtk_clist_set_column_width(GTK_CLIST(self->distClist), 0, 150);
  gtk_clist_set_column_width(GTK_CLIST(self->distClist), 1, 70);
  gtk_clist_set_column_width(GTK_CLIST(self->distClist), 2, 20);
  gtk_clist_set_sort_column(GTK_CLIST(self->distClist), 0);
  gtk_clist_set_sort_type(GTK_CLIST(self->distClist), GTK_SORT_ASCENDING);
  gtk_clist_freeze(GTK_CLIST(self->distClist));
  gtk_object_ref(GTK_OBJECT(gnorpm_app));
  for (tmp = distribGetList(); tmp; tmp = tmp->next) {
	if (!GTK_OBJECT_DESTROYED(GTK_OBJECT(gnorpm_app))) {
	  rpmDistrib *distrib = distribInfoGet(tmp->data);
	  gchar *row[4], buf[21];

	  g_snprintf(buf, 20, "%d", distrib->rating);
	  row[0] = distrib->name;
	  row[1] = distrib->ID;
	  row[2] = buf;
	  row[3] = distrib->myMirror;
	  gtk_clist_append(GTK_CLIST(self->distClist), row);
	}
	else
		return;
  }
  gtk_object_unref(GTK_OBJECT(gnorpm_app));
  gtk_clist_sort(GTK_CLIST(self->distClist));
  gtk_clist_thaw(GTK_CLIST(self->distClist));
  gtk_signal_connect(GTK_OBJECT(self->distClist), "select_row",
		     GTK_SIGNAL_FUNC(rpm_props_dist_select_row), self);
  gtk_container_add(GTK_CONTAINER(wid), self->distClist);
  gtk_widget_show(self->distClist);

  label = gtk_label_new(_("Name:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 1,2,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);
  self->distName = gtk_label_new("");
  gtk_misc_set_alignment(GTK_MISC(self->distName), 0.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), self->distName, 1,3, 1,2,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->distName);

  label = gtk_label_new(_("Origin:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 2,3,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);
  self->distOrigin = gtk_label_new("");
  gtk_misc_set_alignment(GTK_MISC(self->distOrigin), 0.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), self->distOrigin, 1,3, 2,3,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->distOrigin);

  label = gtk_label_new(_("Sources:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 3,4,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);
  self->distSources = gtk_label_new("");
  gtk_misc_set_alignment(GTK_MISC(self->distSources), 0.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), self->distSources, 1,3, 3,4,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->distSources);

  label = gtk_label_new(_("Rating:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 4,5,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);
  self->distRating = gtk_spin_button_new(
	GTK_ADJUSTMENT(gtk_adjustment_new(0.0, -1.0,10000.0, 1.0, 10.0, 10.0)),
	1.0, 0);
  gtk_tooltips_set_tip(tooltip, self->distRating,
		       _("The rating for this distribution (use -1 to ignore this distribution)"), NULL);
  gtk_table_attach(GTK_TABLE(table), self->distRating, 1,2, 4,5,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->distRating);

  label = gtk_label_new(_("Preferred Mirror:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0,1, 5,6,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(label);
  self->distMirror = gtk_combo_new();
  gtk_tooltips_set_tip(tooltip, self->distMirror,
		       _("The mirror to use for this distribution when downloading packages"), NULL);
  gtk_combo_set_value_in_list(GTK_COMBO(self->distMirror), FALSE, FALSE);
  gtk_table_attach(GTK_TABLE(table), self->distMirror, 1,2, 5,6,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->distMirror);

  self->distChange = gtk_button_new_with_label(_("Change"));
  gtk_tooltips_set_tip(tooltip, self->distChange,
		       _("Make the changes to this distribution's settings"), NULL);
  gtk_signal_connect(GTK_OBJECT(self->distChange), "clicked",
		     GTK_SIGNAL_FUNC(rpm_props_dist_changed), self);
  gtk_table_attach(GTK_TABLE(table), self->distChange, 2,3, 5,6,
		   0, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(self->distChange);

  gtk_clist_select_row(GTK_CLIST(self->distClist), 0, 0);

  label = gtk_label_new(_("Distributions"));
  gtk_widget_show(vbox);
  gtk_widget_show(label);
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(self), vbox, label);
#endif

  gtk_signal_connect(GTK_OBJECT(self), "help",
		     GTK_SIGNAL_FUNC(gnome_help_pbox_goto), &help_ref);
  gtk_tooltips_enable(tooltip);
}

GtkWidget *rpm_props_box_new(void) {
  RpmPropsBox *self = gtk_type_new(rpm_props_box_get_type());
  return GTK_WIDGET(self);
}

static void rpm_props_box_flag_toggled(GtkToggleButton *cb, RpmPropsBox *self){
  RpmFlagInfo *info = gtk_object_get_user_data(GTK_OBJECT(cb));
  if (cb->active) {
    self->interfaceFlags |= info->interfaceFlags;
    self->transFlags     |= info->transFlags;
    self->probFilter     |= info->probFilter;
  } else {
    self->interfaceFlags &= ~info->interfaceFlags;
    self->transFlags     &= ~info->transFlags;
    self->probFilter     &= ~info->probFilter;
  }
  gnome_property_box_changed(GNOME_PROPERTY_BOX(self));
}

static GtkWidget *rpm_props_box_fill_frame(RpmPropsBox *self, gchar *name,
					   gint start, gint end,
					   GtkTooltips *tooltip) {
  GtkWidget *frame, *box;
  gint i, state;
  gchar buf[1024];

  frame = gtk_frame_new(name);
  box = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
  gtk_container_set_border_width(GTK_CONTAINER(box), GNOME_PAD);
  gtk_container_add(GTK_CONTAINER(frame), box);
  gtk_widget_show(box);
  for (i = start; i < end+1; i++) {
    self->flags[i] = gtk_check_button_new_with_label(_(prop_flags[i].string));
    gtk_tooltips_set_tip(tooltip, self->flags[i],
			 _(prop_flags[i].help_string) , NULL);
    /* get old value */
    g_snprintf(buf, 1023, "/gnorpm/Flags/%s=false", prop_flags[i].tag);
    state = gnome_config_get_bool(buf);
    if (state) {
      self->interfaceFlags |= prop_flags[i].interfaceFlags;
      self->transFlags     |= prop_flags[i].transFlags;
      self->probFilter     |= prop_flags[i].probFilter;
    }
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(self->flags[i]), state);
    gtk_object_set_user_data(GTK_OBJECT(self->flags[i]),
			     (gpointer)&(prop_flags[i]));
    gtk_signal_connect(GTK_OBJECT(self->flags[i]), "toggled",
		       GTK_SIGNAL_FUNC(rpm_props_box_flag_toggled), self);
    gtk_box_pack_start(GTK_BOX(box), self->flags[i], TRUE, TRUE, 0);
    gtk_widget_show(self->flags[i]);
  }
  return frame;
}

#ifdef WITH_RPMFIND
static void rpm_props_dist_select_row(GtkCList *clist, gint row, gint col,
				      GdkEvent *event, RpmPropsBox *self) {
  gchar *id, *rating, *mirror;
  rpmDistrib *distrib;
  GtkWidget *wid;

  gtk_clist_get_text(clist, row, 1, &id);
  gtk_clist_get_text(clist, row, 2, &rating);
  gtk_clist_get_text(clist, row, 3, &mirror);
  distrib = distribInfoGet(id);

  gtk_label_set(GTK_LABEL(self->distName),
		distrib->name ? distrib->name : "");
  gtk_label_set(GTK_LABEL(self->distOrigin),
		distrib->origin ? distrib->origin : "");
  gtk_label_set(GTK_LABEL(self->distSources),
		distrib->sources ? distrib->sources : "");
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(self->distRating),
			    strtol(rating, NULL, 0));
  if (distrib->mirrors)
    gtk_combo_set_popdown_strings(GTK_COMBO(self->distMirror),
				  distrib->mirrors);
  else
    gtk_list_clear_items(GTK_LIST(GTK_COMBO(self->distMirror)->list), 0, -1);
  if (distrib->origin) {
    wid = gtk_list_item_new_with_label(distrib->origin);
    gtk_container_add(GTK_CONTAINER(GTK_COMBO(self->distMirror)->list),
		      wid);
    gtk_widget_show(wid);
  }
  gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(self->distMirror)->entry),
		     mirror);
}

static void rpm_props_dist_changed(GtkButton *button, RpmPropsBox *self) {
  GtkCList *clist = GTK_CLIST(self->distClist);
  gint row = GPOINTER_TO_INT(clist->selection->data);

  gtk_clist_set_text(clist, row, 2,
	gtk_entry_get_text(GTK_ENTRY(self->distRating)));
  gtk_clist_set_text(clist, row, 3,
	gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(self->distMirror)->entry)));
  gnome_property_box_changed(GNOME_PROPERTY_BOX(self));
}

static gchar* adjust_proxy_text(const gchar *proxyText) {
  gchar *text, *result;

  result = g_strchug(g_strdup(proxyText));
  if (!strlen(result))
    return result;

  if (!strstr(result, "http://")) {
    text = g_strconcat("http://", result, NULL);
    g_free(result);
    result = g_strdup(text);
    g_free(text);
  }
  if (!strstr(result + 7, ":")) {
    /* Left off the proxy port, so insert a reasonable guess (Squid) */
    text = g_strconcat(result, ":3128", NULL);
    g_free(result);
    result = g_strdup(text);
    g_free(text);
  }
  return result;
}
#endif

static void rpm_props_box_destroy(GtkObject *object) {
  RpmPropsBox *self = RPM_PROPS_BOX(object);

  if (self->flags) g_free(self->flags);
  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (* GTK_OBJECT_CLASS(parent_class)->destroy)(object);
}

static void rpm_props_box_apply(GnomePropertyBox *object, gint page_num) {
  RpmPropsBox *self = RPM_PROPS_BOX(object);
  gint i;
  gushort r,g,b,a;
  gchar buf[1024], *text;
  gchar *httpStr = NULL;
  gchar *ftpStr = NULL;
#ifdef WITH_RPMFIND
  gint rows;
  gchar **list;
#endif

  switch (page_num) {
  case RPM_PROPS_FLAGS:
    for (i = 0; i < num_prop_flags; i++) {
      g_snprintf(buf, 1023, "/gnorpm/Flags/%s", prop_flags[i].tag);
      gnome_config_set_bool(buf, GTK_TOGGLE_BUTTON(self->flags[i])->active);
    }
    break;
  case RPM_PROPS_PKGLIST:
    gnome_config_set_bool("/gnorpm/Layout/asIcons",
			  GTK_TOGGLE_BUTTON(self->asIcons)->active);
    gnome_config_set_string("/gnorpm/Paths/rpmDir",
			    gtk_entry_get_text(GTK_ENTRY(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(self->rpmPath)))));
    break;
  case RPM_PROPS_INSTALL:
    gnome_color_picker_get_i16(GNOME_COLOR_PICKER(self->oldColour),
			       &r, &g, &b, &a);
    text = g_strdup_printf("rgb:%04x/%04x/%04x", (gint)r, (gint)g, (gint)b);
    gnome_config_set_string("/gnorpm/Install/oldPkg", text);
    g_free(text);

    gnome_color_picker_get_i16(GNOME_COLOR_PICKER(self->currentColour),
			       &r, &g, &b, &a);
    text = g_strdup_printf("rgb:%04x/%04x/%04x", (gint)r, (gint)g, (gint)b);
    gnome_config_set_string("/gnorpm/Install/curPkg", text);
    g_free(text);

    gnome_color_picker_get_i16(GNOME_COLOR_PICKER(self->newColour),
			       &r, &g, &b, &a);
    text = g_strdup_printf("rgb:%04x/%04x/%04x", (gint)r, (gint)g, (gint)b);
    gnome_config_set_string("/gnorpm/Install/newPkg", text);
    g_free(text);

#ifdef WITH_RPMFIND
    gnome_config_set_string("/gnorpm/rpmfind/rpm-dir",
		gnome_file_entry_get_full_path(
				GNOME_FILE_ENTRY(self->downloadDir),TRUE));
#endif

    text = gtk_editable_get_chars(GTK_EDITABLE(self->rpmDirs), 0, -1);
    for (i = 0; text[i] != '\0'; i++)
      if (text[i] == '\n') text[i] = ':';
    gnome_config_set_string("/gnorpm/Paths/packageLocations", text);
    g_free(text);
    break;
#ifdef WITH_RPMFIND
  case RPM_PROPS_NETWORK:
    text = gtk_editable_get_chars(GTK_EDITABLE(self->httpProxy), 0, -1);
    httpStr = adjust_proxy_text(text);
    g_free(text);
    gnome_config_set_string("/gnorpm/rpmfind/http-proxy", httpStr);
    text = gtk_editable_get_chars(GTK_EDITABLE(self->ftpProxy), 0, -1);
    ftpStr = adjust_proxy_text(text);
    g_free(text);
    gnome_config_set_string("/gnorpm/rpmfind/ftp-proxy", ftpStr);
    gnome_config_set_string("/gnorpm/rpmfind/proxy-user",
			    gtk_entry_get_text(GTK_ENTRY(self->proxyUser)));
    text = g_strdup(gtk_entry_get_text(GTK_ENTRY(self->proxyPass)));
    for (i = 0; text[i] != '\0'; i++)
      text[i] ^= 0x42;
    gnome_config_set_string("/gnorpm/rpmfind/proxy-password", text);
    g_free(text);
    gnome_config_set_int("/gnorpm/rpmfind/expires",
	 gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(self->expires)));
    gnome_config_set_string("/gnorpm/rpmfind/hostname",
			    gtk_entry_get_text(GTK_ENTRY(self->hostname)));
    break;
  case RPM_PROPS_RPMFIND:
    gnome_config_set_string("/gnorpm/rpmfind/server",
			gtk_entry_get_text(GTK_ENTRY(self->metadataServer)));
    gnome_config_set_string("/gnorpm/rpmfind/rpm-dir",
	gnome_file_entry_get_full_path(GNOME_FILE_ENTRY(self->downloadDir),
				       TRUE));
    gnome_config_set_string("/gnorpm/rpmfind/vendor",
			    gtk_entry_get_text(GTK_ENTRY(self->vendor)));
    gnome_config_set_string("/gnorpm/rpmfind/distribution",
			    gtk_entry_get_text(GTK_ENTRY(self->distrib)));
    gnome_config_set_bool("/gnorpm/rpmfind/wantSources",
			  GTK_TOGGLE_BUTTON(self->wantSource)->active);
    gnome_config_set_bool("/gnorpm/rpmfind/wantLatestVersion",
			  GTK_TOGGLE_BUTTON(self->wantLatest)->active);

    if (gnome_config_has_section("/gnorpm/No-Upgrade"))
      gnome_config_clean_section("/gnorpm/No-Upgrade");
    gnome_config_push_prefix("/gnorpm/No-Upgrade/");
    text = gtk_editable_get_chars(GTK_EDITABLE(self->noUpgrades), 0, -1);
    list = g_strsplit(text, "\n", 0);
    g_free(text);
    for (i = 0; list[i] != NULL; i++) {
      g_snprintf(buf, sizeof(buf), "%d", i);
      gnome_config_set_string(buf, list[i]);
    }
    gnome_config_pop_prefix();
    g_strfreev(list);
    updateNoUpgradeList();
    break;
  case RPM_PROPS_DISTRIBS:
    rows = GTK_CLIST(self->distClist)->rows;
    for (i = 0; i < rows; i++) {
      gchar *id, *rating, *mirror;

      gtk_clist_get_text(GTK_CLIST(self->distClist), i, 1, &id);
      gtk_clist_get_text(GTK_CLIST(self->distClist), i, 2, &rating);
      gtk_clist_get_text(GTK_CLIST(self->distClist), i, 3, &mirror);

      distribSetRating(id, strtol(rating, NULL, 0));
      distribSetMirror(id, mirror);
    }
    break;
#endif
  default:
    break;
  }

  gnome_config_sync();
  if (parent_class->apply)
    (* parent_class->apply)(object, page_num);

  g_free(httpStr);
  g_free(ftpStr);
}

void rpm_props_box_get_flags(gint32 *interfaceFlags, gint32 *transFlags,
			     gint32 *probFilter) {
  gint i;
  gchar buf[1024];
  gint32 inter = 0, trans = 0, filter = 0;

  for (i = 0; i < num_prop_flags; i++) {
    g_snprintf(buf, 1023, "/gnorpm/Flags/%s=false", prop_flags[i].tag);
    if (gnome_config_get_bool(buf)) {
      inter  |= prop_flags[i].interfaceFlags;
      trans  |= prop_flags[i].transFlags;
      filter |= prop_flags[i].probFilter;
    }
  }
  if (interfaceFlags) *interfaceFlags = inter;
  if (transFlags)     *transFlags = trans;
  if (probFilter)     *probFilter = filter;
}
