/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __RPM_WEB_FIND_H__
#define __RPM_WEB_FIND_H__

#include <gnome.h>
#include "find/libfind.h"
#include "dbhandle.h"
#include "rpmpackagelist.h"

#define RPM_WEB_FIND(obj) GTK_CHECK_CAST(obj, rpm_web_find_get_type(), RpmWebFind)
#define RPM_WEB_FIND_CLASS(class) GTK_CHECK_CAST_CLASS(class, rpm_web_find_get_type(), RpmWebFindClass)
#define RPM_IS_WEB_FIND(obj) GTK_CHECK_TYPE(obj, rpm_web_find_get_type())

typedef struct _RpmWebFind RpmWebFind;
typedef struct _RpmWebFindClass RpmWebFindClass;

struct _RpmWebFind {
  GtkVBox parent;

  GtkWidget *gentry, *entry, *ctree, *info;
  DBHandle *hdl;
  RpmPackageList *pl;

  GdkPixmap *fld_p, *fldclose_p, *pkg_p;
  GdkBitmap *fld_b, *fldclose_b, *pkg_b;

  gboolean packageSelected;
  rpmPackageAlternate *alt;
  gchar *name;

  /* these are for the idle function that takes care of filling the tree */
  guint fill_tag;
  GList *fill_results, *fill_pos;

  /* this is for the version checker */
  guint vc_tag;
  GtkCTreeNode *vc_node;

  /* callbacks for the created install dialog ... */
  GtkSignalFunc install_cb, upgrade_cb, query_cb, checksig_cb;

  GtkStyle *oldPkg, *newPkg, *curPkg;
};

struct _RpmWebFindClass {
  GtkVBoxClass parent_class;
};

guint rpm_web_find_get_type(void);
GtkWidget *rpm_web_find_new(RpmPackageList *pl);
void rpm_web_find_set_callbacks(RpmWebFind *self,
				GtkSignalFunc install_cb,
				GtkSignalFunc upgrade_cb,
				GtkSignalFunc query_cb,
				GtkSignalFunc checksig_cb);
#endif

