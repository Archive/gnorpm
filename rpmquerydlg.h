/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __RPM_QUERY_DLG_H__
#define __RPM_QUERY_DLG_H__

#include <gnome.h>
#include "rpmquery.h"
#include <rpmlib.h>
#include "dbhandle.h"

#ifdef __cplusplus
extern "C" {
#endif

#define RPM_QUERY_DIALOG(obj) GTK_CHECK_CAST(obj, rpm_query_dialog_get_type(), RpmQueryDialog)
#define RPM_QUERY_DIALOG_CLASS(class) GTK_CHECK_CAST_CLASS(class, rpm_query_dialog_get_type(), RpmQueryDialogClass)
#define RPM_IS_QUERY_DIALOG(obj) GTK_CHECK_TYPE(obj, rpm_query_dialog_get_type())

typedef struct _RpmQueryDialog RpmQueryDialog;
typedef struct _RpmQueryDialogClass RpmQueryDialogClass;

struct _RpmQueryDialog {
  GtkWindow parent;
  GtkWidget *notebook;
  GList *pages;  /* a list of RpmQuery's */
};

struct _RpmQueryDialogClass {
  GtkWindowClass parent_class;
};

typedef void (*GtkRpmCallback)(RpmQuery *info, gpointer data);

guint rpm_query_dialog_get_type(void);
GtkWidget *rpm_query_dialog_new(DBHandle *hdl, GList *indices);
GtkWidget *rpm_query_dialog_new_from_files(DBHandle *hdl, GList *files);
void rpm_query_dialog_add_page(RpmQueryDialog *query, RpmQuery *page);
void rpm_query_dialog_add_pages(RpmQueryDialog *query, DBHandle *hdl,
				GList *indices);
void rpm_query_dialog_add_page_files(RpmQueryDialog *query, DBHandle *hdl,
				     GList *files);
void rpm_query_dialog_set_verify_func(RpmQueryDialog *query, GtkRpmCallback cb,
				      gpointer data);
void rpm_query_dialog_set_uninstall_func(RpmQueryDialog *query,
					 GtkRpmCallback cb, gpointer data);
void rpm_query_dialog_set_install_func(RpmQueryDialog *query,
				       GtkRpmCallback cb, gpointer data);
void rpm_query_dialog_set_upgrade_func(RpmQueryDialog *query,
				       GtkRpmCallback cb, gpointer data);
void rpm_query_dialog_set_checksig_func(RpmQueryDialog *query,
				       GtkRpmCallback cb, gpointer data);
void rpm_query_dialog_set_close_func(RpmQueryDialog *query,
				       GtkRpmCallback cb, gpointer data);

#ifdef __cplusplus
}
#endif

#endif /* __RPM_QUERY_DIALOG_H__ */
