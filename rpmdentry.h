/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __RPM_DENTRY_H__
#define __RPM_DENTRY_H__

#include <gnome.h>
#include <rpmlib.h>
#include "dbhandle.h"

#define RPM_DENTRY_EDIT(obj) GTK_CHECK_CAST(obj, rpm_dentry_edit_get_type(), RpmDentryEdit)
#define RPM_DENTRY_EDIT_CLASS(class) GTK_CHECK_CAST_CLASS(class, rpm_dentry_edit_get_type(), RpmDentryEditClass)
#define RPM_IS_DENTRY_EDIT(obj) GTK_CHECK_TYPE(obj, rpm_dentry_edit_get_type())

typedef struct _RpmDentryEdit RpmDentryEdit;
typedef struct _RpmDentryEditClass RpmDentryEditClass;

struct _RpmDentryEdit {
  GnomeDialog parent;
  DBHandle *hdl;
  GList *indices;
  GtkWidget *menu;
  GtkWidget *notebook;
  GnomeDEntryEdit *dentry;
};

struct _RpmDentryEditClass {
  GnomeDialogClass parent_class;

  void (* save_dentry)(RpmDentryEdit *rde, gchar *name);
};

guint rpm_dentry_edit_get_type(void);
GtkWidget *rpm_dentry_edit_new(DBHandle *hdl, GList *indices);

#endif
