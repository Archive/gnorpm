/* this small example demonstrates how to create a tree of conflicts that
 * result from the removal of a package.  It won't compile with rpm 3.0, but
 * demonstrates the algorithms used. */

#include <rpmlib.h>
#include <glib.h>
#include <fcntl.h>

GHashTable *hash;

void printRemoveDepTree(rpmdb db, char *pkg, int offs, int indent) {
  int i, j, numConflicts;
  rpmDependencies rpmdep;
  struct rpmDependencyConflict *conflicts;

  if (g_hash_table_lookup(hash, pkg))
    /* package already processed ... */
    return;
  g_hash_table_insert(hash, g_strdup(pkg), GINT_TO_POINTER(TRUE));
  
  rpmdep = rpmdepDependencies(db);
  rpmdepRemovePackage(rpmdep, offs);
  if (rpmdepCheck(rpmdep, &conflicts, &numConflicts)) {
    g_warning("rpmdepCheck failed");
    exit(1);
  }
  rpmdepDone(rpmdep);
  for (i = 0; i < numConflicts; i++) {
    gint newOffs;
    dbiIndexSet matches;
    g_print("  ");
    for (j = 0; j < indent; j++) g_print("  ");
    g_print("-- %s requires %s\n", conflicts[i].byName,conflicts[i].needsName);

    if (rpmdbFindByHeader(db, conflicts[i].byHeader, &matches)) {
      g_warning("rpmdbFindByHeader failed");
      exit(1);
    }
    newOffs = matches.recs[0].recOffset;
    dbiFreeIndexRecord(matches);

    printRemoveDepTree(db, conflicts[i].byName, newOffs,indent+1);
  }
  rpmdepFreeConflicts(conflicts, numConflicts);
}

int main(int argc, char *argv[]) {
  rpmdb db;
  char *pkg;
  int offs;
  dbiIndexSet matches;

  if (argc < 1)
    return 1;

  pkg = argv[1];

  if (rpmReadConfigFiles(NULL, NULL, NULL, 0)) {
    g_warning("rpmReadConfigFiles failed");
    return 1;
  }

  if (rpmdbOpen("/", &db, O_RDONLY, 0644)) {
    g_warning("rpmdbOpen failed");
    return 1;
  }

  if (rpmdbFindByLabel(db, pkg, &matches)) {
    g_warning("rpmdbFindByLabel failed");
    return 1;
  }
  if (matches.count < 1) {
    g_warning("no matches for %s found", pkg);
    return 1;
  }
  offs = matches.recs[0].recOffset;
  dbiFreeIndexRecord(matches);

  hash = g_hash_table_new(g_str_hash, g_str_equal);
  g_print("%s\n", pkg);
  printRemoveDepTree(db, pkg, offs, 0);
  return 0;
}
