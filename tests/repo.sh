if [ $# -eq 0 ]; then
  dir=.
else
  dir=$1
fi

dir=`cd $dir; pwd`

for i in `find $dir -name '*.rpm'`; do
  rpm -qp --queryformat "%{name}\t%{version}\t%{release}\t%{group}\t" $i
  echo $i | sed "s,^$dir/,,"
done

