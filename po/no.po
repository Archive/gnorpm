# Norwegian translation of gnorpm (bokm�l dialect).
# Copyright (C) 1999 Free Software Foundation, Inc.
# Kjartan Maraas <kmaraas@online.no>, 1999.
#
msgid ""
msgstr ""
"Project-Id-Version: gnorpm 0.8\n"
"POT-Creation-Date: 2001-04-25 15:44-0400\n"
"PO-Revision-Date: 2000-12-05 15:27+01:00\n"
"Last-Translator: Kjartan Maraas <kmaraas@online.no>\n"
"Language-Team: Norwegian <no@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8-bit\n"

#: checksig.c:43 verify.c:30
msgid "File"
msgstr "Fil"

#: checksig.c:43
msgid "Sig"
msgstr "Sig"

#: checksig.c:43
msgid "Details"
msgstr "Detaljer"

#: checksig.c:54
msgid "Checking Signatures"
msgstr "Sjekker signaturer"

#: checksig.c:100
msgid "error"
msgstr "feil"

#: checksig.c:104
msgid "couldn't open file"
msgstr "kunne ikke �pne filen"

#: checksig.c:111
msgid "could not read lead bytes"
msgstr "kunne ikke lese innledende data"

#: checksig.c:119
msgid "file version doesn't support signatures"
msgstr "filversjonen st�tter ikke signaturer"

#: checksig.c:127
msgid "could not read signature block"
msgstr "kunne ikke lese signaturblokken"

#: checksig.c:135
msgid "no signatures"
msgstr "ingen signaturer"

#: checksig.c:151
msgid "error reading file"
msgstr "feil under lesing av fil"

#: checksig.c:160
msgid "error writing temp file"
msgstr "feil under skriving av midlertidig fil"

#: checksig.c:183 checksig.c:226
msgid "size"
msgstr "st�rrelse"

#: checksig.c:184 checksig.c:210
msgid "size does not match signature"
msgstr "st�rrelsen samsvarer ikke med signaturen"

#: checksig.c:192 checksig.c:233
msgid "md5"
msgstr "md5"

#: checksig.c:193
msgid "md5 sum does not match signature"
msgstr "md5 summen samsvarer ikke med signaturen"

#: checksig.c:204 checksig.c:209 checksig.c:238
msgid "pgp"
msgstr "pgp"

#: checksig.c:217 checksig.c:243
msgid "unknown"
msgstr "ukjent"

#: checksig.c:218
msgid "unknown signature test failed"
msgstr "ukjent signatur test feilet"

#: checksig.c:227 checksig.c:234 checksig.c:239 checksig.c:244
msgid "OK"
msgstr "OK"

#: dbhandle.c:64
msgid "Could not open database"
msgstr "Kunne ikke �pne databasen"

#: gnorpm.c:51
msgid "geometry of main window"
msgstr "hovedvinduets geometri"

#: gnorpm.c:51
msgid "wxh+x+y"
msgstr "wxh+x+y"

#: gnorpm.c:53
msgid "the file system root"
msgstr "filsystemets rot"

#: gnorpm.c:53
msgid "ROOT"
msgstr "ROOT"

#: gnorpm.c:55
msgid "query packages"
msgstr "sp�rring p� pakker"

#: gnorpm.c:57
msgid "packages are in files (rather than db)"
msgstr "pakkene er i filer (ikke i db)"

#: gnorpm.c:59
msgid "install packages"
msgstr "installer pakker"

#: gnorpm.c:61
msgid "upgrade packages"
msgstr "oppgrader pakker"

#: gnorpm.c:63
msgid "verify packages"
msgstr "verifiser pakker"

#: gnorpm.c:65
msgid "check signatures"
msgstr "sjekk signaturer"

#: gnorpm.c:138 mainwin.c:371 mainwin.c:441 mainwin.c:713
#, c-format
msgid "%d packages couldn't be uninstalled"
msgstr "%d pakker kunne ikke avinstalleres"

#: gnorpm.c:155 mainwin.c:485
#, c-format
msgid "Couldn't install %s"
msgstr "Kunne ikke installere %s"

#: gnorpm.c:167 mainwin.c:495
#, c-format
msgid "Couldn't upgrade %s"
msgstr "Kunne ikke oppgradere %s"

#: gnorpm.c:199
msgid "You need to be the superuser to use this option."
msgstr "Du m� v�re superbruker for � bruke dette flagget."

#: gnorpm.c:235 gnorpm.c:264 gnorpm.c:328 gnorpm.c:339
msgid "You must give a package name to query"
msgstr "Du m� oppgi et navn p� pakken det skal utf�res sp�rring p�"

#: gnorpm.c:297
#, c-format
msgid "%s of %d packages failed."
msgstr "%s av %d pakker feilet."

#: gnorpm.c:298 rpminstalldlg.c:359 rpmquery.c:409
msgid "Upgrade"
msgstr "Oppgrader"

#: gnorpm.c:298 mainwin.c:279 mainwin.c:580 mainwin.c:602 rpminstalldlg.c:350
#: rpmquery.c:397 rpmwebfind.c:615
msgid "Install"
msgstr "Installer"

#: install.c:100
msgid " requires "
msgstr " trenger"

#: install.c:102
msgid " conflicts with "
msgstr " er i konflikt med "

#: install.c:176
msgid "Dependency Problems"
msgstr "Avhengighetsproblemer"

#: install.c:185
msgid "The following dependency problems occured:"
msgstr "F�lgende problem med avhengighet oppstod:"

#: install.c:211 install.c:266
msgid ""
"Do you want to ignore these problems?\n"
"(saying yes may make your system unstable)"
msgstr ""
"�nsker du � ignorere disse problemene?\n"
"(svarer du ja kan dette gj�re systemet ustabilt)"

#: install.c:227
msgid "Installation Problems"
msgstr "Installasjonsproblemer"

#: install.c:236
msgid "The following installation problems occured:"
msgstr "F�lgende problemer med installasjon oppstod:"

#: install.c:348
msgid "Dependency check failed."
msgstr "Avhengighets-sjekk feilet."

#: install.c:373
msgid "Upgrading"
msgstr "Oppgraderer"

#: install.c:375
msgid "Installing"
msgstr "Installerer"

#: install.c:421
msgid "Continue Removal"
msgstr "Fortsett fjerning"

#: install.c:430
msgid "Remove the following packages?"
msgstr "Fjern f�lgende pakker?"

#: mainwin.c:134
msgid "Gnome RPM"
msgstr "Gnome RPM"

#: mainwin.c:150
msgid "Packages Selected: 0"
msgstr "Pakker valgt: 0"

#: mainwin.c:192
#, c-format
msgid "Packages selected: %d"
msgstr "Pakker valgt: %d"

#: mainwin.c:227
msgid "_Query..."
msgstr "_Sp�r..."

#: mainwin.c:228 mainwin.c:730 mainwin.c:743
msgid "Get information about the selected packages"
msgstr "Hent informasjon om de valgte pakkene"

#: mainwin.c:231 mainwin.c:732
msgid "_Uninstall"
msgstr "_Avinstaller"

#: mainwin.c:232 mainwin.c:733
msgid "Uninstall the selected packages"
msgstr "Avinstaller valgte pakker"

#: mainwin.c:235 mainwin.c:735
msgid "_Verify"
msgstr "_Verifiser"

#: mainwin.c:236 mainwin.c:736
msgid "Verify the selected packages"
msgstr "Verifiser valgte pakker"

#: mainwin.c:238
msgid "_Create desktop entry..."
msgstr "_Opprett skrivebordsoppf�ring..."

#: mainwin.c:239
msgid "Create desktop entries (for panel) for the selected packages"
msgstr "Opprett skrivebordsoppf�ringer (for panelet) for valgte pakker"

#: mainwin.c:242
msgid "_Quit"
msgstr "Avs_lutt"

#: mainwin.c:242
msgid "Quit GnoRPM"
msgstr "Avslutt GnoRPM"

#: mainwin.c:247
msgid "_Find..."
msgstr "_Finn..."

#: mainwin.c:248
msgid "Search RPM database for packages"
msgstr "S�k etter pakker i RPM-databasen"

#: mainwin.c:251
msgid "Web find..."
msgstr "Finn p� web..."

#: mainwin.c:252 mainwin.c:300
msgid "Find packages on the web with rpmfind"
msgstr "Finn pakker p� web med rpmfind"

#: mainwin.c:256
msgid "_Install..."
msgstr "_Installer..."

#: mainwin.c:257
msgid "Install some new packages"
msgstr "Installer noen nye pakker"

#: mainwin.c:260
msgid "_Preferences..."
msgstr "_Preferanser..."

#: mainwin.c:261
msgid "Alter GnoRPM's preferences"
msgstr "Endre GnoRPM's preferanser"

#: mainwin.c:266
msgid "_About..."
msgstr "_Om..."

#: mainwin.c:266
msgid "Bring up the about box"
msgstr "Hent opp \"om\"-boksen"

#: mainwin.c:273
msgid "_Packages"
msgstr "_Pakker"

#: mainwin.c:274
msgid "_Operations"
msgstr "_Operasjoner"

#: mainwin.c:275
msgid "_Help"
msgstr "_Hjelp"

#: mainwin.c:280
msgid "Select packages for installation"
msgstr "Velg pakker for installasjon"

#: mainwin.c:283
msgid "Unselect"
msgstr "Velg bort"

#: mainwin.c:283
msgid "Unselect all packages"
msgstr "Velg bort alle pakker"

#: mainwin.c:286 rpmfinddlg.c:183 rpmquery.c:453
msgid "Uninstall"
msgstr "Avinstaller"

#: mainwin.c:287
msgid "Uninstall selected packages"
msgstr "Avinstaller valgte pakker"

#: mainwin.c:290 rpmfinddlg.c:174 rpminstalldlg.c:339
msgid "Query"
msgstr "Sp�r"

#: mainwin.c:290
msgid "Query selected packages"
msgstr "Sp�r p� valgte pakker"

#: mainwin.c:293 rpmfinddlg.c:192 rpmquery.c:445
msgid "Verify"
msgstr "Verifiser"

#: mainwin.c:293
msgid "Verify selected packages"
msgstr "Verifiser valgte pakker"

#: mainwin.c:296 rpmfinddlg.c:152
msgid "Find"
msgstr "Finn"

#: mainwin.c:296
msgid "Find packages"
msgstr "Finn pakker"

#: mainwin.c:299
msgid "Web find"
msgstr "Finn p� web"

#: mainwin.c:318 mainwin.c:364 mainwin.c:434
msgid "You need to be the superuser to uninstall packages."
msgstr "Du m� v�re superbruker for � avinstallere pakker."

#: mainwin.c:334 mainwin.c:344 mainwin.c:455
msgid "You need to be the superuser to verify packages."
msgstr "Du m� v�re superbruker for � verifisere pakker."

#: mainwin.c:403
msgid "About GnoRPM"
msgstr "Om GnoRPM"

#: mainwin.c:406
msgid ""
"May be distributed under the terms of the GPL2\n"
"This program uses rpmlib, written by Red Hat"
msgstr ""
"Kan distribueres under betingelsene gitt i GPL2\n"
"Dette programmet bruker rpmlib, skrevet av Red Hat"

#: mainwin.c:526 mainwin.c:544 mainwin.c:562 verify.c:48
msgid "No packages selected"
msgstr "Ingen pakker valgt"

#: mainwin.c:533
#, c-format
msgid "Install of %d packages failed."
msgstr "Installasjon av %d pakker feilet."

#: mainwin.c:553
#, c-format
msgid "Upgrade of %d packages failed."
msgstr "Oppgradering av %d pakker feilet."

#: mainwin.c:573 rpmwebfind.c:609
msgid "You need to be the superuser to install packages."
msgstr "Du m� v�re superbruker for � installere pakker."

#: mainwin.c:627 rpmprops.c:664
msgid "Rpmfind"
msgstr "Rpmfind"

#: mainwin.c:729 mainwin.c:742
msgid "_Query"
msgstr "_Sp�r"

#: rpmdentry.c:87
msgid "Desktop Entry Editor"
msgstr "Redigering av skrivebordsoppf�ring"

#: rpmdentry.c:353
msgid "Save Desktop Entry"
msgstr "Lagre skrivebordsoppf�ring"

#: rpmfinddlg.c:100
msgid "/contain file"
msgstr "/inneholder fil"

#: rpmfinddlg.c:102
msgid "/are in the group"
msgstr "/er i gruppen"

#: rpmfinddlg.c:104
msgid "/provide"
msgstr "/gir"

#: rpmfinddlg.c:106
msgid "/require"
msgstr "/trenger"

#: rpmfinddlg.c:108
msgid "/conflict with"
msgstr "/er i konflikt med"

#: rpmfinddlg.c:110
msgid "/match label"
msgstr "/har lik etikett"

#: rpmfinddlg.c:118
msgid "Find Packages"
msgstr "Finn pakker"

#: rpmfinddlg.c:130
msgid "Find packages that"
msgstr "Finn pakker som"

#: rpminstalldlg.c:182 rpmprops.c:135 rpmwebfind.c:84
msgid "Name"
msgstr "Navn"

#: rpminstalldlg.c:185
msgid "/All packages"
msgstr "/Alle pakker"

#: rpminstalldlg.c:187
msgid "/All but installed packages"
msgstr "/Alle bortsett fra installerte pakker"

#: rpminstalldlg.c:189
msgid "/Only uninstalled packages"
msgstr "/Kun avinstallerte pakker"

#: rpminstalldlg.c:191
msgid "/Only newer packages"
msgstr "/Kun nyere pakker"

#: rpminstalldlg.c:193
msgid "/Uninstalled or newer packages"
msgstr "/Avinstallerte eller nyere pakker"

#: rpminstalldlg.c:222
msgid "Filter:"
msgstr "Filter:"

#: rpminstalldlg.c:274 rpmpackagelist.c:123 rpmprogress.c:85
msgid "Packages"
msgstr "Pakker"

#: rpminstalldlg.c:298 rpminstalldlg.c:873
msgid "Add"
msgstr "Legg til"

#: rpminstalldlg.c:305
msgid ""
"Select\n"
"All"
msgstr ""
"Velg\n"
"Alle"

#: rpminstalldlg.c:312
msgid ""
"Unselect\n"
"All"
msgstr ""
"Velg bort\n"
"Alle"

#: rpminstalldlg.c:319
msgid ""
"Expand\n"
"Tree"
msgstr ""
"Utvid\n"
"Tre"

#: rpminstalldlg.c:326
msgid ""
"Collapse\n"
"Tree"
msgstr ""
"Trekk sammen\n"
"Tre"

#: rpminstalldlg.c:368 rpmquery.c:421
msgid "Check Sig"
msgstr "Sjekk sig"

#: rpminstalldlg.c:658
#, c-format
msgid "Can't open file %s"
msgstr "Kan ikke �pne filen %s"

#: rpminstalldlg.c:667
#, c-format
msgid "%s doesn't appear to be a RPM package"
msgstr "%s ser ikke ut til � v�re en RPM-pakke"

#: find/rdf.c:143 find/rdf.c:149 rpminstalldlg.c:685 rpminstalldlg.c:731
msgid "Unknown"
msgstr "Ukjent"

#: rpminstalldlg.c:865
msgid "Add Packages"
msgstr "Legg til pakker"

#: rpminstalldlg.c:880 rpmquery.c:433
msgid "Close"
msgstr "Lukk"

#: rpminstalldlg.c:1114 rpmquerydlg.c:57
msgid "Package Info"
msgstr "Pakkeinformasjon"

#: rpmpackagelist.c:105 verify.c:30
msgid "Package"
msgstr "Pakke"

#: rpmpackagelist.c:105
msgid "Version"
msgstr "Versjon"

#: rpmpackagelist.c:106
msgid "Release"
msgstr "Utgave"

#: rpmpackagelist.c:106
msgid "Summary"
msgstr "Sammendrag"

#: rpmprogress.c:66
msgid "Done"
msgstr "Ferdig"

#: rpmprogress.c:72
msgid "Remaining"
msgstr "Gjenst�ende"

#: rpmprogress.c:78
msgid "Total"
msgstr "Totalt"

#: rpmprogress.c:110
msgid "Size"
msgstr "St�rrelse"

#: rpmprogress.c:135
msgid "Time"
msgstr "Tid"

#. install options
#: rpmprops.c:76
msgid "No dependency checks"
msgstr "Ingen sjekk for avhengighet"

#: rpmprops.c:77
msgid "Do not check for dependencies on other rpms. Equivalent to --nodeps"
msgstr "Ikke sjekk om pakken er avhengig av andre rpm'er. Tilsvarer --nodeps"

#: rpmprops.c:78
msgid "No reordering"
msgstr "Ingen omstokking"

#: rpmprops.c:79
msgid ""
"Do not reorder package installation to satisfy dependencies. Equivalent to "
"--noorder"
msgstr ""
"Ikke stokk om p� pakkeinstallasjonen for � tilfredstille avhengighet. "
"Tilsvarer \n"
"--noorder"

#: rpmprops.c:81
msgid "Don't run scripts"
msgstr "Ikke kj�r skript"

#: rpmprops.c:82
msgid "Do not run the pre and post install scripts. Equivalent to --noscripts."
msgstr "Ikke kj�r pre- og post-installasjonsskriptene. Tilsvarer --noscripts."

#: rpmprops.c:85
msgid "Allow replacement of packages"
msgstr "Tillat erstatting av pakker"

#: rpmprops.c:86
msgid ""
"Replace packages with a new copy of itself. Equivalent to --replacepkgs."
msgstr "Erstatt pakker med en ny kopi av seg selv. Tilsvarer --replacepkgs."

#: rpmprops.c:88
msgid "Allow replacement of files"
msgstr "Tillat erstatting av filer"

#: rpmprops.c:89
msgid "Replace files owned by another package. Equivalent to --replacefiles."
msgstr "Erstatt filer eid av andre pakker. Tilsvarer --replacefiles."

#: rpmprops.c:91
msgid "Allow upgrade to old version"
msgstr "Tillat oppgradering til en gammel versjon"

#: rpmprops.c:92
msgid ""
"Upgrade packages to an older package if need be. Equivalent to --oldpackage"
msgstr ""
"Oppgrader pakker til en eldre pakke hvis det trengs. Tilsvarer --oldpackage"

#: rpmprops.c:94
msgid "Keep packages made obsolete"
msgstr "Behold utg�tte pakker"

#: rpmprops.c:95
msgid "If a package has been obsoleted, keep it anyway."
msgstr "Hvis en pakke er utg�tt, behold den likevel."

#: rpmprops.c:98
msgid "Don't install documentation"
msgstr "Ikke installer dokumentasjon"

#: rpmprops.c:99
msgid "Do not install any of the doc files. Equivalent to --excludedocs."
msgstr "Ikke installer noen av dokumentasjonsfilene. Tilsvarer --excludedocs."

#: rpmprops.c:101 rpmprops.c:102
msgid "Install all files"
msgstr "Installer alle filer"

#: rpmprops.c:105
msgid "Just update database"
msgstr "Bare oppdater databasen"

#: rpmprops.c:106
msgid ""
"Do not change any files, just update the database as though you did. "
"Equivalent to --justdb"
msgstr ""
"Ikke endre noen filer, bare oppdater databasen som om du gjorede det. \n"
"Tilsvarer --justdb"

#: rpmprops.c:108
msgid "Just test"
msgstr "Bare test"

#: rpmprops.c:109
msgid "Do not actually change anything, just test.  Equivalent to --test"
msgstr "Ikke utf�r endringer, bare test. Tilsvarer --test"

#: rpmprops.c:112
msgid "Don't check package architecture"
msgstr "Ikke sjekk pakke-arkitektur"

#: rpmprops.c:113
msgid ""
"Do not check the system and rpm architectures. Equivalent to --ignorearch"
msgstr "Ikke sjekk system- og rpm-arkitekturen. Tilsvarer --ignorearch"

#: rpmprops.c:115
msgid "Don't check package OS"
msgstr "Ikke sjekk pakkens OS"

#: rpmprops.c:116
msgid "Do not check the package OS field. Equivalent to --ignoreos"
msgstr "Ikke sjekk pakkens OS-felt. Tilsvarer --ignoreos"

#: rpmprops.c:135
msgid "ID"
msgstr "ID"

#: rpmprops.c:135
msgid "Rating"
msgstr "Plassering"

#: rpmprops.c:135
msgid "mirror"
msgstr "speil"

#: rpmprops.c:141
msgid "Preferences"
msgstr "Preferanser"

#: rpmprops.c:154
msgid "Install Options"
msgstr "Alternativer for installasjon"

#: rpmprops.c:158
msgid "Upgrade Options"
msgstr "Alternativer for oppgradering"

#: rpmprops.c:166
msgid "Other Options"
msgstr "Andre alternativer"

#: rpmprops.c:170
msgid "Database Options"
msgstr "Alternativer for database"

#: rpmprops.c:174
msgid "Architecture Options"
msgstr "Alternativer for arkitektur"

#: rpmprops.c:179
msgid "Behaviour"
msgstr "Oppf�rsel"

#: rpmprops.c:187 rpmprops.c:220
msgid "Package Listing"
msgstr "Pakkeliste"

#: rpmprops.c:196
msgid "View as list"
msgstr "Vis som liste"

#: rpmprops.c:198
msgid "Display packages in a list format"
msgstr "Vis pakker i listeformat"

#: rpmprops.c:205
msgid "View as icons"
msgstr "Vis som ikoner"

#: rpmprops.c:207
msgid "Display packages as icons in package list"
msgstr "Vis pakker som ikoner i en pakkeliste"

#: rpmprops.c:229
msgid "Package Colours"
msgstr "Farger for pakker"

#: rpmprops.c:239
msgid "Older Colour:"
msgstr "Farge for eldre:"

#: rpmprops.c:247
msgid ""
"Set the colour used to highlight packages older than the installed version"
msgstr ""
"Sett fargen som brukes for � markere pakker som er eldre enn den installerte "
"versjonen"

#: rpmprops.c:261
msgid "Current Colour:"
msgstr "N�v�rende farge:"

#: rpmprops.c:269
msgid ""
"Set the colour used to highlight packages that are same as the installed "
"version"
msgstr ""
"Sett fargen som skal brukes til � markere pakker som er lik som den "
"installerte versjonen"

#: rpmprops.c:283
msgid "Newer Colour:"
msgstr "Nyere farge:"

#: rpmprops.c:291
msgid ""
"Set the colour used to highlight packages newer than the installed version"
msgstr ""
"Sett fargen som skal brukes for � markere pakker som er nyere enn den "
"installerte versjonen"

#: rpmprops.c:305
msgid "Default File Selection Dialog Path"
msgstr "Forvalgt sti for dialogboksen for valg av filer"

#: rpmprops.c:315
msgid "The default directory where the file selection dialog points at"
msgstr "Forvalgt katalog som dialogboksen for valg av filer peker til"

#: rpmprops.c:327
msgid "RPM Directories"
msgstr "RPM-kataloger"

#: rpmprops.c:340
msgid "Newline separated list of directories where RPMs may be stored"
msgstr "Linjeskift-separert liste over kataloger hvor RPMer kan v�re lagret"

#: rpmprops.c:365
msgid "Install Window"
msgstr "Installasjonsvindu"

#: rpmprops.c:374
msgid "Network Settings"
msgstr "Innstillinger for nettverk"

#: rpmprops.c:385
msgid "HTTP Proxy:"
msgstr "HTTP-stedfortreder:"

#: rpmprops.c:392 rpmprops.c:410
msgid "Format: http://proxyhost:port/"
msgstr "Format: http://stedfortredervert:port/"

#: rpmprops.c:403
msgid "FTP Proxy:"
msgstr "FTP-stedfortreder:"

#: rpmprops.c:421
msgid "Proxy User:"
msgstr "Proxy-bruker:"

#: rpmprops.c:428
msgid "The user name to send to the proxy server"
msgstr "Brukernavnet som skal sendes til proxy-tjeneren"

#: rpmprops.c:439
msgid "Proxy Password:"
msgstr "Proxy-passord:"

#: rpmprops.c:446
msgid "Your password will be not securely stored!"
msgstr "Ditt passord vil ikke lagres p� en sikker m�te!"

#: rpmprops.c:461
msgid "Cache expire:"
msgstr "Cache utg�r:"

#: rpmprops.c:475
msgid "The number of days til a downloaded file expires"
msgstr "Antall dager f�r nedhentet fil utg�r"

#: rpmprops.c:484
msgid "days"
msgstr "dager"

#: rpmprops.c:488
msgid "Local Hostname:"
msgstr "Lokalt vertsnavn:"

#: rpmprops.c:495
msgid "The hostname of the computer -- used to guess distances to mirrors"
msgstr "Vertsnavnet til datamaskinen -- brukes til � gjette avstand til speil"

#: rpmprops.c:506
msgid "Network"
msgstr "Nettverk"

#: rpmprops.c:514
msgid "Rpmfind Options"
msgstr "Rpmfind alternativer"

#: rpmprops.c:524
msgid "Metadata Server:"
msgstr "Tjener for metadata:"

#: rpmprops.c:532
msgid "The server used to download metadata from"
msgstr "Tjeneren det hentes metadata fra"

#: rpmprops.c:555
msgid "Download dir:"
msgstr "Nedlastingskatalog:"

#: rpmprops.c:563
msgid "The directory to place downloaded RPMs in"
msgstr "Katalogen hvor nedlastede RPMer plasseres"

#: rpmprops.c:576 rpmquery.c:201
msgid "Vendor:"
msgstr "Forhandler:"

#: rpmprops.c:583
msgid "The vendor of your distribution (used to sort package alternates)"
msgstr "Produsenten av din distribusjon (brukes til � sortere pakker)"

#: rpmprops.c:593
msgid "Distrib:"
msgstr "Distribut�r:"

#: rpmprops.c:600
msgid "The name of your distribution (used to sort package alternates)"
msgstr "Navnet p� din distribusjon (brukes til � sortere pakker)"

#: rpmprops.c:611
msgid "Want sources"
msgstr "Vil ha kildekode"

#: rpmprops.c:613
msgid "Check this if you want to download source rather than binary packages"
msgstr "Kryss av her hvis du vil hente ned kildekode i stedet for bin�rpakker"

#: rpmprops.c:622
msgid "Want latest version"
msgstr "Vil ha siste versjon"

#: rpmprops.c:624
msgid ""
"Check this if you want the latest version rather than the most compatible "
"version of a package"
msgstr ""
"Kryss av her hvis du vil ha siste versjon heller enn den mest kompatible \n"
"versjonen av en pakke"

#: rpmprops.c:634
msgid "No Upgrade List:"
msgstr "Ingen oppgraderingsliste"

#: rpmprops.c:649
msgid "A newline separated list of packages to never update with rpmfind"
msgstr ""
"En linjeskift-separert liste over pakker som aldri skal oppdateres med "
"rpmfind"

#: rpmprops.c:672
msgid "Distribution Settings"
msgstr "Innstillinger for distribusjon"

#: rpmprops.c:727
msgid "Name:"
msgstr "Navn:"

#: rpmprops.c:738
msgid "Origin:"
msgstr "Opprinnelse:"

#: rpmprops.c:749
msgid "Sources:"
msgstr "Kildekode:"

#: rpmprops.c:760
msgid "Rating:"
msgstr "Plassering:"

#: rpmprops.c:769
msgid "The rating for this distribution (use -1 to ignore this distribution)"
msgstr ""
"Plassering for denne distribusjonen (bruk -1 for � ignorere denne "
"distribusjonen)"

#: rpmprops.c:774
msgid "Preferred Mirror:"
msgstr "Foretrukket speil:"

#: rpmprops.c:781
msgid "The mirror to use for this distribution when downloading packages"
msgstr ""
"Speilet som skal brukes for denne distribusjonen under nedhenting av pakker"

#: rpmprops.c:787
msgid "Change"
msgstr "Endre"

#: rpmprops.c:789
msgid "Make the changes to this distribution's settings"
msgstr "Utf�r endringene av denne distribusjonens instillinger"

#: rpmprops.c:798
msgid "Distributions"
msgstr "Distribusjoner"

#: rpmquery.c:128
msgid "D"
msgstr "D"

#: rpmquery.c:128
msgid "C"
msgstr "C"

#: rpmquery.c:128
msgid "S"
msgstr "S"

#: rpmquery.c:128
msgid "Path"
msgstr "Sti"

#: rpmquery.c:140
msgid "Size:"
msgstr "St�rrelse:"

#: rpmquery.c:152
msgid "Install Date:"
msgstr "Installasjonsdato:"

#: rpmquery.c:157
msgid "not installed"
msgstr "ikke installert"

#: rpmquery.c:164
msgid "Build Host:"
msgstr "Bygget p� vert:"

#: rpmquery.c:176
msgid "Build Date:"
msgstr "Bygget dato:"

#: rpmquery.c:188
msgid "Distribution:"
msgstr "Distribusjon:"

#: rpmquery.c:214
msgid "Group:"
msgstr "Gruppe:"

#: rpmquery.c:227
msgid "Packager:"
msgstr "Pakket av:"

#: rpmquery.c:244
msgid "URL:"
msgstr "URL:"

#: rpmquery.c:466 rpmquery.c:468 rpmquery.c:469 rpmquery.c:470 rpmquery.c:608
#: rpmquery.c:616 rpmquery.c:618 rpmquery.c:621 rpmquery.c:653 rpmquery.c:735
#: rpmquery.c:739 rpmquery.c:740 rpmquery.c:741 rpmquery.c:770 rpmquery.c:775
#: rpmquery.c:842
msgid "<none>"
msgstr "<ingen>"

#: rpmwebfind.c:84
msgid "Distribution"
msgstr "Distribusjon"

#: rpmwebfind.c:102
msgid "Search"
msgstr "S�k"

#: rpmwebfind.c:143
msgid "Download"
msgstr "Nedlasting"

#: rpmwebfind.c:280 rpmwebfind.c:311
#, c-format
msgid "have %s-%s"
msgstr "har %s-%s"

#: rpmwebfind.c:526
msgid "No packages to download"
msgstr "Ingen pakker for nedlasting"

#: rpmwebfind.c:536
msgid "Download files?"
msgstr "Last ned filer?"

#: rpmwebfind.c:545
msgid "Download these packages?"
msgstr "Last ned disse pakkene"

#: rpmwebfind.c:580
msgid "Couldn't download files"
msgstr "kunne ikke laste ned filer"

#: verify.c:30
msgid "Problem"
msgstr "Problem"

#: verify.c:42
msgid "Verifying Packages"
msgstr "Verifiserer pakker"

#: verify.c:127
msgid "missing"
msgstr "mangler"

#: verify.c:129
msgid ", md5"
msgstr ", md5"

#: verify.c:130
msgid ", file size"
msgstr ", filst�rrelse"

#: verify.c:132
msgid ", symbolic link problem"
msgstr ", problem med symbolsk lenke"

#: verify.c:133
msgid ", user"
msgstr ", bruker"

#: verify.c:134
msgid ", group"
msgstr ", gruppe"

#: verify.c:136
msgid ", modification time"
msgstr ", tid for endring"

#: verify.c:137
msgid ", file mode"
msgstr ", filmodus"

#: verify.c:139
msgid ", device file type"
msgstr ", type enhetsfil"

#: verify.c:147 verify.c:153
msgid "*script*"
msgstr "*skript*"

#: verify.c:154
msgid "script problem"
msgstr "problem med skript"

#: verify.c:163
#, c-format
msgid "%d problems found."
msgstr "%d problemer funnet."

#: verify.c:166
msgid "No problems found."
msgstr "Ingen problemer funnet."

#: find/deps.c:96
#, c-format
msgid "Error looking up rpm database for %s"
msgstr "Feil under oppslag etter %s i rpm-databasen"

#: find/distrib.c:72 find/distrib.c:174
#, c-format
msgid "XML file '%s' doesn't seem to be an RDF schema"
msgstr "XML-filen '%s' ser ikke ut til � v�re et RDF-schema"

#: find/distrib.c:79 find/distrib.c:181
#, c-format
msgid "RDF schema '%s' doesn't contain rpm namespace"
msgstr "RDF-schema '%s' inneholder ikke et rpm-navneomr�de"

#: find/distrib.c:86 find/distrib.c:188
#, c-format
msgid "no descriptions in RDF schema '%s'"
msgstr "ingen beskrivelse i RDF-schema '%s'"

#: find/distrib.c:94
#, c-format
msgid "RDF schema '%s' not valid: no ID"
msgstr "RDF-skjema '%s' er ugyldig: ingen ID"

#: find/distrib.c:100
#, c-format
msgid "RDF schema '%s' doesn't match ID %s"
msgstr "RDF-schema '%s' stemmer ikke overens med ID %s"

#: find/distrib.c:139
#, c-format
msgid "%s: malformed Mirrors bag"
msgstr "%s: feilutformet speilsekk"

#: find/distrib.c:142
#, c-format
msgid "%s doesn't export any mirrors"
msgstr "%s eksporterer ikke noen speil"

#: find/distrib.c:200
msgid "description without href"
msgstr "beskrivelse uten href"

#: find/distrib.c:206
#, c-format
msgid "no ID for distrib href=%s"
msgstr "ingen ID for distrib href=%s"

#: find/distrib.c:327
msgid "Couldn't grab metadata server list"
msgstr "Kunne ikke liste over metadata-tjenere"

#: find/distrib.c:335
msgid "metadata.rdf is not an RDF schema"
msgstr "metadata.rdf er ikke et RDF-skjema"

#: find/distrib.c:341
msgid "Metadata.rdf is not an RPM specific RDF schema"
msgstr "Metadata.rdf er ikke et RPM spesifikt eller RDF-skjema"

#: find/distrib.c:347
msgid "Metadata.rdf seems to be empty"
msgstr "Metadata.rdf ser ut til � v�re tom"

#: find/distrib.c:355
msgid "Metadata.rdf schema is invalid: description without href"
msgstr "Metadata.rdf shcema er ugyldig: beskrivelse uten href"

#: find/distrib.c:361
msgid "Metadata.rdf schema is invalid: no URI"
msgstr "Metadata.rdf schema er ugyldig: ingen URI"

#: find/ftp.c:548
msgid "Bad FTP server response"
msgstr "Ugyldig svar fra FTP-tjener"

#: find/ftp.c:551
msgid "FTP IO error"
msgstr "FTP IO-feil"

#: find/ftp.c:554
msgid "FTP server timeout"
msgstr "Tidsavbrudd fra FTP-tjener"

#: find/ftp.c:557
msgid "Unable to lookup FTP server host address"
msgstr "Kunne ikke sl� opp vertsadressen til FTP-tjeneren"

#: find/ftp.c:560
msgid "Unable to lookup FTP server host name"
msgstr "Kunne ikke sl� opp FTP-tjenerens vertsnavn"

#: find/ftp.c:563
msgid "Failed to connect to FTP server"
msgstr "Kunne ikke koble til FTP-tjeneren"

#: find/ftp.c:566
msgid "Failed to establish data connection to FTP server"
msgstr "Kunne ikke etablere dataforbindelse til FTP-tjeneren"

#: find/ftp.c:569
msgid "IO error to local file"
msgstr "IO-feil til lokal fil"

#: find/ftp.c:572
msgid "Error setting remote server to passive mode"
msgstr "Feil ved fors�k p� � sette ekstern tjener til passiv modus"

#: find/ftp.c:575
msgid "File not found on server"
msgstr "Filen ble ikke funnet p� tjeneren"

#: find/ftp.c:579
msgid "FTP Unknown or unexpected error"
msgstr "FTP - Ukjent eller uventet feil"

#: find/guess.c:386
msgid "No packages installed!"
msgstr "Ingen pakker installert!"

#: find/rdf.c:61 find/search.c:395
#, c-format
msgid "%s is not an RDF schema"
msgstr "%s er ikke et RDF-skjema"

#: find/rdf.c:67 find/search.c:401
#, c-format
msgid "%s is not an RPM specific RDF schema"
msgstr "%s er ikke et RPM spesifikt eller RDF-skjema"

#: find/rdf.c:73 find/search.c:407
#, c-format
msgid "%s RDF schema seems empty"
msgstr "RDF-skjemaet %s ser tomt ut"

#: find/rdf.c:84
msgid "rpmOpenRdf: out of memory !"
msgstr "rpmOpenRdf: tom for minne!"

#: find/rdf.c:99 find/search.c:430
#, c-format
msgid "%s RDF schema invalid: no Name"
msgstr "RDF-schema %s ugyldig: uten navn"

#: find/rdf.c:108 find/search.c:441
#, c-format
msgid "%s RDF schema invalid: no Version"
msgstr "RDF-schema %s er ugyldig: uten versjon"

#: find/rdf.c:117 find/search.c:452
#, c-format
msgid "%s RDF schema invalid: no Release"
msgstr "RDF-schema %s er ugyldig: ingen Release"

#: find/rdf.c:131
msgid "None"
msgstr "Ingen"

#: find/rdf.c:161
msgid "unknown/group"
msgstr "ukjent/gruppe"

#: find/rdf.c:167
msgid "no summary"
msgstr "uten sammendrag"

#: find/rdf.c:173
msgid "No description !"
msgstr "Ingen beskrivelse !"

#: find/rdf.c:211
msgid "unknown.host"
msgstr "ukjent.vert"

#: find/rdf.c:242 find/rdf.c:271
#, c-format
msgid "%s: malformed Resource element"
msgstr "%s: feilutformet ressurselement"

#: find/rdf.c:244 find/rdf.c:273
#, c-format
msgid "%s: malformed Provides bag"
msgstr "%s: feilutformet tilbyder-bagg"

#: find/rdf.c:249
#, c-format
msgid "%s: doesn't export any resource"
msgstr "%s: eksporterer ikke noen ressurser"

#: find/rdf.c:314
#, c-format
msgid "Unknown distribution %s"
msgstr "Ukjent distribusjon %s"

#: find/search.c:151
#, c-format
msgid "Expected %s as toplevel element -- got %s"
msgstr "Forventet %s som toppniv�-element -- fikk %s"

#: find/search.c:157 find/search.c:170
#, c-format
msgid "Was not expecting a %s element"
msgstr "Forventet ikke et %s-element"

#: find/search.c:174
#, c-format
msgid "was not expecting a %s element"
msgstr "forventet ikke et %s-element"

#: find/search.c:181
msgid "This state should not have been reached"
msgstr "Denne tilstanden skulle ikke v�rt n�dd"

#: find/search.c:384
#, c-format
msgid "Couldn't get info for resource %s"
msgstr "Kunne ikke hente informasjon for ressurs %s"

#: find/search.c:417
#, c-format
msgid "%s RDF schema invalid: description without href"
msgstr "RDF-skjema %s er ugyldig: beskrivelse uten href"

#: find/trans.c:102
msgid "Setting of proxy failed"
msgstr "Setting av proxy feilet"

#: find/trans.c:115
msgid "Setting proxy user/password pair failed"
msgstr "Setting av bruker/passord informasjon feilet"

#: find/trans.c:124
msgid "Setting of uri failed"
msgstr "Setting av uri feilet"

#: find/trans.c:130
msgid "Preparing request failed"
msgstr "Forberedelse av foresp�rsel feilet"

#: find/trans.c:134
msgid "Couldn't set async mode"
msgstr "Kunne ikke initiere asynkron modus"

#: find/trans.c:144
msgid "User aborted transfer"
msgstr "Overf�ring avbrutt av bruker"

#: find/trans.c:150
msgid "Processing request failed"
msgstr "Behandling av foresp�rsel feilet"

#: find/trans.c:154
#, c-format
msgid "HTTP error: %d %s"
msgstr "HTTP-feil: %d %s"

#: find/trans.c:177
#, c-format
msgid "Couldn't connect to FTP server: %s"
msgstr "Kunne ikke koble til FTP-tjener: %s"

#: find/trans.c:184
#, c-format
msgid "FTP transfer failed: %s"
msgstr "FTP-overf�ring feilet: %s"

#: find/trans.c:198
#, c-format
msgid "Downloading %s"
msgstr "Laster ned %s"

#: find/trans.c:202
msgid "Already busy doing a transfer"
msgstr "Allerede i gang med en overf�ring"

#: find/trans.c:211
msgid "Invalid URL"
msgstr "Ugyldig URL"

#: find/trans.c:235
#, c-format
msgid "Unsupported URL format '%s'"
msgstr "Ust�ttet URL-format '%s'"

#: find/trans.c:298 find/trans.c:331
msgid "Couldn't get data"
msgstr "Fikk ikke data"

#: find/trans.c:306
#, c-format
msgid "Couldn't rename workfile to '%s'"
msgstr "kunne ikke endre navn p� arbeidsfil til '%s'"

#: find/trans.c:346
#, c-format
msgid "couldn't create directory %s"
msgstr "kunne ikke opprette katalog %s"

#: find/trans.c:354
#, c-format
msgid "Couldn't get data at %s"
msgstr "Fikk ikke data fra %s"

#: find/trans.c:368 find/trans.c:373 find/trans.c:377
#, c-format
msgid "Couldn't create directory %s"
msgstr "Kunne ikke opprette katalog %s"

#~ msgid "error: cannot open file %s"
#~ msgstr "feil: kan ikke �pne filen %s"

#~ msgid "/are triggered by"
#~ msgstr "/utl�ses av"
