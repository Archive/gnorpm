#!/bin/sh

xgettext --default-domain=gnorpm --directory=.. \
  --add-comments --keyword=_ --keyword=N_ \
  --files-from=./POTFILES.in \
&& test ! -f gnorpm.po \
   || ( rm -f ./gnorpm.pot \
    && mv gnorpm.po ./gnorpm.pot )
