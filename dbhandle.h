/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __DBHANDLE_H__
#define __DBHANDLE_H__

#include <glib.h>
#include <rpmlib.h>

typedef struct _DBHandle DBHandle;

struct _DBHandle {
  gchar *root;
  rpmdb db;
  gint upcount;
};

DBHandle *db_handle_new(gchar *root);
void db_handle_free(DBHandle *hdl);

void db_handle_db_down(DBHandle *hdl);
void db_handle_db_up(DBHandle *hdl);

#endif
