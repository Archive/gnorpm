/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <gnome.h>
#include <stdio.h>
#include <rpmlib.h>

#include "dbhandle.h"
#include "install.h"
#include "checksig.h"
#include "verify.h"
#include "misc.h"

#include "rpmquerydlg.h"
#include "rpmprops.h"

#ifdef WITH_RPMFIND
#include "find/libfind.h"
#endif


/* from mainwin.c */
GtkWidget *create_main(char *app_id, DBHandle *dbhdl);

static void parseAnArg(poptContext con, enum poptCallbackReason reason,
		       const struct poptOption *opt,
		       const char *arg, void *data);

static const struct poptOption gnorpm_options[] = {
  { NULL, '\0', POPT_ARG_CALLBACK|POPT_CBFLAG_POST,
    &parseAnArg, 0, NULL },
  { "geometry", '\0', POPT_ARG_STRING, NULL, -1,
    N_("geometry of main window"), N_("wxh+x+y") },
  { "root", '\0', POPT_ARG_STRING, NULL, -2,
    N_("the file system root"), N_("ROOT") },
  { "query", 'q', POPT_ARG_NONE, NULL, -3,
    N_("query packages"), NULL },
  { "packages", 'p', POPT_ARG_NONE, NULL, -4,
    N_("packages are in files (rather than db)"), NULL },
  { "install", 'i', POPT_ARG_NONE, NULL, -5,
    N_("install packages"), NULL },
  { "upgrade", 'U', POPT_ARG_NONE, NULL, -6,
    N_("upgrade packages"), NULL },
  { "verify", 'y', POPT_ARG_NONE, NULL, -7,
    N_("verify packages"), NULL },
  { "checksig", 'K', POPT_ARG_NONE, NULL, -8,
    N_("check signatures"), NULL },
  POPT_AUTOHELP
  { NULL, '\0', 0, NULL, 0 }
};

static gint main_x = -1, main_y = -1, main_w = -1, main_h = -1;
static gchar *root = "";
static GList *args = NULL;
static enum { MODE_UNSET, MODE_QUERY, MODE_INSTALL, MODE_UPGRADE,
	      MODE_VERIFY, MODE_CHECKSIG }
  majorMode = MODE_UNSET;
static int isFiles = FALSE;
static GdkWindow *main_window = NULL;
static int needroot = 0;

static void parseAnArg(poptContext con, enum poptCallbackReason reason,
		       const struct poptOption *opt,
		       const char *arg, void *data) {
  gchar *str;

  switch (reason) {
  case POPT_CALLBACK_REASON_OPTION:
    switch (opt->val) {
    case -1: /* --geometry */
      gnome_parse_geometry(arg, &main_x, &main_y, &main_w, &main_h); break;
    case -2: /* --root */
      root = g_strdup(arg); break;
    case -3: /* --query */
      majorMode = MODE_QUERY; break;
    case -4: /* -p */
      isFiles = TRUE; break;
    case -5: /* --install */
      majorMode = MODE_INSTALL; needroot=1; break;
    case -6: /* --upgrade */
      majorMode = MODE_UPGRADE; needroot=1; break;
    case -7: /* --verify */
      majorMode = MODE_VERIFY; needroot=1; break;
    case -8: /* --checksig */
      majorMode = MODE_CHECKSIG;
    }
    break;
  case POPT_CALLBACK_REASON_PRE:
    break;
  case POPT_CALLBACK_REASON_POST:
    while ((str = poptGetArg(con)) != NULL)
      args = g_list_append(args, str);
    break;
  }
}

static gint save_state(GnomeClient *client, gint phase,
		       GnomeRestartStyle save_style, gint shutdown,
		       GnomeInteractStyle interact_style, gint fast,
		       gchar *prog_name);

#ifdef WITH_RPMFIND
static void update_func(void) {
  while (gtk_events_pending())
    gtk_main_iteration();
}
#endif

/* these functions handle the buttons in the "gnorpm -q" window */
static void query_verify_func(RpmQuery *info, gpointer data) {
  verify_one(info->hdl, info->index, 0);
}
static void query_uninstall_func(RpmQuery *info, gpointer data) {
  int failed;
  char buf[512];
  guint interfaceFlags = 0, transFlags = 0, probFilter = 0;
  rpm_props_box_get_flags(&interfaceFlags, &transFlags, &probFilter);

  failed = uninstall_one(info->hdl->root, info->index, transFlags,
                         probFilter, interfaceFlags);
  if (failed) {
    g_snprintf(buf, 511, _("%d packages couldn't be uninstalled"), failed);
    message_box(buf);
    return;
  }
  if (!(transFlags & RPMTRANS_FLAG_TEST)) {
    GtkWidget *parent = GTK_WIDGET(info)->parent;

    gtk_container_remove(GTK_CONTAINER(parent), GTK_WIDGET(info));
  }
}
static void query_install_func(RpmQuery *info, gpointer data) {
  guint interfaceFlags = 0, transFlags = 0, probFilter = 0;
  rpm_props_box_get_flags(&interfaceFlags, &transFlags, &probFilter);

  if (install_one(info->hdl->root, info->fname, NULL, transFlags, probFilter,
                  interfaceFlags, NULL, NULL)) {
    gchar buf[512];
    g_snprintf(buf, 511, _("Couldn't install %s"), info->fname);
    message_box(buf);
  }
}
static void query_upgrade_func(RpmQuery *info, gpointer data) {
  guint interfaceFlags = 0, transFlags = 0, probFilter = 0;
  rpm_props_box_get_flags(&interfaceFlags, &transFlags, &probFilter);

  if (install_one(info->hdl->root, info->fname, NULL,
                  transFlags, probFilter, interfaceFlags | INTER_UPGRADE,
                  NULL, NULL)) {
    gchar buf[512];
    g_snprintf(buf, 511, _("Couldn't upgrade %s"), info->fname);
    message_box(buf);
  }
}
static void query_checksig_func(RpmQuery *info, gpointer data) {
  check_one_sig(info->fname, 0, NULL);
}


int main(int argc, char *argv[]) {
  GtkWidget *app;
  GnomeClient *client;
  DBHandle *dbhdl = NULL;
  GList *pkgs = NULL, *tmp;
  gint res;
  gchar buf[512];
  int interfaceFlags = 0;

  /* i18n stuff */
  bindtextdomain(PACKAGE, GNOMELOCALEDIR);
  textdomain(PACKAGE);
  gnome_init_with_popt_table(PACKAGE, VERSION, argc, argv,
			     gnorpm_options, 0, NULL);

  client = gnome_master_client();
  gtk_signal_connect(GTK_OBJECT(client), "save_yourself",
		     GTK_SIGNAL_FUNC(save_state), argv[0]);
  gtk_signal_connect(GTK_OBJECT(client), "die",
		     GTK_SIGNAL_FUNC(gtk_main_quit), NULL);

  if(geteuid() && needroot)
  {
    GtkWidget *d=gnome_error_dialog(_("You need to be the superuser to use this option."));
    gnome_dialog_run_and_close(GNOME_DIALOG(d));
    gtk_exit(0);
  }

  rpmReadConfigFiles(NULL, NULL);
  setupErrorHandler();

  if (majorMode == MODE_UNSET || majorMode == MODE_QUERY ||
      majorMode == MODE_VERIFY) {
    dbhdl = db_handle_new(root);
    if (dbhdl == NULL)
      return 1;

#ifdef WITH_RPMFIND
    libfind_init(dbhdl->db);
    libfind_set_idle_func(update_func);
#endif
    db_handle_db_down(dbhdl);
  }

  switch (majorMode) {
  case MODE_UNSET:
    app = create_main("gnorpm", dbhdl);
    if (main_w != -1 || main_h != -1)
      gtk_widget_set_usize(app, main_w, main_h);
    if (main_x != -1 || main_y != -1)
      gtk_widget_set_uposition(app, main_x, main_y);
    gtk_widget_show(app);
    main_window = app->window;
    gtk_main();
    break;
  case MODE_QUERY:
    db_handle_db_up(dbhdl);
    if (isFiles) {
      if (!args) {
	message_box(_("You must give a package name to query"));
	return 1;
      }
      app = rpm_query_dialog_new_from_files(dbhdl, args);
    } else {
      pkgs = NULL;
      for (tmp = args; tmp != NULL; tmp = tmp->next) {
#ifdef	HAVE_RPM_4_0
	rpmdbMatchIterator mi;

	mi = rpmdbInitIterator(dbhdl->db, RPMDBI_LABEL, tmp->data, 0);
	while (rpmdbNextIterator(mi) != NULL)
	  pkgs = g_list_append(pkgs,
				 GINT_TO_POINTER(rpmdbGetIteratorOffset(mi)));
	rpmdbFreeIterator(mi);
#else	/* HAVE_RPM_4_0 */
 	dbiIndexSet matches;
	gint i;

	if (rpmdbFindByLabel(dbhdl->db, tmp->data, &matches))
	  continue;
	for (i = 0; i < matches.count; i++)
	  if (matches.recs[i].recOffset != 0)
	    pkgs = g_list_append(pkgs,
				 GINT_TO_POINTER(matches.recs[i].recOffset));
	dbiFreeIndexRecord(matches);
#endif	/* HAVE_RPM_4_0 */
      }
      if (!pkgs) {
	message_box(_("You must give a package name to query"));
	return 1;
      }
      app = rpm_query_dialog_new(dbhdl, pkgs);
      db_handle_db_down(dbhdl);
    }
    if (main_w != -1 || main_h != -1)
      gtk_widget_set_usize(app, main_w, main_h);
    if (main_x != -1 || main_y != -1)
      gtk_widget_set_uposition(app, main_x, main_y);
    gtk_signal_connect(GTK_OBJECT(app), "destroy",
		       GTK_SIGNAL_FUNC(gtk_main_quit), NULL);
    rpm_query_dialog_set_uninstall_func(RPM_QUERY_DIALOG(app),
					query_uninstall_func, NULL);
    rpm_query_dialog_set_verify_func(RPM_QUERY_DIALOG(app),
				     query_verify_func, NULL);
    rpm_query_dialog_set_install_func(RPM_QUERY_DIALOG(app),
				      query_install_func, NULL);
    rpm_query_dialog_set_upgrade_func(RPM_QUERY_DIALOG(app),
				      query_upgrade_func, NULL);
    rpm_query_dialog_set_checksig_func(RPM_QUERY_DIALOG(app),
				      query_checksig_func, NULL);
    rpm_query_dialog_set_close_func(RPM_QUERY_DIALOG(app),
				    (GtkRpmCallback)gtk_main_quit, NULL);
    gtk_widget_show(app);
    main_window = app->window;
    gtk_main();
    break;
  case MODE_UPGRADE:
    interfaceFlags = interfaceFlags | INTER_UPGRADE;
  case MODE_INSTALL:
    res = do_install(root, args, NULL, 0, 0, interfaceFlags, NULL, NULL);
    if (res) {
      g_snprintf(buf, 511, _("%s of %d packages failed."),
		 (majorMode==MODE_UPGRADE)?_("Upgrade"):_("Install"), res);
      message_box(buf);
    }
    break;
  case MODE_VERIFY:
    db_handle_db_up(dbhdl);
    pkgs = NULL;
    for (tmp = args; tmp != NULL; tmp = tmp->next) {
#ifdef	HAVE_RPM_4_0
	rpmdbMatchIterator mi;

	mi = rpmdbInitIterator(dbhdl->db, RPMDBI_LABEL, tmp->data, 0);
	while (rpmdbNextIterator(mi) != NULL)
	  pkgs = g_list_append(pkgs,
				 GINT_TO_POINTER(rpmdbGetIteratorOffset(mi)));
	rpmdbFreeIterator(mi);
#else	/* HAVE_RPM_4_0 */
      dbiIndexSet matches;
      gint i;

      if (rpmdbFindByLabel(dbhdl->db, tmp->data, &matches))
	continue;
      for (i = 0; i < matches.count; i++)
	if (matches.recs[i].recOffset != 0)
	  pkgs = g_list_append(pkgs,
			       GINT_TO_POINTER(matches.recs[i].recOffset));
      dbiFreeIndexRecord(matches);
#endif	/* HAVE_RPM_4_0 */
    }
    if (!pkgs) {
      message_box(_("You must give a package name to query"));
      return 1;
    }
    db_handle_db_down(dbhdl);
    app = verify_packages(dbhdl, pkgs, 0);
    gtk_signal_connect(GTK_OBJECT(app), "destroy",
		       GTK_SIGNAL_FUNC(gtk_main_quit), NULL);
    gtk_main();
    break;
  case MODE_CHECKSIG:
    if (!args) {
      message_box(_("You must give a package name to query"));
      return 1;
    }
    check_sigs(args, 0, &app);
    gtk_signal_connect(GTK_OBJECT(app), "destroy",
		       GTK_SIGNAL_FUNC(gtk_main_quit), NULL);
    gtk_main();
    break;
  }

  if (dbhdl != NULL)
    db_handle_free(dbhdl);
  return 0;
}

static gint save_state(GnomeClient *client, gint phase,
		       GnomeRestartStyle save_style, gint shutdown,
		       GnomeInteractStyle interact_style, gint fast,
		       gchar *prog_name) {
  gint i = 0;
  gchar *geom = NULL;
  if (main_window)
    geom = gnome_geometry_string(main_window);
  if (majorMode == MODE_UNSET) {
    gchar *argv[6];

    argv[i++] = prog_name;
    if (root && *root) {
      argv[i++] = "--root";
      argv[i++] = root;
    }
    argv[i++] = "--geometry";
    argv[i++] = geom;
    gnome_client_set_restart_command(client, i, argv);
    gnome_client_set_clone_command(client, 0, NULL);
    if (geom) g_free(geom);
    return TRUE;
  } else if (majorMode == MODE_QUERY) {
    gchar **argv = g_new(gchar *, 7 + g_list_length(args));
    GList *tmp;
    argv[i++] = prog_name;
    if (root && *root) {
      argv[i++] = "--root";
      argv[i++] = root;
    }
    argv[i++] = "--geometry";
    argv[i++] = geom;
    if (isFiles) {
      argv[i++] = "--query";
      argv[i++] = "-p";
    } else
      argv[i++] = "--query";
    for (tmp = args; tmp != NULL; tmp = tmp->next)
      argv[i++] = tmp->data;
    gnome_client_set_restart_command(client, i, argv);
    gnome_client_set_clone_command(client, 0, NULL);
    g_free(argv);
    if (geom) g_free(geom);
    return TRUE;
  }
  if (geom) g_free(geom);
  return FALSE;
}
