/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gnome.h>
#include <rpmlib.h>
#include <rpmio.h>

#define SIG_SKIP_SIZE 1
#define SIG_SKIP_MD5  2
#define SIG_SKIP_PGP  4
/* for future use */
#define SIG_SKIP_GPG  8

int check_sigs(GList *files, int flags, GtkWidget **win);
int check_one_sig(char *file, int flags, GtkWidget **win);
