/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __RPM_PROGRESS_H__
#define __RPM_PROGRESS_H__

#include <gtk/gtk.h>
#include "dbhandle.h"

#include <time.h>

#define RPM_PROGRESS(obj) GTK_CHECK_CAST(obj, rpm_progress_get_type(), RpmProgress)
#define RPM_PROGRESS_CLASS(class) GTK_CHECK_CAST_CLASS(class, rpm_progress_get_type(), RpmProgressClass)
#define RPM_IS_PROGRESS(obj) GTK_CHECK_TYPE(obj, rpm_progress_get_type())

typedef struct _RpmProgress RpmProgress;
typedef struct _RpmProgressClass RpmProgressClass;

struct _RpmProgress {
  GtkTable parent;

  GtkWidget *pkg_name, *pkg_progress;

  GtkWidget *pkgsDone, *pkgsLeft, *pkgsTot;
  GtkWidget *sizeDone, *sizeLeft, *sizeTot;
  GtkWidget *timeDone, *timeLeft, *timeTot;
  GtkWidget *tot_progress;

  guint numInstalled, numPackages, amountInstalled;
  guint curAmount, curPkgSize, totSize;
  time_t starttime;
};

struct _RpmProgressClass {
  GtkTableClass parent_class;
};

guint rpm_progress_get_type(void);
GtkWidget *rpm_progress_new(guint numPackages, guint totSize);

/* these functions call gtk_main_iteration
 * to make sure changes are displayed */
void rpm_progress_next(RpmProgress *prog, gchar *pkg_name, guint pkg_size);
void rpm_progress_update(RpmProgress *prog, gdouble pcnt);

#endif

