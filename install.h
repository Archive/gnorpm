/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef INSTALL_H
#define INSTALL_H

/* interface flags */
#define INTER_NODEPS  (1 << 0)
#define INTER_NOORDER (1 << 1)  /* only for install */
#define INTER_UPGRADE (1 << 2)

typedef void (*rpmInstallCb)(gchar *name, gchar *group, void *user_data);

int do_install(char *root, GList *pkgs, char *location,
	       int transFlags, int probFilter, int interfaceFlags,
	       rpmInstallCb callback, void *user_data);
int install_one(char *root, char *file, char *location,
		int transFlags, int probFilter, int interfaceFlags,
		rpmInstallCb callback, void *user_data);
gint do_uninstall(char *root, GList *indices, int transFlags,
		  int probFilter, int interfaceFlags);
int uninstall_one(char *root, guint index, int transFlags,
		  int probFilter, int interfaceFlags);

#endif
