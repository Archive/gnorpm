/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __RPM_INSTALL_DLG_H__
#define __RPM_INSTALL_DLG_H__

#include <gnome.h>
#include <rpmlib.h>
#include <rpmio.h>
#include <dirent.h>
#include "dbhandle.h"

#define RPM_INSTALL_DIALOG(obj) GTK_CHECK_CAST(obj, rpm_install_dialog_get_type(), RpmInstallDialog)
#define RPM_Install_DIALOG_CLASS(class) GTK_CHECK_CAST_CLASS(class, rpm_install_dialog_get_type(), RpmInstallDialogClass)
#define RPM_IS_INSTALL_DIALOG(obj) GTK_CHECK_TYPE(obj, rpm_install_dialog_get_type())

typedef enum {
  INSTALL_FILTER_ALL,
  INSTALL_FILTER_NOT_INSTALLED, /* don't show if pkg-ver-rel matches */
  INSTALL_FILTER_UNINSTALLED,   /* don't show if pkg matches */
  INSTALL_FILTER_NEWER,         /* show if pkg matches db, and ver > dbver */
  INSTALL_FILTER_UNINSTALLED_OR_NEWER
} RpmInstallFilter;

typedef struct _RpmInstallDialog RpmInstallDialog;
typedef struct _RpmInstallDialogClass RpmInstallDialogClass;

struct _RpmInstallDialog {
  GtkDialog parent;
  GtkWidget *tree;  /* the list of packages to install */
  GtkWidget *filesel;  /* the file selection dialog */
  DBHandle *hdl;  /* this is needed so that we can shut down the database
		   * before doing an install */
  GList *pkgs;

  RpmInstallFilter filter;
  GtkWidget *filter_menu;

  GtkStyle *oldPkg, *newPkg, *curPkg;  /* styles for new and old packages */

  GtkCTreeNode *base;
  GHashTable *ht;  /* hold path-to-widget mappings */
  GdkPixmap *open_p, *closed_p, *pkg_p, *checkon_p, *checkoff_p;
  GdkBitmap *open_b, *closed_b, *pkg_b;

  GtkWidget *info_frame; /* the frame giving information about the package */

  /* These options are for the directory scanning code */
  guint scan_tag, pos;
  gchar **dirs;
  DIR *scan_dir;
  GList *seen_inodes;	/* Avoids trips around loops in dir structure. */

  guint refilter_tag;
  GList *filter_pos;
};

struct _RpmInstallDialogClass {
  GtkDialogClass parent_class;

  /* the argument is a GList of strings representing the file names */
  void (*    query)(RpmInstallDialog *dlg, GList *files);
  void (*  install)(RpmInstallDialog *dlg, GList *files);
  void (*  upgrade)(RpmInstallDialog *dlg, GList *files);
  void (* checksig)(RpmInstallDialog *dlg, GList *files);
};

guint rpm_install_dialog_get_type(void);
GtkWidget *rpm_install_dialog_new(DBHandle *hdl);
/* add packages from the default locations (specified in config file) */
void rpm_install_dialog_add_default_packages(RpmInstallDialog *self);
GtkCTreeNode *rpm_install_dialog_add_file(RpmInstallDialog *self, gchar *file,
					  gboolean selected);

/* this function will add all the rpms from a colon separated list of
 * directories to the dialog */
void rpm_install_dialog_add_dirs(RpmInstallDialog *self, gchar *dirstring);

#endif

