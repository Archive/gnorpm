/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __GTK_PATH_TREE_H__
#define __GTK_PATH_TREE_H__

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#endif

#define GTK_PATH_TREE(obj) GTK_CHECK_CAST(obj, gtk_path_tree_get_type(), GtkPathTree)
#define GTK_PATH_TREE_CLASS(class) GTK_CHECK_CAST_CLASS(class, gtk_path_tree_get_type(), GtkPathTreeClass)
#define GTK_IS_PATH_TREE(obj) GTK_CHECK_TYPE(obj, gtk_path_tree_get_type())

typedef struct _GtkPathTree GtkPathTree;
typedef struct _GtkPathTreeClass GtkPathTreeClass;

struct _GtkPathTree {
  GtkCTree parent;
  GtkCTreeNode *base;
  GHashTable *ht;  /* hold path-to-widget mappings */
  GdkPixmap *open_p, *closed_p;
  GdkBitmap *open_b, *closed_b;
};

struct _GtkPathTreeClass {
  GtkCTreeClass parent_class;

  void (* path_selected) (GtkPathTree *tree, gchar *path);
};

guint gtk_path_tree_get_type(void);
GtkWidget *gtk_path_tree_new(gchar *base_item);

GtkCTreeNode *gtk_path_tree_add_path(GtkPathTree *tree, gchar *path);

void gtk_path_tree_freeze(GtkPathTree *tree);
void gtk_path_tree_thaw(GtkPathTree *tree);

#ifdef __cplusplus
}
#endif

#endif /* __GTK_PATH_TREE_H__ */

