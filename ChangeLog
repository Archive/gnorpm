2002-04-27 Malcolm Tredinnick <malcolm@commsecure.com.au>

	* Makefile.am: The 'make snap' target now makes the generated
	  gnorpm.spec file valid for the title of the snapshot tarball's
	  name. This is mostly for my own benefit.

	* gnorpm.spec.in: Remove some of the cruft. Make it a bit more
	  flexible. Don't strip the binary (helps with bug reports).

	* verify.c: Removed a compiler warning by adding a cast.

	* glibwww/.cvsignore: Forgot to nuke this earlier. Now hopefully
	  this directory will die, die, die.

	* rpminstalldlg.c: Attempting to add a package to the install
	  window that is already there will now do nothing (fixes bug
	  #57444).

	* rpminstalldlg.c: Prior to installing or upgading packages,
	  weed out any duplicate names. The package with the most recent
	  version and release strings is the one selected for
	  installation (fixes part of #58175). Not completely polished yet,
	  since it just spews out a list of duplicate packages to stdout,
	  rather than a popup box, but it works.

	* rpmprogress.c (rpm_progress_new): If we are about to create a
	  progress bar that is zero units long, make it one unit long
	  instead (this allows zero byte rpms to be installed without
	  GTK warnings appearing).

2002-04-26 Malcolm Tredinnick <malcolm@commsecure.com.au>

	* rpminstalldlg.c, rpminstalldlg.h: When recursing through
	  directories to add files to the Install window, detect loops
	  by keeping a list of inodes visited (fixes bug #58459).

2002-04-25 Malcolm Tredinnick <malcolm@commsecure.com.au>

	* mainwin.c: Multiple calls to rpm_preferences_cb now check to
	  see if we are already trying to build the preferences box and
	  just, for example, waiting for a download to complete. Fixes
	  bugs #76968, #79182, #69011 and #58285.

	* glibwww/README, tests/glibwww.c, tests/g_LoadToFile.c:
	  Removed (cleaning up the source tree).

2002-01-15 Hasbullah Bin Pit <sebol@ikhlas.com>	

	* configure.ini: Added Malay (ms)to ALL_LINGUAS.
	* po/ms.po: Added Malay Malay Translation.

2001-11-30  Malcolm Tredinnick <malcolm@commsecure.com.au>
	
	* configure.in, acconfig.h: Test for libdb-3.2 before all
	  others. Check for rpm-4.0.3. (RH)

	* gnorpm.spec.in: Replace 'Copyright' with 'License'.

	* install.c: Make it work with both rpm-4.0.1 and rpm-4.0.3
	  (the type of rpmDependenyConflict changed).

	* checksig.h, rpminstalldlg.h, rpmquery.h, install.c: #include
	  <rpmio.h> in a few places where it is now needed (avoids some
	  deprecated stuff). (RH)

	* The changes marked (RH) came from the patches in the latest Red Hat
	  (7.2) SRPM for gnorpm.

2001-11-22  Wang Jian <lark@linux.net.cn>

	* configure.in(ALL_LINGUAS): zh_CN.GB2312 -> zh_CN.

2001-10-29  Carlos Perell� Mar�n <carlos@gnome-db.org>

	* configure.in (ALL_LINGUAS): Added pt

2001-09-26  Malcolm Tredinnick <malcolm@commsecure.com.au>

	* rpmpackagelist.h: Fixed a typo in a function prototype.

2001-09-24  Malcolm Tredinnick <malcolm@commsecure.com.au>

	* configure.in: Moved the check for libghttp inside the
	  --enable-rpmfind option setup, since otherwise you could fail
	  the config for a library that wasn't needed.

	* rpmprops.c: wrapped a few variables inside "#ifdef RPMFIND"
	  constructs so that everything builds without warnings.

2001-09-23  Malcolm Tredinnick <malcolm@commsecure.com.au>

	* gnorpm.c, install.c, find/guess.c find/tester.c: Do some
	  casting and add an #ifdef so that it builds without warnings
	  when rpmfind is enabled (still some issues in the
	  --disable-rpmfind case).

2001-09-07  Malcolm Tredinnick <malcolm@commsecure.com.au>

	* rpminstalldlg.c: Cleaned up an error message.
	* mainwin.c: After uninstalling every package in a query window,
	  destroy the query window. Fixes bug #58452.
	* Started a general pass to move reasonably common code into one
	  function and make the other cases wrappers that call the one
	  true function.

2001-09-05  Malcolm Tredinnick <malcolm@commsecure.com.au>

	* mainwin.c, rpmpackagelist.*: Double clicking on a package icon
	  now displays the query window (same as context menu -> Query).
	  Fixes bug #58173.

2001-09-04  Abel Cheung  <maddog@linux.org.hk>

	* gnorpm.desktop: Rename zh_TW.Big5 to zh_TW .

2001-08-31  Abel Cheung  <maddog@linux.org.hk>

	* configure.in (ALL_LINGUAS): zh_TW.Big5 -> zh_TW

2001-08-09  Zbigniew Chyla  <cyba@gnome.pl>

	* configure.in (ALL_LINGUAS): Added pl.

	* gnorpm.desktop: Small correction in Polish translation.

2001-08-08  Abel Cheung  <maddog@linux.or.hk>

	* gnorpm.desktop: Added traditional Chinese strings.

2000-05-24  Malcolm Tredinnick <malcolm@commsecure.com.au>

	* Removed 'rpm' target from Makefile. It hasn't worked for a
	  while and was becoming less and less portable. All the
	  remaining Makefile targets now work.

2000-05-05  Malcolm Tredinnick <malcolm@commsecure.com.au>

	* Made gnorpm.spec.in.
	* rpmquerydlg.c: removed use of GtkNotebookPage, since this will
	  no longer be available in GTK+ 2.0 and we can manage without
	  it even in gtk-1.2.

2000-04-19  Malcolm Tredinnick <malcolm@commsecure.com.au>

	* Removed boom.xpm -- it hasn't been required for ages.
	* Changed gnorpm.spec to insert the correct version number in the
	  "Source:" tag. This still isn't a perfect solution (need to
	  update spec version all the time), but at least "make dist && rpm
	  -ta..." now works.
	* Fixed gnorpm.sgml so that the 'Introduction' help page is now
	  installed and called correctly.
	* Removed uninstall option from the context menu when the user does
	  not have root privileges.
	* Choosing 'uninstall' in the 'find package' or 'query' dialog
	  boxes now tells a non-root user that they need to be superuser,
	  rather than trying to uninstall and serving up a possibly cryptic
	  rpmlib error message.
	* Choosing 'find', selecting a package and then 'query' now works
	  properly (it was previously broken in the rpm 4.0 case).

2001-03-06  Alan Cox <alan@redhat.com>

	* Submitted a set of patches from Jeff Johnson
	  - Fix up rpm 4.0.1 support
	  - Remove rpm 2.5 support
	  - Move towards the goal of a constant rpm api

2001-02-14  Akira TAGOH <tagoh@gnome.gr.jp>

	* gnorpm.desktop: Added Japanese entry.

2000-12-13  Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz>
	
	* Added sk to ALL_LINGUAS
	* gnorpm.desktop: Added Slovak strings.

2000-12-12  Alan Cox

	* Sort icons into alphabetical order

2000-12-11  Alan Cox <alan@redhat.com>

	* Fixed the nasty flashing dialog bug by forcing the progress bar
	  onto the app status
	* Made ftp handling a bit nicer

2000-11-15  Malcolm Tredinnick <malcolm@commsecure.com.au>
	
	* Release lock on rpm database after cancelling an uninstall.
	* Added pt_BR to ALL_LINGUAS
	
2000-10-30  Malcolm Tredinnick <malcolm@commsecure.com.au>

	* No longer crashes horribly when trying to install rpms that are
	  badly formed.
	* Changed error message when trying to do things requiring superuser
	  privileges to indicate that that option (not the whole app)
	  requires special privileges.

2000-10-22  Kjartan Maraas  <kmaraas@gnome.org>

	* gnorpm.keys: Update some.
	
2000-10-17  Alan Cox <alan@redhat.com>

	* rpmcompat.h was wrong for RPMv4 and core dumped all the time
	* OLDFILENAME doesnt work for RPMv4 very well. Use the newer stuff
	  if we can 
	* rpmdentry in new mode checked the wrong number of files

2000-10-11  Malcolm Tredinnick <malcolm@commsecure.com.au>

	* Fixed problems where the http or ftp proxies were entered
	  without a leading http:// or trailing port number (a
	  reasonable guess is made for the latter).
	* Verified that most of the other open proxy bugs have been
	  fixed to some degree of satisfaction and can now be closed.

2000-10-10  Christophe Merlet  <christophe@merlet.net>

	* find/rdf.c, find/search.c: Corrected some strings.

2000-10-03  Christophe Merlet  <christophe@merlet.net>

	* gnorpm.desktop, gnorpm.keys: Added French strings.

2000-09-24  Alan Cox <alan@redhat.com>

	* Fixed reported untranslated strings (RH #17664)
	* Fixed crash on 'create desktop entry' (RH #17666)
	* Much improved the binary selection heuristic in this function
	* Remove verify from context menu for non root (RH #16763)

2000-09-22  Christopher R. Gabriel  <cgabriel@softwarelibero.org>

	* configure.in (ALL_LINGUAS): added 'it'

2000-09-18  Alan Cox	<alan@redhat.com>

	* Added the RPM 4 patch from Jeff Johnson <jbj@redhat.com>

2000-09-13 Christophe Merlet <c.merlet@agglo-pau.fr>

	* An updated french translation for the upcoming gnorpm-0.95
	* An updated POTFILES.in for gnorpm-0.95

2000-09-12  Alan Cox    <alan@redhat.com>

	* Many bugs fixed over the past few weeks including a security hole
	* Malcolm also fixed some of these.

2000-09-06  Dan Damian  <dand@dnttm.ro>

	* configure.in (ALL_LINGUAS): Added Romanian (ro).

2000-05-06  Jesus Bravo Alvarez  <jba@pobox.com>

	* configure.in (ALL_LINGUAS): Added Galician (gl).
	* gnorpm.desktop: Added Galician entry.

2000-05-03  Pablo Saratxaga <pablo@mandrakesoft.com>

	* configure.in (ALL_LINGUAS): added Lithuanian

2000-04-18  Pablo Saratxaga <pablo@mandrakesoft.com>

	* configure.in (ALL_LINGUAS): added Catalan

2000-03-13 Alastair McKinstry <mckinstry@computer.org>

	* configure.in (ALL_LINGUAS): Added Irish translation.

	* gnorpm.desktop: ditto.

2000-01-31  Yuan-Chung Cheng <platin@linux.org.tw>

        * configure.in: Added "zh_CN.GB2312" to ALL_LINGUAS.

1999-12-18  James Henstridge  <james@daa.com.au>

	* gnorpm.c: use newer libfind API.

	* rpmprops.c: use newer libfind API.  Each of the callbacks holds a
	reference to the RpmPropsBox object.  This way, they won't be left
	with a dangling reference if the window is closed before they
	complete.  The callbacks check to see if the object has been destroyed
	before doing anything though, so they don't cause a segfault.

1999-12-16  James Henstridge  <james@daa.com.au>

	* find/deps.[ch]: converted dependencies code over to using glibwww.

1999-11-23  James Henstridge  <james@daa.com.au>

	* find/libfind.c: deleted libfind_set_idle_func.  Renamed libfind_init
	to match new naming scheme.

	* find/libfind.h: changed function names.

	* find/search.c (rpmfind_alternate_get_info): use new rdf.h APIs

	* find/search.h (rpmfind_alternate_get_info): use new rdf.h APIs

	* find/rdf.[ch]: changed to new function names

	* find/search.c: same here.

	* find/distrib.c: fixed up references to guess functions.

	* find/guess.[ch]: changed exported function names, reindented.

1999-11-22  James Henstridge  <james@daa.com.au>

	* find/search.c: updated to use glibwww.
	(rpmfind_apropos_search): use xmlSAXUserParseFile so we don't need to
	use symbols that are internal to libxml.

	* find/util.[ch]: new files containing some useful routines used
	in other parts of libfind.

1999-11-21  James Henstridge  <james@daa.com.au>

	* find/ftp.c: removed file -- it is obsoleted by the glibwww stuff.
	
	* find/distrib.c: updated to use glibwww.  This breaks other stuff,
	but is the first step to changing over.

1999-11-18  James Henstridge  <james@daa.com.au>

	* glibwww/testglibwww.c: miscelaneous changes.  I can now download a
	page which requires authentication without crashing.  I think I have
	got rid of the crashes it had before.

	* glibwww/glibwww-trans.c (after_load_to_file): do not call finish
	function if we are just authenticating or performing a redirect.  In
	these cases, the after function will be called again later.
	(after_load_to_mem): same here.
	(glibwww_load_to_file): attach error stream to data stream.
	(glibwww_load_to_mem): same here.

	* glibwww/gnome-dialogs.c (glibwww_prompt): ignore HT_MSG_FILENAME
	prompts.  I already know what I want to call the file, and the calls
	to this in HTFSave.c would cause segfaults if I/O events occur
	while the dialog is showing.
	(*): Also fixed up some calls to g_strconcat that did not have the
	trailing NULL argument.

1999-11-04  Yukihiro Nakai <nakai@gnome.gr.jp>

	* configure.in (ALL_LINGUAS): added 'ja'.

1999-19-29  Yuri Syrota  <rasta@renome.rovno.ua>

	* configure.in (ALL_LINGUAS): added 'uk'.

1999-09-29  James Henstridge  <james@daa.com.au>

	* glibwww/testglibwww.c: changed it to a graphical program to test
	out the gnome dialogs.

	* glibwww/gnome-dialogs.c: an implementation of the HTAlert dialogs
	in terms of gnome dialogs.  This should allow entry of authentication
	information for web sites or proxies.

1999-09-24  James Henstridge  <james@daa.com.au>

	* find/trans.c, find/trans.h: modified to use glibwww for transfers.
	I will have to migrate the rest of the code away from this interface
	to an asynchronous one, but this quickly removes the dependency on
	libghttp and the ftp code that was conflicting with rpm-3.0.2.

	* find/Makefile.am (libfind_a_SOURCES): removed ftp.[ch] from file
	list.  They are not used anymore.

	* Makefile.am: use correct link flags for libwww libraries.
	
	* configure.in (LIBWWW_CFLAGS, LIBWWW_LIBS): added checks for libwww.
	The check also tries to minimise the number of mini libwww's that
	get linked in.

	* glibwww/glibwww-callbacks.c: fixed a few of the libwww event
	problems.  Hopefully it won't crash on the third download now.

1999-09-22  James Henstridge  <james@daa.com.au>

	* glibwww/testglibwww.c: a simple test utility to try out some of the
	glibwww wrapper.

	* glibwww/glibwww-*.c: a thin wrapper around libwww for use with
	gnome applications.  I wrote this because libwww's headers are quite
	dirty (they define PACKAGE, VERSION and _ for starters) and also
	complicated.  This interface provides functions for performing
	asynchronous http and ftp transfers, optionally with proxies using
	the glib main loop.  I have not implemented the HTAlert stuff yet
	though.  Some form of streaming interface is also needed.

1999-09-03  Zbigniew Chyla  <chyla@alice.ci.pwr.wroc.pl>

	* gnorpm.desktop: Added Polish translation.

1999-09-01  Pablo Saratxaga <pablo@mandrakesoft.com>

	* configure.in: Added da to ALL_LINGUAS

1999-08-03    <sipan@mit.edu>
      
        * configure.in: Added ru to ALL_LINGUAS

1999-07-16  James Henstridge  <james@daa.com.au> 
	* find/trans.c: set the user agent header to "Gnome-RPM/VERSION
	libghttp/unknwon".

1999-07-06  James Henstridge  <james@daa.com.au>

	* configure.in: added in checks to make sure people use new enough
	versions of the libghttp and libxml libraries to compile gnorpm.

	* find/search.c: updated file to use the SAX API found in libxml-1.3.

	* po/*.po: fixed up spelling mistake in all po files as well.
	
	* po/zh_TW.Big5.po, po/de.po: updated translation files.
	
	* rpmprops.c: fixed two spelling mistakes found by Karsten Weiss.

1999-06-30  James Henstridge  <james@daa.com.au>

	* find/deps.c: commented out the print statements for a lot of the
	debugging output.

	* find/trans.c: added code to set the proxy username/password pair.

	* help/C/gnorpm.sgml: added documentation about the preferences
	dialog.

	* rpmprops.h, rpmprops.c: updated the properties dialog to allow
	you to modify all the new configuration settings.  Also connected
	the help button to the help system.  Fixed the tooltips problem --
	they were being added, but not enabled.  Also added tooltips to
	the options that did not have them.

	* help/C/gnorpm.sgml: new documentation for gnorpm in docbook format.
	Also switched over to a layout that should make it easier to add
	translations of the help files to the package.

	* gnorpm.keys: add the a check signature option for GMC's context
	menus.

	* gnorpm.c: added --verify and --checksig arguments.

	* checksig.h, checksig.c, verify.h, verify.c: changed functions so
	I can get a reference to the created window.

	* rpminstalldlg.c: added a select all button and a collapse tree
	button to the install dialog.
	(rpm_install_dialog_add_dirs): do not stop a previously running
	directory scan if there is one running -- instead append the new
	directories to the list.
	(rpm_install_dialog_add_file): if the file is actually a directory,
	call ..._add_dirs to add all RPMs in that directory.  This also
	allows you to drag a directory of RPMs from gmc to gnorpm.

1999-06-29  James Henstridge  <james@daa.com.au>

	* rpminstalldlg.c (rpm_install_dialog_tree_click): catch double
	clicks to the tree.  If the double click is to a leaf node, toggle
	its selected state.

	* rpmwebfind.c: now the full package list is not cached, so we don't
	need to record whether or not to free the results list after filling
	the tree on the left.

	* find/search.c: switched over to using a SAX parser for parsing the
	index file.  This seems to have knocked between 10 and 20 Mbytes off
	the memory footprint of gnorpm when using the rpmfind window.  I
	should have tried this sooner :)

1999-06-24  James Henstridge  <james@daa.com.au>

	* rpmwebfind.h, rpmwebfind.c: moved the code that fills the package
	tree from a function that calls gtk_main_iteration() to an idle
	function.  As well as making the interface more responsive, this
	seems to have increased the speed of filling the tree a bit, which
	is a bonus.  Also, the code to interrupt the filling of the tree
	is a bit easier to understand (this code is used if you start a
	search while the tree is being filled).

	* find/distrib.h, find/distrib.c: some changes so that distribution
	metadata is only downloaded when requested, rather than all in one
	go when primeDistList() is called.  This won't prevent all the data
	being downloaded when the preferences dialog is displayed, but for
	normal web find stuff, it should reduce the downloads a bit.

	* find/search.c: fixed a bug where the full list of packages was
	being truncated sometimes.  This bug was pointed out by
	steve@itcom.net.

	* rpmfinddlg.c: changed an occurence of N_ to _, which was picked up
	by Bo.

	* configure.in: added translation

	* po/sv.po: new translation from Bo Serrander.

1999-06-07  James Henstridge  <james@daa.com.au>

	* verify.c: Made the output of the verify dialog a little more
	friendly to people who haven't heard the phrase "no news is good
	news" :)  It now specifically says that no problems were found.
	Credit for this goes to Nick Lamb <njl98r@ecs.soton.ac.uk>.

1999-06-06  James Henstridge  <james@daa.com.au>

	* rpminstalldlg.c: made the filter selection menu look a bit nicer.

1999-06-05  James Henstridge  <james@daa.com.au>

	* checksig.c: small hack to disable PGP signature checks if the
	~/.pgp directory does not exist.  This prevents problems on systems
	where pgp is installed but not set up for the user account.

	* rpminstalldlg.c: fixed a bug in the install dialog that I introduced
	with the last commit.

	* rpmwebfind.h, rpmwebfind.c: added current package colouring to the
	rpmfind dialog.

	* rpminstalldlg.h, rpminstalldlg.c: implemented filters for the
	intstall dialog.  You can now choose one of five filters to reduce
	the number of packages you have to browse through.  Also allow
	colouring of current packages (only visible in the "all packages"
	filter).

1999-06-03  James Henstridge  <james@daa.com.au>

	* install.c (addDepNodes): do not expand a node if we get a zero
	db record offset.

	* find/distrib.c: made sure that at least one entry is in the list
	of metadata servers.

	* install.c (displayDepProblems): updated the dependency conflict
	dialog to show a tree of conflicts, so you can see the wider
	ramifications of removing a package (try removing gtk+ as an
	example).  I still need to add a button to "remove all these
	dependent packages before performing transaction".  The code
	handles dependency loops, and has hooks to retrieve the list of
	dependent package db offsets.

	* rpmwebfind.h, rpmwebfind.c: now colour entries in this tree the
	same way they are in the install dialog.
	
	* find/search.c: added a statAlternatives that will deduce the
	status of an alternative.

	* find/search.h: added a status field to the rpmPackageAlternate
	structure.

	* rpminstalldlg.c: Added a few extra directories to the default list.
	Now you should be able to see RPMs off the source or applications
	disk if they are mounted (assuming that the same directory names are
	used as on my RH5.1 CDs).

1999-06-02  James Henstridge  <james@daa.com.au>

	* rpmwebfind.c: now web find does not display the upgrade button.
	It instead has a download button that will only download the package
	(and its dependencies), but not try to install it.

	* rpmquery.h, rpmquery.c: split rpm_query_add_install_buttons into
	rpm_query_add_install_button and rpm_query_add_upgrade_button.  Made
	it so that the upgrade button is not added to the dialog if there
	is no package by that name in the rpm database.

	* rpminstalldlg.c: added "unselect all" and "expand tree" buttons to
	help manipulate the tree.

1999-06-01  James Henstridge  <james@daa.com.au>

	* rpmwebfind.c: modified to set both install and upgrade callbacks
	on the install dialog.
	(rpm_web_find_transfer): new function.  Will use it as a callback for
	the upgrade button replacement.

	* mainwin.c: removed the upgrade menu item and toolbar button.  These
	are now handled by the install dialog.

	* rpminstalldlg.h, rpminstalldlg.c: added a small information frame
	to the right hand side of the install dialog, and also one to the
	associated file selection dialog.
	Also added an upgrade button to the dialog.  This is logically where
	the button should reside.

1999-05-30  James Henstridge  <james@daa.com.au>

	* rpminstalldlg.c: implemented nifty column resizing in the tree.
	Now extra space gets allocated to the first column.  It would be
	nice if there was an easier way to implement this.

	* mainwin.c, rpmwebfind.c: made changes to match the new interface
	for the RpmInstallDialog.

	* rpminstalldlg.h, rpminstalldlg.c: Changed over to the new install
	interface.  It now lists the packages in their groups.  The package
	list is primed with the packages from a number of directories, and
	can filter out packages that are already installed, and colour
	packages to indicate if they are newer or older than those on the
	system.
	I also fixed a file descriptor leak (only found it after trying to add
	all the packages from the CD to the list).

1999-05-29  James Henstridge  <james@daa.com.au>

	* gnorpm.c, rpmdentry.c, rpmfinddlg.c, rpmpackagelist.c, rpmquery.c:
	* rpmquerydlg.c, rpmwebfind.c, verify.c, mainwin.c: changed the way
	the handle to the rpm database is kept.  Now we only open the database
	when we want to access it, and leave it closed when not doing anything.
	This should allow you to run two copies of gnorpm, or use the command
	line rpm program to perform an install while gnorpm is running.
	This does mean however, that code that wants to access the database
	must be surrounded with calls to db_up and db_down.

	* dbhandle.h, dbhandle.c: you can now do multiple db_up and db_down
	calls, and it will only open or close the database when the up/down
	count passes zero.

1999-05-26  James Henstridge  <james@daa.com.au>

	* Makefile.am (data_DATA): added gnorpmrc.ko from Byeong-Chan Kim
	<redhands@linux.sarang.net>.  He also sent me a translation, but
	there already apears to be one.

	* configure.in (ALL_LINGUAS): added 'zh_TW.Big5' translation from
	Fang Chun-Chih <ccfang1@ms21.hinet.net>.

1999-04-18  Sung-Hyun Nam  <namsh@lgic.co.kr>

	* configure.in (ALL_LINGUAS): added 'ko'.
	* po/ko.po: new. korean translation.

1999-04-16  Matt Wilson  <msw@redhat.com>

	* mainwin.c: fixed calls to do_install that had argument order
	wrong.  Fixes upgrades, possibly other bugs in RPM 3.0 support.

1999-04-12  James Henstridge  <james@daa.com.au>

	* gnorpm.keys: I made a slight error in the upgrade action -- it did
	the same thing as install.

	* mainwin.c: don't need to include boom.xpm anymore -- using stock
	pixmaps for that stuff now.

	* gnorpm.c: now command line install/upgrade actually works.  Also an
	error dialog is displayed if you do a command line query with no
	packages selected.  Everything is ready for new release now.

1999-04-12  Matt Wilson  <msw@redhat.com>

	* gnorpm.c: fixed typo in query_install_func() that broke RPM
	3.0 builds.
	* install.c: added some sanity checks to avoid segfaults with
	NULL transaction sets.

1999-04-12  James Henstridge  <james@daa.com.au>

	* gnorpm.c: made all the buttons in the dialog displayed by
	"gnorpm -q" function correctly.

	* rpmwebfind.c: added a bit of code so that if you start a new search
	while the old search's results were being displayed, the old search
	results display procedure would quit.  Previously the undisplayed
	results of the old search would be appended to the end of the newer
	results.

	* NEWS: added some info about changes in this version.

	* find/rdf_api.h: changed some quotes to angle brackets for some
	libxml include files.

	* rpmwebfind.c: added a feature to tell if you have already installed
	a package listed in the rpmfind listing.

	* AUTHORS: mentioned Matt Wilson here for his work on the RPM 3.0
	support, and Daniel Veillard for the inspiration (and some of the
	code) for the rpmfind part of gnorpm.

	* gnorpm.keys: new file.  This adds some actions to the context menu
	for RPMs in MC (and any other program that uses the mime-info files).

1999-04-09  Matt Wilson  <msw@redhat.com>

	* mainwin.c: added calls to set up close buttons on query dialogs
	and rpmfind dialogs
	* rpmquery.[ch]: added a new signal thaht is emitted when a close
	button is pressed, added rpm_query_add_close_button() that adds
	a close button to the buttonbox of a query page.
	* rpmquerydlg.[ch]: added rpm_query_dialog_set_close_func() that 
	sets the "close" signal handler for all the pages in a query
	dialog.
	* rpmwebfind.c: added a call to rpm_query_add_close_button()

1999-04-06  James Henstridge  <james@daa.com.au>

	* verify.c: fixed a bug when verifying packages with zero files.
	The count variable wasn't being set to zero, so a segfault was
	occuring.

1999-04-03  Matt Wilson  <msw@redhat.com>

	* install.c: added support for RPM 3.0 problem reporting and
	user override, added progress bars for RPM 3.0 installation and
	upgrades.

1999-03-29  James Henstridge  <james@daa.com.au>

	* mainwin.c(context_uninstall): Applied a patch to fix the uninstall
	context menu.  It wasn't removing the icon from the display when the
	package was uninstalled.

	* mainwin.c: store the progress bar keys in an GSList in the widget
	data.  This way, if the user starts another download in the middle
	of the first, we won't get warnings and maybe crashes.

	* mainwin.c, gnorpm.c: made the main window resizeable.

	* gnorpm.c: made gnorpm quit on die signals from the session manager.

1999-03-12  Matt Wilson  <msw@gimp.org>

	* install.c, mainwin.c: fixed build problems against RPM 2.5

1999-03-12  Matt Wilson  <msw@gimp.org>

	* checksig.c: include config.h so that rpmcompat.h will know
	which version of RPM the system has

	* install.c, gnorpm.c, mainwin.c: added support for RPM 3.0
	transaction flags and problem filter arguments.

	* install.h: removed unused #defines

	* rpmpackagelist.c: changes in the clist minimum clist column
	size by Adrian.

	* rpmprops.c, rpmprops.h: RPM 3.0 transaction flags and problem
	set filter support added.  Tooltips were added for the option
	checkboxes from Adrian Likins <adrian@gimp.org>.  Entries for
	directories were changed from GtkEntries to GnomeFileEntries.

	* rpmquery.c: clist minimum size and autoresize and abels set to
	line wrap for various labels by Adrian.

1999-03-12  Matt Wilson  <msw@gimp.org>

	* README, checksig.c, checksig.h, dbhandle.c, dbhandle.h, gnorpm.c,
	gnorpm.desktop, gnorpm.html, gnorpm.spec, gtkpathtree.c, gtkpathtree.h,
	install.c, install.h, mainwin.c, misc.c, misc.h, rpmcompat.h,
	rpmdentry.c, rpmdentry.h, rpmfinddlg.c, rpmfinddlg.h, rpminstalldlg.c,
	rpminstalldlg.h, rpmpackagelist.c, rpmpackagelist.h, rpmprogress.c,
	rpmprogress.h, rpmprops.c, rpmprops.h, rpmquery.c, rpmquery.h,
	rpmquerydlg.c, rpmquerydlg.h, rpmwebfind.c, rpmwebfind.h, verify.c,
	verify.h, find/ftp.c, find/ftp.h, po/de.po, po/tr.po:
	Changed all references of Redhat to Red Hat.

1999-03-12  Matt Wilson  <msw@gimp.org>

	* Makefile.am: changed LDADD to not include -ldb as RPM needs
	-ldb1 on glibc 2.1 systems and gnome-libs already has the correct
	libdb library in `gnome-config libgnomeui --libs`.  Also added
	rpmcompat.h to gnorpm_SOURCES.

	* acconfig.h: added new #define for HAVE_RPM_3_0

	* checksig.c: converted to be compatible with the RPM 3.0 API.

	* configure.in: added tests for -ldb1, RPM 3.0, db1/db.h, and
	db_186.h

	* gnorpm.c: changed "static isFiles" to "static int isFiles"
	Added new RPM 3.0 startup code.  Converted installFlags to use
	internally defined flags instead of depreciated RPMINSTALL flags.

	* install.c: added RPM 3.0 callback function, do_install,
	do_uninstall, and converted touse interally defined flags

	* install.h: define flags internally

	* mainwin.c: use internal flags

	* rpmcompat.h: new file that contains macros for RPM 2.5 systems
	that provide compatibility for the new rpmio FD_t wrappers

	* rpminstalldlg.c: converted to use RPM 3.0 API.

	* rpmprops.c: disabled on RPM 3.0 for now until the proper
	problem filter code can be written to support this.

	* rpmquery.c: modified to be compatible with RPM 3.0 API.

	* rpmquerydlg.c: changed "static querydialog_type" to
	"static int querydialog_type"

1999-03-09  James Henstridge  <james@daa.com.au>

	* mainwin.c: now the install and upgrade routines will update the
	display (adding groups to the tree, refreshing the right pane).

	* po/de.po: added a german translation, from Karsten Weiss.

	* install.[ch]: added a callback feature so the program can get a
	notification for each package installed.

	* mainwin.c: added a context menu with query, uninstall and verify on
	it.  They act on a single package like in glint.
	Made the uninstall routines update the package listing.  Also clicking
	the uninstall button on the query windows will remove the associated
	page from the notebook.

	* rpmpackagelist.c: added rpm_package_list_update_pane to resync the
	current pane with the state of the rpm db.  This is to be used after
	removals, installs and upgrades.
	Added a signal context_menu that gets called on right clicks in the
	right pane.
	(rpm_package_list_add_group) new function to add a group to the tree.
	This is useful for installations and upgrades.

	* find/ftp.c: fixed a bug in the ftp code.  This one was pointed out
	by Rene Moeijes <rene@moeijes.demon.nl>

1999-02-27  James Henstridge  <james@daa.com.au>

	* mainwin.c, rpmprops.c: default view mode is as icons now.

	* topic.dat, gnorpm.html: updated the help file a little.

	* po/tr.po: got updated translation from Frank K. Gurkaynak
	<kgf@WPI.EDU>

1999-02-26  James Henstridge  <james@daa.com.au>

	* rpmwebfind.c: fixed a bug where it would download the same file
	repeatedly rather than moving onto the next one to get.

	* find/search.c: fixed a bug in the search code.  It would loose the
	head of the search results (was ignoring return of g_list_sort).

1999-02-25  James Henstridge  <james@daa.com.au>

	* mainwin.c, rpminstalldlg.c: added drag and drop functionality.  You
	can now drag rpms onto the main window to bring up an install dialog,
	or drag rpms to an install dialog to add them to the list.

1999-02-17  James Henstridge  <james@daa.com.au>

	* checksig.[ch]: added files to allow doing signature checks on RPMs
	Also made changes to.
	* mainwin.c, rpminstalldlg.[ch], rpmquery.[ch], rpmquerydlg.[ch],
	rpmwebfind.[ch]: changes to allow signatures checking.

	* rpmprops.c: some changes so that the mirrors list is correct.  Also
	list origin server at bottom of mirror list.

	* find/distrib.c: if no mirrors exist, set preferred mirror to origin
	server.

1999-02-12  James Henstridge  <james@daa.com.au>

	* configure.in: made it default to building the rpmfind stuff.

	* find/README: added a readme file for the library.

	* find/*.[ch]: added copyright messages to the top of the sources.

1999-02-11  James Henstridge  <james@daa.com.au>

	* rpmprops.[ch]: added extra notebook tab to configure this new
	functionality.

	* find/deps.[ch]: added support for a `no upgrade' list, for packages
	that shouldn't be upgraded during rpmfind opperations.

1999-02-10  James Henstridge  <james@daa.com.au>

	* rpmwebfind.c: similar changes to speed things up.  Initial ctree
	fill is started from an idle function, which means the window
	will display before this starts (gives user feadback).  Also
	now freeze the ctree while adding items to it (big speedup).  Also
	set the ctree to insensitive while doing updates to prevent the user
	from interrupting these events and allow the interface to stay
	responsive.

	* find/search.c: made some changes so duplicates don't turn up in
	search results.  Also added some code so UI isn't frozen while
	searching.

	* gtkpathtree.[ch]: converted to use GtkCTree rather than GtkTree.
	This should reduce memory usage a bit for the item, and fix up those
	problems where the expanders didn't show up until you resized the
	tree pane.

	* rpmpackagelist.c: changed default package icon to tigert's new one.
	Also, this icon is now compiled in, making one less file to install.

1999-01-28  James Henstridge  <james@daa.com.au>

	* rpmwebfind.h, rpmwebfind.c: The frontend for doing rpmfind searches.

	* rpmquery.h, rpmquery.c: Made changes so that you can display
	information from an RDF description of a package.  Also did some
	cleanups to allow changing info in the pane.

1999-01-24  James Henstridge  <james@daa.com.au>

	* rpmprops.h, rpmprops.c: Added some extra tabs for rpmfind
	configuration options.

	* find/*: added a set of files that implement the internals of
	rpmfind's protocols.  It only relies on glib, libgnome, libxml
	and libghttp.  All gui stuff gets added to the main program,
	surrounded by #ifdef WITH_RPMFIND.

1999-01-13  James Henstridge  <james@daa.com.au>

	* configure.in, Makefile.am: Start of changes to integrate rpmfind
	functionality.

1998-11-25  James Henstridge  <james@james.daa.com.au>

	* gnorpm.c: changed over to POPT so that gnorpm will compile.

	* rpmpackagelist.[ch]: added support for the new icon list code.

	* rpmquery.c: added a scrolled window around the clist.

	* rpmquerydlg.[ch]: RpmQueryDialog now descends from GtkWindow,
	and doesn't have the close button down the bottom.  I did this
	because of a message from someone from the gnome-gui list saying
	that it didn't make sense to have it there.

	* misc.c, install.c: changed occurences of gnome_dialog_set_modal to
	gtk_window_set_modal.
