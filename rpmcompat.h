/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __RPMCOMPAT_H__
#define __RPMCOMPAT_H__

#if !defined(HAVE_RPM_3_0) && !defined(HAVE_RPM_4_0)
#define FD_t int
#define fdOpen open
#define fdLseek lseek
#define fdClose close
#define fdRead read
#define fdWrite write
#define fdFileno(x) x
#endif /* !HAVE_RPM_3_0 */

#endif /* __RPMCOMPAT_H__ */
