/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "config.h"
#include "checksig.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "misc.h"
#include "rpmcompat.h"

/* these are in rpmlib but not in rpmlib.h */
int readLead(FD_t fd, struct rpmlead *lead);
int rpmReadSignature(FD_t fd, Header *header, short sig_type);

int check_sigs(GList *files, int flags, GtkWidget **awin) {
  GList *tmp;
  FD_t fd, ofd;
  int res, res2, res3, rownum;
  struct rpmlead lead;
  char result[1024], *sigtarget;
  unsigned char buffer[8192];
  Header sig;
  HeaderIterator sigIter;
  gint32 tag, type, count;
  const void *ptr;
  gchar *row[3] = { N_("File"), N_("Sig"), N_("Details") };
  GtkStyle *errStyle;

  GtkWidget *win, *w, *list;

  /* disable pgp checks if there is no ~/.pgp directory ... */
  sigtarget = gnome_util_prepend_user_home(".pgp");
  if (!g_file_exists(sigtarget))
    flags |= SIG_SKIP_PGP;
  g_free(sigtarget);

  win = gnome_dialog_new(_("Checking Signatures"), GNOME_STOCK_BUTTON_CLOSE,
			 NULL);
  gnome_dialog_set_sensitive(GNOME_DIALOG(win), 0, FALSE);
  gnome_dialog_set_close(GNOME_DIALOG(win), TRUE);
  gnome_dialog_close_hides(GNOME_DIALOG(win), FALSE);
  gtk_window_set_policy(GTK_WINDOW(win), FALSE, TRUE, TRUE);
  set_icon(win);

  if (awin)
    *awin = win;

  w = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_usize(w, 320, 150);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(w),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(win)->vbox), w, TRUE, TRUE, 0);
  gtk_widget_show(w);

  row[0] = _(row[0]);
  row[1] = _(row[1]);
  row[2] = _(row[2]);
  list = gtk_clist_new_with_titles(3, row);
  gtk_clist_set_selection_mode(GTK_CLIST(list), GTK_SELECTION_BROWSE);
  gtk_clist_column_titles_passive(GTK_CLIST(list));
  gtk_clist_set_column_width(GTK_CLIST(list), 0, 150);
  gtk_clist_set_column_width(GTK_CLIST(list), 1, 50);
  gtk_container_add(GTK_CONTAINER(w), list);
  gtk_widget_show(list);

  errStyle = gtk_style_copy(gtk_widget_get_style(list));
  errStyle->fg[GTK_STATE_NORMAL].red = 0xffff;
  errStyle->fg[GTK_STATE_NORMAL].green = 0;
  errStyle->fg[GTK_STATE_NORMAL].blue = 0;
  errStyle->fg[GTK_STATE_NORMAL].pixel = 0;
  gdk_color_alloc(gtk_widget_get_colormap(list),
		  &(errStyle->fg[GTK_STATE_NORMAL]));

  gtk_widget_show(win);
  while (gtk_events_pending())
    gtk_main_iteration();
  res = 0;
  for (tmp = files; tmp; tmp = tmp->next) {
    gchar *file = tmp->data;
    gchar template[] = "/tmp/gnorpmdata.XXXXXX", *tmpfile;

    row[0] = g_basename(file);
    row[1] = _("error");
    row[2] = "";
    fd = fdOpen(file, O_RDONLY, 0);
    if (fdFileno(fd) < 0) {
      row[2] = _("couldn't open file");
      rownum = gtk_clist_append(GTK_CLIST(list), row);
      gtk_clist_set_row_style(GTK_CLIST(list), rownum, errStyle);
      res++;
      continue;
    }
    if (readLead(fd, &lead)) {
      row[2] = _("could not read lead bytes");
      rownum = gtk_clist_append(GTK_CLIST(list), row);
      gtk_clist_set_row_style(GTK_CLIST(list), rownum, errStyle);
      res++;
      fdClose(fd);
      continue;
    }
    if (lead.major == 1) {
      row[2] = _("file version doesn't support signatures");
      rownum = gtk_clist_append(GTK_CLIST(list), row);
      gtk_clist_set_row_style(GTK_CLIST(list), rownum, errStyle);
      res++;
      fdClose(fd);
      continue;
    }
    if (rpmReadSignature(fd, &sig, lead.signature_type)) {
      row[2] = _("could not read signature block");
      rownum = gtk_clist_append(GTK_CLIST(list), row);
      gtk_clist_set_row_style(GTK_CLIST(list), rownum, errStyle);
      res++;
      fdClose(fd);
      continue;
    }
    if (!sig) {
      row[2] = _("no signatures");
      rownum = gtk_clist_append(GTK_CLIST(list), row);
      gtk_clist_set_row_style(GTK_CLIST(list), rownum, errStyle);
      res++;
      fdClose(fd);
      continue;
    }
    row[0] = lead.name;
    tmpfile = tmpnam(template);

    ofd = fdOpen(tmpfile, O_WRONLY|O_CREAT, 0644);
    while ((count = fdRead(fd, buffer, sizeof(buffer))) != 0) {
      if (count == -1) {
	fdClose(ofd);
	fdClose(fd);
	unlink(tmpfile);
	row[2] = _("error reading file");
	rownum = gtk_clist_append(GTK_CLIST(list), row);
	gtk_clist_set_row_style(GTK_CLIST(list), rownum, errStyle);
	continue;
      }
      if (fdWrite(ofd, buffer, count) < 0) {
	fdClose(ofd);
	fdClose(fd);
	unlink(tmpfile);
	row[2] = _("error writing temp file");
	rownum = gtk_clist_append(GTK_CLIST(list), row);
	gtk_clist_set_row_style(GTK_CLIST(list), rownum, errStyle);
	continue;
      }
    }
    fdClose(fd);
    fdClose(ofd);

    sigIter = headerInitIterator(sig);
    res2 = 0;
    while (headerNextIterator(sigIter, &tag, &type, &ptr, &count)) {
      if (tag == RPMSIGTAG_PGP && (flags & SIG_SKIP_PGP))
	continue;
      else if ((tag == RPMSIGTAG_MD5 || tag == RPMSIGTAG_LEMD5_2 ||
		tag == RPMSIGTAG_LEMD5_1) && (flags & SIG_SKIP_MD5))
	continue;
      else if (tag == RPMSIGTAG_SIZE && (flags & SIG_SKIP_SIZE))
	continue;

      if ((res3 = rpmVerifySignature(tmpfile, tag, ptr, count, result)) != 0)
	switch (tag) {
	case RPMSIGTAG_SIZE:
	  row[1] = _("size");
	  row[2] = _("size does not match signature");
	  rownum = gtk_clist_append(GTK_CLIST(list), row);
	  gtk_clist_set_row_style(GTK_CLIST(list), rownum, errStyle);
	  res2 = 1;
	  break;
	case RPMSIGTAG_MD5:
	case RPMSIGTAG_LEMD5_1:
	case RPMSIGTAG_LEMD5_2:
	  row[1] = _("md5");
	  row[2] = _("md5 sum does not match signature");
	  rownum = gtk_clist_append(GTK_CLIST(list), row);
	  gtk_clist_set_row_style(GTK_CLIST(list), rownum, errStyle);
	  res2 = 1;
	  break;
	case RPMSIGTAG_PGP:
	  if (res3 == RPMSIG_NOKEY) {
	    char *keypos = strstr(result, "Key ID");
	    char col3[] = "Don't have key ID \0\0\0\0\0\0\0\0\0";

	    strncat(col3, keypos + 7, 8);
	    row[1] = _("pgp");
	    row[2] = col3;
	    rownum = gtk_clist_append(GTK_CLIST(list), row);
	    gtk_clist_set_row_style(GTK_CLIST(list), rownum, errStyle);
	  } else {
	    row[1] = _("pgp");
	    row[2] = _("size does not match signature");
	    rownum = gtk_clist_append(GTK_CLIST(list), row);
	    gtk_clist_set_row_style(GTK_CLIST(list), rownum, errStyle);
	    res2 = 1;
	  }
	  break;
	default:
	  row[1] = _("unknown");
	  row[2] = _("unknown signature test failed");
	  rownum = gtk_clist_append(GTK_CLIST(list), row);
	  gtk_clist_set_row_style(GTK_CLIST(list), rownum, errStyle);
	  res2 = 1;
	}
      else
	switch(tag) {
	case RPMSIGTAG_SIZE:
	  row[1] = _("size");
	  row[2] = _("OK");
	  gtk_clist_append(GTK_CLIST(list), row);
	  break;
	case RPMSIGTAG_MD5:
	case RPMSIGTAG_LEMD5_1:
	case RPMSIGTAG_LEMD5_2:
	  row[1] = _("md5");
	  row[2] = _("OK");
	  gtk_clist_append(GTK_CLIST(list), row);
	  break;
	case RPMSIGTAG_PGP:
	  row[1] = _("pgp");
	  row[2] = _("OK");
	  gtk_clist_append(GTK_CLIST(list), row);
	  break;
	default:
	  row[1] = _("unknown");
	  row[2] = _("OK");
	  gtk_clist_append(GTK_CLIST(list), row);
	}
      while (gtk_events_pending())
	gtk_main_iteration();
    }
    headerFreeIterator(sigIter);
    res += res2;
    unlink(tmpfile);
  }
  gnome_dialog_set_sensitive(GNOME_DIALOG(win), 0, TRUE);
  gtk_style_unref(errStyle);
  return res;
}

int check_one_sig(char *file, int flags, GtkWidget **win) {
  GList *files = g_list_append(NULL, file);
  int res = check_sigs(files, flags, win);

  g_list_free(files);
  return res;
}
