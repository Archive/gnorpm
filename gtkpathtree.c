/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <gnome.h>
#include "gtkpathtree.h"
#include <string.h>

#include "dir-open.xpm"
#include "dir-close.xpm"

#define PATH_DATA "path_data"
enum {
  PATH_SELECTED,
  LAST_SIGNAL
};

static GtkCTreeClass *parent_class = NULL;
static guint path_signals[LAST_SIGNAL] = { 0 };

static void gtk_path_tree_class_init(GtkPathTreeClass *klass);
static void gtk_path_tree_init(GtkPathTree *tree);
static void gtk_path_tree_select_row(GtkPathTree *tree, GtkCTreeNode *row,
				     int col);
static void gtk_path_tree_destroy(GtkObject *object);
static void gtk_path_tree_marshal_signal(GtkObject *object, GtkSignalFunc func,
					gpointer func_data, GtkArg *args);

guint gtk_path_tree_get_type() {
  static guint pathtree_type = 0;
  if (!pathtree_type) {
    GtkTypeInfo pathtree_info = {
      "GtkPathTree",
      sizeof(GtkPathTree),
      sizeof(GtkPathTreeClass),
      (GtkClassInitFunc) gtk_path_tree_class_init,
      (GtkObjectInitFunc) gtk_path_tree_init,
      (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL
    };
    pathtree_type = gtk_type_unique(gtk_ctree_get_type(), &pathtree_info);
  }
  return pathtree_type;
}

static void gtk_path_tree_class_init(GtkPathTreeClass *klass) {
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class(gtk_ctree_get_type());

  path_signals[PATH_SELECTED] =
    gtk_signal_new("path_selected",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GtkPathTreeClass, path_selected),
		   gtk_path_tree_marshal_signal,
		   GTK_TYPE_NONE, 1,
		   GTK_TYPE_STRING);
  gtk_object_class_add_signals(object_class, path_signals, LAST_SIGNAL);

  object_class->destroy = gtk_path_tree_destroy;
}

static void gtk_path_tree_init(GtkPathTree *self) {
  gchar *text[1] = { "" };

  gtk_ctree_construct(GTK_CTREE(self), 1, 0, NULL);
  gtk_ctree_set_indent(GTK_CTREE(self), 10);
  gtk_clist_set_selection_mode(GTK_CLIST(self), GTK_SELECTION_BROWSE);
  gtk_clist_set_column_auto_resize(GTK_CLIST(self), 0, TRUE);

  self->ht = g_hash_table_new(g_str_hash, g_str_equal);

  self->open_p = gdk_pixmap_colormap_create_from_xpm_d(NULL,
	gtk_widget_get_colormap(GTK_WIDGET(self)), &(self->open_b), NULL,
	DIRECTORY_OPEN_XPM);
  self->closed_p = gdk_pixmap_colormap_create_from_xpm_d(NULL,
	gtk_widget_get_colormap(GTK_WIDGET(self)), &(self->closed_b), NULL,
	DIRECTORY_CLOSE_XPM);

  self->base = gtk_ctree_insert_node(GTK_CTREE(self), NULL, NULL, text, 2,
				     self->closed_p, self->closed_b,
				     self->open_p, self->open_b, FALSE, TRUE);
  gtk_ctree_select(GTK_CTREE(self), self->base);

  gtk_signal_connect(GTK_OBJECT(self), "tree_select_row",
		     GTK_SIGNAL_FUNC(gtk_path_tree_select_row), NULL);
}

GtkWidget *gtk_path_tree_new(gchar *base_item) {
  GtkPathTree *self;
  gchar *group;

  self = gtk_type_new(gtk_path_tree_get_type());
  /* set the base item's label */
  gtk_ctree_node_set_pixtext(GTK_CTREE(self), self->base, 0, base_item,
			     2, self->open_p, self->open_b);

  group = g_strdup(base_item);
  g_hash_table_insert(self->ht, group, self->base);
  gtk_ctree_node_set_row_data(GTK_CTREE(self), self->base, group);
  return GTK_WIDGET(self);
}

void gtk_path_tree_freeze(GtkPathTree *tree) {
  gtk_clist_freeze(GTK_CLIST(tree));
  g_hash_table_freeze(tree->ht);
}
void gtk_path_tree_thaw(GtkPathTree *tree) {
  g_hash_table_thaw(tree->ht);
  gtk_clist_thaw(GTK_CLIST(tree));
}

GtkCTreeNode *gtk_path_tree_add_path(GtkPathTree *tree, gchar *path) {
  gchar *path_stub, *path_parent, *text[1] = { "" };
  GtkCTreeNode *parent, *ret;

  if ((ret = g_hash_table_lookup(tree->ht, path)) != NULL)
    return ret;

  path_stub = strrchr(path, '/');
  if (!path_stub) {
    parent = tree->base;
    path_stub = path;
  } else {
    path_parent = g_new(char, path_stub - path + 1);
    strncpy(path_parent, path, path_stub - path);
    path_parent[path_stub - path] = '\0';
    parent = gtk_path_tree_add_path(tree, path_parent);
    g_free(path_parent);
    path_stub++;
  }

  text[0] = path_stub;
  ret = gtk_ctree_insert_node(GTK_CTREE(tree), parent, NULL, text, 2,
			      tree->closed_p, tree->closed_b,
			      tree->open_p, tree->open_b, FALSE, FALSE);
  gtk_ctree_sort_node(GTK_CTREE(tree), parent);

  /* update path->widget hash table.  widget->path translation is done through
   * object data. */
  path_stub = g_strdup(path);
  g_hash_table_insert(tree->ht, path_stub, ret);
  gtk_ctree_node_set_row_data(GTK_CTREE(tree), ret, path_stub);
  return ret;
}

static void gtk_path_tree_destroy(GtkObject *object) {
  GtkPathTree *self;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GTK_IS_PATH_TREE(object));

  self = GTK_PATH_TREE(object);

  /* clean up pixmaps ... */
  gdk_pixmap_unref(self->open_p);   gdk_bitmap_unref(self->open_b);
  gdk_pixmap_unref(self->closed_p); gdk_bitmap_unref(self->closed_b);

  /* the first argument to the GHFunc is the key, which we want to free ... */
  g_hash_table_foreach(self->ht, (GHFunc) g_free, NULL);
  g_hash_table_destroy(self->ht);

  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (* GTK_OBJECT_CLASS(parent_class)->destroy)(object);
}

static void gtk_path_tree_select_row(GtkPathTree *tree, GtkCTreeNode *node,
				      gint col) {
  gchar *path;

  path = gtk_ctree_node_get_row_data(GTK_CTREE(tree), node);

  gtk_signal_emit(GTK_OBJECT(tree), path_signals[PATH_SELECTED], path);
}

static void gtk_path_tree_marshal_signal(GtkObject *object, GtkSignalFunc func,
					gpointer func_data, GtkArg *args) {

  typedef void (*sig_func)(GtkObject *h, gchar *path, gpointer func_data);
  sig_func rfunc = (sig_func)func;

  (*rfunc)(object, GTK_VALUE_STRING(args[0]), func_data);
}

