/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __RPM_QUERY_H__
#define __RPM_QUERY_H__

#include <gtk/gtk.h>
#include <rpmlib.h>
#include <rpmio.h>

#include "dbhandle.h"

#ifdef WITH_RPMFIND
#include "find/rdf.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define RPM_QUERY(obj) GTK_CHECK_CAST(obj, rpm_query_get_type(), RpmQuery)
#define RPM_QUERY_CLASS(class) GTK_CHECK_CAST_CLASS(class, rpm_query_get_type(), RpmQueryClass)
#define RPM_IS_QUERY(obj) GTK_CHECK_TYPE(obj, rpm_query_get_type())

typedef enum {
  RPM_QUERY_DB,
  RPM_QUERY_FILE,
#ifdef WITH_RPMFIND
  RPM_QUERY_RDF,
#endif
} RpmQuerySource;

typedef struct _RpmQuery RpmQuery;
typedef struct _RpmQueryClass RpmQueryClass;

struct _RpmQuery {
  GtkTable parent;
  DBHandle *hdl;
  RpmQuerySource source;
  guint index; /* this is set iff the file has been installed */
  gchar *fname;           /* this is set iff read from a file */
  gchar *pkg_name, *desc;
  /* info labels */
  GtkWidget *pkgname, *size, *install, *bhost, *bdate, *dist, *vend;
  GtkWidget *group, *packager, *url;
  GtkWidget *description; /* GtkText storing description */
  GtkWidget *files;       /* GtkCList storing file descriptions */
  GtkWidget *action_area; /* a button box */

#ifdef WITH_RPMFIND
  rpmData *rdfData;
#endif
};

struct _RpmQueryClass {
  GtkTableClass parent_class;

  void (*verify)(RpmQuery *query);
  void (*uninstall)(RpmQuery *query);
  void (*install)(RpmQuery *query);
  void (*upgrade)(RpmQuery *query);
  void (*checksig)(RpmQuery *query);
  void (*close)(RpmQuery *query);
};

guint rpm_query_get_type(void);
GtkWidget *rpm_query_new(void);
GtkWidget *rpm_query_new_from_index(DBHandle *hdl, guint index);
GtkWidget *rpm_query_new_from_file(DBHandle *hdl, gchar *file);
gint rpm_query_set_from_db(RpmQuery *query, DBHandle *hdl, guint index);
gint rpm_query_set_from_file(RpmQuery *query, DBHandle *hdl, gchar *file);

void rpm_query_add_install_button(RpmQuery *self);
void rpm_query_add_upgrade_button(RpmQuery *self);
void rpm_query_add_checksig_button(RpmQuery *self);
void rpm_query_add_close_button(RpmQuery *self);
void rpm_query_add_opp_buttons(RpmQuery *self);
void rpm_query_clear(RpmQuery *self);

#ifdef WITH_RPMFIND
GtkWidget *rpm_query_new_from_rdf(DBHandle *hdl, rpmData *data);
gint rpm_query_set_from_rdf(RpmQuery *query, DBHandle *hdl, rpmData *data);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __RPM_QUERY_H__ */
