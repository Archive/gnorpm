/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __RPMDATA_H__
#define __RPMDATA_H__

#include <glib.h>
#include <sys/types.h>

/*
 * structure associated with an rpm
 */

typedef struct rpm_data {
  char *subdir;		        /* subdirectory holding the rpm */
  char *filename;		/* name of the file */
  char *name;			/* name of the software */
  char *version;		/* version of the software */
  char *release;		/* software release */
  char *url;                    /* URL for the software */
  char *arch;                   /* the architecture system */
  char *os;                     /* the target system */
  char *distribution;		/* general OS distribution */
  char *vendor;		        /* general OS vendor */
  char *packager;		/* the packager */
  char *group;		        /* type of software */
  char *summary;		/* 1 line summary */
  char *description;            /* short description */
  char *copyright;              /* software copyright */
  char *changelog;              /* changelog */
  char *srcrpm;		        /* source RPM */
  time_t date;		        /* date of packaging */
  time_t stamp;		        /* modification file of the archive */
  gint32 size;		        /* size of the software */
  char *host;			/* build host */
  GList *resources;             /* list of the resources */
  GList *requires;              /* list of the requires */
  char *filelist;               /* the filelist */

  gboolean nofree;              /* TRUE if structure shouldn't be free'd */
} rpmData, *rpmDataPtr;

#endif /* __RPMDATA_H__ */
