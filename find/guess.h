/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef GUESS_H
#define GUESS_H

#include <glib.h>
#include <rpmlib.h>

typedef enum {
  NORTH_AMERICA = 1,
  SOUTH_AMERICA = 2,
  NORTH_AFRICA  = 3,
  SOUTH_AFRICA  = 4,
  EUROPE        = 5,
  MIDDLE_EAST   = 6,
  ASIA          = 7,
  AUSTRALIA     = 8
} Continent;

typedef enum {
  NO_REGION       = 0,
  WESTERN_EUROPE  = 1,
  CENTRAL_EUROPE  = 2,
  EASTERN_EUROPE  = 3,
  SCANDINAVIA     = 4,

  MEDITERRANEAN   = 5,
  NE_AMERICA      = 6,
  NW_AMERICA      = 7,
  CENTRAL_AMERICA = 8
} Region;

typedef struct _Country Country;
struct _Country {
  char *name;
  char *code;
  int no;
  Continent continent;
  Region zone1, zone2, zone3;
};

/* the countries array -- you may want to use it */
extern Country countries[];
extern guint num_countries;

/* initialise some values for guess functions */
void guessInit(rpmdb db);
/* work out a score for a given URL.  The higher the score, the closer the
 * site is likely to be to this computer.  A hash table of scores is kept
 * to speed up lookups. */
guint guessServerScore(const char *hostname);
guint guessUrlScore(const char *URL);

/* guess these values.  The distribution and vendor are the most common pair
 * found in the current values. */
const char *guessDistribution(void);
const char *guessVendor(void);
const char *guessArch(void);
const char *guessOs(void);

#endif
