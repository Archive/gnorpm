/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "guess.h"
#include "trans.h"
#include <stdlib.h>
#include <gnome.h>
#include <string.h>

extern ComplainFunc complain;

/*
 * generated from http://www.nw.com/zone/iso-country-codes (iso 3166) using:
 * s/\([A-Z:(),'\.\- ]*\)    *\([A-Z][A-Z]\)    *\([A-Z][A-Z][A-Z]\)    *\([0-9]
*\)/{"\1", "\2", \4, 0},/
 */
Country countries[] = {
{"AFGHANISTAN                                  ", "AF",   4, 6, 0, 0, 0},
{"ALBANIA                                      ", "AL",   8, 5, 2, 0, 0},
{"ALGERIA                                      ", "DZ",  12, 3, 0, 0, 0},
{"AMERICAN SAMOA                               ", "AS",  16, 0, 0, 0, 0},
{"ANDORRA                                      ", "AD",  20, 5, 1, 0, 0},
{"ANGOLA                                       ", "AO",  24, 4, 0, 0, 0},
{"ANGUILLA                                     ", "AI", 660, 0, 0, 0, 0},
{"ANTARCTICA                                   ", "AQ",  10, 0, 0, 0, 0},
{"ANTIGUA AND BARBUDA                          ", "AG",  28, 1, 0, 0, 0},
{"ARGENTINA                                    ", "AR",  32, 2, 0, 0, 0},
{"ARMENIA                                      ", "AM",  51, 5, 3, 0, 0},
{"ARUBA                                        ", "AW", 533, 0, 0, 0, 0},
{"AUSTRALIA                                    ", "AU",  36, 8, 0, 0, 0},
{"AUSTRIA                                      ", "AT",  40, 5, 1, 2, 0},
{"AZERBAIJAN                                   ", "AZ",  31, 6, 0, 0, 0},
{"BAHAMAS                                      ", "BS",  44, 1, 0, 0, 0},
{"BAHRAIN                                      ", "BH",  48, 6, 0, 0, 0},
{"BANGLADESH                                   ", "BD",  50, 7, 0, 0, 0},
{"BARBADOS                                     ", "BB",  52, 1, 0, 0, 0},
{"BELARUS                                      ", "BY", 112, 5, 3, 0, 0},
{"BELGIUM                                      ", "BE",  56, 5, 1, 0, 0},
{"BELIZE                                       ", "BZ",  84, 1, 0, 0, 0},
{"BENIN                                        ", "BJ", 204, 4, 0, 0, 0},
{"BERMUDA                                      ", "BM",  60, 1, 0, 0, 0},
{"BHUTAN                                       ", "BT",  64, 6, 0, 0, 0},
{"BOLIVIA                                      ", "BO",  68, 2, 0, 0, 0},
{"BOSNIA AND HERZEGOWINA                       ", "BA",  70, 5, 2, 3, 0},
{"BOTSWANA                                     ", "BW",  72, 4, 0, 0, 0},
{"BOUVET ISLAND                                ", "BV",  74, 0, 0, 0, 0},
{"BRAZIL                                       ", "BR",  76, 2, 0, 0, 0},
{"BRITISH INDIAN OCEAN TERRITORY               ", "IO",  86, 0, 0, 0, 0},
{"BRUNEI DARUSSALAM                            ", "BN",  96, 7, 0, 0, 0},
{"BULGARIA                                     ", "BG", 100, 5, 2, 3, 0},
{"BURKINA FASO                                 ", "BF", 854, 4, 0, 0, 0},
{"BURUNDI                                      ", "BI", 108, 4, 0, 0, 0},
{"CAMBODIA                                     ", "KH", 116, 7, 0, 0, 0},
{"CAMEROON                                     ", "CM", 120, 4, 0, 0, 0},
{"CANADA                                       ", "CA", 124, 1, 0, 0, 0},
{"CAPE VERDE                                   ", "CV", 132, 0, 0, 0, 0},
{"CAYMAN ISLANDS                               ", "KY", 136, 0, 0, 0, 0},
{"CENTRAL AFRICAN REPUBLIC                     ", "CF", 140, 4, 0, 0, 0},
{"CHAD                                         ", "TD", 148, 4, 0, 0, 0},
{"CHILE                                        ", "CL", 152, 2, 0, 0, 0},
{"CHINA                                        ", "CN", 156, 7, 0, 0, 0},
{"CHRISTMAS ISLAND                             ", "CX", 162, 0, 0, 0, 0},
{"COCOS (KEELING) ISLANDS                      ", "CC", 166, 0, 0, 0, 0},
{"COLOMBIA                                     ", "CO", 170, 2, 0, 0, 0},
{"COMOROS                                      ", "KM", 174, 4, 0, 0, 0},
{"CONGO                                        ", "CG", 178, 4, 0, 0, 0},
{"CONGO, THE DEMOCRATIC REPUBLIC OF THE        ", "CD", 180, 4, 0, 0, 0},
{"COOK ISLANDS                                 ", "CK", 184, 0, 0, 0, 0},
{"COSTA RICA                                   ", "CR", 188, 2, 0, 0, 0},
{"COTE D'IVOIRE                                ", "CI", 384, 4, 0, 0, 0},
{"CROATIA (local name: Hrvatska)               ", "CU", 191, 5, 2, 3, 0},
{"CUBA                                         ", "CU", 192, 1, 0, 0, 0},
{"CYPRUS                                       ", "CY", 196, 5, 2, 0, 0},
{"CZECH REPUBLIC                               ", "CZ", 203, 5, 1, 2, 0},
{"DENMARK                                      ", "DK", 208, 5, 1, 4, 0},
{"DJIBOUTI                                     ", "DJ", 262, 6, 0, 0, 0},
{"DOMINICA                                     ", "DM", 212, 1, 0, 0, 0},
{"DOMINICAN REPUBLIC                           ", "DO", 214, 1, 0, 0, 0},
{"EAST TIMOR                                   ", "TP", 626, 7, 0, 0, 0},
{"ECUADOR                                      ", "EC", 218, 1, 0, 0, 0},
{"EGYPT                                        ", "EG", 818, 3, 0, 0, 0},
{"EL SALVADOR                                  ", "SV", 222, 1, 0, 0, 0},
{"EQUATORIAL GUINEA                            ", "GQ", 226, 4, 0, 0, 0},
{"ERITREA                                      ", "ER", 232, 0, 0, 0, 0},
{"ESTONIA                                      ", "EE", 233, 5, 1, 4, 0},
{"ETHIOPIA                                     ", "ET", 231, 4, 0, 0, 0},
{"FALKLAND ISLANDS (MALVINAS)                  ", "FK", 238, 2, 0, 0, 0},
{"FAROE ISLANDS                                ", "FO", 234, 0, 0, 0, 0},
{"FIJI                                         ", "FJ", 242, 8, 0, 0, 0},
{"FINLAND                                      ", "FI", 246, 5, 4, 1, 6},
{"FRANCE                                       ", "FR", 250, 5, 1, 5, 0},
{"FRANCE, METROPOLITAN                         ", "FX", 249, 5, 1, 5, 0},
{"FRENCH GUIANA                                ", "GF", 254, 2, 0, 0, 0},
{"FRENCH POLYNESIA                             ", "PF", 258, 8, 0, 0, 0},
{"FRENCH SOUTHERN TERRITORIES                  ", "TF", 260, 0, 0, 0, 0},
{"GABON                                        ", "GA", 266, 4, 0, 0, 0},
{"GAMBIA                                       ", "GM", 270, 0, 0, 0, 0},
{"GEORGIA                                      ", "GE", 268, 0, 0, 0, 0},
{"GERMANY                                      ", "DE", 276, 5, 1, 4, 0},
{"GHANA                                        ", "GH", 288, 4, 0, 0, 0},
{"GIBRALTAR                                    ", "GI", 292, 5, 1, 5, 0},
{"GREECE                                       ", "GR", 300, 5, 1, 5, 3},
{"GREENLAND                                    ", "GL", 304, 0, 4, 6, 0},
{"GRENADA                                      ", "GD", 308, 1, 0, 0, 0},
{"GUADELOUPE                                   ", "GP", 312, 1, 0, 0, 0},
{"GUAM                                         ", "GU", 316, 8, 0, 0, 0},
{"GUATEMALA                                    ", "GT", 320, 2, 0, 0, 0},
{"GUINEA                                       ", "GN", 324, 4, 0, 0, 0},
{"GUINEA-BISSAU                                ", "GW", 624, 4, 0, 0, 0},
{"GUYANA                                       ", "GY", 328, 2, 0, 0, 0},
{"HAITI                                        ", "HT", 332, 1, 0, 0, 0},
{"HEARD AND MC DONALD ISLANDS                  ", "HM", 334, 0, 0, 0, 0},
{"HOLY SEE (VATICAN CITY STATE)                ", "VA", 336, 5, 1, 0, 0},
{"HONDURAS                                     ", "HN", 340, 2, 0, 0, 0},
{"HONG KONG                                    ", "HK", 344, 7, 0, 0, 0},
{"HUNGARY                                      ", "HU", 348, 5, 1, 2, 0},
{"ICELAND                                      ", "IS", 352, 5, 4, 6, 0},
{"INDIA                                        ", "IN", 356, 7, 0, 0, 0},
{"INDONESIA                                    ", "ID", 360, 7, 0, 0, 0},
{"IRAN (ISLAMIC REPUBLIC OF)                   ", "IR", 364, 6, 0, 0, 0},
{"IRAQ                                         ", "IQ", 368, 6, 0, 0, 0},
{"IRELAND                                      ", "IE", 372, 5, 1, 0, 0},
{"ISRAEL                                       ", "IL", 376, 6, 0, 0, 0},
{"ITALY                                        ", "IT", 380, 5, 1, 5, 0},
{"JAMAICA                                      ", "JM", 388, 1, 0, 0, 0},
{"JAPAN                                        ", "JP", 392, 7, 0, 0, 0},
{"JORDAN                                       ", "JO", 400, 6, 0, 0, 0},
{"KAZAKHSTAN                                   ", "KZ", 398, 6, 0, 0, 0},
{"KENYA                                        ", "KE", 404, 4, 0, 0, 0},
{"KIRIBATI                                     ", "KI", 296, 0, 0, 0, 0},
{"KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF       ", "KP", 408, 7, 0, 0, 0},
{"KOREA, REPUBLIC OF                           ", "KR", 410, 7, 0, 0, 0},
{"KUWAIT                                       ", "KW", 414, 6, 0, 0, 0},
{"KYRGYZSTAN                                   ", "KG", 417, 6, 0, 0, 0},
{"LAO PEOPLE'S DEMOCRATIC REPUBLIC             ", "LA", 418, 7, 0, 0, 0},
{"LATVIA                                       ", "LV", 428, 5, 1, 4, 0},
{"LEBANON                                      ", "LB", 422, 6, 0, 0, 0},
{"LESOTHO                                      ", "LS", 426, 4, 0, 0, 0},
{"LIBERIA                                      ", "LR", 430, 4, 0, 0, 0},
{"LIBYAN ARAB JAMAHIRIYA                       ", "LY", 434, 6, 0, 0, 0},
{"LIECHTENSTEIN                                ", "LI", 438, 5, 1, 0, 0},
{"LITHUANIA                                    ", "LT", 440, 5, 1, 4, 0},
{"LUXEMBOURG                                   ", "LU", 442, 5, 1, 0, 0},
{"MACAU                                        ", "MO", 446, 7, 0, 0, 0},
{"MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF   ", "MK", 807, 5, 1, 2, 0},
{"MADAGASCAR                                   ", "MG", 450, 4, 0, 0, 0},
{"MALAWI                                       ", "MW", 454, 0, 0, 0, 0},
{"MALAYSIA                                     ", "MY", 458, 7, 0, 0, 0},
{"MALDIVES                                     ", "MV", 462, 0, 0, 0, 0},
{"MALI                                         ", "ML", 466, 4, 0, 0, 0},
{"MALTA                                        ", "MT", 470, 4, 0, 0, 0},
{"MARSHALL ISLANDS                             ", "MH", 584, 0, 0, 0, 0},
{"MARTINIQUE                                   ", "MQ", 474, 1, 0, 0, 0},
{"MAURITANIA                                   ", "MR", 478, 4, 0, 0, 0},
{"MAURITIUS                                    ", "MU", 480, 4, 0, 0, 0},
{"MAYOTTE                                      ", "YT", 175, 4, 0, 0, 0},
{"MEXICO                                       ", "MX", 484, 1, 0, 0, 0},
{"MICRONESIA, FEDERATED STATES OF              ", "FM", 583, 8, 0, 0, 0},
{"MOLDOVA, REPUBLIC OF                         ", "MD", 498, 5, 2, 0, 0},
{"MONACO                                       ", "MC", 492, 5, 1, 5, 0},
{"MONGOLIA                                     ", "MN", 496, 7, 0, 0, 0},
{"MONTSERRAT                                   ", "MS", 500, 0, 0, 0, 0},
{"MOROCCO                                      ", "MA", 504, 3, 0, 0, 0},
{"MOZAMBIQUE                                   ", "MZ", 508, 4, 0, 0, 0},
{"MYANMAR                                      ", "MM", 104, 0, 0, 0, 0},
{"NAMIBIA                                      ", "NA", 516, 4, 0, 0, 0},
{"NAURU                                        ", "NR", 520, 0, 0, 0, 0},
{"NEPAL                                        ", "NP", 524, 7, 0, 0, 0},
{"NETHERLANDS                                  ", "NL", 528, 5, 1, 0, 0},
{"NETHERLANDS ANTILLES                         ", "AN", 530, 1, 0, 0, 0},
{"NEW CALEDONIA                                ", "NC", 540, 8, 0, 0, 0},
{"NEW ZEALAND                                  ", "NZ", 554, 8, 0, 0, 0},
{"NICARAGUA                                    ", "NI", 558, 2, 0, 0, 0},
{"NIGER                                        ", "NE", 562, 4, 0, 0, 0},
{"NIGERIA                                      ", "NG", 566, 4, 0, 0, 0},
{"NIUE                                         ", "NU", 570, 0, 0, 0, 0},
{"NORFOLK ISLAND                               ", "NF", 574, 0, 0, 0, 0},
{"NORTHERN MARIANA ISLANDS                     ", "MP", 580, 0, 0, 0, 0},
{"NORWAY                                       ", "NO", 578, 5, 4, 1, 0},
{"OMAN                                         ", "OM", 512, 6, 0, 0, 0},
{"PAKISTAN                                     ", "PK", 586, 6, 0, 0, 0},
{"PALAU                                        ", "PW", 585, 0, 0, 0, 0},
{"PANAMA                                       ", "PA", 591, 1, 0, 0, 0},
{"PAPUA NEW GUINEA                             ", "PG", 598, 8, 0, 0, 0},
{"PARAGUAY                                     ", "PY", 600, 2, 0, 0, 0},
{"PERU                                         ", "PE", 604, 2, 0, 0, 0},
{"PHILIPPINES                                  ", "PH", 608, 7, 0, 0, 0},
{"PITCAIRN                                     ", "PN", 612, 0, 0, 0, 0},
{"POLAND                                       ", "PL", 616, 5, 1, 2, 0},
{"PORTUGAL                                     ", "PT", 620, 5, 1, 5, 0},
{"PUERTO RICO                                  ", "PR", 630, 1, 0, 0, 0},
{"QATAR                                        ", "QA", 634, 0, 0, 0, 0},
{"REUNION                                      ", "RE", 638, 4, 0, 0, 0},
{"ROMANIA                                      ", "RO", 642, 5, 1, 2, 0},
{"RUSSIAN FEDERATION                           ", "RU", 643, 5, 1, 2, 0},
{"RWANDA                                       ", "RW", 646, 4, 0, 0, 0},
{"SAINT KITTS AND NEVIS                        ", "KN", 659, 0, 0, 0, 0},
{"SAINT LUCIA                                  ", "LC", 662, 0, 0, 0, 0},
{"SAINT VINCENT AND THE GRENADINES             ", "VC", 670, 0, 0, 0, 0},
{"SAMOA                                        ", "WS", 882, 8, 0, 0, 0},
{"SAN MARINO                                   ", "SM", 674, 0, 0, 0, 0},
{"SAO TOME AND PRINCIPE                        ", "ST", 678, 0, 0, 0, 0},
{"SAUDI ARABIA                                 ", "SA", 682, 6, 0, 0, 0},
{"SENEGAL                                      ", "SN", 686, 4, 0, 0, 0},
{"SEYCHELLES                                   ", "SC", 690, 4, 0, 0, 0},
{"SIERRA LEONE                                 ", "SL", 694, 2, 0, 0, 0},
{"SINGAPORE                                    ", "SG", 702, 7, 0, 0, 0},
{"SLOVAKIA (Slovak Republic)                   ", "SK", 703, 5, 1, 2, 0},
{"SLOVENIA                                     ", "SI", 705, 5, 1, 5, 0},
{"SOLOMON ISLANDS                              ", "SB",  90, 0, 0, 0, 0},
{"SOMALIA                                      ", "SO", 706, 4, 0, 0, 0},
{"SOUTH AFRICA                                 ", "ZA", 710, 4, 0, 0, 0},
{"SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS ", "GS", 239, 0, 0, 0, 0},
{"SPAIN                                        ", "ES", 724, 5, 1, 5, 0},
{"SRI LANKA                                    ", "LK", 144, 7, 0, 0, 0},
{"ST. HELENA                                   ", "SH", 654, 4, 0, 0, 0},
{"ST. PIERRE AND MIQUELON                      ", "PM", 666, 1, 0, 0, 0},
{"SUDAN                                        ", "SD", 736, 4, 0, 0, 0},
{"SURINAME                                     ", "SR", 740, 0, 0, 0, 0},
{"SVALBARD AND JAN MAYEN ISLANDS               ", "SJ", 744, 0, 0, 0, 0},
{"SWAZILAND                                    ", "SZ", 748, 0, 0, 0, 0},
{"SWEDEN                                       ", "SE", 752, 5, 4, 1, 0},
{"SWITZERLAND                                  ", "CH", 756, 5, 1, 0, 0},
{"SYRIAN ARAB REPUBLIC                         ", "SY", 760, 6, 0, 0, 0},
{"TAIWAN, PROVINCE OF CHINA                    ", "TW", 158, 7, 0, 0, 0},
{"TAJIKISTAN                                   ", "TJ", 762, 6, 0, 0, 0},
{"TANZANIA, UNITED REPUBLIC OF                 ", "TZ", 834, 4, 0, 0, 0},
{"THAILAND                                     ", "TH", 764, 7, 0, 0, 0},
{"TOGO                                         ", "TG", 768, 4, 0, 0, 0},
{"TOKELAU                                      ", "TK", 772, 0, 0, 0, 0},
{"TONGA                                        ", "TO", 776, 4, 0, 0, 0},
{"TRINIDAD AND TOBAGO                          ", "TT", 780, 0, 0, 0, 0},
{"TUNISIA                                      ", "TN", 788, 3, 0, 0, 0},
{"TURKEY                                       ", "TR", 792, 5, 1, 5, 6},
{"TURKMENISTAN                                 ", "TM", 795, 6, 0, 0, 0},
{"TURKS AND CAICOS ISLANDS                     ", "TC", 796, 0, 0, 0, 0},
{"TUVALU                                       ", "TV", 798, 8, 0, 0, 0},
{"UGANDA                                       ", "UG", 800, 4, 0, 0, 0},
{"UKRAINE                                      ", "UA", 804, 5, 2, 1, 0},
{"UNITED ARAB EMIRATES                         ", "AE", 784, 6, 0, 0, 0},
{"UNITED KINGDOM                               ", "UK", 826, 5, 1, 0, 0},
{"UNITED STATES                                ", "US", 840, 1, 0, 0, 0},
{"UNITED STATES MINOR OUTLYING ISLANDS         ", "UM", 581, 0, 0, 0, 0},
{"URUGUAY                                      ", "UY", 858, 2, 0, 0, 0},
{"UZBEKISTAN                                   ", "UZ", 860, 6, 0, 0, 0},
{"VANUATU                                      ", "VU", 548, 0, 0, 0, 0},
{"VENEZUELA                                    ", "VE", 862, 2, 0, 0, 0},
{"VIET NAM                                     ", "VN", 704, 7, 0, 0, 0},
{"VIRGIN ISLANDS (BRITISH)                     ", "VG",  92, 0, 0, 0, 0},
{"VIRGIN ISLANDS (U.S.)                        ", "VI", 850, 0, 0, 0, 0},
{"WALLIS AND FUTUNA ISLANDS                    ", "WF", 876, 8, 0, 0, 0},
{"WESTERN SAHARA                               ", "EH", 732, 3, 0, 0, 0},
{"YEMEN                                        ", "YE", 887, 6, 0, 0, 0},
{"YUGOSLAVIA                                   ", "YU", 891, 5, 1, 0, 0},
{"ZAMBIA                                       ", "ZM", 894, 4, 0, 0, 0},
{"ZIMBABWE                                     ", "ZW", 716, 4, 0, 0, 0},
/* Those don't give any info about their actual location use USA */
#define DOTCOM_NUM 239
{"COM_DOMAIN                                   ", "COM",840, 1, 0, 0, 0},
{"EDU_DOMAIN                                   ", "EDU",840, 1, 0, 0, 0},
{"NET_DOMAIN                                   ", "NET",840, 1, 0, 0, 0},
{"GOV_DOMAIN                                   ", "GOV",840, 1, 0, 0, 0},
{"ORG_DOMAIN                                   ", "ORG",840, 1, 0, 0, 0},
};
guint num_countries = sizeof(countries) / sizeof(Country);

static guint myCountryIndex = DOTCOM_NUM; /* country code (default == .com) */
static char *myHostname = NULL, *myDistribution = NULL, *myVendor = NULL;
static char *myArch = NULL, *myOs = NULL;

/* returns the index into the countries array for the given hostname
 * On errors, defaults to COM_DOMAIN */
static gint countryNum(const char *hostname) {
  int i;
  char *pos;

  pos = strrchr(hostname, '.');
  if (!pos)
    return DOTCOM_NUM;
  pos++;
  for (i = 0; i < num_countries; i++)
    if (!g_strcasecmp(pos, countries[i].code))
      return i;
  return DOTCOM_NUM;
}

typedef struct _distCounter {
  guint count;
  char *distribution;
  char *vendor;
} distCounter;

static void freeDistCounter(distCounter *cnt) {
  if (cnt->distribution) g_free(cnt->distribution);
  if (cnt->vendor) g_free(cnt->vendor);
  g_free(cnt);
}

void guessInit(rpmdb db) {
  static gboolean runAlready = FALSE;
  char *str;
  char buf[256];
  

  if (runAlready) return;
  runAlready = TRUE;

  /* find distribution and vendor */
  str = gnome_config_get_string("/gnorpm/rpmfind/distribution");
  if (!str) {
    GList *dists = NULL;
#ifdef	HAVE_RPM_4_0
    rpmdbMatchIterator mi;
    Header h;
    GList *tmp;
    char *dist, *vend;

    mi = rpmdbInitIterator(db, RPMDBI_PACKAGES, NULL, 0);
    while ((h = rpmdbNextIterator(mi)) != NULL) {
#else
    int offset;

    offset = rpmdbFirstRecNum(db);
    while (offset) {
      Header h = rpmdbGetRecord(db, offset);
      GList *tmp;
      char *dist, *vend;

      if (!h)
	continue;
#endif	/* HAVE_RPM_4_0 */
      headerGetEntry(h, RPMTAG_DISTRIBUTION, NULL, (void **)&dist, NULL);
      headerGetEntry(h, RPMTAG_VENDOR, NULL, (void **)&vend, NULL);
      for (tmp = dists; tmp; tmp = tmp->next) {
	distCounter *dc = tmp->data;
	if (dist && !strcmp(dist, dc->distribution) &&
	    vend && !strcmp(dist, dc->vendor)) {
	  dc->count++; /* match */
	  /* sort to reduce string comparisons for most popular dists */
	  while (tmp->prev &&
		 ((distCounter *)tmp->prev->data)->count < dc->count) {
	    GList *tmp2 = tmp->prev; /* swap elements */
	    tmp->prev = tmp2->prev;
	    tmp2->next = tmp->next;
	    tmp->next = tmp2;
	    tmp2->prev = tmp;
	  }
	  break; /* match */
	}
      }
      if (!tmp) { /* no match */
	distCounter *dc = g_new(distCounter, 1);
	dc->distribution = g_strdup(dist?dist:"");
	dc->vendor = g_strdup(vend?vend:"");
	dc->count = 1;
	dists = g_list_append(dists, dc);
      }
#ifndef	HAVE_RPM_4_0
      headerFree(h);
      offset = rpmdbNextRecNum(db, offset);
    }
#else	/* HAVE_RPM_4_0 */
    }
    rpmdbFreeIterator(mi);
#endif	/* HAVE_RPM_4_0 */
    if (!dists)
      complain(_("No packages installed!"));
    else {
      myDistribution = g_strdup(((distCounter *)dists->data)->distribution);
      myVendor = g_strdup(((distCounter *)dists->data)->vendor);
      g_list_foreach(dists, (GFunc)freeDistCounter, NULL);
      g_list_free(dists);
    }
    gnome_config_set_string("/gnorpm/rpmfind/distribution", myDistribution);
    gnome_config_set_string("/gnorpm/rpmfind/vendor", myVendor);
  } else {
    myDistribution = str;
    myVendor = gnome_config_get_string("/gnorpm/rpmfind/vendor");
  }
  /* get Arch/Os info */
  rpmGetMachine((const char **)&myArch, (const char **)&myOs);

  /* stupid routine to normalise myArch for intel boxes */
  myArch = g_strdup(myArch);
  if (myArch[0]=='i' && myArch[1]!='\0' && myArch[2]=='8' && myArch[3]=='6')
    myArch[1] = '3';

  /* see if hostname was set previously */
  myHostname = gnome_config_get_string("/gnorpm/rpmfind/hostname");
  if (!myHostname) {
    if (gethostname(buf, sizeof(buf)) < 0)
      strcpy(buf, "noname.com");
    gnome_config_set_string("/gnorpm/rpmfind/hostname", buf);
    myHostname = g_strdup(buf);
  }
  myCountryIndex = countryNum(myHostname);

  /* sync any data */
  gnome_config_sync();
}

/* get a score for servers in this country (countries[index].code) */
static guint countryScore(gint index) {
  int ret = 0;
  int no = countries[myCountryIndex].no;
  Region zone1 = countries[myCountryIndex].zone1;
  Region zone2 = countries[myCountryIndex].zone2;
  Region zone3 = countries[myCountryIndex].zone3;

  if (index < 0) return 0;
  if (index >= num_countries) return 0;
  if (countries[index].no == no)
    ret += 1000;
  if (zone1 != NO_REGION && (zone1 == countries[index].zone1 ||
			     zone1 == countries[index].zone2 ||
			     zone1 == countries[index].zone3))
    ret += 500;
  if (zone2 != NO_REGION && (zone2 == countries[index].zone1 ||
			     zone2 == countries[index].zone2 ||
			     zone2 == countries[index].zone3))
    ret += 200;
  if (zone3 != NO_REGION && (zone3 == countries[index].zone1 ||
			     zone3 == countries[index].zone2 ||
			     zone3 == countries[index].zone3))
    ret += 100;
  if (countries[myCountryIndex].continent == countries[index].continent)
    ret += 10;

  return ret;
}

static guint hostnameScore(const char *hostname) {
  const char *suffix, *mysuffix;

  /* check the last .zzz */
  suffix = strrchr(hostname, '.');
  if (suffix) suffix++;
  else        return 0;
  mysuffix = strrchr(myHostname, '.');
  if (mysuffix) mysuffix++;
  else          return 0;
  if (g_strcasecmp(suffix, mysuffix) != 0) return 0;

  /* check the last .yyy.zzz */
  for (suffix--; suffix > hostname && *suffix != '.'; suffix--) ;
  if (suffix == hostname) return 1000;
  for (mysuffix--; mysuffix > myHostname && *mysuffix != '.'; mysuffix--) ;
  if (mysuffix == myHostname) return 1000;
  if (g_strcasecmp(suffix, mysuffix) != 0) return 1000;
  
  /* check the last .xxx.yyy.zzz */
  for (suffix--; suffix > hostname && *suffix != '.'; suffix--) ;
  if (suffix == hostname) return 10000;
  for (mysuffix--; mysuffix > myHostname && *mysuffix != '.'; mysuffix--) ;
  if (mysuffix == myHostname) return 10000;
  if (g_strcasecmp(suffix, mysuffix) != 0) return 10000;
  
  return 100000;
}

guint guessServerScore(const char *hostname) {
  static GHashTable *hostScores = NULL;
  gpointer tmp;
  guint index, score;

  /* setup a cache of host scores */
  if (!hostScores)
    hostScores = g_hash_table_new(g_str_hash, g_str_equal);
  /* need to use lookup_extended, since a score may be 0 */
  if (g_hash_table_lookup_extended(hostScores, hostname, NULL, &tmp))
    return GPOINTER_TO_UINT(tmp);

  index = countryNum(hostname);
  score = countryScore(index);
  /*g_message("Host=%s, index=%d, c_score=%d", hostname, index, score);*/
  if (score >= 1000) {
    /* is within the same country */
    guint score2 = hostnameScore(hostname);
    /*g_message("  h_score=%d", score2);*/
    if (score2 > score)
      score = score2;
  }
  g_hash_table_insert(hostScores, g_strdup(hostname), GUINT_TO_POINTER(score));
  return score;
}

guint guessUrlScore(const char *URL) {
  const char *host, *ptr;
  char server[512];

  for (host = URL; *host; host++)
    if (host[0] == '/' && host[1] == '/')
      break;
  if (*host == '\0')
    return 0;
  host += 2;
  for (ptr = host; *ptr; ptr++)
    if (*ptr == '/' || *ptr == ':')
      break;
  strncpy(server, host, ptr - host);
  server[ptr-host] = 0;

  return guessServerScore(server);
}

const char *guessDistribution(void) {
  return (const char *)myDistribution;
}

const char *guessVendor(void) {
  return (const char *)myVendor;
}

const char *guessArch(void) {
  return (const char *)myArch;
}

const char *guessOs(void) {
  return (const char *)myOs;
}

