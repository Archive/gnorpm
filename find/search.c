/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "search.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgnome/libgnome.h>
#include <time.h>
/* this is needed for use of the SAX XML parser interface */
#include <parserInternals.h>
#include "rdf_api.h"
#include "rdf.h"
#include "trans.h"
#include "distrib.h"
#include "guess.h"

#ifdef HAVE_REGEX_H
#include <regex.h>
#endif

/* undef this if you don't want to cache rpmData structures */
#define RPM_DATA_CACHE

extern ComplainFunc complain;
extern GVoidFunc libfind_idle_func;

/* --- These functions form a SAX parser for the fullIndex.rdf index -- */
/* (we use this instead of the other RDF interface because the memory
 *  usage skyrockets when we use it to load 3.5Mbytes of XML) */
typedef enum {
  PARSER_START,
  PARSER_IN_RDF,
  PARSER_IN_DESCRIPTION,
  PARSER_IN_NAME,
  PARSER_IN_SUMMARY,
} ParserState;

typedef struct _SearchParseState {
  /* tag names -- when the xmlnamespaces, the correct prefixes are
   * prepended to the tags, and saved here for later comparison */
  gchar *rdf, *description, *name, *summary;
  GString *nameData, *summaryData;
  GList *results, *tail;

  /* this is for the search interface ... */
  const gchar *search; /* this is NULL if we want all items */
#ifdef HAVE_REGEX_H
  regex_t reg;
#endif

  ParserState state;
  GHashTable *dupCheck;
} SearchParseState;

static void searchStartDocument(SearchParseState *state) {
  state->rdf = state->description = state->name = state->summary = NULL;

  state->state = PARSER_START;
  state->nameData = g_string_sized_new(128);
  state->summaryData = g_string_sized_new(256);

  state->results = state->tail = NULL;

  /* we only bother with a hash table for dup checking when listing all
   * packages */
  if (!state->search)
    state->dupCheck = g_hash_table_new(g_str_hash, g_str_equal);

#ifdef HAVE_REGEX_H
  if (state->search) {
#ifdef REG_ICASE
# ifdef REG_NOSUB
    regcomp(&(state->reg), state->search, REG_ICASE|REG_NOSUB);
# else
    regcomp(&(state->reg), state->search, REG_ICASE);
# endif
#else
# ifdef REG_NOSUB
    regcomp(&(state->reg), state->search, REG_NOSUB);
# else
    regcomp(&(state->reg), state->search, 0);
# endif
#endif
  }
#endif
}
static void searchEndDocument(SearchParseState *state) {
  g_free(state->rdf);
  g_free(state->description);
  g_free(state->name);
  g_free(state->summary);
  g_string_free(state->nameData, TRUE);
  g_string_free(state->summaryData, TRUE);
  if (!state->search)
    g_hash_table_destroy(state->dupCheck);
  state->tail = NULL;

#ifdef HAVE_REGEX_H
  if (state->search)
    regfree(&(state->reg));
#endif
}
static void searchStartElement(SearchParseState *state, const CHAR *name,
			       const CHAR **attrs) {
  int i;

  switch (state->state) {
  case PARSER_START:
    for (i = 0; attrs[i] != NULL; i++) {
      const CHAR *aname = attrs[i++], *adata = attrs[i];
      if (!strncmp(aname, "xmlns:", 6)) {
	aname += 6;
	if (!strcmp(adata, "http://www.w3.org/TR/WD-rdf-syntax#")) {
	  g_free(state->rdf);
	  state->rdf = g_strconcat(aname, ":RDF", NULL);
	  g_free(state->description);
	  state->description = g_strconcat(aname, ":Description", NULL);
	} else if (!strcmp(adata, "http://www.rpm.org/")) {
	  g_free(state->name);
	  state->name = g_strconcat(aname, ":Name", NULL);
	  g_free(state->summary);
	  state->summary = g_strconcat(aname, ":Summary", NULL);
	}
      }
    }
    g_assert(state->rdf != NULL);
    g_assert(state->description != NULL);
    g_assert(state->name != NULL);
    g_assert(state->summary != NULL);
    if (strcmp(name, state->rdf))
      complain(_("Expected %s as toplevel element -- got %s"),state->rdf,name);

    state->state = PARSER_IN_RDF;
    break;
  case PARSER_IN_RDF:
    if (strcmp(name, state->description))
      complain(_("Was not expecting a %s element"), name);
    g_string_truncate(state->nameData, 0);
    g_string_truncate(state->summaryData, 0);
    state->state = PARSER_IN_DESCRIPTION;
    break;
  case PARSER_IN_DESCRIPTION:
    if (!strcmp(name, state->name)) {
      g_string_truncate(state->nameData, 0);
      state->state = PARSER_IN_NAME;
    } else if (!strcmp(name, state->summary)) {
      g_string_truncate(state->summaryData, 0);
      state->state = PARSER_IN_SUMMARY;
    } else
      complain(_("Was not expecting a %s element"), name);
    break;
  case PARSER_IN_NAME:
  case PARSER_IN_SUMMARY:
    complain(_("was not expecting a %s element"), name);
    break;
  }
}
static void searchEndElement(SearchParseState *state, const CHAR *name) {
  switch (state->state) {
  case PARSER_START:
    complain(_("This state should not have been reached"));
    break;
  case PARSER_IN_RDF:
    break;
  case PARSER_IN_DESCRIPTION:
    //    g_message("End description for %s", state->nameData->str);
    if (state->search) {
      regmatch_t pmatch;
      gchar *name = state->nameData->str;
      gchar *summary = state->summaryData->str;

      if (
#ifdef HAVE_REGEX_H
	  (name && !regexec(&(state->reg), name, 1, &pmatch, 0)) ||
	  (summary && !regexec(&(state->reg), summary, 1, &pmatch, 0)) ||
#elif defined(HAVE_STRSTR)
	  (name && strstr(name, state->search)) ||
	  (summary && strstr(summar, state->search)) ||
#else
#error "You need regex code or strstr for this to work"
#endif
	  FALSE)
	if (!g_list_find_custom(state->results, name, (GCompareFunc)strcmp)) {
	  if (state->tail) {
	    g_list_append(state->tail, g_strdup(name));
	    state->tail = g_list_last(state->tail);
	  } else
	    state->results = state->tail = g_list_append(NULL, g_strdup(name));
	}
    } else {
      if (!g_hash_table_lookup(state->dupCheck, state->nameData->str)) {
	gchar *key = g_strdup(state->nameData->str);

	if (state->tail) {
	  g_list_append(state->tail, key);
	  state->tail = g_list_last(state->tail);
	} else
	  state->results = state->tail = g_list_append(NULL, key);
	g_hash_table_insert(state->dupCheck, key, GINT_TO_POINTER(TRUE));
      }
    }
    state->state = PARSER_IN_RDF;
    break;
  case PARSER_IN_NAME:
  case PARSER_IN_SUMMARY:
    state->state = PARSER_IN_DESCRIPTION;
    break;
  }
}
static void searchCharacters(SearchParseState *state, const CHAR *chars,int len) {
  int i;

  switch (state->state) {
  case PARSER_IN_NAME:
    for (i = 0; i < len; i++)
      g_string_append_c(state->nameData, chars[i]);
    break;
  case PARSER_IN_SUMMARY:
    for (i = 0; i < len; i++)
      g_string_append_c(state->summaryData, chars[i]);
    break;
  default:
    break;
  }
}

static xmlEntityPtr searchGetEntity(SearchParseState *state, const CHAR *name) {
  return xmlGetPredefinedEntity(name);
}

static void searchComment(SearchParseState *state, const char *msg) {
  g_log("XML", G_LOG_LEVEL_MESSAGE, "%s", msg);
}

static void searchWarning(SearchParseState *state, const char *msg, ...) {
  va_list args;

  va_start(args, msg);
  g_logv("XML", G_LOG_LEVEL_WARNING, msg, args);
  va_end(args);
}

static void searchError(SearchParseState *state, const char *msg, ...) {
  va_list args;

  va_start(args, msg);
  g_logv("XML", G_LOG_LEVEL_CRITICAL, msg, args);
  va_end(args);
}

static void searchFatalError(SearchParseState *state, const char *msg, ...) {
  va_list args;

  va_start(args, msg);
  g_logv("XML", G_LOG_LEVEL_ERROR, msg, args);
  va_end(args);
}

static xmlSAXHandler searchSAXParser = {
   0, /* internalSubset */
   0, /* isStandalone */
   0, /* hasInternalSubset */
   0, /* hasExternalSubset */
   0, /* resolveEntity */
   (getEntitySAXFunc)searchGetEntity, /* getEntity */
   0, /* entityDecl */
   0, /* notationDecl */
   0, /* attributeDecl */
   0, /* elementDecl */
   0, /* unparsedEntityDecl */
   0, /* setDocumentLocator */
   (startDocumentSAXFunc)searchStartDocument, /* startDocument */
   (endDocumentSAXFunc)searchEndDocument, /* endDocument */
   (startElementSAXFunc)searchStartElement, /* startElement */
   (endElementSAXFunc)searchEndElement, /* endElement */
   0, /* reference */
   (charactersSAXFunc)searchCharacters, /* characters */
   0, /* ignorableWhitespace */
   0, /* processingInstruction */
   (commentSAXFunc)searchComment, /* comment */
   (warningSAXFunc)searchWarning, /* warning */
   (errorSAXFunc)searchError, /* error */
   (fatalErrorSAXFunc)searchFatalError, /* fatalError */
};

GList *aproposSearch(const gchar *search) {
  xmlParserCtxtPtr ctxt;
  SearchParseState state;
  char *filename = update_file("resources/fullIndex.rdf.gz");

  state.search = search;

  ctxt = xmlCreateFileParserCtxt(filename);
  g_free(filename);
  if (ctxt == NULL) return NULL;
  ctxt->sax = &searchSAXParser;
  ctxt->userData = &state;

  xmlParseDocument(ctxt);

  if (!ctxt->wellFormed) {
    g_list_foreach(state.results, (GFunc)g_free, NULL);
    g_list_free(state.results);
    state.results = NULL;
  }
  ctxt->sax = NULL;
  xmlFreeParserCtxt(ctxt);

  return g_list_sort(state.results, (GCompareFunc)g_strcasecmp);
}


GList *getFullPackageList(void) {
  return aproposSearch(NULL);
}

void freeApropos(GList *apropos) {
  g_list_foreach(apropos, (GFunc)g_free, NULL);
  g_list_free(apropos);
}

void freeRpmPackageAlternate(rpmPackageAlternate *alt) {
  if (alt->name) g_free(alt->name);
  if (alt->version) g_free(alt->version);
  if (alt->release) g_free(alt->release);
  if (alt->arch) g_free(alt->arch);
  if (alt->os) g_free(alt->os);
  if (alt->distribution) g_free(alt->distribution);
  if (alt->vendor) g_free(alt->vendor);
  if (alt->subdir) g_free(alt->subdir);
  if (alt->key) g_free(alt->key);
  g_free(alt);
}

static gboolean alternateCompatible(rpmPackageAlternate *alt) {
  gboolean sources=gnome_config_get_bool("/gnorpm/rpmfind/wantSources=false");

  if (sources) {
    if (!g_strcasecmp(alt->arch, "src"))
      return TRUE;
    else
      return FALSE;
  }
  /* nparch packages are always valid */
  if (!g_strcasecmp(alt->arch, "noarch"))
    return TRUE;
  if (g_strcasecmp(alt->os, guessOs()))
    return FALSE;
  if (!g_strcasecmp(alt->arch, guessArch()))
    return TRUE;
  return FALSE;
}

GList *alternateSearch(const char *resource) {
  char *tmp = g_strdup_printf("resources/%s.rdf", resource);
  char *file = url_get_to_temp(tmp);
  rdfSchema rdf;
  rdfNamespace rdfNs, rpmNs;
  rdfDescription desc;
  GList *ret = NULL;

  g_free(tmp);
  if (file == NULL) {
    complain(_("Couldn't get info for resource %s"), resource);
    return NULL;
  }
  rdf = rdfRead(file);
  unlink(file);
  g_free(file);
  if (rdf == NULL)
    return NULL;

  rdfNs = rdfGetNamespace(rdf, "http://www.w3.org/TR/WD-rdf-syntax#");
  if (rdfNs == NULL) {
    complain(_("%s is not an RDF schema"), resource);
    rdfDestroySchema(rdf);
    return NULL;
  }
  rpmNs = rdfGetNamespace(rdf, "http://www.rpm.org/");
  if (rpmNs == NULL) {
    complain(_("%s is not an RPM specific RDF schema"), resource);
    rdfDestroySchema(rdf);
    return NULL;
  }
  desc = rdfFirstDescription(rdf);
  if (desc == NULL) {
    complain(_("%s RDF schema seems empty"), resource);
    rdfDestroySchema(rdf);
    return NULL;
  }
  while (desc != NULL) {
    char *URL, *value = rdfGetDescriptionHref(rdf, desc);
    rpmPackageAlternate *alt;

    if (value != NULL) URL = value;
    else {
      complain(_("%s RDF schema invalid: description without href"), resource);
      rdfDestroySchema(rdf);
      freeAlternates(ret);
      return NULL;
    }
    
    alt = g_new0(rpmPackageAlternate, 1);
    alt->status = UNKNOWN_PACKAGE;
    rdfGetValue(desc, "Name", rpmNs, &value, NULL);
    if (value != NULL) {
      alt->name = g_strdup(value);
      free(value);
    } else {
      complain(_("%s RDF schema invalid: no Name"), resource);
      rdfDestroySchema(rdf);
      freeRpmPackageAlternate(alt);
      freeAlternates(ret);
      return NULL;
    }
    rdfGetValue(desc, "Version", rpmNs, &value, NULL);
    if (value != NULL) {
      alt->version = g_strdup(value);
      free(value);
    } else {
      complain(_("%s RDF schema invalid: no Version"), resource);
      rdfDestroySchema(rdf);
      freeRpmPackageAlternate(alt);
      freeAlternates(ret);
      return NULL;
    }
    rdfGetValue(desc, "Release", rpmNs, &value, NULL);
    if (value != NULL) {
      alt->release = g_strdup(value);
      free(value);
    } else {
      complain(_("%s RDF schema invalid: no Release"), resource);
      rdfDestroySchema(rdf);
      freeRpmPackageAlternate(alt);
      freeAlternates(ret);
      return NULL;
    }
    rdfGetValue(desc, "Arch", rpmNs, &value, NULL);
    if (value != NULL) {
      alt->arch = g_strdup(value);
      free(value);
    }
    rdfGetValue(desc, "Os", rpmNs, &value, NULL);
    if (value != NULL) {
      alt->os = g_strdup(value);
      free(value);
    }
    rdfGetValue(desc, "Distribution", rpmNs, &value, NULL);
    if (value != NULL) {
      alt->distribution = g_strdup(value);
      free(value);
    }
    rdfGetValue(desc, "Vendor", rpmNs, &value, NULL);
    if (value != NULL) {
      alt->vendor = g_strdup(value);
      free(value);
    }
    rdfGetValue(desc, "Date", rpmNs, &value, NULL);
    if (value != NULL) {
      alt->date = strtol(value, NULL, 0);
      free(value);
    }
    rdfGetValue(desc, "Size", rpmNs, &value, NULL);
    if (value != NULL) {
      alt->size = strtol(value, NULL, 0);
      free(value);
    }
    rdfGetValue(desc, "Subdir", rpmNs, &value, NULL);
    if (value != NULL) {
      alt->subdir = g_strdup(value);
      free(value);
    }

    /* a unique key for this alternate record */
    alt->key = g_strdup_printf("%s/%s-%s-%s", alt->subdir, alt->name,
				alt->version, alt->release);

    /* only list package if we could actualy install it on this system */
    if (alternateCompatible(alt))
      ret = g_list_append(ret, alt);
    else
      freeRpmPackageAlternate(alt);

    desc = rdfNextDescription(desc);
  }
  return ret;
}

/* generate a number based on a version number string that can be compared
 * against other such numbers to give a reasonable idea  of which package
 * is newer */
static guint normaliseVersion(const char *version) {
  gchar **components = g_strsplit(version, ".", 3);
  guint ret = 0;

  if (components[0] != NULL) {
    ret += strtol(components[0], NULL, 0) * 1000000;
    if (components[1] != NULL) {
      ret += strtol(components[1], NULL, 0) * 1000;
      if (components[2] != NULL)
	ret += strtol(components[2], NULL, 0);
    }
  }
  g_strfreev(components);
  return ret;
}

#define HIGH_RATING 1000
#define MED_RATING   500
#define LOW_RATING   100

guint alternateScore(rpmPackageAlternate *alt,
		     const char *preferredDist,
		     const char *preferredVendor) {
  gboolean version_only;
  guint score = 0;
  gint distrib_rating;

  distrib_rating = distribInfoGet(alt->subdir)->rating;
  if (distrib_rating < 0) return 0;
  score += distrib_rating;
  version_only =
    gnome_config_get_bool("/gnorpm/rpmfind/wantLatestVersion=false");
  if (version_only)
    return normaliseVersion(alt->version);
  else
    score += normaliseVersion(alt->version) / 1000;
  if (preferredDist && preferredVendor) {
    if (!strcmp(alt->vendor, preferredVendor)) {
      score += MED_RATING;
      if (!strcmp(alt->distribution, preferredDist))
	score += HIGH_RATING;
    }
    if (!strcmp(alt->vendor, guessVendor())) {
      score += LOW_RATING;
      if (!strcmp(alt->distribution, guessDistribution()))
	score += MED_RATING;
    }
  } else
    if (!strcmp(alt->vendor, guessVendor())) {
      score += MED_RATING;
      if (!strcmp(alt->distribution, guessDistribution()))
	score += HIGH_RATING;
    }

  /* maybe put the time check in here.  Is it really necessary? */
  /* file size weighting towards small rpms goes in here */

  return score;
}

/* XXX not thread safe, but how do I get the data in there? */
static struct _preferredInfo {
  const char *dist, *vendor;
} info;

static int alternate_compare(rpmPackageAlternate *a, rpmPackageAlternate *b) {
  gint score_a = alternateScore(a, info.dist, info.vendor);
  gint score_b = alternateScore(b, info.dist, info.vendor);

  if (score_a == score_b) return 0;
  if (score_a > score_b) return -1;
  return 1;
}

GList *sortAlternates(GList *alternates,
		      const char *preferredDist,
		      const char *preferredVendor) {
  info.dist = preferredDist;
  info.vendor = preferredVendor;
  return g_list_sort(alternates, (GCompareFunc)alternate_compare);
}

void freeAlternates(GList *alternates) {
  g_list_foreach(alternates, (GFunc)freeRpmPackageAlternate, NULL);
  g_list_free(alternates);
}

void statAlternates(rpmdb db, GList *alternates) {
  Header installed = NULL;
  GList *tmp;
  gchar *prevname = NULL;

  for (tmp = alternates; tmp; tmp = tmp->next) {
    rpmPackageAlternate *alt = tmp->data;

    /* a slight optimisation, since usually all alternates have the same name*/
    if (!prevname || strcmp(prevname, alt->name) != 0) {

#ifdef	HAVE_RPM_4_0
      rpmdbMatchIterator mi;
      Header tmp;

      if (installed) headerFree(installed);
      installed = NULL;
      mi = rpmdbInitIterator(db, RPMDBI_LABEL, alt->name, 0);
      while ((tmp = rpmdbNextIterator(mi)) != NULL) {
	if (!installed) installed = headerLink(tmp);
	else {
	  if (rpmVersionCompare(installed, tmp) < 0) {
	    headerFree(installed);
	    installed = headerLink(tmp);
	  }
	}
      }
      rpmdbFreeIterator(mi);
#else	/* HAVE_RPM_4_0 */
      dbiIndexSet matches;
    
      if (installed) headerFree(installed);
      installed = NULL;
      if (!rpmdbFindByLabel(db, alt->name, &matches)) {
	gint i;

	for (i = 0; i < matches.count; i++) {
	  gint index = matches.recs[i].recOffset;
	  Header tmp = rpmdbGetRecord(db, index);

	  if (!installed) installed = tmp;
	  else {
	    if (rpmVersionCompare(installed, tmp) < 0) {
	      headerFree(installed);
	      installed = tmp;
	    } else
	      headerFree(tmp);
	  }
	}
	dbiFreeIndexRecord(matches);
      }
#endif	/* HAVE_RPM_4_0 */

    }

    /* installed holds the header for the newest version of the package,
     * or is NULL if the package is not installed */
    if (!installed)
      alt->status = UNKNOWN_PACKAGE;
    else {
      gint res;
      Header this = headerNew();

      headerAddEntry(this, RPMTAG_NAME,    RPM_STRING_TYPE, alt->name, 1);
      headerAddEntry(this, RPMTAG_VERSION, RPM_STRING_TYPE, alt->version, 1);
      headerAddEntry(this, RPMTAG_RELEASE, RPM_STRING_TYPE, alt->release, 1);

      res = rpmVersionCompare(this, installed);
      if (res < 0)
	alt->status = OLD_PACKAGE;
      else if (res == 0)
	alt->status = CURRENT_PACKAGE;
      else
	alt->status = NEW_PACKAGE;
      headerFree(this);
    }
    prevname = alt->name;
  }
  if (installed) headerFree(installed);
}

void listAlternates(GList *alternates) {
  GList *tmp;

  g_print("Guess info:\nvendor=%s dist=%s os=%s arch=%s\n",
	  guessVendor(), guessDistribution(), guessOs(), guessArch());

  g_print("Number of alternates: %d\n", g_list_length(alternates));
  for (tmp = alternates; tmp; tmp = tmp->next) {
    rpmPackageAlternate *alt = tmp->data;
    g_print("%s-%s-%s.%s.%s.rpm  Score: %d\n", alt->name, alt->version,
	    alt->release, alt->arch, alt->os,
	    alternateScore(alt, NULL, NULL));
    g_print("  %s/%s  Subdir: %s\n", alt->vendor, alt->distribution,
	    alt->subdir);
    g_print("  size=%d date=%s\n", alt->size, ctime(&(alt->date)));
  }
}

rpmData *alternateGetInfo(rpmPackageAlternate *alt) {
#ifdef RPM_DATA_CACHE
  static GHashTable *cache = NULL;
  rpmData *ret;

  if (!cache)
    cache = g_hash_table_new(g_str_hash, g_str_equal);
  ret = g_hash_table_lookup(cache, alt->key);
  if (!ret) {
    ret = rpmOpenPackage(alt->subdir, alt->name, alt->version,
			 alt->release, alt->arch);
    if(ret!=NULL)
    {
      ret->nofree = TRUE;
      g_hash_table_insert(cache, g_strdup(alt->key), ret);
    }
  }
  return ret;
#else
  return rpmOpenPackage(alt->subdir, alt->name, alt->version,
			alt->release, alt->arch);
#endif
}
