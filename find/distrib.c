/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "distrib.h"
#include <libgnome/libgnome.h>
#include <stdlib.h>
#include <string.h>
#include "trans.h"
#include "guess.h"
#include "rdf_api.h"

extern ComplainFunc complain;
static GHashTable *distributions = NULL;
static GList *distNames = NULL;

static gint mirror_compare(gconstpointer a, gconstpointer b) {
  gint score_a = guessUrlScore(a);
  gint score_b = guessUrlScore(b);

  if (score_a == score_b) return 0;
  if (score_a > score_b) return -1;
  return 1;
}

static void fill_distrib_struct(rpmDistrib *distrib) {
  rdfSchema rdf;
  rdfNamespace rdfNs, rpmNs;
  rdfDescription desc;
  rdfBag mirrors;
  rdfElement mirror;
  char *file, *tmp, *value, *norm;

  /* convert /'s in name to _'s */
  norm = g_strdelimit(g_strdup(distrib->ID), "/",'_');
  tmp = g_strconcat("/gnorpm/dist_ratings/", norm, "=0", NULL);
  distrib->rating = gnome_config_get_int(tmp);
  g_free(tmp);
  tmp = g_strconcat("/gnorpm/preferred_mirrors/", norm, NULL);
  distrib->myMirror = gnome_config_get_string(tmp);
  g_free(tmp);

  tmp = g_strconcat("resources/distribs/", norm, ".rdf", NULL);

  g_free(norm);
  file = update_file(tmp);
  g_free(tmp);

  rdf = rdfRead(file);
  if (rdf == NULL) { g_free(file); return; }

  rdfNs = rdfGetNamespace(rdf, "http://www.w3.org/TR/WD-rdf-syntax#");
  if (rdfNs == NULL) {
    complain(_("XML file '%s' doesn't seem to be an RDF schema"), file);
    g_free(file);
    rdfDestroySchema(rdf);
    return;
  }
  rpmNs = rdfGetNamespace(rdf, "http://www.rpm.org/");
  if (rpmNs == NULL) {
    complain(_("RDF schema '%s' doesn't contain rpm namespace"), file);
    g_free(file);
    rdfDestroySchema(rdf);
    return;
  }
  desc = rdfFirstDescription(rdf);
  if (desc == NULL) {
    complain(_("no descriptions in RDF schema '%s'"), file);
    g_free(file);
    rdfDestroySchema(rdf);
    return;
  }

  rdfGetValue(desc, "ID", rpmNs, &value, NULL);
  if (value == NULL) {
    complain(_("RDF schema '%s' not valid: no ID"), file);
    g_free(file);
    rdfDestroySchema(rdf);
    return;
  }
  if (strcmp(distrib->ID, value)) {
    complain(_("RDF schema '%s' doesn't match ID %s"), file, distrib->ID);
    g_free(file);
    rdfDestroySchema(rdf);
    return;
  }
  free(value);

  rdfGetValue(desc, "Name", rpmNs, &value, NULL);
  if (value) {
    if (distrib->name) g_free(distrib->name);
    distrib->name = g_strdup(value);
    free(value);
  }
  rdfGetValue(desc, "Origin", rpmNs, &value, NULL);
  if (value) {
    if (distrib->origin) g_free(distrib->origin);
    distrib->origin = g_strdup(value);
    free(value);
  }
  rdfGetValue(desc, "Sources", rpmNs, &value, NULL);
  if (value) {
    if (distrib->sources) g_free(distrib->sources);
    distrib->sources = g_strdup(value);
    free(value);
  }
  rdfGetValue(desc, "Mirrors", rpmNs, NULL, &mirrors);
  if (mirrors != NULL)
    for (mirror=rdfFirstChild(mirrors); mirror; mirror=rdfNextElem(mirror)) {
      const char *name = rdfElemGetPropertyName(mirror);
      if (name != NULL && !strcmp(name, "Mirror") &&
	  rdfElemGetNamespace(mirror) == rpmNs) {
	value = rdfGetElementResource(rdf, mirror);
	if (value != NULL) {
	  distrib->mirrors = g_list_insert_sorted(distrib->mirrors,
						  g_strdup(value),
						  mirror_compare);
	  free(value);
	}
      } else
	complain(_("%s: malformed Mirrors bag"), file);
    }
  else
    complain(_("%s doesn't export any mirrors"), distrib->ID);

  /* set a default preferred mirror */
  if (!distrib->myMirror) {
    if (distrib->mirrors)
      distrib->myMirror = g_strdup((char *)distrib->mirrors->data);
    else if (distrib->origin)
      distrib->myMirror = g_strdup(distrib->origin);
  }

  rdfDestroySchema(rdf);
  g_free(file);
  distrib->filled = TRUE;
}

static void primeDistList(none) {
  static gboolean already_primed = FALSE;
  char *origin, *file, *ID;
  rdfSchema rdf;
  rdfNamespace rdfNs, rpmNs;
  rdfDescription desc;

  if (already_primed)
    return;

  distributions = g_hash_table_new(g_str_hash, g_str_equal);
  file = update_file("resources/distribs/list.rdf");
  rdf = rdfRead(file);
  if (rdf == NULL) { g_free(file); return; }

  rdfNs = rdfGetNamespace(rdf, "http://www.w3.org/TR/WD-rdf-syntax#");
  if (rdfNs == NULL) {
    complain(_("XML file '%s' doesn't seem to be an RDF schema"), file);
    g_free(file);
    rdfDestroySchema(rdf);
    return;
  }
  rpmNs = rdfGetNamespace(rdf, "http://www.rpm.org/");
  if (rpmNs == NULL) {
    complain(_("RDF schema '%s' doesn't contain rpm namespace"), file);
    g_free(file);
    rdfDestroySchema(rdf);
    return;
  }
  desc = rdfFirstDescription(rdf);
  if (desc == NULL) {
    complain(_("no descriptions in RDF schema '%s'"), file);
    g_free(file);
    rdfDestroySchema(rdf);
    return;
  }
  g_free(file);
  g_hash_table_freeze(distributions);
  while (desc != NULL) {
    rpmDistrib *distrib;

    origin = rdfGetDescriptionAbout(rdf, desc);
    if (origin == NULL) {
      complain(_("description without href"));
      rdfDestroySchema(rdf);
      return;
    }
    rdfGetValue(desc, "ID", rpmNs, &ID, NULL);
    if (ID == NULL) {
      complain(_("no ID for distrib href=%s"), origin);
      rdfDestroySchema(rdf);
      return;
    }
    distrib = g_new0(rpmDistrib, 1);
    distrib->ID = g_strdup(ID);
    free(ID);
    distrib->origin = g_strdup(origin);
    free(origin);

    distrib->filled = FALSE;
    /*fill_distrib_struct(distrib);*/

    distNames = g_list_append(distNames, distrib->ID);
    g_hash_table_insert(distributions, distrib->ID, distrib);
    desc = rdfNextDescription(desc);
  }
  g_hash_table_thaw(distributions);
  rdfDestroySchema(rdf);
}

GList *distribGetList(void) {
  if (!distNames)
    primeDistList();
  return distNames;
}

rpmDistrib *distribInfoGet(char *ID) {
  rpmDistrib *ret;
  char *tail, *head;

  if (!distributions)
    primeDistList();
  
  ret = g_hash_table_lookup(distributions, ID);
  if (ret) {
    if (!ret->filled)
      fill_distrib_struct(ret);
    return ret;
  }

  /* tail points to end of list */
  tail = &ID[strlen(ID)-1];

  while (tail > ID) {
    while (tail > ID && tail[0] != '/')
      tail--;
    head = g_strndup(ID, (tail-ID));
    ret = g_hash_table_lookup(distributions, head);
    g_free(head);
    if (ret) {
      if (!ret->filled)
	fill_distrib_struct(ret);
      return ret;
    }
    tail--;
  }
  
  ret = g_new0(rpmDistrib, 1);
  ret->ID = g_strdup(ID);
  g_hash_table_insert(distributions, ret->ID, ret);
  return ret;
}

void distribInfoShow(char *ID) {
  rpmDistrib *distrib = distribInfoGet(ID);
  GList *tmp;
  
  g_print("Info for distribution %s:\n", ID);
  if (distrib->name)
    g_print("Name:    %s\n", distrib->name);
  if (distrib->origin)
    g_print("Origin:  %s\n", distrib->origin);
  if (distrib->sources)
    g_print("Sources: %s\n", distrib->sources);
  g_print("Rating:  %d\n", distrib->rating);
  g_print("Mirrors:\n");
  for (tmp = distrib->mirrors; tmp; tmp = tmp->next)
    g_print("  %d: %s\n", guessUrlScore(tmp->data), (char *)tmp->data);
  if (distrib->myMirror)
    g_print("Favourit mirror: %s\n", distrib->myMirror);
  g_print("\n");
}

void distribSetRating(char *ID, int rating) {
  rpmDistrib *distrib = distribInfoGet(ID);
  char *norm = g_strdelimit(g_strdup(distrib->ID), "/",'_');
  char *key = g_strconcat("/gnorpm/dist_ratings/", norm, NULL);

  g_free(norm);
  distrib->rating = rating;
  gnome_config_set_int(key, rating);
  gnome_config_sync();
  g_free(key);
}

void distribSetMirror(char *ID, const char *mirror) {
  rpmDistrib *distrib = distribInfoGet(ID);
  char *norm = g_strdelimit(g_strdup(distrib->ID), "/",'_');
  char *key = g_strconcat("/gnorpm/preferred_mirrors/", norm, NULL);

  g_free(norm);
  if (distrib->myMirror) g_free(distrib->myMirror);
  distrib->myMirror = g_strdup(mirror);
  gnome_config_set_string(key, mirror);
  gnome_config_sync();
  g_free(key);
}

GList *metadataGetList(void) {
  static GList *list = NULL;
  char *file;
  rdfSchema rdf;
  rdfNamespace rdfNs, rpmNs;
  rdfDescription desc;
  
  if (list)
    return list;

  file = update_file("resources/distribs/metadata.rdf");
  if (file == NULL) {
    complain(_("Couldn't grab metadata server list"));
    return NULL;
  }
  rdf = rdfRead(file);
  g_free(file);
  if (rdf == NULL) return NULL;
  rdfNs = rdfGetNamespace(rdf, "http://www.w3.org/TR/WD-rdf-syntax#");
  if (rdfNs == NULL) {
    complain(_("metadata.rdf is not an RDF schema"));
    rdfDestroySchema(rdf);
    return NULL;
  }
  rpmNs = rdfGetNamespace(rdf, "http://www.rpm.org/");
  if (rdfNs == NULL) {
    complain(_("Metadata.rdf is not an RPM specific RDF schema"));
    rdfDestroySchema(rdf);
    return NULL;
  }
  desc = rdfFirstDescription(rdf);
  if (desc == NULL) {
    complain(_("Metadata.rdf seems to be empty"));
    rdfDestroySchema(rdf);
    return NULL;
  }
  while (desc != NULL) {
    char *URI, *origin = rdfGetDescriptionAbout(rdf, desc);

    if (origin == NULL) {
      complain(_("Metadata.rdf schema is invalid: description without href"));
      rdfDestroySchema(rdf);
      return NULL;
    }
    rdfGetValue(desc, "URI", rpmNs, &URI, NULL);
    if (URI == NULL) {
      complain(_("Metadata.rdf schema is invalid: no URI"));
      rdfDestroySchema(rdf);
      return NULL;
    }
    list = g_list_append(list, g_strdup(URI));
    free(origin);
    free(URI);

    desc = rdfNextDescription(desc);
  }
  rdfDestroySchema(rdf);

  /* if no metadata servers are listed, also list the primary rpmfind server.
   */
  if (!list)
    list = g_list_append(list, g_strdup("http://rufus.w3.org/linux/RDF"));

  return list;
}
