/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef SEARCH_H
#define SEARCH_H

#include <glib.h>
#include <sys/types.h>
#include <rpmlib.h>
#include "rpmdata.h"

/* returns the complete list of packages in the rpmfind system.  Do not free
 * the result.  The result is cached, and gets calculated the first time you
 * call this function, or aproposSearch. */
GList *getFullPackageList(void);

/* returns a list of strings.  The return value should be freed
 * with freeApropos. */
GList *aproposSearch(const char *search);
void freeApropos(GList *apropos);

typedef struct _rpmPackageAlternate {
  char *name;
  char *version;
  char *release;
  char *arch;
  char *os;
  char *distribution;
  char *vendor;
  time_t date;
  gint32 size;
  char *subdir;

  char *key;  /* key = $subdir/$name-$version-$release */
  enum { UNKNOWN_PACKAGE, CURRENT_PACKAGE, OLD_PACKAGE, NEW_PACKAGE } status;
} rpmPackageAlternate;

/* free a single rpmPackageAlternate structure */
void freeRpmPackageAlternate(rpmPackageAlternate *alt);

/* find out what alternatives are available for a given package name.
 * The return is a list of rpmPackageAlternate structures */
GList *alternateSearch(const char *resource);
void freeAlternates(GList *alternates);

/* attempt to rate a given distribution.  You can push ratings in a
 * certain direction by giving the prefered dist/vendor pair.
 * It will also use the systems dist/vendor pair and the distribution's
 * rating. */
guint alternateScore(rpmPackageAlternate *alt,
		     const char *preferedDist,
		     const char *preferedVendor);

/* Sort a list of alternatives using the given prefered dist/vendor pair.
 * note that this function is not currently thread safe :( */
GList *sortAlternates(GList *alternates,
		      const char *preferredDist,
		      const char *preferredVendor);

/* Fill the status field of each alternative with information from the db */
void statAlternates(rpmdb db, GList *alternatives);

/* display the list of alternates on the screen (for debugging) */
void listAlternates(GList *alternates);

/* get the full info on a particular alternate */
rpmData *alternateGetInfo(rpmPackageAlternate *alt);

#endif
