/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "trans.h"
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <stdarg.h>

#include <gnome.h>
#include <ghttp.h>
#include "ftp.h"

#define USER_AGENT_STRING "Gnome-RPM/" VERSION" libghttp/unknown"

UrlStartFunc downloadStart = NULL;
UrlProgressFunc downloadProgress = NULL;
UrlDoneFunc downloadDone = NULL;
int downloadAbort = 0;
ComplainFunc complain = NULL;

void set_complain_func(ComplainFunc func) {
	complain = func;
}

void url_set_callbacks(UrlStartFunc start, UrlProgressFunc progress,
		       UrlDoneFunc done) {
  downloadStart = start;
  downloadProgress = progress;
  downloadDone = done;
}

static gboolean url_split(char *url, char **prot, char **host, int *port,
		     char **path) {
  char *ptr;
  char *myprot, *myhost;
  int myport = 21;

  ptr = strchr(url, ':');
  if (!ptr || ptr[1] != '/' || ptr[2] != '/')
    return FALSE;
  myprot = g_strndup(url, ptr - url);

  myhost = ptr + 3;
  ptr = strchr(myhost, ':');
  if (ptr) {
    myhost = g_strndup(myhost, ptr - myhost);
    myport = strtol(ptr, &ptr, 0);
    if (ptr[0] != '/') {
      g_free(myprot);
      g_free(myhost);
      return FALSE;
    }
  } else if ((ptr = strchr(myhost, '/')))
    myhost = g_strndup(myhost, ptr - myhost);
  else {
    g_free(myprot);
    return FALSE;
  }

  if (prot) *prot = myprot;
  else      g_free(myprot);
  if (host) *host = myhost;
  else      g_free(myhost);
  if (port) *port = myport;
  if (path) *path = g_strdup(ptr);
  return TRUE;
}

static int http_transfer(char *url, FILE *file, char *proxy) {
  int length = -1, i;
  ghttp_request *request = NULL;
  ghttp_status status;
  char *body = NULL;

  request = ghttp_request_new();
  if (!request)
    goto err_occured;
  if (proxy && ghttp_set_proxy(request, proxy) != 0) {
    complain(_("Setting of proxy failed"));
    g_free(proxy);
    goto err_occured;
  }
  if (proxy) {
    gchar *user, *pass;
    g_free(proxy);
    user = gnome_config_get_string("/gnorpm/rpmfind/proxy-user=");
    pass = gnome_config_get_string("/gnorpm/rpmfind/proxy-pass");
    if (pass)
      for (i = 0; pass[i] != '\0'; i++)
	pass[i] ^= 0x42;
    if (user[0] != '\0' && ghttp_set_proxy_authinfo(request, user, pass)) {
      complain(_("Setting proxy user/password pair failed"));
      g_free(user);
      g_free(pass);
      goto err_occured;
    }
    g_free(user);
    g_free(pass);
  }
  if (ghttp_set_uri(request, url) != 0) {
    complain(_("Setting of uri failed"));
    goto err_occured;
  }
  ghttp_set_header(request, http_hdr_Connection, "close");
  ghttp_set_header(request, http_hdr_User_Agent, USER_AGENT_STRING);
  if (ghttp_prepare(request) != 0) {
    complain(_("Preparing request failed"));
    goto err_occured;
  }
  if (ghttp_set_sync(request, ghttp_async)) {
    complain(_("Couldn't set async mode"));
    goto err_occured;
  }
  
  while ((status = ghttp_process(request)) == ghttp_not_done) {
    ghttp_current_status curStat = ghttp_get_status(request);
    if (downloadProgress && !downloadAbort)
      (*downloadProgress)(curStat.bytes_read, curStat.bytes_total);
    if (downloadAbort)
    {
	complain(_("User aborted transfer"));
	status = ghttp_error;
        break;
    }
  }
  if (status == ghttp_error) {
    complain(_("Processing request failed"));
    goto err_occured;
  }
  if (ghttp_status_code(request) != 200) {
    complain(_("HTTP error: %d %s"), ghttp_status_code(request),
	      ghttp_reason_phrase(request));
    goto err_occured;
  }
  length = ghttp_get_body_len(request);
  body = ghttp_get_body(request);
  if (body != NULL)
    fwrite(body, length, 1, file);
  else
    g_warning("Aarrrgh -- body == NULL");

 err_occured:
  if (request)
    ghttp_request_destroy(request);
  return length;
}

static int ftp_transfer(char *host, int port, char *path, FILE *file) {
  int ftpconn, fd;
  int res;

  ftpconn = ftpOpen(host, "anonymous", "gnorpm@", NULL, port);
  if (ftpconn < 0) {
    complain(_("Couldn't connect to FTP server: %s"), ftpStrerror(ftpconn));
    return -1;
  }
  fd = fileno(file);

  res = ftpGetFile(ftpconn, path, fd);
  if (res < 0) {
    complain(_("FTP transfer failed: %s"), ftpStrerror(ftpconn));
    res = -1;
  }

  ftpClose(ftpconn);
  return res;
}

/* writes the information stored at the given url to the given file */
int url_get_to_file(char *url, FILE *file) {
  char *prot, *host, *path;
  int res = -1, port;
  static int busy=0;

  complain(_("Downloading %s"), url);

  if(busy)
  {
	complain(_("Already busy doing a transfer"));
	return -1;
  }
  busy=1;
  downloadAbort=0;

  if (downloadStart)
    (*downloadStart)(url);
  if (!url_split(url, &prot, &host, &port, &path)) {
    complain(_("Invalid URL"));
    busy=0;
    return -1;
  }
  if (!g_strcasecmp(prot, "http")) {
    char *proxy = gnome_config_get_string("/gnorpm/rpmfind/http-proxy=");

    if (proxy[0] == '\0') {
      g_free(proxy);
      proxy = NULL;
    }
    res = http_transfer(url, file, proxy);
  } else if (!g_strcasecmp(prot, "ftp")) {
    char *proxy = gnome_config_get_string("/gnorpm/rpmfind/ftp-proxy=");
  
    if (proxy[0] == '\0') {
      g_free(proxy);
      proxy = NULL;
    }
    if (proxy)
      res = http_transfer(url, file, proxy);
    else
      res = ftp_transfer(host, port, path, file);
  } else
    complain(_("Unsupported URL format '%s'"), prot);
  g_free(prot);
  g_free(host);
  g_free(path);
  if (downloadDone)
    (*downloadDone)();
  busy=0;
  return res;
}

/* convert a relative pathname to the full pathname, on the user's selected
 * server. */
static char *remote_file(char *relative) {
  char *server, *ret;

  if(access("/etc/redhat-release",0)==0)
    server = gnome_config_get_string(
     		"/gnorpm/rpmfind/server=http://www.redhat.com/RDF");
  else
    server = gnome_config_get_string(
		"/gnorpm/rpmfind/server=http://rufus.w3.org/linux/RDF");
  ret = g_strconcat(server, "/", relative, NULL);
  g_free(server);
  return ret;
}

/* make sure that the given file is up-to-date.  The filename is relative to
 * the rpmfind root you are using (default http://rufus.w3.org/linux/RDF).
 * The local filename is returned (it should be g_free'd).  If the local
 * copy is too old, download a new version */
char *update_file(char *filename) {
  char *localfile, *tmp;
  char *workfile;
  /* maximum age of cached file in seconds */
  int max_age = gnome_config_get_int("/gnorpm/rpmfind/expire=14") * 86400;
  struct stat info;

  /* get the local filename.  In shell syntax, it will be:
   * "$HOME/.gnome/gnorpm.d/$filename" */
  tmp = g_strconcat("gnorpm.d/", filename, NULL);
  localfile = gnome_util_home_file(tmp);
  g_free(tmp);

  tmp = g_strconcat("gnorpm.d/", filename, ".download", NULL);
  workfile = gnome_util_home_file(tmp);
  g_free(tmp);

  unlink(workfile);

  /* if local file doesn't exist, or is too old, reget it. */
  if (!g_file_exists(localfile) || 
      (!stat(localfile, &info) && info.st_mtime + max_age < time(NULL))) {
    FILE *fd = fopen(workfile, "wb");
    char *url = remote_file(filename);
    int len;
        
    /*printf("Getting '%s'\n", url);*/
    
    len = url_get_to_file(url, fd);

    fclose(fd);
    g_free(url);
    if (len <= 0) {
      complain(_("Couldn't get data"));
      unlink(localfile);
      g_free(localfile);
      g_free(workfile);
      return NULL;
    }
    if(rename(workfile,localfile)<0)
    {
	complain(_("Couldn't rename workfile to '%s'"), localfile);
	unlink(localfile);
        g_free(workfile);
	g_free(localfile);
	return NULL;
    }
    
  }
  g_free(workfile);
  return localfile;
}

char *url_get_to_temp(char *relative) {
  char *template = gnome_util_home_file("gnorpm.d/fetchXXXXXX");
  char *url = remote_file(relative);
  FILE *fd;
  int len;

  template = tmpnam(template);
  fd = fopen(template, "wb");
  len = url_get_to_file(url, fd);
  g_free(url);
  fclose(fd);
  
  if (len <= 0) {
    complain(_("Couldn't get data"));
    unlink(template);
    g_free(template);
    return NULL;
  }
  return template;
}

char *url_download_file(char *url) {
  char *dir = gnome_config_get_string("/gnorpm/rpmfind/rpm-dir");
  char *filename;
  FILE *fp;
  int len;

  if (!g_file_exists(dir) && mkdir(dir, 0755))
    complain(_("couldn't create directory %s"), dir);

  filename = g_strconcat(dir, "/", g_basename(url), ".tmp", NULL);
  g_free(dir);
  fp = fopen(filename, "wb");
  len = url_get_to_file(url, fp);
  fclose(fp);
  if (len <= 0) {
    complain(_("Couldn't get data at %s"), url);
    unlink(filename);
    g_free(filename);
    return NULL;
  }
  return filename;
}

/* create directories needed to store local copies of some metadata */
void cache_create_dirs(void) {
  char *dir;

  dir = gnome_util_home_file("gnorpm.d");
  if (!g_file_exists(dir) && mkdir(dir, 0755))
    complain(_("Couldn't create directory %s"), dir);
  g_free(dir);

  dir = gnome_util_home_file("gnorpm.d/resources");
  if (!g_file_exists(dir) && mkdir(dir, 0755))
    complain(_("Couldn't create directory %s"), dir);
  g_free(dir);
  dir = gnome_util_home_file("gnorpm.d/resources/distribs");
  if (!g_file_exists(dir) && mkdir(dir, 0755))
    complain(_("Couldn't create directory %s"), dir);
  g_free(dir);

  /* make sure we have the rpm-dir config entry set up */
  dir = gnome_config_get_string("/gnorpm/rpmfind/rpm-dir");
  if (!dir) {
    dir = gnome_util_prepend_user_home("rpms");
    gnome_config_set_string("/gnorpm/rpmfind/rpm-dir", dir);
  }
  g_free(dir);
}
