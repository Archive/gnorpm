/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef HTTP_H
#define HTTP_H

#include <stdio.h>
#include <gnome.h>

/* put the data stored at url into the file fd.  Return the length of the
 * contents, or -1 for error */
int url_get_to_file(char *url, FILE *fd);

/* make sure that the local copy of file is up to date with that off the
 * rpmfind server.  Currently this just works on a simple expiration
 * time system.  The filename should be relative to the RDF database
 * root.  The returned filename is the local copy, and should be g_free'd
 * when you are finished with it.  NULL is returned for error */
char *update_file(char *filename);

/* get the given url (relative to RDF root) to a temporary file.  The
 * temp filename is returned (must be g_free'd), or NULL on an error.
 * It is the callers reponsibility to unlink() the file when finished
 * with it. */
char *url_get_to_temp(char *filename);

/* download the given url to the rpms directory for this user */
char *url_download_file(char *url);

/* setup local directories for downloaded files.  Currently these are:
 *   $HOME/.gnome/gnorpm.d
 *   $HOME/.gnome/gnorpm.d/resources
 *   $HOME/.gnome/gnorpm.d/resources/distribs
 * This function should be called before update_file is called() */
void cache_create_dirs(void);

typedef void (*UrlStartFunc)(char *url);
typedef void (*UrlProgressFunc)(unsigned long done, unsigned long total);
typedef void (*UrlDoneFunc)(void);

/* set callbacks for downloading */
void url_set_callbacks(UrlStartFunc start, UrlProgressFunc progress,
		       UrlDoneFunc done);

/* Signature for error callback function */
typedef void (*ComplainFunc)(char *, ...);

void set_complain_func(ComplainFunc func);

#endif
