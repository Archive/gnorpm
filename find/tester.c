/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <libgnome/libgnome.h>
#include "libfind.h"
#include <rpmlib.h>
#include <fcntl.h>
#include <unistd.h>

/*
static void cb(gpointer data, gpointer user_data) {
  g_print("%s\n", (char *)data);
}
*/

int main(int argc, char *argv[]) {
  rpmdb db;

  gnomelib_init("tester", "0.0");
  rpmReadRC(NULL);
  rpmdbOpen(NULL, &db, O_RDONLY, 0644);
  guessInit(db);
  cache_create_dirs();
  update_file("resources/fullIndex.rdf.gz");
  update_file("resources/distribs/metadata.rdf");
  update_file("resources/distribs/list.rdf");
  update_file("resources/distribs/redhat_5.1_i386.rdf");
  /*distribInfoShow("redhat/5.1/i386");*/
  /*g_list_foreach(aproposSearch("pixmap"), cb, NULL);*/
  /*rpmDataShow(rpmOpenPackage("gnome/GNOME/redhat/0.99.1/i386", "gnome-libs",
    "0.99.1", "1", "i386")); */
  /*{
    GList *alternates = alternateSearch("libqt.so.1");
    alternates = sortAlternates(alternates, NULL, NULL);
    listAlternates(alternates);
    g_print("\nBestGuess:\n");
    rpmDataShow(alternateGetInfo(alternates->data));
    freeAlternates(alternates);
    }*/
  /*g_list_foreach(resolveName(argc>1?argv[1]:"gnome-libs", db), cb, NULL);*/
  /*{
    FILE *fp = fopen("tmp.ftp", "wb");
    url_get_to_file("ftp://home.daa.com.au/etc/passwd", fp);
    fclose(fp);
    }*/
  printf("downloadfile: %s\n", url_download_file("ftp://ftp.daa.com.au/pub/james/python/pygtk-0.5.10.README"));
}



