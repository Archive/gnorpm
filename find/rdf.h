/*
 * rdf.h : interfaces for the RDF encoding/decoding of RPM informations.
 *
 * See Copyright for the status of this software.
 *
 * $Id$
 */

#ifndef __INCLUDE_RDF_H__
#define __INCLUDE_RDF_H__

#include "rpmdata.h"

/* open an RDF schema that describes an rpm */
rpmData *rpmOpenRdfFile(char *file);
/* open the given RDF schema off the metadata server */
rpmData *rpmOpenPackage(char *distID, char *name, char *version,
			char *release, char *arch);

/* get the URL where this particular package can be downloaded from.
 * The result should be g_free'd */
char *rpmDataGetURL(rpmData *rpm);

/* free an rpmData structure */
void rpmDataFree(rpmData *rpm);
/* display information about an rpmData structure */
void rpmDataShow(rpmData *rpm);

#endif /* __INCLUDE_RDF_H__ */
