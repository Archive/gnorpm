/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef DISTRIB_H
#define DISTRIB_H

#include <glib.h>

typedef struct _rpmDistrib rpmDistrib;

struct _rpmDistrib {
  char *ID;
  int rating;
  char *name;
  char *origin;
  char *sources;
  char *myMirror;
  GList *mirrors; /* a sorted list of mirrors for this distribution */
  gboolean filled; /* for internal use only ... */
};

/* do not free the return of any of these functions */

/* get a list of all the known distributions */
GList *distribGetList(void);
/* get information about this distribution */
rpmDistrib *distribInfoGet(char *ID);
/* display info on a distribution */
void distribInfoShow(char *ID);

/* set info about a particular mirror */
void distribSetRating(char *ID, int rating);
void distribSetMirror(char *ID, const char *mirror);

/* get a list of the metadata servers */
GList *metadataGetList(void);

#endif
