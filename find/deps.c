/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "deps.h"
#include <string.h>
#include <libgnome/libgnome.h>

#include "rdf.h"
#include "trans.h"

extern ComplainFunc complain;

typedef enum {
  PKG_NOT_TESTED=0, /* no tests have been performed */
  PKG_INSTALLED,    /* package is already on system */
  PKG_DOWNLOAD,     /* the package has been scheduled for download */
  PKG_NOT_FOUND     /* package was not found */
} PackageStatus;

/* holds state information useful during a deps check */
typedef struct _DepsCheck DepsCheck;
struct _DepsCheck {
  GHashTable *pkg_stats; /* status of looked up packages */
  GHashTable *datas;     /* rpmData structures, for speed */
  GNode *deps;           /* rpmData structures in dependency tree */
  gboolean sources, latest;

  rpmdb db;
};

/* do a check to see if a particular requirement is provided localy */
static gboolean haveLocalProvide(rpmdb db, char *name) {
#ifndef	HAVE_RPM_4_0
  dbiIndexSet matches;
#endif

  if (name[0] == '/' && g_file_exists(name))
    return TRUE;
  else
#ifdef	HAVE_RPM_4_0
  { rpmdbMatchIterator mi;
    Header h;

    mi = rpmdbInitIterator(db, RPMTAG_PROVIDENAME, name, 0);
    while ((h = rpmdbNextIterator(mi)) != NULL) {
      rpmdbFreeIterator(mi);
      return TRUE;
    }
    rpmdbFreeIterator(mi);
    
    mi = rpmdbInitIterator(db, RPMDBI_LABEL, name, 0);
    while ((h = rpmdbNextIterator(mi)) != NULL) {
      rpmdbFreeIterator(mi);
      return TRUE;
    }
    rpmdbFreeIterator(mi);

  }
#else	/* HAVE_RPM_4_0 */
  if (rpmdbFindByProvides(db, name, &matches) == 0) {
    int i;

    for (i = 0; i < matches.count; i++) {
      Header h = rpmdbGetRecord(db, matches.recs[i].recOffset);
      if (h) {
	headerFree(h);
	dbiFreeIndexRecord(matches);
	return TRUE;
      }
    }
    dbiFreeIndexRecord(matches);
  } else {
    int res = rpmdbFindByLabel(db, name, &matches);
    if (res == 1) {
      /* nothing */
    } else if (res == 2)
      complain(_("Error looking up rpm database for %s"), name);
    else {
      int i;
      
      for (i = 0; i < matches.count; i++) {
	Header h = rpmdbGetRecord(db, matches.recs[i].recOffset);
	if (h) {
	  headerFree(h);
	  dbiFreeIndexRecord(matches);
	  return TRUE;
	}
      }
      dbiFreeIndexRecord(matches);
    }
  }
#endif	/* HAVE_RPM_4_0 */
  return FALSE;
}

struct TraverseData {
  char *name;
  gboolean ret;
};

static gboolean checkProvide(GNode *node,
			     struct TraverseData *data) {
  rpmData *rpm = node->data;
  GList *tmp;

  if (!rpm) return FALSE;

  if (!strcmp(rpm->name, data->name)) {
    data->ret = TRUE;
    return TRUE;
  }
  for (tmp = rpm->resources; tmp; tmp = tmp->next)
    if (!strcmp(tmp->data, data->name)) {
      data->ret = TRUE;
      return TRUE;
    }
  return FALSE;
}

/* return TRUE if all the dependencies for the package can be met.  If
 * TRUE, the deps node should have its data member set to the rpmData
 * structure for this package, and add child nodes for its dependencies
 * (making sure they aren't already provided).  Otherwise it shouldn't
 * add children to deps alone and return FALSE. */
static gboolean getRequires(rpmPackageAlternate *alt, DepsCheck *dc,
			    GNode *deps) {
  rpmData *rpm;
  GList *dep;
  struct TraverseData trav_data = {NULL, FALSE};

  /*g_print("Checking requires for package %s\n", alt->key);*/
  rpm = g_hash_table_lookup(dc->datas, alt->key);
  if (!rpm) {
    rpm = alternateGetInfo(alt);
    if (rpm == NULL)
      return FALSE;
    g_hash_table_insert(dc->datas, g_strdup(alt->key), rpm);
  }
  /* sources don't depend on anything */
  if (dc->sources)
    return TRUE;
  /* if this RPM already occurs in the dependency tree, return a satisfied
   * dependency */
  trav_data.name = rpm->name;
  g_node_traverse(dc->deps, G_IN_ORDER, G_TRAVERSE_ALL, -1,
		  (GNodeTraverseFunc)checkProvide, &trav_data);
  if (trav_data.ret)
    return TRUE;

  /*g_print("  Alright -- do the deps stuff now\n");*/

  deps->data = rpm;

  /*g_print("Num resources: %d\n", g_list_length(rpm->resources));*/
  for (dep = rpm->requires; dep; dep = dep->next) {
    char *name = dep->data;
    PackageStatus res =
      GPOINTER_TO_UINT(g_hash_table_lookup(dc->pkg_stats, name));
    GNode *tmp_node;

    /*g_print("  Resolving dependency %s\n", name);*/
    switch (res) {
    case PKG_NOT_FOUND:
      /* the package has previously been looked up and couldn't be found */
      /*g_print("  Previous lookup gave failure\n");*/
      for (tmp_node = g_node_first_child(deps); tmp_node;
	   tmp_node = g_node_next_sibling(tmp_node))
	g_node_destroy(tmp_node);
      return FALSE;
    case PKG_INSTALLED:
      /* the package is already on the local computer */
      /*g_print("  Previous lookup found package was already installed\n");*/
      continue; /* tp next dep */
    case PKG_DOWNLOAD:
    case PKG_NOT_TESTED:
      if (haveLocalProvide(dc->db, name)) {
	g_hash_table_insert(dc->pkg_stats, g_strdup(name),
			    GUINT_TO_POINTER(PKG_INSTALLED));
	/*g_print("  Found package on local maching\n");*/
	continue;
      } else if (dontUpgrade(name) ||
	  !strcmp(name, "glibc") || !strncmp(name, "glibc.so", 8) ||
	  !strcmp(name, "libc") || !strncmp(name, "libc.so", 7)) {
	g_hash_table_insert(dc->pkg_stats, g_strdup(name),
			    GUINT_TO_POINTER(PKG_NOT_FOUND));
	  
	for (tmp_node = g_node_first_child(deps); tmp_node;
	     tmp_node = g_node_next_sibling(tmp_node))
	  g_node_destroy(tmp_node);
	/*g_print("  Not allowed to do network install of %s\n", name);*/
	return FALSE;
      } else {
	GList *alts = alternateSearch(name), *tmp;
	GNode *child_deps = g_node_append_data(deps, NULL);
	/* sort the alternates */
	alts = sortAlternates(alts, rpm->distribution, rpm->vendor);
	for (tmp = alts; tmp; tmp = tmp->next)
	  if (getRequires(tmp->data, dc, child_deps))
	    break;
	/*g_print("  satisfied by %s\n",((rpmPackageAlternate *)tmp->data)->key);*/
	freeAlternates(alts);
	if (!tmp) {
	  g_hash_table_insert(dc->pkg_stats, g_strdup(name),
			      GUINT_TO_POINTER(PKG_NOT_FOUND));
	  
	  for (tmp_node = g_node_first_child(deps); tmp_node;
	       tmp_node = g_node_next_sibling(tmp_node))
	    g_node_destroy(tmp_node);
	  /*g_print("  Couldn't resolve dependency %s from network\n", name);*/
	  return FALSE;
	}
	/* we must have found an acceptable match */
	g_hash_table_insert(dc->pkg_stats, g_strdup(name),
			    GUINT_TO_POINTER(PKG_DOWNLOAD));
	continue;
      }
    }
  }
  return TRUE;
}

static gboolean collect_urls(GNode *node, GList **urls) {
  rpmData *rpm = node->data;
  char *url;
  GList *tmp;
  
  if (!rpm) return FALSE;
  url = rpmDataGetURL(rpm);
  for (tmp = *urls; tmp; tmp = tmp->next)
    if (!strcmp(url, tmp->data))
      break;
  if (!tmp)
    *urls = g_list_prepend(*urls, url);
  else
    g_free(url);
  return FALSE;
}

static gboolean clean_pkg_stats(char *key, PackageStatus status) {
  g_free(key);
  return TRUE;
}
static gboolean clean_datas(char *key, rpmData *data) {
  g_free(key);
  rpmDataFree(data);
  return TRUE;
}

GList *resolveName(char *name, rpmdb db) {
  DepsCheck dc;
  GList *alts, *tmp, *ret = NULL;
  gboolean sources=gnome_config_get_bool("/gnorpm/rpmfind/wantSources=false");
  gboolean latest =
    gnome_config_get_bool("/gnorpm/rpmfind/wantLatestVersion=false");
  gboolean res = FALSE;

  if (!latest && haveLocalProvide(db, name))
    return NULL;
  dc.pkg_stats = g_hash_table_new(g_str_hash, g_str_equal);
  dc.datas = g_hash_table_new(g_str_hash, g_str_equal);
  dc.deps = g_node_new(NULL);
  dc.db = db;
  dc.sources = sources;
  dc.latest = latest;
  
  alts = alternateSearch(name);
  alts = sortAlternates(alts, NULL, NULL);
  for (tmp = alts; tmp; tmp = tmp->next) {
    if ((res = getRequires(tmp->data, &dc, dc.deps)) == TRUE)
      break;
  }
  freeAlternates(alts);

  /* flatten deps here and produce URL list, and remove duplicates */
  if (res)
    g_node_traverse(dc.deps, G_POST_ORDER, G_TRAVERSE_ALL, -1,
		    (GNodeTraverseFunc)collect_urls, &ret);

  g_hash_table_foreach_remove(dc.pkg_stats, (GHRFunc)clean_pkg_stats, NULL);
  g_hash_table_destroy(dc.pkg_stats);
  g_hash_table_foreach_remove(dc.datas, (GHRFunc)clean_datas, NULL);
  g_hash_table_destroy(dc.datas);
  g_node_destroy(dc.deps);

  return ret;
}

GList *resolveAlternate(rpmPackageAlternate *alt, rpmdb db) {
  DepsCheck dc;
  GList *ret = NULL;
  gboolean sources=gnome_config_get_bool("/gnorpm/rpmfind/wantSources=false");
  gboolean latest =
    gnome_config_get_bool("/gnorpm/rpmfind/wantLatestVersion=false");
  gboolean res;

  dc.pkg_stats = g_hash_table_new(g_str_hash, g_str_equal);
  dc.datas = g_hash_table_new(g_str_hash, g_str_equal);
  dc.deps = g_node_new(NULL);
  dc.db = db;
  dc.sources = sources;
  dc.latest = latest;
  
  res = getRequires(alt, &dc, dc.deps);

  /* flatten deps here and produce URL list, and remove duplicates */
  if (res)
    g_node_traverse(dc.deps, G_POST_ORDER, G_TRAVERSE_ALL, -1,
		    (GNodeTraverseFunc)collect_urls, &ret);

  g_hash_table_foreach_remove(dc.pkg_stats, (GHRFunc)clean_pkg_stats, NULL);
  g_hash_table_destroy(dc.pkg_stats);
  g_hash_table_foreach_remove(dc.datas, (GHRFunc)clean_datas, NULL);
  g_hash_table_destroy(dc.datas);
  g_node_destroy(dc.deps);

  return ret;
}

static GList *noUpgradeList = NULL;

GList *getNoUpgradeList(void) {
  if (!noUpgradeList)
    updateNoUpgradeList();
  return noUpgradeList;
}

void updateNoUpgradeList(void) {
  int i;
  gchar key[13], *value;

  if (noUpgradeList) {
    g_list_foreach(noUpgradeList, (GFunc)g_free, NULL);
    g_list_free(noUpgradeList);
    noUpgradeList = NULL;
  }
  gnome_config_push_prefix("/gnorpm/No-Upgrade/");
  for (i = 0; ; i++) {
    g_snprintf(key, sizeof(key), "%d", i);
    value = gnome_config_get_string(key);
    if (!value)
      break;
    noUpgradeList = g_list_append(noUpgradeList, value);
  }
  if (i == 0) {
    gnome_config_set_string("0", "libc");
    gnome_config_set_string("1", "glibc");
    gnome_config_set_string("2", "shlibs");
    noUpgradeList = g_list_append(noUpgradeList, g_strdup("libc"));
    noUpgradeList = g_list_append(noUpgradeList, g_strdup("glibc"));
    noUpgradeList = g_list_append(noUpgradeList, g_strdup("shlibs"));
  }
  gnome_config_pop_prefix();
}

gboolean dontUpgrade(gchar *name) {
  GList *tmp;

  if (!strncmp(name, "libc.so.", 8)) return TRUE;
  if (!noUpgradeList)
    updateNoUpgradeList();
  for (tmp = noUpgradeList; tmp; tmp = tmp->next)
    if (!strcmp(tmp->data, name))
      return TRUE;
  return FALSE;
}
