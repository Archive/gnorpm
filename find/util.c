/* -*- Mode: C; c-basic-offset: 4 -*- */
/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "util.h"

#include <libgnome/libgnome.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

gboolean
rpmfind_is_uptodate(const gchar *filename, guint age)
{
    struct stat info;
    guint seconds = age * 86400; /* 60*60*24 */

    return stat(filename, &info) == 0 && info.st_mtime + seconds > time(NULL);
}

gchar *
rpmfind_local_filename(const gchar *filename)
{
    gchar *tmp = g_strconcat("gnorpm.d", G_DIR_SEPARATOR_S, filename, NULL);
    gchar *ret = gnome_util_home_file(tmp);

    g_free(tmp);
    return ret;
}

gchar *
rpmfind_remote_filename(const gchar *filename)
{
    gchar *server = gnome_config_get_string(
                "/gnorpm/rpmfind/server=http://rufus.w3.org/linux/RDF");
    gchar *ret = g_strconcat(server, "/", filename, NULL);

    g_free(server);
    return ret;
}

gchar *
rpmfind_tmp_filename(void)
{
    gchar *template = gnome_util_home_file("gnorpm.d/fetchXXXXXX");

    return tmpnam(template);
}

