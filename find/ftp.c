/* libfind - a helper library for use with rpmfind operations.
 * This file Copyright (C) Red Hat Software.
 * Some modifications by James Henstridge.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#if HAVE_CONFIG_H
# include "config.h"
/*# include "miscfn.h"*/
#else
#define HAVE_MACHINE_TYPES_H 1
#define HAVE_ALLOCA_H 1
#define HAVE_NETINET_IN_SYSTM_H 1
#define HAVE_SYS_SOCKET_H 1
#endif

#if HAVE_MACHINE_TYPES_H
# include <machine/types.h>
#endif

#if HAVE_ALLOCA_H
# include <alloca.h>
#endif

#if HAVE_SYS_SOCKET_H
# include <sys/socket.h>
#endif

#if HAVE_NETINET_IN_SYSTM_H
# include <sys/types.h>
# include <netinet/in_systm.h>
#endif

#ifdef HAVE_SYS_SELECT_H
#  include <sys/select.h>
#endif

#if ! HAVE_HERRNO
extern int h_errno;
#endif

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <pwd.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#include <gnome.h>
#include <libgnome/libgnome.h>

#define TIMEOUT_SECS 60
#define BUFFER_SIZE 4096

#ifndef IPPORT_FTP
# define IPPORT_FTP 21
#endif

#if defined(USE_ALT_DNS) && USE_ALT_DNS 
#include "dns.h"
#endif

#include "ftp.h"

static int ftpCheckResponse(int sock, char ** str);
static int ftpCommand(int sock, char * command, ...);
static int ftpReadData(int sock, int out, unsigned long size);
static int getHostAddress(const char * host, struct in_addr * address);

extern void (*downloadProgress)(unsigned long done, unsigned long total);

/* XXX - bring ftp.c up to date, add autoconf checks */
#define HAVE_INET_ATON

#ifndef HAVE_INET_ATON
static int inet_aton(const char *cp, struct in_addr *inp) {
    long addr;

    addr = inet_addr(cp);
    if (addr == ((long) -1)) return 0;

    memcpy(inp, &addr, sizeof(addr));
    return 1;
}
#endif

static int ftpCheckResponse(int sock, char ** str) {
    static char buf[BUFFER_SIZE + 1];
    int bufLength = 0; 
    fd_set emptySet, readSet;
    char * chptr, * start;
    struct timeval timeout;
    int bytesRead, rc = 0;
    int doesContinue = 1;
    char errorCode[4];
    int loops = 0;
 
    errorCode[0] = '\0';
    
    do {
	FD_ZERO(&emptySet);
	FD_ZERO(&readSet);
	FD_SET(sock, &readSet);

	timeout.tv_sec = TIMEOUT_SECS/30;
	timeout.tv_usec = 0;
    
	rc = select(sock + 1, &readSet, &emptySet, &emptySet, &timeout);
	if (rc < 1) {
	    if (rc==0) 
	    {
	    	if(loops==30)
			return FTPERR_BAD_SERVER_RESPONSE;
		else
		{
			loops++;
			while(gtk_events_pending())
				gtk_main_iteration();
			continue;
		}
	    }
	    else
		rc = FTPERR_UNKNOWN;
	} else
	    rc = 0;

	if(sizeof(buf)-bufLength-1)
	{	    
	    bytesRead = read(sock, buf + bufLength, sizeof(buf) - bufLength - 1);
	    if(bytesRead == 0)
		    return FTPERR_BAD_SERVER_RESPONSE;
	    bufLength += bytesRead;
	}

	buf[bufLength] = '\0';

//	printf("READ %s\n", buf);
	
	/* divide the response into lines, checking each one to see if 
	   we are finished or need to continue */

	start = chptr = buf;

	do {
	    while (*chptr != '\n' && *chptr) chptr++;

	    if (*chptr == '\n') {
		*chptr = '\0';
		if (*(chptr - 1) == '\r') *(chptr - 1) = '\0';
		if (str) *str = start;

		if (errorCode[0]) {
		    if (!strncmp(start, errorCode, 3) && start[3] == ' ')
			doesContinue = 0;
		} else {
		    strncpy(errorCode, start, 3);
		    errorCode[3] = '\0';
		    if (start[3] != '-') {
			doesContinue = 0;
		    } 
		}

		start = chptr + 1;
		chptr++;
	    } else {
		chptr++;
	    }
	} while (*chptr);

	if (doesContinue && chptr > start) {
	    memcpy(buf, start, chptr - start - 1);
	    bufLength = chptr - start - 1;
	} else {
	    bufLength = 0;
	}
    } while (doesContinue && !rc);

    if (*errorCode == '4' || *errorCode == '5') {
	if (!strncmp(errorCode, "550", 3)) {
	    return FTPERR_FILE_NOT_FOUND;
	}

	return FTPERR_BAD_SERVER_RESPONSE;
    }

    if (rc) return rc;

    return 0;
}

int ftpCommand(int sock, char * command, ...) {
    va_list ap;
    int len;
    char * s;
    char * buf;
    int rc;

    va_start(ap, command);
    len = strlen(command) + 2;
    s = va_arg(ap, char *);
    while (s) {
	len += strlen(s) + 1;
	s = va_arg(ap, char *);
    }
    va_end(ap);

    buf = alloca(len + 1);

    va_start(ap, command);
    strcpy(buf, command);
    strcat(buf, " ");
    s = va_arg(ap, char *);
    while (s) {
	strcat(buf, s);
	strcat(buf, " ");
	s = va_arg(ap, char *);
    }
    va_end(ap);

    buf[len - 2] = '\r';
    buf[len - 1] = '\n';
    buf[len] = '\0';
    
//    printf("SENT :%s", buf);
     
    if (write(sock, buf, len) != len) {
        return FTPERR_SERVER_IO_ERROR;
    }

    if ((rc = ftpCheckResponse(sock, NULL)))
	return rc;

    return 0;
}

#if !defined(USE_ALT_DNS) || !USE_ALT_DNS 
static int mygethostbyname(const char * host, struct in_addr * address) {
    struct hostent * hostinfo;

    hostinfo = gethostbyname(host);
    if (!hostinfo) return 1;

    memcpy(address, hostinfo->h_addr_list[0], hostinfo->h_length);
    return 0;
}
#endif

static int getHostAddress(const char * host, struct in_addr * address) {
    if (isdigit(host[0])) {
      if (!inet_aton(host, address)) {
	  return FTPERR_BAD_HOST_ADDR;
      }
    } else {
      if (mygethostbyname(host, address)) {
	  errno = h_errno;
	  return FTPERR_BAD_HOSTNAME;
      }
    }
    
    return 0;
}

int ftpOpen(char * host, char * name, char * password, char * proxy,
	    int port) {
    static int sock;
    /*static char * lastHost = NULL;*/
    struct in_addr serverAddress;
    struct sockaddr_in destPort;
    struct passwd * pw;
    char * buf;
    int rc;

    if (port < 0) port = IPPORT_FTP;

    if (!name)
	name = "anonymous";

    if (!password) {
	if (getuid()) {
	    pw = getpwuid(getuid());
	    password = alloca(strlen(pw->pw_name) + 2);
	    strcpy(password, pw->pw_name);
	    strcat(password, "@");
	} else {
	    password = "root@";
	}
    }

    if (proxy) {
	buf = alloca(strlen(name) + strlen(host) + 5);
	sprintf(buf, "%s@%s", name, host);
	name = buf;
	host = proxy;
    }

    if ((rc = getHostAddress(host, &serverAddress))) return rc;

    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
    if (sock < 0) {
        return FTPERR_FAILED_CONNECT;
    }

    destPort.sin_family = AF_INET;
    destPort.sin_port = htons(port);
    destPort.sin_addr = serverAddress;

    /* ftpCheckResponse() assumes the socket is nonblocking */
    if (fcntl(sock, F_SETFL, O_NONBLOCK)) {
	close(sock);
        return FTPERR_FAILED_CONNECT;
    }
    
    if (connect(sock, (struct sockaddr *) &destPort, sizeof(destPort))) {
        if(errno != EINPROGRESS)
        {
	    close(sock);
            return FTPERR_FAILED_CONNECT;
        }
    }


    if ((rc = ftpCheckResponse(sock, NULL))) {
        return rc;     
    }

    if ((rc = ftpCommand(sock, "USER", name, NULL))) {
	close(sock);
	return rc;
    }

    if ((rc = ftpCommand(sock, "PASS", password, NULL))) {
	close(sock);
	return rc;
    }

    if ((rc = ftpCommand(sock, "TYPE", "I", NULL))) {
	close(sock);
	return rc;
    }

    return sock;
}

int ftpReadData(int sock, int out, unsigned long size) {
    char buf[BUFFER_SIZE];
    fd_set emptySet, readSet;
    struct timeval timeout;
    int bytesRead, rc;
    unsigned long totalBytesRead = 0;
    int loops=0;
    
    while (1) {
	FD_ZERO(&emptySet);
	FD_ZERO(&readSet);
	FD_SET(sock, &readSet);

	timeout.tv_sec = TIMEOUT_SECS/30;
	timeout.tv_usec = 0;
    
	rc = select(sock + 1, &readSet, &emptySet, &emptySet, &timeout);
	if (rc == 0) {
	    if(loops==30)
	    {
		close(sock);
		return FTPERR_SERVER_TIMEOUT;
	    }
	    loops++;
	    while(gtk_events_pending())
		gtk_main_iteration();
	    continue;
	} else if (rc < 0) {
	    close(sock);
	    return FTPERR_UNKNOWN;
	}
	loops=0;

	bytesRead = read(sock, buf, sizeof(buf));
	totalBytesRead += bytesRead;

	if (downloadProgress)
	  (*downloadProgress)(totalBytesRead, size);
	if (bytesRead == 0) {
	    close(sock);
	    return totalBytesRead;
	}

	if (write(out, buf, bytesRead) != bytesRead) {
	    close(sock);
	    return FTPERR_FILE_IO_ERROR;
	}
    }
}

int ftpGetFileDesc(int sock, char * remotename, unsigned long *sizep) {
    int dataSocket;
    struct sockaddr_in dataAddress;
    int i, j;
    char * passReply;
    char * chptr;
    char * retrCommand;
    int rc, namelen = strlen(remotename);

    /* added by James Henstridge, so we can work out percentage of work to do*/
    if (sizep) {
      unsigned long ret_code, size;

      if (write(sock, "SIZE ", 5) != 5 ||
	  write(sock, remotename, namelen) != namelen ||
	  write(sock, "\r\n", 2) != 2)
        return FTPERR_SERVER_IO_ERROR;

      if ((rc = ftpCheckResponse(sock, &passReply)))
	return FTPERR_BAD_SERVER_RESPONSE;

      if (sscanf(passReply, "%lu %lu", &ret_code, &size) != 2)
	return FTPERR_BAD_SERVER_RESPONSE;
      *sizep = size;
    }

    if (write(sock, "PASV\r\n", 6) != 6) {
        return FTPERR_SERVER_IO_ERROR;
    }
    if ((rc = ftpCheckResponse(sock, &passReply)))
	return FTPERR_PASSIVE_ERROR;

    chptr = passReply;
    while (*chptr && *chptr != '(') chptr++;
    if (*chptr != '(') return FTPERR_PASSIVE_ERROR; 
    chptr++;
    passReply = chptr;
    while (*chptr && *chptr != ')') chptr++;
    if (*chptr != ')') return FTPERR_PASSIVE_ERROR;
    *chptr-- = '\0';

    while (*chptr && *chptr != ',') chptr--;
    if (*chptr != ',') return FTPERR_PASSIVE_ERROR;
    chptr--;
    while (*chptr && *chptr != ',') chptr--;
    if (*chptr != ',') return FTPERR_PASSIVE_ERROR;
    *chptr++ = '\0';
    
    /* now passReply points to the IP portion, and chptr points to the
       port number portion */

    dataAddress.sin_family = AF_INET;
    if (sscanf(chptr, "%d,%d", &i, &j) != 2) {
	return FTPERR_PASSIVE_ERROR;
    }
    dataAddress.sin_port = htons((i << 8) + j);

    chptr = passReply;
    while (*chptr++) {
	if (*chptr == ',') *chptr = '.';
    }

    if (!inet_aton(passReply, &dataAddress.sin_addr)) 
	return FTPERR_PASSIVE_ERROR;

    dataSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
    if (dataSocket < 0) {
        return FTPERR_FAILED_CONNECT;
    }

    retrCommand = alloca(strlen(remotename) + 20);
    sprintf(retrCommand, "RETR %s\r\n", remotename);
    i = strlen(retrCommand);
   
    if (write(sock, retrCommand, i) != i) {
        return FTPERR_SERVER_IO_ERROR;
    }
    
    
    if (fcntl(sock, F_SETFL, O_NONBLOCK)) {
	close(sock);
        return FTPERR_FAILED_CONNECT;
    }

    if (connect(dataSocket, (struct sockaddr *) &dataAddress, 
	        sizeof(dataAddress))) {
        if(errno != EINPROGRESS)
        {
	    close(dataSocket);
            return FTPERR_FAILED_DATA_CONNECT;
        }
    }

    if ((rc = ftpCheckResponse(sock, NULL))) {
	close(dataSocket);
	return rc;
    }

    return dataSocket;
}

int ftpGetFileDone(int sock) {
    if (ftpCheckResponse(sock, NULL)) {
	return FTPERR_BAD_SERVER_RESPONSE;
    }

    return 0;
}

int ftpGetFile(int sock, char * remotename, int dest) {
    int dataSocket, rc;
    unsigned long size;

    dataSocket = ftpGetFileDesc(sock, remotename, &size);
    if (dataSocket < 0) return dataSocket;

    rc = ftpReadData(dataSocket, dest, size);
    close(dataSocket);
    
    if (rc) return rc;

    return ftpGetFileDone(sock);
}

void ftpClose(int sock) {
    close(sock);
}

const char *ftpStrerror(int errorNumber) {
  switch (errorNumber) {
    case FTPERR_BAD_SERVER_RESPONSE:
      return _("Bad FTP server response");

    case FTPERR_SERVER_IO_ERROR:
      return _("FTP IO error");

    case FTPERR_SERVER_TIMEOUT:
      return _("FTP server timeout");

    case FTPERR_BAD_HOST_ADDR:
      return _("Unable to lookup FTP server host address");

    case FTPERR_BAD_HOSTNAME:
      return _("Unable to lookup FTP server host name");

    case FTPERR_FAILED_CONNECT:
      return _("Failed to connect to FTP server");

    case FTPERR_FAILED_DATA_CONNECT:
      return _("Failed to establish data connection to FTP server");

    case FTPERR_FILE_IO_ERROR:
      return _("IO error to local file");

    case FTPERR_PASSIVE_ERROR:
      return _("Error setting remote server to passive mode");

    case FTPERR_FILE_NOT_FOUND:
      return _("File not found on server");

    case FTPERR_UNKNOWN:
    default:
      return _("FTP Unknown or unexpected error");
  }
}
  
