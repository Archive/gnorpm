/* libfind - a helper library for use with rpmfind operations.
 * Copyright (C) 1999  James Henstridge
 * Concepts for rpmfind Copyright (C) 199x  Daniel Veillard
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef DEPS_H
#define DEPS_H

#include <glib.h>
#include <rpmlib.h>
#include "search.h"

GList *resolveName(char *name, rpmdb db);
GList *resolveAlternate(rpmPackageAlternate *alt, rpmdb db);

/* get the list of files not to upgrade during rpmfind opperations */
GList *getNoUpgradeList(void);
/* reread no upgrade list from disk */
void updateNoUpgradeList(void);
/* return TRUE if package shouldn't be upgraded */
gboolean dontUpgrade(gchar *name);

#endif
