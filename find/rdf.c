/*
 * rdf.c : implementation for the RDF encoding/decoding of RPM informations.
 *
 * See Copyright for the status of this software.
 *
 * $Id$
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <libgnome/libgnome.h>

#include <sys/stat.h>
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <time.h>
#include <errno.h>
#include <ctype.h>

#include "rdf_api.h"

#include "rdf.h"
#include <rpmlib.h>
#include "trans.h"
#include "distrib.h"

extern ComplainFunc complain;

/*
 * Open an RDF file call the parser to create a XML tree
 * Then walk the tree and build an rpmData structure for
 * the corresponding package.
 */
rpmData *rpmOpenRdfFile(char *file) {
    rdfSchema rdf;
    rdfNamespace rpmNs;
    rdfNamespace rdfNs;
    rdfDescription desc;
    rdfBag bag;
    rdfElement elem;
    char *value;
    char *name;
    rpmData *rpm;
    struct stat buf;

    rdf = rdfRead(file);
    if (rdf == NULL) return(NULL);

    /*
     * Start the analyze, check that's an RDf for RPM packages.
     */
    rdfNs = rdfGetNamespace(rdf, "http://www.w3.org/TR/WD-rdf-syntax#");
    if (rdfNs == NULL) {
        complain(_("%s is not an RDF schema"), file);
	rdfDestroySchema(rdf);
	return(NULL);
    }
    rpmNs = rdfGetNamespace(rdf, "http://www.rpm.org/");
    if (rdfNs == NULL) {
        complain(_("%s is not an RPM specific RDF schema"), file);
	rdfDestroySchema(rdf);
	return(NULL);
    }
    desc = rdfFirstDescription(rdf);
    if (rdfNs == NULL) {
        complain(_("%s RDF schema seems empty"), file);
	rdfDestroySchema(rdf);
	return(NULL);
    }

    /*
     * We are pretty sure that it will be a valid schema,
     * Allocate the rpmData structure, initialize it.
     */
    rpm = g_new0(rpmData, 1);
    if (rpm == NULL) {
        complain(_("rpmOpenRdf: out of memory !"));
	rdfDestroySchema(rdf);
	return(NULL);
    }
    memset(rpm, 0, sizeof(rpmData));

    stat(file, &buf);
    rpm->stamp = buf.st_mtime;

    /*
     * Now extract all the metadata informations from the RDF tree
     */
    rdfGetValue(desc, "Name", rpmNs, &value, NULL);
    if (value != NULL) rpm->name = value;
    else {
        complain(_("%s RDF schema invalid: no Name"), file);
	rdfDestroySchema(rdf);
	return(NULL);
    }
    rdfGetValue(desc, "Version", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->version = g_strdup(value);
      free(value);
    } else {
        complain(_("%s RDF schema invalid: no Version"), file);
	rdfDestroySchema(rdf);
	return(NULL);
    }
    rdfGetValue(desc, "Release", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->release = g_strdup(value);
      free(value);
    } else {
        complain(_("%s RDF schema invalid: no Release"), file);
	rdfDestroySchema(rdf);
	return(NULL);
    }
    rdfGetValue(desc, "URL", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->url = g_strdup(value);
      free(value);
    } else rpm->url = NULL;

    rdfGetValue(desc, "Arch", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->arch = g_strdup(value);
      free(value);
    } else rpm->arch = g_strdup(_("None"));

    rdfGetValue(desc, "Os", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->os = g_strdup(value);
      free(value);
    } else rpm->os = g_strdup("linux");

    rdfGetValue(desc, "Distribution", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->distribution = g_strdup(value);
      free(value);
    } else rpm->distribution = g_strdup(_("Unknown"));

    rdfGetValue(desc, "Vendor", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->vendor = g_strdup(value);
      free(value);
    } else rpm->vendor = g_strdup(_("Unknown"));

    rdfGetValue(desc, "Packager", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->packager = g_strdup(value);
      free(value);
    } else rpm->packager = NULL;

    rdfGetValue(desc, "Group", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->group = g_strdup(value);
      free(value);
    } else rpm->group = g_strdup(_("unknown/group"));
    
    rdfGetValue(desc, "Summary", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->summary = g_strdup(value);
      free(value);
    } else rpm->summary = g_strdup(_("no summary"));

    rdfGetValue(desc, "Description", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->description = g_strdup(value);
      free(value);
    } else rpm->description = g_strdup(_("No description !"));

    rdfGetValue(desc, "Copyright", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->copyright = g_strdup(value);
      free(value);
    } else rpm->copyright = NULL;

    rdfGetValue(desc, "Changelog", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->changelog = g_strdup(value);
      free(value);
    } else rpm->changelog = NULL;

    rdfGetValue(desc, "Sources", rpmNs, &value, NULL);
    if (value != NULL) {
        rpm->srcrpm = g_strdup(value);
	free(value);
    } else
      rpm->srcrpm = g_strdup("");

    rdfGetValue(desc, "Size", rpmNs, &value, NULL);
    if (value != NULL) {
        rpm->size = strtol(value, NULL, 0);
	free(value);
    } else rpm->size = 0;

    rdfGetValue(desc, "Date", rpmNs, &value, NULL);
    if (value != NULL) {
        rpm->date = strtol(value, NULL, 0);
	free(value);
    } else rpm->date = 0;

    rdfGetValue(desc, "BuildHost", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->host = g_strdup(value);
      free(value);
    } else
      rpm->host = g_strdup(_("unknown.host"));

    rdfGetValue(desc, "Files", rpmNs, &value, NULL);
    if (value != NULL) {
      rpm->filelist = g_strdup(value);
      free(value);
    } else
      rpm->filelist = NULL;

    /*
     * Fetching packages provided is a bit more tricky, one have to
     * find the RDF Bag, and scan it's values.
     */
    rdfGetValue(desc, "Provides", rpmNs, NULL, &bag);
    if (bag != NULL) {
        elem = rdfFirstChild(bag);

	while (elem != NULL) {
	    /*
	     * Check that we are scanning an RPM Resource.
	     */
	    name = rdfElemGetPropertyName(elem);
	    if ((name != NULL) &&
	        (!strcmp(name, "Resource")) &&
	        (rdfElemGetNamespace(elem) == rpmNs)) {
		value = rdfElemGetValue(elem);
		if (value != NULL) {
		    rpm->resources = g_list_append(rpm->resources,
						   g_strdup(value));
		    free(value);
		} else
		  complain(_("%s: malformed Resource element"), file);
	    } else
	      complain(_("%s: malformed Provides bag"), file);
	    if (name != NULL) free(name);
	    elem = rdfNextElem(elem);
	}
    } else
        complain(_("%s: doesn't export any resource"), file);

    /*
     * idem for the dependencies.
     */
    rdfGetValue(desc, "Requires", rpmNs, NULL, &bag);
    if (bag != NULL) {
        elem = rdfFirstChild(bag);
	while (elem != NULL) {
	    /*
	     * Check that we are scanning an RPM Resource.
	     */
	    name = rdfElemGetPropertyName(elem);
	    if ((name != NULL) &&
	        (!strcmp(name, "Resource")) &&
	        (rdfElemGetNamespace(elem) == rpmNs)) {
		value = rdfElemGetValue(elem);
		if (value != NULL) {
		    rpm->requires = g_list_append(rpm->requires,
						  g_strdup(value));
		    free(value);
		} else
		  complain(_("%s: malformed Resource element"), file);
	    } else
	      complain(_("%s: malformed Provides bag"), file);
	    if (name != NULL) free(name);
	    elem = rdfNextElem(elem);
	}
    }

    /*
     * Finish filling the rpmData structure.
     */
    rpm->filename = g_strdup_printf("%s-%s-%s.rpm",
            rpm->name, rpm->version, rpm->release);

    /*
     * Cleanup.
     */
    rdfDestroySchema(rdf);
    return(rpm);
}

rpmData *rpmOpenPackage(char *distID, char *name, char *version,
			char *release, char *arch) {
  char *tmp = g_strdup_printf("%s/%s-%s-%s.%s.rdf", distID, name,
			       version, release, arch);
  char *file = url_get_to_temp(tmp);
  rpmData *rpm;

  g_free(tmp);
  if(file==NULL)
	return NULL;
  rpm = rpmOpenRdfFile(file);
  rpm->subdir = g_strdup(distID);
  unlink(file);
  g_free(file);
  return rpm;
}

char *rpmDataGetURL(rpmData *rpm) {
  rpmDistrib *distrib = distribInfoGet(rpm->subdir);
  char *part = rpm->subdir + strlen(distrib->ID);

  if (!distrib) {
    complain(_("Unknown distribution %s"), rpm->subdir?rpm->subdir:"(null)");
    return NULL;
  }
  return g_strconcat(distrib->myMirror, part, "/",  rpm->name, "-",
		     rpm->version, "-", rpm->release, ".", rpm->arch,
		     ".rpm", NULL);
}

void rpmDataFree(rpmData *rpm) {
  /* for data caches */
  if (rpm->nofree) return;

  if (rpm->subdir) g_free(rpm->subdir);
  if (rpm->filename) g_free(rpm->filename);
  if (rpm->name) g_free(rpm->name);
  if (rpm->version) g_free(rpm->version);
  if (rpm->release) g_free(rpm->release);
  if (rpm->url) g_free(rpm->url);
  if (rpm->arch) g_free(rpm->arch);
  if (rpm->os) g_free(rpm->os);
  if (rpm->distribution) g_free(rpm->distribution);
  if (rpm->vendor) g_free(rpm->vendor);
  if (rpm->packager) g_free(rpm->packager);
  if (rpm->group) g_free(rpm->group);
  if (rpm->summary) g_free(rpm->summary);
  if (rpm->description) g_free(rpm->description);
  if (rpm->copyright) g_free(rpm->copyright);
  if (rpm->changelog) g_free(rpm->changelog);
  if (rpm->srcrpm) g_free(rpm->srcrpm);
  if (rpm->host) g_free(rpm->host);
  g_list_foreach(rpm->resources, (GFunc)g_free, NULL);
  g_list_free(rpm->resources);
  g_list_foreach(rpm->requires, (GFunc)g_free, NULL);
  g_list_free(rpm->requires);
  if (rpm->filelist) g_free(rpm->filelist);

  g_free(rpm);
}

void rpmDataShow(rpmData *rpm) {
  g_print("Name        : %-27s Distribution: %s\n", rpm->name,
	  rpm->distribution?rpm->distribution:"(none)");
  g_print("Version     : %-27s       Vendor: %s\n", rpm->version,
	  rpm->vendor?rpm->vendor:"(none)");
  g_print("Release     : %-27s Build Date: %s", rpm->release,
	  ctime(&(rpm->date)));
  g_print("Install Date: %-27s   Build Host: %s\n", "(not installed)",
	  rpm->host);
  g_print("Group       : %-27s   Source RPM: %s\n", rpm->group, rpm->srcrpm);
  g_print("Size        : %d\n", rpm->size);
  if (rpm->packager)
    g_print("Packager    : %s\n", rpm->packager);
  if (rpm->url)
    g_print("URL         : %s\n", rpm->url);
  g_print("Summary     : %s\n", rpm->summary);
  g_print("Description :\n%s\n\n", rpm->description);
  if (rpm->resources || rpm->requires) {
    GList *tmp1 = rpm->resources, *tmp2 = rpm->requires;
    g_print("%-30s%-30s\n", "Provides:", "Requires:");
    while (tmp1 || tmp2) {
      if (tmp1 && tmp2)
	g_print("%-30s%-30s\n", (char *)tmp1->data, (char*)tmp2->data);
      else if (tmp1)
	g_print("%-30s\n", (char *)tmp1->data);
      else if (tmp2)
	g_print("%-30s%-30s\n", "", (char *)tmp2->data);
      if (tmp1) tmp1 = tmp1->next;
      if (tmp2) tmp2 = tmp2->next;
    }
    g_print("\n");
  }
  if (rpm->filelist)
    g_print("Files:\n%s\n", rpm->filelist);
}
