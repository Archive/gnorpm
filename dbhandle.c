/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "dbhandle.h"
#include "misc.h"
#include <gnome.h>
#include <fcntl.h>
#include <unistd.h>

DBHandle *db_handle_new(gchar *root) {
  DBHandle *self = g_new(DBHandle, 1);

  self->root = g_strdup(root);
  if (rpmdbOpen(root, &self->db, O_RDONLY, 0644)) {
    g_free(self);
    return NULL;
  }
  self->upcount = 1;
  return self;
}

void db_handle_free(DBHandle *self) {
  g_free(self->root);
  if (self->db) rpmdbClose(self->db);
  g_free(self);
}

/* These two functions should be used around installs and uninstalls, as those
 * operations need to open the database read/write (and I don't see any reason
 * to keep the database open like that all the time -- It is better to share
 */
/* closes the db handle, if it hasn't been already */
void db_handle_db_down(DBHandle *self) {
  self->upcount--;
  if (self->upcount == 0) {
    if (self->db) rpmdbClose(self->db);
    self->db = NULL;
  }
}

/* opens it again (if it isn't already open) */
void db_handle_db_up(DBHandle *self) {
  self->upcount++;
  if (self->upcount == 1)
    if (rpmdbOpen(self->root, &self->db, O_RDONLY, 0644)) {
      statusbar_msg(_("Could not open database"));
      self->db = NULL;
    }
}
