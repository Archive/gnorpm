/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999 James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <gnome.h>
#include <rpmlib.h>

#include "dbhandle.h"
#include "rpmpackagelist.h"
#include "rpmquerydlg.h"
#include "rpmfinddlg.h"
#include "rpminstalldlg.h"
#include "rpmdentry.h"
#include "rpmprops.h"
#include "install.h"
#include "verify.h"
#include "checksig.h"
#include "misc.h"
#include "pixmaps.h"

#ifdef WITH_RPMFIND
#include "find/libfind.h"
#include "rpmwebfind.h"
#endif

GnomeApp *gnorpm_app = NULL;
GtkWidget *statusbar = NULL;

enum { TARGET_URI_LIST };
static GtkTargetEntry drop_types[] = {
  { "text/uri-list", 0, TARGET_URI_LIST}
};
static gint n_drop_types = sizeof(drop_types) / sizeof(drop_types[0]);

/* prototype to get rid of warnings ... */
GtkWidget *create_main(char *app_id, DBHandle *hdl);

static void create_menus(GtkWidget *app, GtkWidget *package_list);
static void update_label(RpmPackageList *pl, GnomeAppBar *status);

static void mainwin_drop_cb(GnomeApp *app, GdkDragContext *context,
			    gint x, gint y, GtkSelectionData *selection_data,
			    guint info, guint time, RpmPackageList *pl);

static void mainwin_context_menu(RpmPackageList *pl, GdkEventButton *event,
				 guint index);

static void generic_query(RpmPackageList *pl, gpointer index);

static guint interfaceFlags = 0, transFlags = 0, probFilter = 0;

#ifdef WITH_RPMFIND

extern int downloadAbort;

static void gnorpm_download_abort(gpointer crap)
{
  downloadAbort=1;
}

static void mainwin_closed(gpointer crap) {
	downloadAbort = 1;
	statusbar = NULL;
}

static void gnorpm_download_start(char *url) {
  char *msg = g_strconcat("Downloading ", url, " ...", NULL);
  GnomeAppProgressKey key = gnome_app_progress_manual(gnorpm_app, msg,
						      gnorpm_download_abort, NULL);
  GSList *list;

  list = gtk_object_get_data(GTK_OBJECT(gnorpm_app), "download_key");
  list = g_slist_prepend(list, key);
  gtk_object_set_data(GTK_OBJECT(gnorpm_app), "download_key", list);
  g_free(msg);
  while (gtk_events_pending())
    gtk_main_iteration();
}

static void gnorpm_download_progress(unsigned long done, unsigned long total) {
  GSList *list;
  GnomeAppProgressKey key;

  if(!downloadAbort && !GTK_OBJECT_DESTROYED(GTK_OBJECT(gnorpm_app))) {
    list = gtk_object_get_data(GTK_OBJECT(gnorpm_app), "download_key");
    g_return_if_fail(list != NULL);
    key = (GnomeAppProgressKey) list->data;

    if (total == 0) total = 1;
    gnome_app_set_progress(key, ((gdouble)done)/total);
  }

  while (gtk_events_pending())
    gtk_main_iteration();
}

static void gnorpm_download_done(void) {
  GSList *list;
  GnomeAppProgressKey key;

  if(!downloadAbort && !GTK_OBJECT_DESTROYED(GTK_OBJECT(gnorpm_app))) {
    list = gtk_object_get_data(GTK_OBJECT(gnorpm_app), "download_key");
    g_return_if_fail(list != NULL);
    key = (GnomeAppProgressKey) list->data;
    list = g_slist_remove(list, key);

    gnome_app_progress_done(key);
    gnome_appbar_refresh(GNOME_APPBAR(gnorpm_app->statusbar));
    gtk_object_set_data(GTK_OBJECT(gnorpm_app), "download_key", list);
  }
}
#endif

GtkWidget *create_main(char *app_id, DBHandle *hdl) {
  GtkWidget *app, *pl;

  gnome_preferences_set_statusbar_dialog(TRUE);
  
  app = gnome_app_new(app_id, _("Gnome RPM"));
  gnorpm_app = GNOME_APP(app);
  gtk_signal_connect(GTK_OBJECT(app), "destroy",
		     GTK_SIGNAL_FUNC(gtk_main_quit), NULL);

  pl = rpm_package_list_new(hdl);
  gnome_app_set_contents(GNOME_APP(app), pl);
  gtk_widget_show(pl);

  gtk_signal_connect(GTK_OBJECT(pl), "context_menu",
		     GTK_SIGNAL_FUNC(mainwin_context_menu), NULL);
  gtk_signal_connect(GTK_OBJECT(pl), "query",
	  	     GTK_SIGNAL_FUNC(generic_query), NULL);

  statusbar = gnome_appbar_new(TRUE, TRUE, GNOME_PREFERENCES_USER);
  gnome_app_set_statusbar(GNOME_APP(app), statusbar);
  gtk_widget_show(statusbar);

  gnome_appbar_push(GNOME_APPBAR(statusbar), _("Packages Selected: 0"));
  gtk_signal_connect(GTK_OBJECT(pl), "selection_changed",
		     GTK_SIGNAL_FUNC(update_label), statusbar);

  create_menus(app, pl);
  set_icon(app);

#ifdef WITH_RPMFIND
  url_set_callbacks(gnorpm_download_start,
		    gnorpm_download_progress,
		    gnorpm_download_done);
  set_complain_func(statusbar_msg);
#endif

  /* get the saved properties */
  rpm_props_box_get_flags(&interfaceFlags, &transFlags, &probFilter);
  
  if (gnome_config_get_bool("/gnorpm/Layout/asIcons=true"))
    rpm_package_list_set_mode(RPM_PACKAGE_LIST(pl), RPM_PACKAGE_ILIST);

  /* set up the drag and drop routines for the main window */
  gtk_drag_dest_set(GTK_WIDGET(app),
		    GTK_DEST_DEFAULT_MOTION |
		    GTK_DEST_DEFAULT_HIGHLIGHT |
		    GTK_DEST_DEFAULT_DROP,
		    drop_types, n_drop_types,
		    GDK_ACTION_COPY);
  gtk_signal_connect(GTK_OBJECT(app), "drag_data_received",
		     GTK_SIGNAL_FUNC(mainwin_drop_cb), pl);

  gtk_widget_set_usize(app, 650, 350);
  gtk_window_set_policy(GTK_WINDOW(app), TRUE, TRUE, FALSE);

  return app;
}

/* the updating of the label is deferred in order to not cause flashing
 * of the package list */
static guint label_idle_tag = 0;
static gint update_idle_cb(GnomeAppBar *status) {
  RpmPackageList *pl = gtk_object_get_user_data(GTK_OBJECT(status));
  char buf[512];
  g_snprintf(buf, 511, _("Packages selected: %d"),
	     g_list_length(pl->selection));
  gnome_appbar_pop(status);
  gnome_appbar_push(status, buf);
  label_idle_tag = 0;
  return FALSE;
}
static void update_label(RpmPackageList *pl, GnomeAppBar *status) {
  gtk_object_set_user_data(GTK_OBJECT(status), pl);
  if (!label_idle_tag)
    label_idle_tag = gtk_idle_add((GtkFunction)update_idle_cb, status);
}

#ifdef WITH_RPMFIND
static void rpm_show_web_find(GtkWidget *mi, RpmPackageList *pi);
#endif
static void rpm_clear_selection(GtkWidget *mi, RpmPackageList *pl);
static void rpm_install_pkgs(GtkWidget *mi, RpmPackageList *pl);
static void rpm_uninstall_pkgs(GtkWidget *mi, RpmPackageList *pl);
static void rpm_verify_pkgs(GtkWidget *mi, RpmPackageList *pl);
static void rpm_query_pkgs(GtkWidget *mi, RpmPackageList *pl);
static void rpm_dentry_pkgs(GtkWidget *mi, RpmPackageList *pl);
static void rpm_about(GtkWidget *mi);
static void rpm_find(GtkWidget *mi, RpmPackageList *pl);

static void rpm_preferences_cb(GtkWidget *mi, RpmPackageList *pl);

static void create_menus(GtkWidget *app, GtkWidget *pl) {
#ifndef GNOMEUIINFO_ITEM_STOCK_DATA
#define GNOMEUIINFO_ITEM_STOCK_DATA(label, tip, cb, data, xpm) \
                   {GNOME_APP_UI_ITEM, label, tip, cb, data, NULL, \
                    GNOME_APP_PIXMAP_STOCK, xpm, 0, (GdkModifierType)0, NULL}
#endif

  GnomeUIInfo pkg_menu[] = {
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("_Query..."),
			N_("Get information about the selected packages"),
			rpm_query_pkgs, pl, RPM_STOCK_MENU_QUERY),
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("_Uninstall"),
			N_("Uninstall the selected packages"),
			rpm_uninstall_pkgs, pl,	RPM_STOCK_MENU_UNINSTALL),
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("_Verify"),
			N_("Verify the selected packages"),
			rpm_verify_pkgs, pl, RPM_STOCK_MENU_VERIFY),
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("_Create desktop entry..."),
	N_("Create desktop entries (for panel) for the selected packages"),
				rpm_dentry_pkgs,pl, GNOME_STOCK_MENU_BLANK),
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_ITEM_STOCK(N_("_Quit"), N_("Quit GnoRPM"), gtk_main_quit,
			   GNOME_STOCK_MENU_QUIT),
    GNOMEUIINFO_END
  };
  GnomeUIInfo ops_menu[] = {
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("_Find..."),
				N_("Search RPM database for packages"),
				rpm_find, pl, GNOME_STOCK_MENU_SEARCH),
#ifdef WITH_RPMFIND
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("Web find..."),
			N_("Find packages on the web with rpmfind"),
			rpm_show_web_find, pl, RPM_STOCK_MENU_RPMFIND),
#endif
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("_Install..."),
				N_("Install some new packages"),
				rpm_install_pkgs, pl, RPM_STOCK_MENU_INSTALL),
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("_Preferences..."),
				N_("Alter GnoRPM's preferences"),
				rpm_preferences_cb, pl, GNOME_STOCK_MENU_PREF),
    GNOMEUIINFO_END
  };
  GnomeUIInfo help_menu[] = {
    GNOMEUIINFO_ITEM_STOCK(N_("_About..."), N_("Bring up the about box"),
			   rpm_about, GNOME_STOCK_MENU_ABOUT),
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_HELP("gnorpm"),
    GNOMEUIINFO_END
  };
  GnomeUIInfo menus[] = {
    GNOMEUIINFO_SUBTREE(N_("_Packages"), pkg_menu),
    GNOMEUIINFO_SUBTREE(N_("_Operations"), ops_menu),
    GNOMEUIINFO_SUBTREE(N_("_Help"), help_menu),
    GNOMEUIINFO_END
  };
  GnomeUIInfo toolbar[] = {
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("Install"),
				N_("Select packages for installation"),
                          rpm_install_pkgs, pl, RPM_STOCK_PIXMAP_INSTALL),
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("Unselect"), N_("Unselect all packages"),
                          rpm_clear_selection, pl, RPM_STOCK_PIXMAP_UNSELECT),
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("Uninstall"),
				N_("Uninstall selected packages"),
                          rpm_uninstall_pkgs, pl, RPM_STOCK_PIXMAP_UNINSTALL),
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("Query"), N_("Query selected packages"),
				rpm_query_pkgs, pl,
				RPM_STOCK_PIXMAP_QUERY),
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("Verify"), N_("Verify selected packages"),
                          rpm_verify_pkgs, pl, RPM_STOCK_PIXMAP_VERIFY),
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("Find"), N_("Find packages"), rpm_find, pl,
				GNOME_STOCK_PIXMAP_SEARCH),
#ifdef WITH_RPMFIND
    GNOMEUIINFO_ITEM_STOCK_DATA(N_("Web find"),
			N_("Find packages on the web with rpmfind"),
			rpm_show_web_find, pl, RPM_STOCK_PIXMAP_RPMFIND),
#endif
    GNOMEUIINFO_END
  };

  gnome_app_create_menus(GNOME_APP(app), menus);
  gnome_app_install_menu_hints(GNOME_APP(app), menus);
  gnome_app_create_toolbar(GNOME_APP(app), toolbar);
}

static void rpm_clear_selection(GtkWidget *mi, RpmPackageList *pl) {
  rpm_package_list_clear_selection(pl);
}

static void rpm_uninstall_pkgs(GtkWidget *mi, RpmPackageList *pl) {
  if(geteuid())
  {
    GtkWidget *d=gnome_error_dialog(_("You need to be the superuser to uninstall packages."));
    gnome_dialog_run_and_close(GNOME_DIALOG(d));
    return;
  }

  do_uninstall(pl->hdl->root, pl->selection, transFlags, probFilter,
	       interfaceFlags);

  /* update the package list display */
  rpm_package_list_clear_selection(pl);
  rpm_package_list_update_pane(pl);
}

static void rpm_verify_pkgs(GtkWidget *mi, RpmPackageList *pl) {
  if(geteuid())
  {
    GtkWidget *d=gnome_error_dialog(_("You need to be the superuser to verify packages."));
    gnome_dialog_run_and_close(GNOME_DIALOG(d));
    return;
  }
  verify_packages(pl->hdl, pl->selection, 0);
}

static void verify_func(RpmQuery *info, gpointer data) {
  if(geteuid())
  {
    GtkWidget *d=gnome_error_dialog(_("You need to be the superuser to verify packages."));
    gnome_dialog_run_and_close(GNOME_DIALOG(d));
    return;
  }
  verify_one(info->hdl, info->index, 0);
}

static void close_func(RpmQuery *info, gpointer data) {
  GtkWidget *win = (GtkWidget *) data;
    
  gtk_widget_destroy(win);
}

static void uninstall_func(RpmQuery *info, gpointer data) {
  int failed;
  char buf[512];
  RpmPackageList *pl = RPM_PACKAGE_LIST(data);

  if(geteuid())
  {
    GtkWidget *d=gnome_error_dialog(_("You need to be the superuser to uninstall packages."));
    gnome_dialog_run_and_close(GNOME_DIALOG(d));
    return;
  }
  failed = uninstall_one(info->hdl->root, info->index, transFlags,
			 probFilter, interfaceFlags);
  if (failed) {
    g_snprintf(buf, 511, _("%d packages couldn't be uninstalled"), failed);
    message_box(buf);
    return;
  }
  /* update the package list display */
  rpm_package_list_update_pane(pl);
  rpm_package_list_unselect(pl, info->index);

  /* remove the package from the query window */
  if (!(transFlags & RPMTRANS_FLAG_TEST)) {
    GtkWidget *container = GTK_WIDGET(info)->parent;

    gtk_container_remove(GTK_CONTAINER(container), GTK_WIDGET(info));
      if (gtk_container_children(GTK_CONTAINER(container)) == NULL)
	gtk_widget_destroy(container->parent);
  }
}

static void rpm_query_display(RpmPackageList *pl, GList *indices) {
  GtkWidget *win;

  if (indices == NULL) return;
  win = rpm_query_dialog_new(pl->hdl, indices);
  rpm_query_dialog_set_verify_func(RPM_QUERY_DIALOG(win), verify_func, NULL);
  rpm_query_dialog_set_uninstall_func(RPM_QUERY_DIALOG(win), uninstall_func,
				      pl);
  rpm_query_dialog_set_close_func(RPM_QUERY_DIALOG(win), close_func, win);
  gtk_widget_show(win);
}

static void rpm_query_pkgs(GtkWidget *mi, RpmPackageList *pl) {
  rpm_query_display(pl, pl->selection);
}

static void rpm_about(GtkWidget *mi) {
  GtkWidget *about;
  const char *authors[] = {"James Henstridge", NULL};

  about = gnome_about_new(_("About GnoRPM"), VERSION,
                          "Copyright (C) 1998-1999, James Henstridge",
                          authors,
                          _("May be distributed under the terms of the GPL2\n"
                          "This program uses rpmlib, written by Red Hat"),
                          NULL);
  gtk_widget_show(about);
}

static void rpm_find_query_cb(RpmFindDialog *dlg, GList *indices,
			RpmPackageList *pl) {
  rpm_query_display(pl, indices);
}

static void rpm_find_uninstall_cb(RpmFindDialog *dlg, GList *indices,
			RpmPackageList *pl) {
  int failed;
  char buf[512];
  GList *tmp;

  if (indices == NULL) return;
  if(geteuid())
  {
    GtkWidget *d=gnome_error_dialog(_("You need to be the superuser to uninstall packages."));
    gnome_dialog_run_and_close(GNOME_DIALOG(d));
    return;
  }
  failed = do_uninstall(pl->hdl->root, indices, transFlags, probFilter,
			interfaceFlags);
  if (failed) {
    g_snprintf(buf, 511, _("%d packages couldn't be uninstalled"), failed);
    message_box(buf);
  }
  /* update the package list display */
  rpm_package_list_update_pane(pl);
  for (tmp = indices; tmp; tmp = tmp->next)
    rpm_package_list_unselect(pl, GPOINTER_TO_UINT(tmp->data));
}

static void rpm_find_verify_cb(RpmFindDialog *dlg, GList *indices,
			RpmPackageList *pl) {
  if (indices == NULL) return;
  if(geteuid())
  {
    GtkWidget *d=gnome_error_dialog(_("You need to be the superuser to verify packages."));
    gnome_dialog_run_and_close(GNOME_DIALOG(d));
    return;
  }
  verify_packages(pl->hdl, indices, 0);
}

static void rpm_find(GtkWidget *mi, RpmPackageList *pl) {
  GtkWidget *find;

  find = rpm_find_dialog_new(pl->hdl);
  gtk_signal_connect(GTK_OBJECT(find), "query_records",
		     GTK_SIGNAL_FUNC(rpm_find_query_cb), pl);
  gtk_signal_connect(GTK_OBJECT(find), "uninstall_records",
		     GTK_SIGNAL_FUNC(rpm_find_uninstall_cb), pl);
  gtk_signal_connect(GTK_OBJECT(find), "verify_records",
		     GTK_SIGNAL_FUNC(rpm_find_verify_cb), pl);
  gtk_widget_show(find);
}

static void gnorpm_update_tree(gchar *pkgName, gchar *groupName, void *data) {
  RpmPackageList *pl = RPM_PACKAGE_LIST(data);

  rpm_package_list_add_group(pl, groupName);
}

static void install_func(RpmQuery *info, RpmPackageList *pl) {
  if (install_one(info->hdl->root, info->fname, NULL, transFlags, probFilter,
		  interfaceFlags, gnorpm_update_tree, pl)) {
    gchar buf[512];
    g_snprintf(buf, 511, _("Couldn't install %s"), info->fname);
    message_box(buf);
  }
  rpm_package_list_update_pane(pl);
}
static void upgrade_func(RpmQuery *info, RpmPackageList *pl) {
  if (install_one(info->hdl->root, info->fname, NULL,
		  transFlags, probFilter, interfaceFlags | INTER_UPGRADE,
		  gnorpm_update_tree, pl)) {
    gchar buf[512];
    g_snprintf(buf, 511, _("Couldn't upgrade %s"), info->fname);
    message_box(buf);
  }
  rpm_package_list_update_pane(pl);
}
static void checksig_func(RpmQuery *info) {
  check_one_sig(info->fname, 0, NULL);
}

static void rpm_instpkgs_query(RpmInstallDialog *dlg, GList *files,
			       RpmPackageList *pl) {
  GtkWidget *query;

  if (files == NULL) return;
  query = rpm_query_dialog_new_from_files(pl->hdl, files);
  rpm_query_dialog_set_install_func(RPM_QUERY_DIALOG(query),
				    (GtkRpmCallback)install_func, pl);
  rpm_query_dialog_set_upgrade_func(RPM_QUERY_DIALOG(query),
				    (GtkRpmCallback)upgrade_func, pl);
  rpm_query_dialog_set_checksig_func(RPM_QUERY_DIALOG(query),
				      (GtkRpmCallback)checksig_func, NULL);
  rpm_query_dialog_set_close_func(RPM_QUERY_DIALOG(query),
				      (GtkRpmCallback)close_func, query);
  gtk_widget_show(query);
}

static void rpm_install_pkgs_cb(RpmInstallDialog *dlg, GList *files,
				RpmPackageList *pl) {
  gint res;

  if (files == NULL) {
    message_box(_("No packages selected"));
    return;
  }
  res = do_install(dlg->hdl->root, files, NULL, transFlags, probFilter,
		   interfaceFlags, gnorpm_update_tree, pl);
  if (res) {
    gchar buf[512];
    g_snprintf(buf, 511, _("Install of %d packages failed."), res);
    message_box(buf);
  }
  rpm_package_list_update_pane(pl);
}

static void rpm_upgrade_pkgs_cb(RpmInstallDialog *dlg, GList *files,
				RpmPackageList *pl) {
  gint res;

  if (files == NULL) {
    message_box(_("No packages selected"));
    return;
  }
  res = do_install(dlg->hdl->root, files, NULL,
		   transFlags, probFilter, interfaceFlags | INTER_UPGRADE,
		   gnorpm_update_tree, pl);

  if (res) {
    gchar buf[512];
    g_snprintf(buf, 511, _("Upgrade of %d packages failed."), res);
    message_box(buf);
  }

  rpm_package_list_update_pane(pl);
}

static void rpm_check_sigs_cb(RpmInstallDialog *dlg, GList *files) {
  if (files == NULL) {
    message_box(_("No packages selected"));
    return;
  }
  check_sigs(files, 0, NULL);
}

static void rpm_install_pkgs(GtkWidget *mi, RpmPackageList *pl) {
  GtkWidget *win;

  if(geteuid())
  {
    GtkWidget *d=gnome_error_dialog(_("You need to be the superuser to install packages."));
    gnome_dialog_run_and_close(GNOME_DIALOG(d));
    return;
  }

  win = rpm_install_dialog_new(pl->hdl);
  rpm_install_dialog_add_default_packages(RPM_INSTALL_DIALOG(win));
  gtk_window_set_title(GTK_WINDOW(win), _("Install"));
  gtk_signal_connect(GTK_OBJECT(win), "query",
		     GTK_SIGNAL_FUNC(rpm_instpkgs_query), pl);
  gtk_signal_connect(GTK_OBJECT(win), "install",
		     GTK_SIGNAL_FUNC(rpm_install_pkgs_cb), pl);
  gtk_signal_connect(GTK_OBJECT(win), "upgrade",
		     GTK_SIGNAL_FUNC(rpm_upgrade_pkgs_cb), pl);
  gtk_signal_connect(GTK_OBJECT(win), "checksig",
		     GTK_SIGNAL_FUNC(rpm_check_sigs_cb), NULL);
  gtk_widget_show(win);
  /* do_install( ... , installFlags ) */
}

static void mainwin_drop_cb(GnomeApp *app, GdkDragContext *context,
			    gint x, gint y, GtkSelectionData *selection_data,
			    guint info, guint time, RpmPackageList *pl) {
  GList *names, *list;
  GtkWidget *win;

  switch (info) {
  case TARGET_URI_LIST:
    win = rpm_install_dialog_new(pl->hdl);
    gtk_window_set_title(GTK_WINDOW(win), _("Install"));
    gtk_signal_connect(GTK_OBJECT(win), "query",
		       GTK_SIGNAL_FUNC(rpm_instpkgs_query), pl);
    gtk_signal_connect(GTK_OBJECT(win), "install",
		       GTK_SIGNAL_FUNC(rpm_install_pkgs_cb), pl);
    gtk_signal_connect(GTK_OBJECT(win), "checksig",
		       GTK_SIGNAL_FUNC(rpm_check_sigs_cb), NULL);
    list = gnome_uri_list_extract_filenames(selection_data->data);
    for (names = list; names; names = names->next)
      rpm_install_dialog_add_file(RPM_INSTALL_DIALOG(win), names->data, TRUE);
    gnome_uri_list_free_strings(list);
    gtk_widget_show(win);
    break;
  default:
  }
}

#ifdef WITH_RPMFIND
static void rpm_show_web_find(GtkWidget *mi, RpmPackageList *pl) {
  GtkWidget *win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  GtkWidget *webfind = rpm_web_find_new(pl);

  gtk_signal_connect_object(GTK_OBJECT(RPM_WEB_FIND(webfind)->info), "close",
			    GTK_SIGNAL_FUNC(gtk_widget_destroy),
			    GTK_OBJECT(win));
  gtk_window_set_title(GTK_WINDOW(win), _("Rpmfind"));
  gtk_object_weakref(GTK_OBJECT(mi), (GtkDestroyNotify) mainwin_closed,
 		     NULL);
  rpm_web_find_set_callbacks(RPM_WEB_FIND(webfind),
			     GTK_SIGNAL_FUNC(rpm_install_pkgs_cb),
			     GTK_SIGNAL_FUNC(rpm_upgrade_pkgs_cb),
			     GTK_SIGNAL_FUNC(rpm_instpkgs_query),
			     GTK_SIGNAL_FUNC(rpm_check_sigs_cb));
  set_icon(win);
  gtk_container_add(GTK_CONTAINER(win), webfind);
  gtk_widget_show(webfind);
  gtk_widget_show(win);
}
#endif

static void rpm_dentry_pkgs(GtkWidget *mi, RpmPackageList *pl) {
  GtkWidget *win;

  win = rpm_dentry_edit_new(pl->hdl, pl->selection);
  gtk_widget_show(win);
}

static void rpm_preferences_apply_cb(RpmPropsBox *box, gint page_num,
				     RpmPackageList *pl) {
  switch (page_num) {
  case RPM_PROPS_FLAGS:
    interfaceFlags = box->interfaceFlags;
    transFlags     = box->transFlags;
    probFilter     = box->probFilter;
    break;
  case RPM_PROPS_PKGLIST:
    if (GTK_TOGGLE_BUTTON(box->asIcons)->active)
      rpm_package_list_set_mode(pl, RPM_PACKAGE_ILIST);
    else
      rpm_package_list_set_mode(pl, RPM_PACKAGE_CLIST);
  }
}

static void rpm_preferences_destroy_cb(GtkWidget *prefs, gpointer *ptr) {
  *ptr = NULL;
}

static void rpm_preferences_cb(GtkWidget *mi, RpmPackageList *pl) {
  static GtkWidget *win = NULL;
  static gint in_setup = 0;

  /* The UI style guides say that more than one preferences dialog is bad.
   * We also need to check that we aren't currently setting up the box and just
   * waiting for a download to finish. If in_setup is set, just do nothing and
   * the user can wait for the currently running box to appear. Otherwise, bad
   * things happen. */
  if (win) {
    if (win->window)
      gdk_window_raise(win->window);
  } else if (!in_setup) {
    in_setup = 1;
    win = rpm_props_box_new();
    gnome_dialog_set_parent(GNOME_DIALOG(win), GTK_WINDOW(gnorpm_app));
    gtk_signal_connect(GTK_OBJECT(win), "destroy",
		       GTK_SIGNAL_FUNC(rpm_preferences_destroy_cb), &win);
    gtk_signal_connect(GTK_OBJECT(win), "apply",
		       GTK_SIGNAL_FUNC(rpm_preferences_apply_cb), pl);
    gtk_widget_show(win);
	in_setup = 0;
  }
}

static void context_query(GtkWidget *mi, gpointer index_p) {
  RpmPackageList *pl = gtk_object_get_data(GTK_OBJECT(mi), "package-list");

  generic_query(pl, index_p);
}

static void generic_query(RpmPackageList *pl, gpointer index_p) {
  GList item;

  item.data = index_p;
  item.next = item.prev = NULL;
  rpm_query_display(pl, &item);
}

static void context_uninstall(GtkWidget *mi, gpointer index_p) {
  gint failed;
  guint index = GPOINTER_TO_UINT(index_p);

  RpmPackageList *pl = gtk_object_get_data(GTK_OBJECT(mi), "package-list");

  failed = uninstall_one(pl->hdl->root, index, transFlags, probFilter,
			 interfaceFlags);
  if (failed) {
    char buf[512];
    g_snprintf(buf, 512, _("%d packages couldn't be uninstalled"), failed);
    message_box(buf);
    return;
  }
  rpm_package_list_unselect(pl, index);
  rpm_package_list_update_pane(pl);
}

static void context_verify(GtkWidget *mi, gpointer index_p) {
  guint index = GPOINTER_TO_UINT(index_p);
  RpmPackageList *pl = gtk_object_get_data(GTK_OBJECT(mi), "package-list");

  verify_one(pl->hdl, index, 0);
}

static GnomeUIInfo context_menu[] = {
    GNOMEUIINFO_ITEM_STOCK(N_("_Query"),
			N_("Get information about the selected packages"),
			context_query, RPM_STOCK_MENU_QUERY),
    GNOMEUIINFO_ITEM_STOCK(N_("_Uninstall"),
			N_("Uninstall the selected packages"),
			context_uninstall, RPM_STOCK_MENU_UNINSTALL),
    GNOMEUIINFO_ITEM_STOCK(N_("_Verify"),
			N_("Verify the selected packages"),
			context_verify, RPM_STOCK_MENU_VERIFY),
    GNOMEUIINFO_END
};

static GnomeUIInfo context_menu_user[] = {
    GNOMEUIINFO_ITEM_STOCK(N_("_Query"),
			N_("Get information about the selected packages"),
			context_query, RPM_STOCK_MENU_QUERY),
    GNOMEUIINFO_END
};

static void mainwin_context_menu(RpmPackageList *pl, GdkEventButton *event,
				 guint index) {
  GtkWidget *menu;

  menu = gtk_object_get_data(GTK_OBJECT(pl), "context-menu");
  if (!menu) {
    if(geteuid())
    {
      menu = gnome_popup_menu_new(context_menu_user);
      gnome_app_install_menu_hints(gnorpm_app, context_menu_user);
      gtk_object_set_data(GTK_OBJECT(context_menu_user[0].widget), "package-list",pl);
    }
    else
    {
      menu = gnome_popup_menu_new(context_menu);
      gnome_app_install_menu_hints(gnorpm_app, context_menu);
      gtk_object_set_data(GTK_OBJECT(context_menu[0].widget), "package-list",pl);
      gtk_object_set_data(GTK_OBJECT(context_menu[1].widget), "package-list",pl);
      gtk_object_set_data(GTK_OBJECT(context_menu[2].widget), "package-list",pl);
    }
    gtk_object_set_data(GTK_OBJECT(pl), "context-menu", menu);
  }
  gnome_popup_menu_do_popup(menu, NULL, NULL, event, GUINT_TO_POINTER(index));
}
