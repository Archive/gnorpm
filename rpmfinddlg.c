/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "rpmfinddlg.h"
#include <gdk/gdkkeysyms.h>
#include "misc.h"
#include "pixmaps.h"

enum {
  QUERY_RECORDS,
  UNINSTALL_RECORDS,
  VERIFY_RECORDS,
  LAST_SIGNAL
};
static guint finddlg_signals[LAST_SIGNAL];

enum { FIND_BY_FILE, FIND_BY_GROUP, FIND_BY_PROVIDES,
       FIND_BY_REQUIREDBY, FIND_BY_CONFLICTS, FIND_BY_LABEL,
     };

static void rpm_find_dialog_class_init(RpmFindDialogClass *klass);
static void rpm_find_dialog_init(RpmFindDialog *dlg);
static void rpm_find_dialog_marshal_signal(GtkObject *object,
					   GtkSignalFunc func, gpointer data,
					   GtkArg *args);

static void menu_callback(RpmFindDialog *dlg, guint fint_type, GtkWidget *wid);
static void rpm_find_dialog_find(RpmFindDialog *dlg);
static void rpm_find_dialog_query(RpmFindDialog *dlg);
static void rpm_find_dialog_uninstall(RpmFindDialog *dlg);
static void rpm_find_dialog_verify(RpmFindDialog *dlg);

guint rpm_find_dialog_get_type(void) {
  static guint finddialog_type = 0;
  if (!finddialog_type) {
    GtkTypeInfo finddialog_info = {
      "RpmFindDialog",
      sizeof(RpmFindDialog),
      sizeof(RpmFindDialogClass),
      (GtkClassInitFunc) rpm_find_dialog_class_init,
      (GtkObjectInitFunc) rpm_find_dialog_init,
      (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };
    finddialog_type = gtk_type_unique(gtk_dialog_get_type(), &finddialog_info);
  }
  return finddialog_type;
}

static void rpm_find_dialog_class_init(RpmFindDialogClass *klass) {
  finddlg_signals[QUERY_RECORDS] =
    gtk_signal_new("query_records",
		   GTK_RUN_FIRST,
		   GTK_OBJECT_CLASS(klass)->type,
		   GTK_SIGNAL_OFFSET(RpmFindDialogClass, query_records),
		   rpm_find_dialog_marshal_signal,
		   GTK_TYPE_NONE, 1,
		   GTK_TYPE_POINTER); /* a GList of gints */
  finddlg_signals[UNINSTALL_RECORDS] =
    gtk_signal_new("uninstall_records",
		   GTK_RUN_FIRST,
		   GTK_OBJECT_CLASS(klass)->type,
		   GTK_SIGNAL_OFFSET(RpmFindDialogClass, uninstall_records),
		   rpm_find_dialog_marshal_signal,
		   GTK_TYPE_NONE, 1,
		   GTK_TYPE_POINTER); /* a GList of gints */
  finddlg_signals[VERIFY_RECORDS] =
    gtk_signal_new("verify_records",
		   GTK_RUN_FIRST,
		   GTK_OBJECT_CLASS(klass)->type,
		   GTK_SIGNAL_OFFSET(RpmFindDialogClass, verify_records),
		   rpm_find_dialog_marshal_signal,
		   GTK_TYPE_NONE, 1,
		   GTK_TYPE_POINTER); /* a GList of gints */
  gtk_object_class_add_signals(GTK_OBJECT_CLASS(klass), finddlg_signals,
			       LAST_SIGNAL);
}

static void rpm_find_dialog_init(RpmFindDialog *self) {
  GtkWidget *vbox, *hbox, *button, *wid;
  GtkItemFactoryEntry entries[] = {
    { N_("/contain file"), NULL, (GtkItemFactoryCallback)menu_callback,
      FIND_BY_FILE, NULL },
    { N_("/are in the group"), NULL, (GtkItemFactoryCallback)menu_callback,
      FIND_BY_GROUP, NULL },
    { N_("/provide"), NULL, (GtkItemFactoryCallback)menu_callback,
      FIND_BY_PROVIDES, NULL },
    { N_("/require"), NULL, (GtkItemFactoryCallback)menu_callback,
      FIND_BY_REQUIREDBY, NULL },
    { N_("/conflict with"), NULL, (GtkItemFactoryCallback)menu_callback,
      FIND_BY_CONFLICTS, NULL },
    { N_("/match label"), NULL, (GtkItemFactoryCallback)menu_callback,
      FIND_BY_LABEL, NULL },
  };
  GtkItemFactory *ifactory;
  gint i;

  for (i = 0; i < sizeof(entries)/sizeof(GtkItemFactoryEntry); i++)
    entries[i].path = _(entries[i].path);
  gtk_window_set_title(GTK_WINDOW(self), _("Find Packages"));
  set_icon(GTK_WIDGET(self));
  
  vbox = gtk_vbox_new(FALSE, 5);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(self)->vbox), vbox, TRUE, TRUE, 0);
  gtk_widget_show(vbox);

  hbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, TRUE, 0);
  gtk_widget_show(hbox);

  wid = gtk_label_new(_("Find packages that"));
  gtk_box_pack_start(GTK_BOX(hbox), wid, FALSE, TRUE, 0);
  gtk_widget_show(wid);

  ifactory = gtk_item_factory_new(GTK_TYPE_MENU, "<find>", NULL);
  gtk_item_factory_create_items(ifactory,
				sizeof(entries)/sizeof(GtkItemFactoryEntry),
				entries, self);
  wid = gtk_option_menu_new();
  gtk_option_menu_set_menu(GTK_OPTION_MENU(wid), ifactory->widget);
  gtk_box_pack_start(GTK_BOX(hbox), wid, FALSE, TRUE, 0);
  gtk_widget_show(wid);

  self->gentry = gnome_entry_new("RpmFindDlgHistory");
  gtk_combo_disable_activate(GTK_COMBO(self->gentry));
  self->entry = gnome_entry_gtk_entry(GNOME_ENTRY(self->gentry));
  gtk_signal_connect_object(GTK_OBJECT(self->entry), "activate",
			    GTK_SIGNAL_FUNC(rpm_find_dialog_find),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(hbox), self->gentry, TRUE, TRUE, 0);
  gtk_widget_show(self->gentry);

  button = gtk_button_new_with_label(_("Find"));
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_find_dialog_find),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, TRUE, 0);
  gtk_widget_show(button);

  hbox = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(hbox),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);

  self->list = gtk_list_new();
  gtk_list_set_selection_mode(GTK_LIST(self->list), GTK_SELECTION_MULTIPLE);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(hbox), self->list);
  gtk_widget_show(self->list);

  /* it looks nicer if all buttons are the same size */
  gtk_box_set_homogeneous(GTK_BOX(GTK_DIALOG(self)->action_area), TRUE);

  button = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
				RPM_STOCK_PIXMAP_QUERY), _("Query"));
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_find_dialog_query),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(self)->action_area), button,
		     FALSE, FALSE, 0);
  gtk_widget_show(button);

  button = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
				RPM_STOCK_PIXMAP_UNINSTALL), _("Uninstall"));
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_find_dialog_uninstall),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(self)->action_area), button,
		     FALSE, FALSE, 0);
  gtk_widget_show(button);

  button = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
				RPM_STOCK_PIXMAP_VERIFY), _("Verify"));
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_find_dialog_verify),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(self)->action_area), button,
		     FALSE, FALSE, 0);
  gtk_widget_show(button);

  button = gnome_stock_button(GNOME_STOCK_BUTTON_CLOSE);
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(gtk_widget_destroy),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(self)->action_area), button,
		     FALSE, FALSE, 0);
  gtk_widget_show(button);

  self->hdl = NULL;
}

GtkWidget *rpm_find_dialog_new(DBHandle *hdl) {
  RpmFindDialog *self = gtk_type_new(rpm_find_dialog_get_type());

  self->hdl = hdl;
  return GTK_WIDGET(self);
}

static void menu_callback(RpmFindDialog *dlg, guint find_type,
			  GtkWidget *wid) {
  dlg->find_type = find_type;
}

static void rpm_find_dialog_find(RpmFindDialog *self) {
  GtkWidget *item;
  GList *items = NULL;
#ifdef HAVE_RPM_4_0
  rpmdbMatchIterator mi;
  gint rpmtag = -1;
#else
  dbiIndexSet matches;
  gchar buf[512], *s1, *s2, *s3;
  gint res = 1, i;
#endif
  gchar *search;
  Header h;

  g_return_if_fail(self->hdl != NULL);
  db_handle_db_up(self->hdl);
  g_return_if_fail(self->hdl->db != NULL);
  
  search = gtk_entry_get_text(GTK_ENTRY(self->entry));

#ifdef HAVE_RPM_4_0
  switch (self->find_type) {
  case FIND_BY_FILE:
    rpmtag = RPMTAG_BASENAMES;         break;
  case FIND_BY_GROUP:
    rpmtag = RPMTAG_GROUP;             break;
  case FIND_BY_PROVIDES:
    rpmtag = RPMTAG_PROVIDENAME;       break;
  case FIND_BY_REQUIREDBY:
    rpmtag = RPMTAG_REQUIRENAME;       break;
  case FIND_BY_CONFLICTS:
    rpmtag = RPMTAG_CONFLICTNAME;      break;
  case FIND_BY_LABEL:
    rpmtag = RPMDBI_LABEL;             break;
  }
  gtk_list_clear_items(GTK_LIST(self->list), 0, -1);
  if (rpmtag < 0) return;
  mi = rpmdbInitIterator(self->hdl->db, rpmtag, search, 0);
  while ((h = rpmdbNextIterator(mi)) != NULL) {
    const gchar *s1, *s2, *s3;
    gchar buf[512];
    headerNVR(h, &s1, &s2, &s3);
    g_snprintf(buf, sizeof(buf)-1, "%s-%s-%s", s1, s2, s3);
    item = gtk_list_item_new_with_label(buf);
    gtk_object_set_user_data(GTK_OBJECT(item), 
	                     GINT_TO_POINTER(rpmdbGetIteratorOffset(mi)));
    items = g_list_prepend(items, item);
    gtk_widget_show(item);
  }
  gtk_list_append_items(GTK_LIST(self->list), items);
  db_handle_db_down(self->hdl);

#else  /* !HAVE_RPM_4_0 */

  switch (self->find_type) {
  case FIND_BY_FILE:
    res = rpmdbFindByFile(self->hdl->db, search, &matches);      break;
  case FIND_BY_GROUP:
    res = rpmdbFindByGroup(self->hdl->db, search, &matches);      break;
  case FIND_BY_PROVIDES:
    res = rpmdbFindByProvides(self->hdl->db, search, &matches);   break;
  case FIND_BY_REQUIREDBY:
    res = rpmdbFindByRequiredBy(self->hdl->db, search, &matches); break;
  case FIND_BY_CONFLICTS:
    res = rpmdbFindByConflicts(self->hdl->db, search, &matches);  break;
  case FIND_BY_LABEL:
    res = rpmdbFindByLabel(self->hdl->db, search, &matches);      break;
  }
  gtk_list_clear_items(GTK_LIST(self->list), 0, -1);
  if (res) return;
  for (i = matches.count - 1; i >= 0; i--) {
    gint index;
    index = matches.recs[i].recOffset;
    if (!index) continue;  /* some queries return this index causing a segv */
    h = rpmdbGetRecord(self->hdl->db, index);
    headerGetEntry(h, RPMTAG_NAME,    NULL, (void**)&s1, NULL);
    headerGetEntry(h, RPMTAG_VERSION, NULL, (void**)&s2, NULL);
    headerGetEntry(h, RPMTAG_RELEASE, NULL, (void**)&s3, NULL);
    g_snprintf(buf, 511, "%s-%s-%s", s1, s2, s3);
    headerFree(h);
    item = gtk_list_item_new_with_label(buf);
    gtk_object_set_user_data(GTK_OBJECT(item), GINT_TO_POINTER(index));
    items = g_list_prepend(items, item);
    gtk_widget_show(item);
  }
  gtk_list_append_items(GTK_LIST(self->list), items);
  dbiFreeIndexRecord(matches);
  db_handle_db_down(self->hdl);
#endif /* HAVE_RPM_4_0 */
}

static void rpm_find_dialog_query(RpmFindDialog *self) {
  GList *indices = NULL, *selection;
  GtkObject *item;

  selection = GTK_LIST(self->list)->selection;
  for (; selection != NULL; selection = selection->next) {
    item = selection->data;
    indices = g_list_prepend(indices, gtk_object_get_user_data(item));
  }
  gtk_signal_emit(GTK_OBJECT(self), finddlg_signals[QUERY_RECORDS], indices);
  g_list_free(indices);
}

static void rpm_find_dialog_uninstall(RpmFindDialog *self) {
  GList *indices = NULL, *selection;
  GtkObject *item;

  selection = GTK_LIST(self->list)->selection;
  for (; selection != NULL; selection = selection->next) {
    item = selection->data;
    indices = g_list_prepend(indices, gtk_object_get_user_data(item));
  }
  gtk_signal_emit(GTK_OBJECT(self), finddlg_signals[UNINSTALL_RECORDS],
		  indices);
  g_list_free(indices);
}

static void rpm_find_dialog_verify(RpmFindDialog *self) {
  GList *indices = NULL, *selection;
  GtkObject *item;

  selection = GTK_LIST(self->list)->selection;
  for (; selection != NULL; selection = selection->next) {
    item = selection->data;
    indices = g_list_prepend(indices, gtk_object_get_user_data(item));
  }
  gtk_signal_emit(GTK_OBJECT(self), finddlg_signals[VERIFY_RECORDS], indices);
  g_list_free(indices);
}

static void rpm_find_dialog_marshal_signal(GtkObject *object,
					   GtkSignalFunc func, gpointer data,
					   GtkArg *args) {
  typedef void (*sig_func)(GtkObject *o, GList *indices, gpointer data);
  sig_func rfunc = (sig_func)func;

  (*rfunc)(object, GTK_VALUE_POINTER(args[0]), data);
}
