/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __RPM_PROPS_H__
#define __RPM_PROPS_H__

#include <gnome.h>

#define RPM_PROPS_BOX(obj) GTK_CHECK_CAST(obj, rpm_props_box_get_type(), RpmPropsBox)
#define RPM_PROPS_BOX_CLASS(class) GTK_CHECK_CAST_CLASS(class, rpm_props_box_get_type(), RpmPropsBoxClass)
#define RPM_IS_PROPS_BOX(obj) GTK_CHECK_TYPE(obj, rpm_props_box_get_type())

/* these are the page numbers */
#define RPM_PROPS_FLAGS 0
#define RPM_PROPS_PKGLIST 1
#define RPM_PROPS_INSTALL 2

#ifdef WITH_RPMFIND
#define RPM_PROPS_NETWORK 3
#define RPM_PROPS_RPMFIND 4
#define RPM_PROPS_DISTRIBS 5
#endif

typedef struct _RpmPropsBox RpmPropsBox;
typedef struct _RpmPropsBoxClass RpmPropsBoxClass;

struct _RpmPropsBox {
  GnomePropertyBox parent;

  /* flag members */
  GtkWidget **flags;
  gint32 interfaceFlags, transFlags, probFilter;

  /* package list members */
  GtkWidget *asList, *asIcons;

  /* install window members */
  GtkWidget *oldColour, *currentColour, *newColour;
  GtkWidget *rpmPath, *rpmDirs;

#ifdef WITH_RPMFIND
  /* network members */
  GtkWidget *httpProxy, *ftpProxy, *proxyUser, *proxyPass;
  GtkWidget *expires, *hostname;
  /* rpmfind members */
  GtkWidget *metadataServer, *downloadDir, *vendor, *distrib;
  GtkWidget *wantSource, *wantLatest, *noUpgrades;
  /* distributions members */
  GtkWidget *distClist, *distName, *distOrigin, *distSources;
  GtkWidget *distRating, *distMirror, *distChange;
#endif
};

struct _RpmPropsBoxClass {
  GnomePropertyBoxClass parent_class;
};

guint rpm_props_box_get_type(void);
GtkWidget *rpm_props_box_new(void);

/* read properties from the config file, and construct flags */
void rpm_props_box_get_flags(gint32 *interfaceFlags, gint32 *installFlags,
			     gint32 *uninstallFlags);
#endif

