/* define this to the name of the package */
#undef PACKAGE

/* define this to the version number of the program */
#undef VERSION

/* define if you are using rpm >= 4.0.2 */
#undef HAVE_RPM_4_0_2

/* define if you are using rpm >= 4.0.1 */
#undef HAVE_RPM_4_0_1

/* define if you are using rpm >= 4.0 */
#undef HAVE_RPM_4_0

/* define if you are using rpm >= 3.0 */
#undef HAVE_RPM_3_0

/* if rpmfind extensions have been enabled */
#undef WITH_RPMFIND

/* define to enable National Language Support */
#undef ENABLE_NLS

/* define if you have the "catgets" message translation function */
#undef HAVE_CATGETS

/* define if you have the "gettext" message translation function */
#undef HAVE_GETTEXT

/* define if you have lc_messages */
#undef HAVE_LC_MESSAGES

/* define if you have the stpcpy function */
#undef HAVE_STPCPY

/* define if netdb.h defines h_errno */
#undef HAVE_HERRNO

