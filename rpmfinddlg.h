/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __RPM_FIND_DLG_H__
#define __RPM_FIND_DLG_H__

#include <gnome.h>
#include <rpmlib.h>
#include "dbhandle.h"

#define RPM_FIND_DIALOG(obj) GTK_CHECK_CAST(obj, rpm_find_dialog_get_type(), RpmFindDialog)
#define RPM_FIND_DIALOG_CLASS(class) GTK_CHECK_CAST_CLASS(class, rpm_find_dialog_get_type(), RpmFindDialogClass)
#define RPM_IS_FIND_DIALOG(obj) GTK_CHECK_TYPE(obj, rpm_find_dialog_get_type())

typedef struct _RpmFindDialog RpmFindDialog;
typedef struct _RpmFindDialogClass RpmFindDialogClass;

struct _RpmFindDialog {
  GtkDialog parent;
  DBHandle *hdl;
  gint find_type;
  GtkWidget *entry; /* the search string */
  GtkWidget *gentry; /* the gnome entry */
  GtkWidget *list;  /* the list of find results */
};

struct _RpmFindDialogClass {
  GtkDialogClass parent_class;

  void (*query_records)(RpmFindDialog *dlg, GList *indices);
  void (*uninstall_records)(RpmFindDialog *dlg, GList *indices);
  void (*verify_records)(RpmFindDialog *dlg, GList *indices);
};

guint rpm_find_dialog_get_type(void);
GtkWidget *rpm_find_dialog_new(DBHandle *hdl);

#endif
