/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "rpminstalldlg.h"
#include "rpmquerydlg.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include "misc.h"
#include "pixmaps.h"
#include "rpmcompat.h"

#include "package.xpm"
#include "dir-open.xpm"
#include "dir-close.xpm"
#include "checkon.xpm"
#include "checkoff.xpm"

enum {
  QUERY,
  INSTALL,
  UPGRADE,
  CHECKSIG,
  LAST_SIGNAL
};
static guint installdlg_signals[LAST_SIGNAL];

typedef enum {
  UNKNOWN_PACKAGE,  /* no version of the package is installed on the system */
  CURRENT_PACKAGE,  /* this exact package is installed on the system */
  OLD_PACKAGE,      /* this package is older than the version on the system */
  NEW_PACKAGE       /* this package is newer than the version on the system */
} PackageStatus;

typedef struct {
  PackageStatus status;
  gboolean selected;
  gchar *filename;
  gchar *rpmname;
  gchar *version;
  gchar *release;
} PackageInfo;

static GtkObjectClass *parent_class = NULL;

enum { TARGET_URI_LIST };
static GtkTargetEntry drop_types[] = {
  { "text/uri-list", 0, TARGET_URI_LIST}
};
static gint n_drop_types = sizeof(drop_types) / sizeof(drop_types[0]);

static void rpm_install_dialog_class_init(RpmInstallDialogClass *klass);
static void rpm_install_dialog_init(RpmInstallDialog *dlg);

static void rpm_install_dialog_refilter(RpmInstallDialog *self);
static void rpm_install_dialog_show_filesel(RpmInstallDialog *dlg);
static void rpm_install_dialog_select(RpmInstallDialog *dlg);
static void rpm_install_dialog_unselect(RpmInstallDialog *dlg);
static void rpm_install_dialog_expand(RpmInstallDialog *dlg);
static void rpm_install_dialog_collapse(RpmInstallDialog *dlg);
static void rpm_install_dialog_destroy(GtkObject *obj);
static void rpm_install_dialog_select_row(RpmInstallDialog *dlg,
					  GtkCTreeNode *node, gint column);
static gint rpm_install_dialog_tree_click(RpmInstallDialog *dlg,
					  GdkEventButton *event);

static void rpm_install_dialog_query(RpmInstallDialog *dlg);
static void rpm_install_dialog_install(RpmInstallDialog *dlg);
static void rpm_install_dialog_upgrade(RpmInstallDialog *dlg);
static void rpm_install_dialog_checksig(RpmInstallDialog *dlg);
static void rpm_install_dialog_marshal_signal(GtkObject *object,
					      GtkSignalFunc func,
					      gpointer data, GtkArg *args);
static void rpm_install_dialog_drop_cb(RpmInstallDialog *dlg,
				       GdkDragContext *context,
				       gint x, gint y,
				       GtkSelectionData *selection_data,
				       guint info, guint time);

static GtkWidget *info_frame_new(void);
static void info_frame_clear(GtkWidget *frame);
static void info_frame_set(GtkWidget *frame, gchar *filename);

guint rpm_install_dialog_get_type(void) {
  static guint instdlg_type = 0;
  if (!instdlg_type) {
    GtkTypeInfo instdlg_info = {
      "RpmInstallDialog",
      sizeof(RpmInstallDialog),
      sizeof(RpmInstallDialogClass),
      (GtkClassInitFunc) rpm_install_dialog_class_init,
      (GtkObjectInitFunc) rpm_install_dialog_init,
      (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
    };
    instdlg_type = gtk_type_unique(gtk_dialog_get_type(), &instdlg_info);
  }
  return instdlg_type;
}

static void rpm_install_dialog_class_init(RpmInstallDialogClass *klass) {
  GtkObjectClass *object_class;

  object_class = GTK_OBJECT_CLASS(klass);
  parent_class = gtk_type_class(gtk_dialog_get_type());

  installdlg_signals[QUERY] =
    gtk_signal_new("query",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmInstallDialogClass, query),
		   rpm_install_dialog_marshal_signal,
		   GTK_TYPE_NONE, 1,
		   GTK_TYPE_POINTER);
  installdlg_signals[INSTALL] =
    gtk_signal_new("install",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmInstallDialogClass, install),
		   rpm_install_dialog_marshal_signal,
		   GTK_TYPE_NONE, 1,
		   GTK_TYPE_POINTER);
  installdlg_signals[UPGRADE] =
    gtk_signal_new("upgrade",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmInstallDialogClass, upgrade),
		   rpm_install_dialog_marshal_signal,
		   GTK_TYPE_NONE, 1,
		   GTK_TYPE_POINTER);
  installdlg_signals[CHECKSIG] =
    gtk_signal_new("checksig",
		   GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(RpmInstallDialogClass, checksig),
		   rpm_install_dialog_marshal_signal,
		   GTK_TYPE_NONE, 1,
		   GTK_TYPE_POINTER);
  gtk_object_class_add_signals(object_class, installdlg_signals, LAST_SIGNAL);

  object_class->destroy = rpm_install_dialog_destroy;
}

static void resize_column(GtkCList *ctree, GtkAllocation *a) {
  gint size;

  /* where did I get the number 15? It is 3*CELL_SPACING + 4*COLUMN_INSET
   * (constants from gtkclist.c).  That is, COLUMN_INSET pixels on each
   * side of each column, and CELL_SPACING pixels between columns (I
   * think the ends are included in this number) */
  size = ctree->hadjustment->page_size - ctree->column[1].width - 15;
  gtk_clist_set_column_width(ctree, 0, size);
}

static void menu_callback(RpmInstallDialog *self, RpmInstallFilter filter,
			  GtkWidget *wid) {
  if (self->filter != filter) {
    self->filter = filter;
    rpm_install_dialog_refilter(self);
  }
}

static void rpm_install_dialog_init(RpmInstallDialog *self) {
  GtkWidget *hbox, *bbox, *button, *wid;
  GtkItemFactory *ifactory;
  gchar *text[2] = { N_("Name"), "" };
  gint i;
  GtkItemFactoryEntry entries[] = {
    { N_("/All packages"), NULL, (GtkItemFactoryCallback)menu_callback,
      INSTALL_FILTER_ALL, NULL },
    { N_("/All but installed packages"), NULL,
      (GtkItemFactoryCallback)menu_callback,INSTALL_FILTER_NOT_INSTALLED,NULL},
    { N_("/Only uninstalled packages"), NULL,
      (GtkItemFactoryCallback)menu_callback,INSTALL_FILTER_UNINSTALLED, NULL },
    { N_("/Only newer packages"), NULL,
      (GtkItemFactoryCallback)menu_callback, INSTALL_FILTER_NEWER, NULL },
    { N_("/Uninstalled or newer packages"), NULL,
      (GtkItemFactoryCallback)menu_callback,
      INSTALL_FILTER_UNINSTALLED_OR_NEWER, NULL },
  };

  /* set up drag and drop thingy */
  gtk_drag_dest_set(GTK_WIDGET(self),
		    GTK_DEST_DEFAULT_MOTION |
		    GTK_DEST_DEFAULT_HIGHLIGHT |
		    GTK_DEST_DEFAULT_DROP,
		    drop_types, n_drop_types,
		    GDK_ACTION_COPY);
  gtk_signal_connect(GTK_OBJECT(self), "drag_data_received",
		     GTK_SIGNAL_FUNC(rpm_install_dialog_drop_cb), NULL);

  set_icon(GTK_WIDGET(self));

  for (i = 0; i < sizeof(entries)/sizeof(GtkItemFactoryEntry); i++)
    entries[i].path = _(entries[i].path);
  ifactory = gtk_item_factory_new(GTK_TYPE_MENU, "<install>", NULL);
  gtk_item_factory_create_items(ifactory,
				sizeof(entries)/sizeof(GtkItemFactoryEntry),
				entries, self);

  hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
  gtk_container_set_border_width(GTK_CONTAINER(hbox), GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(self)->vbox), hbox, FALSE, TRUE, 0);
  gtk_widget_show(hbox);

  wid = gtk_label_new(_("Filter:"));
  gtk_box_pack_start(GTK_BOX(hbox), wid, FALSE, TRUE, 0);
  gtk_widget_show(wid);
  
  self->filter_menu = gtk_option_menu_new();
  gtk_option_menu_set_menu(GTK_OPTION_MENU(self->filter_menu),
			   ifactory->widget);
  gtk_option_menu_set_history(GTK_OPTION_MENU(self->filter_menu),
			      INSTALL_FILTER_NOT_INSTALLED);
  gtk_box_pack_start(GTK_BOX(hbox), self->filter_menu, FALSE, TRUE, 0);
  gtk_widget_show(self->filter_menu);

  hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
  gtk_container_set_border_width(GTK_CONTAINER(hbox), GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(self)->vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);

  wid = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_usize(wid, 200, 150);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(wid),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start(GTK_BOX(hbox), wid, TRUE, TRUE, 0);
  gtk_widget_show(wid);

  /* set up the ctree ... */
  text[0] = _(text[0]);
  self->tree = gtk_ctree_new_with_titles(2, 0, text);
  gtk_ctree_set_indent(GTK_CTREE(self->tree), 10);
  gtk_clist_set_selection_mode(GTK_CLIST(self->tree), GTK_SELECTION_BROWSE);
  gtk_clist_set_column_width(GTK_CLIST(self->tree), 0, 285);
  gtk_clist_set_column_width(GTK_CLIST(self->tree), 1, 10);
  gtk_clist_set_column_max_width(GTK_CLIST(self->tree), 1, 10);
  gtk_clist_set_column_min_width(GTK_CLIST(self->tree), 1, 10);
  gtk_signal_connect_after(GTK_OBJECT(self->tree), "size_allocate",
			   GTK_SIGNAL_FUNC(resize_column), NULL);

  self->open_p = gdk_pixmap_colormap_create_from_xpm_d(NULL,
        gtk_widget_get_colormap(GTK_WIDGET(self)), &(self->open_b), NULL,
        DIRECTORY_OPEN_XPM);
  self->closed_p = gdk_pixmap_colormap_create_from_xpm_d(NULL,
        gtk_widget_get_colormap(GTK_WIDGET(self)), &(self->closed_b), NULL,
        DIRECTORY_CLOSE_XPM);
  self->pkg_p = gdk_pixmap_colormap_create_from_xpm_d(NULL,
	gtk_widget_get_colormap(GTK_WIDGET(self)), &(self->pkg_b), NULL,
        package_xpm);
  self->checkon_p = gdk_pixmap_colormap_create_from_xpm_d(NULL,
        gtk_widget_get_colormap(GTK_WIDGET(self)), NULL, NULL,
        checkon_xpm);
  self->checkoff_p = gdk_pixmap_colormap_create_from_xpm_d(NULL,
        gtk_widget_get_colormap(GTK_WIDGET(self)), NULL, NULL,
        checkoff_xpm);

  text[0] = _("Packages");
  self->base = gtk_ctree_insert_node(GTK_CTREE(self->tree), NULL, NULL,
				     text, 2, self->closed_p, self->closed_b,
				     self->open_p, self->open_b, FALSE,
				     TRUE);
  gtk_ctree_select(GTK_CTREE(self->tree), self->base);

  gtk_container_add(GTK_CONTAINER(wid), self->tree);
  gtk_widget_show(self->tree);

  gtk_signal_connect_object(GTK_OBJECT(self->tree), "button_press_event",
			    GTK_SIGNAL_FUNC(rpm_install_dialog_tree_click),
			    GTK_OBJECT(self));
  gtk_signal_connect_object(GTK_OBJECT(self->tree), "tree_select_row",
			    GTK_SIGNAL_FUNC(rpm_install_dialog_select_row),
			    GTK_OBJECT(self));

  self->ht = g_hash_table_new(g_str_hash, g_str_equal);

  bbox = gtk_vbutton_box_new();
  gtk_button_box_set_layout(GTK_BUTTON_BOX(bbox), GTK_BUTTONBOX_START);
  gtk_box_pack_start(GTK_BOX(hbox), bbox, FALSE, TRUE, 0);
  gtk_widget_show(bbox);

  button = gtk_button_new_with_label(_("Add"));
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_install_dialog_show_filesel),
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(bbox), button);
  gtk_widget_show(button);

  button = gtk_button_new_with_label(_("Select\nAll"));
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_install_dialog_select),
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(bbox), button);
  gtk_widget_show(button);

  button = gtk_button_new_with_label(_("Unselect\nAll"));
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_install_dialog_unselect),
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(bbox), button);
  gtk_widget_show(button);

  button = gtk_button_new_with_label(_("Expand\nTree"));
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_install_dialog_expand),
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(bbox), button);
  gtk_widget_show(button);

  button = gtk_button_new_with_label(_("Collapse\nTree"));
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_install_dialog_collapse),
			    GTK_OBJECT(self));
  gtk_container_add(GTK_CONTAINER(bbox), button);
  gtk_widget_show(button);

  self->info_frame = info_frame_new();
  gtk_box_pack_start(GTK_BOX(hbox), self->info_frame, FALSE, TRUE, 0);
  gtk_widget_show(self->info_frame);

  hbox = GTK_DIALOG(self)->action_area;
  button = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
				RPM_STOCK_PIXMAP_QUERY), _("Query"));
  gtk_widget_set_usize(button, 100 /* GNOME_BUTTON_WIDTH */, -1);
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_install_dialog_query),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 0);
  gtk_widget_show(button);

  if(geteuid()==0)
  {
      button = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
				RPM_STOCK_PIXMAP_INSTALL), _("Install"));
      gtk_widget_set_usize(button, 100 /* GNOME_BUTTON_WIDTH */, -1);
      gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_install_dialog_install),
			    GTK_OBJECT(self));
      gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 0);
      gtk_widget_show(button);

      button = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
    				RPM_STOCK_PIXMAP_UPGRADE), _("Upgrade"));
      gtk_widget_set_usize(button, 100 /* GNOME_BUTTON_WIDTH */, -1);
      gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_install_dialog_upgrade),
			    GTK_OBJECT(self));
      gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 0);
      gtk_widget_show(button);
  }
  button = gnome_pixmap_button(gnome_stock_pixmap_widget(NULL,
				RPM_STOCK_PIXMAP_VERIFY), _("Check Sig"));
  gtk_widget_set_usize(button, 100 /* GNOME_BUTTON_WIDTH */, -1);
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_install_dialog_checksig),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 0);
  gtk_widget_show(button);

  button = gnome_stock_button(GNOME_STOCK_BUTTON_CLOSE);
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(gtk_widget_destroy),
			    GTK_OBJECT(self));
  gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 0);
  gtk_widget_show(button);

  self->filesel = NULL;
  self->hdl = NULL;

  self->oldPkg = NULL;
  self->newPkg = NULL;

  self->scan_tag = 0;
  self->dirs = NULL;
  self->scan_dir = NULL;

  self->pkgs = NULL;
  self->filter = INSTALL_FILTER_NOT_INSTALLED;
  self->refilter_tag = 0;
}

GtkWidget *rpm_install_dialog_new(DBHandle *hdl) {
  RpmInstallDialog *self = gtk_type_new(rpm_install_dialog_get_type());

  self->hdl = hdl;

  return GTK_WIDGET(self);
}

void rpm_install_dialog_add_default_packages(RpmInstallDialog *self) {
  gchar *locations = gnome_config_get_string("/gnorpm/Paths/packageLocations="
					     "/mnt/cdrom/RedHat/RPMS:"
					     "/mnt/cdrom/SRPMS:"
					     "/mnt/cdrom/RPMS:"
					     "/mnt/cdrom/i386:"
					     "/mnt/cdrom/alpha:"
					     "/mnt/cdrom/sparc:"
					     "/mnt/cdrom/noarch:"
					     "/usr/src/redhat/RPMS/i386:"
					     "/usr/src/redhat/RPMS/alpha:"
					     "/usr/src/redhat/RPMS/sparc:"
					     "/usr/src/redhat/RPMS/noarch");

  rpm_install_dialog_add_dirs(self, locations);
  g_free(locations);
}

/* returns the status of a particular package in relation to those installed
 * on the system currently */
static PackageStatus check_package_status(DBHandle *hdl, Header h) {
  gchar *name;
#ifndef	HAVE_RPM_4_0
  dbiIndexSet matches;
#endif

  db_handle_db_up(hdl);
  if (hdl->db == NULL) {
    db_handle_db_down(hdl);
    headerGetEntry(h, RPMTAG_NAME, NULL, (void **)&name, NULL);
    g_message("Something confused for %s", name);
    return UNKNOWN_PACKAGE;
  }

  headerGetEntry(h, RPMTAG_NAME, NULL, (void **)&name, NULL);
#ifdef	HAVE_RPM_4_0
 {  rpmdbMatchIterator mi;
    Header tmp, best = NULL;
    gint i;
    mi = rpmdbInitIterator(hdl->db, RPMDBI_LABEL, name, 0);
    if (rpmdbGetIteratorCount(mi) == 0) {
      rpmdbFreeIterator(mi);
      db_handle_db_down(hdl);
      return UNKNOWN_PACKAGE;
    }
    while ((tmp = rpmdbNextIterator(mi)) != NULL) {
      if (!best) best = headerLink(tmp);
      else {
	if (rpmVersionCompare(best, tmp) < 0) {
	  headerFree(best);
	  best = headerLink(tmp);
	}
      }
    }
    rpmdbFreeIterator(mi);

    i = rpmVersionCompare(h, best);
    headerFree(best);
    db_handle_db_down(hdl);
    if (i < 0) return OLD_PACKAGE;
    else if (i == 0) return CURRENT_PACKAGE;
    else return NEW_PACKAGE;
 }

#else	/* HAVE_RPM_4_0 */

  if (!rpmdbFindByLabel(hdl->db, name, &matches)) {
    /* packages by this name found on the system */
    Header best = NULL;
    gint i;

    for (i = 0; i < matches.count; i++) {
      gint index = matches.recs[i].recOffset;
      Header tmp = rpmdbGetRecord(hdl->db, index);

      if (!best) best = tmp;
      else {
	if (rpmVersionCompare(best, tmp) < 0) {
	  headerFree(best);
	  best = tmp;
	} else
	  headerFree(tmp);
      }
    }
    dbiFreeIndexRecord(matches);

    i = rpmVersionCompare(h, best);
    headerFree(best);
    db_handle_db_down(hdl);
    if (i < 0) return OLD_PACKAGE;
    else if (i == 0) return CURRENT_PACKAGE;
    else return NEW_PACKAGE;
  } else {
    db_handle_db_down(hdl);
    return UNKNOWN_PACKAGE;
  }
#endif	/* HAVE_RPM_4_0 */

}

static GtkCTreeNode *get_group_node(RpmInstallDialog *self, gchar *group) {
  gchar *path_stub, *path_parent, *text[2] = { "", "" };
  GtkCTreeNode *parent, *ret;

  if ((ret = g_hash_table_lookup(self->ht, group)) != NULL)
    return ret;

  path_stub = strrchr(group, '/');
  if (!path_stub) {
    parent = self->base;
    path_stub = group;
  } else {
    path_parent = g_strndup(group, path_stub - group);
    parent = get_group_node(self, path_parent);
    g_free(path_parent);
    path_stub++;
  }
  text[0] = path_stub;
  ret = gtk_ctree_insert_node(GTK_CTREE(self->tree), parent, NULL, text, 2,
			      self->closed_p, self->closed_b, self->open_p,
			      self->open_b, FALSE, FALSE);
  gtk_ctree_sort_node(GTK_CTREE(self->tree), parent);

  path_stub = g_strdup(group);
  g_hash_table_insert(self->ht, path_stub, ret);
  return ret;
}

static void set_node_state(RpmInstallDialog *self, GtkCTreeNode *node,
			   gboolean checked) {
  PackageInfo *info = gtk_ctree_node_get_row_data(GTK_CTREE(self->tree), node);

  info->selected = checked;
  if (checked)
    gtk_ctree_node_set_pixmap(GTK_CTREE(self->tree), node,1,
			      self->checkon_p, NULL);
  else
    gtk_ctree_node_set_pixmap(GTK_CTREE(self->tree), node,1,
			      self->checkoff_p, NULL);
}

static gboolean get_node_state(RpmInstallDialog *self, GtkCTreeNode *node) {
  PackageInfo *info = gtk_ctree_node_get_row_data(GTK_CTREE(self->tree), node);

  return info->selected;
}

/* make a particular node visible by expanding its parents, and scrolling the
 * ctree appropriately */
static void show_node(GtkCTree *ctree, GtkCTreeNode *node) {
  GtkCTreeNode *parent = GTK_CTREE_ROW(node)->parent;

  while (parent != NULL) {
    gtk_ctree_expand(ctree, parent);
    parent = GTK_CTREE_ROW(parent)->parent;
  }
  if (gtk_ctree_node_is_visible(ctree, node) != GTK_VISIBILITY_FULL)
    gtk_ctree_node_moveto(ctree, node, 0, 0.5, 0.0);
  gtk_ctree_select(ctree, node);
}

static gboolean pkg_visible(RpmInstallDialog *self, PackageStatus status) {
  switch (self->filter) {
  case INSTALL_FILTER_ALL:
    return TRUE;
  case INSTALL_FILTER_NOT_INSTALLED:
    return (status != CURRENT_PACKAGE);
  case INSTALL_FILTER_UNINSTALLED:
    return (status == UNKNOWN_PACKAGE);
  case INSTALL_FILTER_NEWER:
    return (status == NEW_PACKAGE);
  case INSTALL_FILTER_UNINSTALLED_OR_NEWER:
    return (status == NEW_PACKAGE || status == UNKNOWN_PACKAGE);
  }
  g_assert_not_reached();
  return FALSE; 
}

static gchar *extract_pkg_name(const gchar *fullname) {
  return g_strdup(g_basename(fullname));
}

static gint compare_func(gconstpointer lhs, gconstpointer rhs) {
  gchar *name1, *name2;
  gint result;

  if (!lhs) {
      /* This happens at subfolder nodes, for example. */
      return 1;
  }
  name1 = extract_pkg_name(((PackageInfo *) lhs)->filename);
  name2 = extract_pkg_name(((PackageInfo *) rhs)->filename);
  result = strcmp(name1, name2);
  g_free(name1);
  g_free(name2);
  return result;
}

static GtkCTreeNode *create_ctree_node(RpmInstallDialog *self,
				       GtkCTreeNode *parent,
				       gchar *row[], PackageInfo *info) {
  GtkCTreeNode *node;

  /* Don't do anything if this package is already in the tree. */
  if (gtk_ctree_find_by_row_data_custom(GTK_CTREE(self->tree), parent, info,
			  compare_func) != NULL)
      return NULL;

  node = gtk_ctree_insert_node(GTK_CTREE(self->tree), parent, NULL, row, 2,
			       self->pkg_p, self->pkg_b, self->pkg_p,
			       self->pkg_b, TRUE, FALSE);
  gtk_ctree_sort_node(GTK_CTREE(self->tree), parent);
  gtk_ctree_node_set_row_data(GTK_CTREE(self->tree), node, info);

  /* now to do the node colouring ... */
  if (info->status == OLD_PACKAGE) {
    if (self->oldPkg == NULL) {
      GdkColor colour;
      gchar *colour_name = gnome_config_get_string("/gnorpm/Install/oldPkg=grey50");
      self->oldPkg = gtk_style_copy(gtk_widget_get_style(self->tree));
      if (gdk_color_parse(colour_name, &colour) &&
	  gdk_color_alloc(gtk_widget_get_colormap(self->tree), &colour))
	self->oldPkg->fg[GTK_STATE_NORMAL] = colour;
      g_free(colour_name);
    }
    gtk_ctree_node_set_row_style(GTK_CTREE(self->tree), node, self->oldPkg);
  } else if (info->status == NEW_PACKAGE) {
    if (self->newPkg == NULL) {
      GdkColor colour;
      gchar *colour_name = gnome_config_get_string("/gnorpm/Install/newPkg=blue");
      self->newPkg = gtk_style_copy(gtk_widget_get_style(self->tree));
      if (gdk_color_parse(colour_name, &colour) &&
	  gdk_color_alloc(gtk_widget_get_colormap(self->tree), &colour))
	self->newPkg->fg[GTK_STATE_NORMAL] = colour;
      g_free(colour_name);
    }
    gtk_ctree_node_set_row_style(GTK_CTREE(self->tree), node, self->newPkg);
  } else if (info->status == CURRENT_PACKAGE) {
    if (self->curPkg == NULL) {
      GdkColor colour;
      gchar *colour_name = gnome_config_get_string("/gnorpm/Install/curPkg=green4");
      self->curPkg = gtk_style_copy(gtk_widget_get_style(self->tree));
      if (gdk_color_parse(colour_name, &colour) &&
	  gdk_color_alloc(gtk_widget_get_colormap(self->tree), &colour))
	self->curPkg->fg[GTK_STATE_NORMAL] = colour;
      g_free(colour_name);
    }
    gtk_ctree_node_set_row_style(GTK_CTREE(self->tree), node, self->curPkg);
  }
  if (info->selected) {
    set_node_state(self, node, TRUE);
    show_node(GTK_CTREE(self->tree), node);
  } else
    set_node_state(self, node, FALSE);

  return node;
}

GtkCTreeNode *rpm_install_dialog_add_file(RpmInstallDialog *self,
					  gchar *fname, gboolean selected) {
  gchar buf[512], *s1, *s2, *s3, *row[2] = {"", ""};
  FD_t fd;
  int isSource;
  Header h;
  GtkCTreeNode *parent;
  PackageInfo *info;

  if (g_file_test(fname, G_FILE_TEST_ISDIR)) {
    rpm_install_dialog_add_dirs(self, fname);
    return NULL;
  }

  fd = fdOpen(fname, O_RDONLY, 0);
  if (fdFileno(fd) < 0) {
    if (selected) { /* don't give error when performing directory scan */
      s1 = strrchr(fname, '/');
      s1++;
      g_snprintf(buf, 511, _("Can't open file %s"), s1);
      message_box(buf);
    }
    return NULL;
  }
  if (rpmReadPackageHeader(fd, &h, &isSource, NULL, NULL)) {
    if (selected) { /* don't give error when performing directory scan */
      s1 = strrchr(fname, '/');
      s1++;
      g_snprintf(buf, 511, _("%s doesn't appear to be a RPM package"), s1);
      message_box(buf);
    }
    fdClose(fd);
    return NULL;
  }
  fdClose(fd);
  info = g_new(PackageInfo, 1);
  info->status = UNKNOWN_PACKAGE;
  info->selected = selected;
  info->filename = g_strdup(fname);
  self->pkgs = g_list_prepend(self->pkgs, info);
  if (!h) {
    if (!pkg_visible(self, info->status))
      return NULL;
	/* FIXME: This is a bad situation -- no known info about the package. */
    s1 = strrchr(fname, '/');
    if (s1) s1++; else s1 = fname;
    g_snprintf(buf, 511, "%s%s", s1, isSource?" (S)":"");
    parent = get_group_node(self, _("Unknown"));
	info->rpmname = NULL; /* FIXME: extract possible name from filename? */
	info->version = NULL;
	info->release = NULL;
  } else {
    if (!isSource) info->status = check_package_status(self->hdl, h);
    if (!pkg_visible(self, info->status)) {
      headerFree(h);
      return NULL;
    }

    headerGetEntry(h, RPMTAG_NAME,    NULL, (void**)&s1, NULL);
    headerGetEntry(h, RPMTAG_VERSION, NULL, (void**)&s2, NULL);
    headerGetEntry(h, RPMTAG_RELEASE, NULL, (void**)&s3, NULL);
    info->rpmname = g_strdup(s1);
    info->version = g_strdup(s2);
    info->release = g_strdup(s3);
    g_snprintf(buf, 511, "%s-%s-%s%s", s1, s2, s3, isSource?" (S)":"");
    headerGetEntry(h, RPMTAG_GROUP,   NULL, (void**)&s1, NULL);
    parent = get_group_node(self, s1);
    headerFree(h);
  }
  row[0] = buf;
  return create_ctree_node(self, parent, row, info);
}

static gint refilter_cb(RpmInstallDialog *self) {
  PackageInfo *info;

  if (!self->filter_pos)
    goto finish;
    
  info = self->filter_pos->data;
  if (pkg_visible(self, info->status)) {
    FD_t fd;
    int isSource;
    Header h;
    gchar buf[512], *s1, *s2, *s3, *row[2] = { "", "" };
    GtkCTreeNode *parent;

    fd = fdOpen(info->filename, O_RDONLY, 0);
    if (fdFileno(fd) < 0)
      goto finish;
    if (rpmReadPackageHeader(fd, &h, &isSource, NULL, NULL)) {
      fdClose(fd);
      goto finish;
    }
    fdClose(fd);
    if (!h) {
      s1 = strrchr(info->filename, '/');
      if (s1) s1++; else s1 = info->filename;
      g_snprintf(buf, 511, "%s%s", s1, isSource?" (S)":"");
      parent = get_group_node(self, _("Unknown"));
    } else {
      headerGetEntry(h, RPMTAG_NAME,    NULL, (void**)&s1, NULL);
      headerGetEntry(h, RPMTAG_VERSION, NULL, (void**)&s2, NULL);
      headerGetEntry(h, RPMTAG_RELEASE, NULL, (void**)&s3, NULL);
      g_snprintf(buf, 511, "%s-%s-%s%s", s1, s2, s3, isSource?" (S)":"");
      headerGetEntry(h, RPMTAG_GROUP,   NULL, (void**)&s1, NULL);
      parent = get_group_node(self, s1);
      headerFree(h);
    }
    row[0] = buf;
    create_ctree_node(self, parent, row, info);
  }
 finish:
  if (self->filter_pos)
    self->filter_pos = self->filter_pos->next;
  if (self->filter_pos)
    return TRUE;
  self->refilter_tag = 0;
  gtk_widget_set_sensitive(self->filter_menu, TRUE);
  return FALSE;
}

static gboolean clean_hash(gpointer key, gpointer value, gpointer user_data) {
  g_free(key);
  return TRUE;
}

static void rpm_install_dialog_refilter(RpmInstallDialog *self) {
  GtkCTreeNode *node;

  if (self->refilter_tag)
    gtk_idle_remove(self->refilter_tag);

  /* clear out the tree ... */
  for (node = GTK_CTREE_ROW(self->base)->children;
       node != NULL && GTK_CTREE_ROW(node) != NULL;
       node = GTK_CTREE_ROW(self->base)->children)
    gtk_ctree_remove_node(GTK_CTREE(self->tree), node);
  gtk_ctree_expand(GTK_CTREE(self->tree), self->base);

  /* clear out the hash table ... */
  g_hash_table_freeze(self->ht);
  g_hash_table_foreach_remove(self->ht, clean_hash, NULL);
  g_hash_table_thaw(self->ht);

  gtk_widget_set_sensitive(self->filter_menu, FALSE);
  self->filter_pos = self->pkgs;
  self->refilter_tag = gtk_idle_add((GtkFunction)refilter_cb, self);
}

static void rpm_install_dialog_select_row(RpmInstallDialog *self,
					  GtkCTreeNode *node, gint column) {
  PackageInfo *info;
  if (!GTK_CTREE_ROW(node)->is_leaf) {
    info_frame_clear(self->info_frame);
    return;
  }

  info = gtk_ctree_node_get_row_data(GTK_CTREE(self->tree), node);
  info_frame_set(self->info_frame, info->filename);

  if (column == 1) {
    gboolean old_state = get_node_state(self, node);
    set_node_state(self, node, !old_state);
  }
}

static gint rpm_install_dialog_tree_click(RpmInstallDialog *self,
					  GdkEventButton *event) {
  if (event->type == GDK_2BUTTON_PRESS && event->button == 1) {
    /* a double click on the tree ... */
    gint row;
    GtkCTreeNode *node;
    gboolean old_state;

    if (!gtk_clist_get_selection_info(GTK_CLIST(self->tree), event->x,
				      event->y, &row, NULL))
      return FALSE;
    node = gtk_ctree_node_nth(GTK_CTREE(self->tree), row);
    if (!GTK_CTREE_ROW(node)->is_leaf)
      return FALSE;

    /* we now have a double left click event on a leaf node of the tree ... */
    old_state = get_node_state(self, node);
    set_node_state(self, node, !old_state);
    return TRUE;
  }
  return FALSE;
}
static void rpm_install_dialog_drop_cb(RpmInstallDialog *self,
				       GdkDragContext *context,
				       gint x, gint y,
				       GtkSelectionData *selection_data,
				       guint info, guint time) {
  GList *names, *list;

  switch (info) {
  case TARGET_URI_LIST:
    list = gnome_uri_list_extract_filenames(selection_data->data);
    for (names = list; names; names = names->next) {
      rpm_install_dialog_add_file(self, names->data, TRUE);
    }
    gnome_uri_list_free_strings(list);
    break;
  default:
  }
}

static void rpm_install_dialog_add(RpmInstallDialog *self) {
  gchar *fname;

  fname = gtk_file_selection_get_filename(GTK_FILE_SELECTION(self->filesel));
  rpm_install_dialog_add_file(self, fname, TRUE);
}

static void file_selected(GtkFileSelection *filesel) {
  GtkWidget *frame = gtk_object_get_data(GTK_OBJECT(filesel), "info-frame");
  gchar *filename = gtk_file_selection_get_filename(filesel);

  info_frame_set(frame, filename);
}

static void filesel_destroy(RpmInstallDialog *self) {
  self->filesel = NULL;
}
static void rpm_install_dialog_show_filesel(RpmInstallDialog *self) {
  GtkWidget *button, *label, *frame, *box;
  gchar *string, *dir;

  if (self->filesel != NULL) {
    gdk_window_raise(self->filesel->window);
    return;
  }
  self->filesel = gtk_file_selection_new(_("Add Packages"));
  gtk_signal_connect_object(GTK_OBJECT(self->filesel), "destroy",
			    GTK_SIGNAL_FUNC(filesel_destroy),GTK_OBJECT(self));
  /* security, if for some reason you want to run this SUID */
  gtk_file_selection_hide_fileop_buttons(GTK_FILE_SELECTION(self->filesel));

  button = GTK_FILE_SELECTION(self->filesel)->ok_button;
  label = GTK_BUTTON(button)->child;
  gtk_label_set(GTK_LABEL(label), _("Add"));
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(rpm_install_dialog_add),
			    GTK_OBJECT(self));

  button = GTK_FILE_SELECTION(self->filesel)->cancel_button;
  label = GTK_BUTTON(button)->child;
  gtk_label_set(GTK_LABEL(label), _("Close"));
  gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
			    GTK_SIGNAL_FUNC(gtk_widget_destroy),
			    GTK_OBJECT(self->filesel));

  box = GTK_FILE_SELECTION(self->filesel)->file_list;
  gtk_signal_connect_object(GTK_OBJECT(box), "select_row",
			    GTK_SIGNAL_FUNC(file_selected),
			    GTK_OBJECT(self->filesel));
  box = box->parent; /* the scrolled window */
  box = box->parent; /* the horizontal box containing dir/file lists */

  frame = info_frame_new();
  gtk_object_set_data(GTK_OBJECT(self->filesel), "info-frame", frame);
  gtk_box_pack_start(GTK_BOX(box), frame, FALSE, TRUE, 0);
  gtk_widget_show(frame);

  string = gnome_config_get_string(
			"/gnorpm/Paths/rpmDir=/mnt/cdrom/RedHat/RPMS");
  if (string[strlen(string) - 1] != PATH_SEP) {
    dir = g_copy_strings(string, PATH_SEP_STR, NULL);
    g_free(string);
  } else
    dir = string;
  if (g_file_exists(dir))
    gtk_file_selection_set_filename(GTK_FILE_SELECTION(self->filesel),
				    dir);
  g_free(dir);

  gtk_widget_show(self->filesel);
}

static gint scan_directory(RpmInstallDialog *self) {
  struct dirent *de = readdir(self->scan_dir);
  gchar *filename;
  gint inode;
  struct stat buf;

  while (de == NULL) {
    closedir(self->scan_dir);
    self->scan_dir = NULL;
    while (self->scan_dir == NULL) {
      self->pos++;
      if (self->dirs[self->pos] == NULL) { /* end of list */
	g_strfreev(self->dirs);
	self->dirs = NULL;
	self->scan_tag = 0;
	gtk_widget_set_sensitive(self->filter_menu, TRUE);
	g_list_free(self->seen_inodes);
	self->seen_inodes = NULL;
	return FALSE;
      }
      self->scan_dir = opendir(self->dirs[self->pos]);
    }
    de = readdir(self->scan_dir);
  }
  /* now we have a valid dirent structure */

  /* skip . and .. */
  if (de->d_name[0] == '.' &&
      (de->d_name[1] == '\0' || (de->d_name[1]=='.' && de->d_name[2]=='\0')))
    return TRUE;
  
  /* Check if the directory is already in the 'seen_inodes' list. If so, we
     ignore the current occurence, since otherwise we would be inserting a loop
     and the directory scan would never end (a big problem in GNOME 1, when
     $HOME/.gnome contains a link to $HOME). */
  filename = g_concat_dir_and_file(self->dirs[self->pos], de->d_name);
  stat(filename, &buf);
  inode = buf.st_ino;
  if (g_list_find(self->seen_inodes, GINT_TO_POINTER(inode)) == NULL) {
      self->seen_inodes = g_list_append(self->seen_inodes,
		      GINT_TO_POINTER(inode));
      rpm_install_dialog_add_file(self, filename, FALSE);
  }
  g_free(filename);
  return TRUE;
}

void rpm_install_dialog_add_dirs(RpmInstallDialog *self, gchar *dirstring) {
  gchar **dirs = g_strsplit(dirstring, ":", 0);
  gint inode;
  struct stat buf;

  if (self->scan_tag) {
    /* a directory scan is currently running -- append dirs to list */
    int len1, len2;

    for (len1 = 0; self->dirs[len1] != NULL; len1++)
      ;
    for (len2 = 0; dirs[len2] != NULL; len2++)
      ;
    /* resize the dirs array */
    self->dirs = g_renew(gchar *, self->dirs, len1+len2+1);
    /* tack the new directories onto the end of the list */
    g_memmove(&(self->dirs[len1]), dirs, (len2+1)*sizeof(gchar *));
    g_free(dirs); /* don't free the actual strings */
    return;
  }

  self->scan_tag = 0;
  self->pos = 0;
  self->dirs = dirs;
  self->scan_dir = opendir(self->dirs[0]);
  while (self->scan_dir == NULL) {
    self->pos++;
    if (self->dirs[self->pos] == NULL) {
      g_strfreev(self->dirs); self->dirs = NULL;
      return;
    }
    self->scan_dir = opendir(self->dirs[self->pos]);
  }
  /* Seed the list of already processed inodes with the starting directory. */
  stat(self->dirs[self->pos], &buf);
  inode = buf.st_ino;
  self->seen_inodes = g_list_append(self->seen_inodes, GINT_TO_POINTER(inode));

  gtk_widget_set_sensitive(self->filter_menu, FALSE);
  self->scan_tag = gtk_idle_add((GtkFunction)scan_directory, self);
}

typedef struct {
  RpmInstallDialog *self;
  gint duplicate; /* are duplicate entries allowed in the lists? */
  GList *list;
  GList *package;
  GList *version;
  GList *release;
  GList *clashes;
} recurseStruct;

static gint name_compare(gconstpointer lhs, gconstpointer rhs) {
	return strcmp(lhs, rhs);
}

static void add_to_recurse_struct(recurseStruct *r, gchar *filename,
		gchar *package, gchar *version, gchar *release) {
  r->list = g_list_prepend(r->list, filename);
  r->package = g_list_prepend(r->package, package);
  r->version = g_list_prepend(r->version, version);
  r->release = g_list_prepend(r->release, release);
}

static void remove_from_recurse_struct(recurseStruct *r, gchar *filename,
		gchar *package, gchar *version, gchar *release) {
  r->list = g_list_remove(r->list, filename);
  r->package = g_list_remove(r->package, package);
  r->version = g_list_remove(r->version, version);
  r->release = g_list_remove(r->release, release);
}

static void recurse_func(GtkCTree *ctree, GtkCTreeNode *node, gpointer data) {
  recurseStruct *rs = data;
  gchar *package, *version, *release, *filename;
  gint position, cmp_r, cmp_v;
  GList *item;

  if (GTK_CTREE_ROW(node)->is_leaf && get_node_state(rs->self, node)) {
    PackageInfo *info = gtk_ctree_node_get_row_data(ctree, node);
    if (rs->duplicate) {
      rs->list = g_list_prepend(rs->list, info->filename);
      return;
    }
    if ((item = g_list_find_custom(rs->package, info->rpmname,
				    name_compare))  == NULL) {
      add_to_recurse_struct(rs, info->filename, info->rpmname, info->version,
		      info->release);
    } else {
      position = g_list_position(rs->package, item);
      package = (gchar *) g_list_nth_data(rs->package, position);
      version = (gchar *) g_list_nth_data(rs->version, position);
      release = (gchar *) g_list_nth_data(rs->release, position);
      filename = (gchar *) g_list_nth_data(rs->list, position);
      cmp_v = strcmp(version, info->version);
      cmp_r = strcmp(release, info->release);
      /* If the new package has a later version number, or the same version
       * number and later release number as an existing entry, remove the
       * existing entry and add in the new entry. Otherwise, do nothing. */
      if ((cmp_v == 0 && cmp_r < 0) || cmp_v < 0) {
	remove_from_recurse_struct(rs, filename, package, version, release);
	add_to_recurse_struct(rs, info->filename, info->rpmname,
			info->version, info->release);
	if (g_list_index(rs->clashes, package) == -1)
	  rs->clashes = g_list_append(rs->clashes, package);
      }
    }
  }
}

/*** DEBUG ***/
static void my_debug_func(gpointer data, gpointer user) {
  recurseStruct *rs = (recurseStruct *) user;
  gint position;
  gchar *version, *release, *filename;
  GList *item;

  item = g_list_find_custom(rs->package, data, name_compare);
  position = g_list_position(rs->package, item);
  version = (gchar *) g_list_nth_data(rs->version, position);
  release = (gchar *) g_list_nth_data(rs->release, position);
  filename = (gchar *) g_list_nth_data(rs->list, position);
  printf("Discarding extra versions of %s (installing v. %s, r. %s).\n",
		  (gchar *) data, version, release);
}
/*** DEBUG ***/

/* Get all selected packages from the tree. If two packages with the same name
 * are selected, only the latest one (using (version, release) ordering) will be
 * added to the list. */
static GList *get_selected_packages(RpmInstallDialog *self, gint duplicate) {
  recurseStruct rs;

  rs.self = self;
  rs.duplicate = duplicate;
  rs.list = NULL;
  rs.package = NULL;
  rs.version = NULL;
  rs.release = NULL;
  rs.clashes = NULL;
  gtk_ctree_pre_recursive(GTK_CTREE(self->tree), self->base, recurse_func,
		  &rs);
  /* FIXME: popup list of clashes */
/*** DEBUG ***/
  g_list_foreach(rs.clashes, my_debug_func, &rs);
/*** DEBUG ***/
  g_list_free(rs.package);
  g_list_free(rs.version);
  g_list_free(rs.release);
  g_list_free(rs.clashes);
  return rs.list;
}

static void select_recurse(GtkCTree *ctree, GtkCTreeNode *node,
			     gpointer data) {
  RpmInstallDialog *self = data;

  if (GTK_CTREE_ROW(node)->is_leaf)
    set_node_state(self, node, TRUE);
}
static void rpm_install_dialog_select(RpmInstallDialog *self) {
  gtk_ctree_pre_recursive(GTK_CTREE(self->tree), self->base,
			  select_recurse, self);
}

static void unselect_recurse(GtkCTree *ctree, GtkCTreeNode *node,
			     gpointer data) {
  RpmInstallDialog *self = data;

  if (GTK_CTREE_ROW(node)->is_leaf)
    set_node_state(self, node, FALSE);
}
static void rpm_install_dialog_unselect(RpmInstallDialog *self) {
  GList *tmp;

  gtk_ctree_pre_recursive(GTK_CTREE(self->tree), self->base,
			  unselect_recurse, self);

  /* unselect packages that are not currently displayed as well ... */
  for (tmp = self->pkgs; tmp; tmp = tmp->next)
    ((PackageInfo *)tmp->data)->selected = FALSE;
}

static void rpm_install_dialog_expand(RpmInstallDialog *self) {
  gtk_ctree_expand_recursive(GTK_CTREE(self->tree), self->base);
}

static void rpm_install_dialog_collapse(RpmInstallDialog *self) {
  gtk_ctree_collapse_recursive(GTK_CTREE(self->tree), self->base);
  gtk_ctree_expand(GTK_CTREE(self->tree), self->base);
}

static void rpm_install_dialog_query(RpmInstallDialog *self) {
  GList *files = get_selected_packages(self, 1);

  gtk_signal_emit(GTK_OBJECT(self), installdlg_signals[QUERY], files);
  g_list_free(files);
}

static void rpm_install_dialog_install(RpmInstallDialog *self) {
  GList *files = get_selected_packages(self, 0);

  gtk_signal_emit(GTK_OBJECT(self), installdlg_signals[INSTALL], files);
  g_list_free(files);
}

static void rpm_install_dialog_upgrade(RpmInstallDialog *self) {
  GList *files = get_selected_packages(self, 0);

  gtk_signal_emit(GTK_OBJECT(self), installdlg_signals[UPGRADE], files);
  g_list_free(files);
}

static void rpm_install_dialog_checksig(RpmInstallDialog *self) {
  GList *files = get_selected_packages(self, 1);

  gtk_signal_emit(GTK_OBJECT(self), installdlg_signals[CHECKSIG], files);
  g_list_free(files);
}

static void rpm_install_dialog_destroy(GtkObject *object) {
  RpmInstallDialog *self = RPM_INSTALL_DIALOG(object);

  if (self->filesel) gtk_widget_destroy(self->filesel);

  gdk_pixmap_unref(self->open_p);     gdk_bitmap_unref(self->open_b);
  gdk_pixmap_unref(self->closed_p);   gdk_bitmap_unref(self->closed_b);
  gdk_pixmap_unref(self->pkg_p);      gdk_bitmap_unref(self->pkg_b);
  gdk_pixmap_unref(self->checkon_p);
  gdk_pixmap_unref(self->checkoff_p);

  if (self->oldPkg) gtk_style_unref(self->oldPkg);
  if (self->newPkg) gtk_style_unref(self->newPkg);
  if (self->curPkg) gtk_style_unref(self->curPkg);

  g_hash_table_foreach(self->ht, (GHFunc) g_free, NULL);
  g_hash_table_destroy(self->ht);

  g_list_foreach(self->pkgs, (GFunc)g_free, NULL);
  g_list_free(self->pkgs);

  if (self->scan_tag) gtk_idle_remove(self->scan_tag);
  if (self->refilter_tag) gtk_idle_remove(self->refilter_tag);
  if (self->dirs)     g_strfreev(self->dirs);
  if (self->scan_dir) closedir(self->scan_dir);

  if (parent_class->destroy)
    (* parent_class->destroy)(object);
}

static void rpm_install_dialog_marshal_signal(GtkObject *object,
					      GtkSignalFunc func,
					      gpointer data, GtkArg *args) {
  typedef void (*sig_func)(GtkObject *o, GList *files, gpointer data);
  sig_func rfunc = (sig_func)func;

  (*rfunc)(object, GTK_VALUE_POINTER(args[0]), data);
}


/* this should probably be a full widget, but this quick hack works fine */
GtkWidget *info_frame_new(void) {
  GtkWidget *frame, *vbox, *name, *version, *summary, *hsep;

  frame = gtk_frame_new(_("Package Info"));
  gtk_widget_set_usize(frame, 140, -1);

  vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD_SMALL);
  gtk_container_add(GTK_CONTAINER(frame), vbox);
  gtk_widget_show(vbox);

  name = gtk_label_new("");
  gtk_label_set_line_wrap(GTK_LABEL(name), TRUE);
  gtk_label_set_justify(GTK_LABEL(name), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment(GTK_MISC(name), 0.0, 0.5);
  gtk_box_pack_start(GTK_BOX(vbox), name, FALSE, TRUE, 0);
  gtk_widget_show(name);

  version = gtk_label_new("");
  gtk_label_set_line_wrap(GTK_LABEL(version), TRUE);
  gtk_label_set_justify(GTK_LABEL(version), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment(GTK_MISC(version), 0.0, 0.5);
  gtk_box_pack_start(GTK_BOX(vbox), version, FALSE, TRUE, 0);
  gtk_widget_show(version);

  hsep = gtk_hseparator_new();
  gtk_box_pack_start(GTK_BOX(vbox), hsep, FALSE, TRUE, 0);

  summary = gtk_label_new("");
  gtk_widget_set_usize(summary, 130, -1);
  gtk_label_set_line_wrap(GTK_LABEL(summary), TRUE);
  gtk_label_set_justify(GTK_LABEL(summary), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment(GTK_MISC(summary), 0.0, 0.5);
  gtk_box_pack_start(GTK_BOX(vbox), summary, FALSE, TRUE, 0);
  gtk_widget_show(summary);

  gtk_object_set_data(GTK_OBJECT(frame), "rpm-name", name);
  gtk_object_set_data(GTK_OBJECT(frame), "rpm-version", version);
  gtk_object_set_data(GTK_OBJECT(frame), "rpm-hsep", hsep);
  gtk_object_set_data(GTK_OBJECT(frame), "rpm-summary", summary);

  return frame;
}

void info_frame_clear(GtkWidget *frame) {
  GtkLabel *name    = gtk_object_get_data(GTK_OBJECT(frame), "rpm-name");
  GtkLabel *version = gtk_object_get_data(GTK_OBJECT(frame), "rpm-version");
  GtkWidget *hsep   = gtk_object_get_data(GTK_OBJECT(frame), "rpm-hsep");
  GtkLabel *summary = gtk_object_get_data(GTK_OBJECT(frame), "rpm-summary");

  gtk_label_set_text(name, "");
  gtk_label_set_text(version, "");
  gtk_widget_hide(hsep);
  gtk_label_set_text(summary, "");
}

void info_frame_set(GtkWidget *frame, gchar *filename) {
  GtkLabel *name    = gtk_object_get_data(GTK_OBJECT(frame), "rpm-name");
  GtkLabel *version = gtk_object_get_data(GTK_OBJECT(frame), "rpm-version");
  GtkWidget *hsep   = gtk_object_get_data(GTK_OBJECT(frame), "rpm-hsep");
  GtkLabel *summary = gtk_object_get_data(GTK_OBJECT(frame), "rpm-summary");

  gchar buf[512], *s1, *s2;
  FD_t fd;
  int isSource;
  Header h;

  fd = fdOpen(filename, O_RDONLY, 0);
  if (fdFileno(fd) < 0) {
    info_frame_clear(frame);
    return;
  }
  if (rpmReadPackageHeader(fd, &h, &isSource, NULL, NULL)) {
    info_frame_clear(frame);
    fdClose(fd);
    return;
  }
  fdClose(fd);
  if (!h && isSource) {
    s1 = strrchr(filename, '/');
    if (s1)
      s1++;
    else
      s1 = filename;
    g_snprintf(buf, 511, "%s (S)", s1);
    gtk_label_set_text(name, buf);
    gtk_label_set_text(version, "");
    gtk_widget_hide(hsep);
    gtk_label_set_text(summary, "");
  } else {
    headerGetEntry(h, RPMTAG_NAME, NULL, (void **)&s1, NULL);
    gtk_label_set_text(name, s1);
    headerGetEntry(h, RPMTAG_VERSION, NULL, (void **)&s1, NULL);
    headerGetEntry(h, RPMTAG_RELEASE, NULL, (void **)&s2, NULL);
    g_snprintf(buf, 511, "%s-%s%s", s1, s2, isSource ? " (S)" : "");
    gtk_label_set_text(version, buf);
    gtk_widget_show(hsep);
    headerGetEntry(h, RPMTAG_SUMMARY, NULL, (void **)&s1, NULL);
    gtk_label_set_text(summary, s1);
    headerFree(h);
  }
}
