/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <gnome.h>
#include <rpmlib.h>
#include "misc.h"

/* a nice rpm icon */
#include "rpm-icon.xpm"

extern GnomeApp *gnorpm_app;
extern GtkWidget *statusbar;

void message_box(char *msg) {
  GtkWidget *win;

  if (gnorpm_app)
    win = gnome_app_message(gnorpm_app, msg);
  else
    win = gnome_ok_dialog(msg);
  if (win)
    gnome_dialog_run_and_close(GNOME_DIALOG(win));
}

static void error_handler() {
  GtkWidget *win;

  if (gnorpm_app)
    win = gnome_app_error(gnorpm_app, rpmErrorString());
  else
    win = gnome_ok_dialog(rpmErrorString());
  if (win)
    gnome_dialog_run_and_close(GNOME_DIALOG(win));
}

void setupErrorHandler() {
  rpmErrorSetCallback(error_handler);
}

void set_icon(GtkWidget *w) {
  GdkWindow *win;
  GdkPixmap *icon;
  GdkBitmap *mask;

  gtk_widget_realize(w);
  win = w->window;
  if (!win) return;

  icon = gdk_pixmap_create_from_xpm_d(win, &mask, NULL, rpm_icon);
  gdk_window_set_icon(win, NULL, icon, mask);
}

void statusbar_msg(char *txt, ...)
{
	char buf[512];
	va_list ap;

	va_start(ap, txt);
	vsnprintf(buf,512,txt,ap);
	va_end(ap);

	if(statusbar)
	{
		gnome_appbar_pop(GNOME_APPBAR(statusbar));
  		gnome_appbar_push(GNOME_APPBAR(statusbar), buf);
	}
	else g_warning(buf);
}
