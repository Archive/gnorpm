/* GnoRPM - A GNOME front end for the Red Hat Package Manager (RPM)
 * Copyright (C) 1998-1999  James Henstridge
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <gnome.h>

#include <rpmlib.h>
#include <rpmio.h>

#include "rpmprogress.h"

#include "install.h"
#include "misc.h"

/* I used the rpm source code as a template for these functions, so I just
 * thought I would say that portions of this are probably Copyright Red Hat
 */

/* what these routines will assume the size of old source packages is */
#define UNKNOWN_SIZE 1024

/* In rpm <= 4.0.1, rpmDependencyConflict is a structure. In rpm > 4.0.1, the
 * structure is called rpmDependencyConflict_s and rpmDependencyConflict is a
 * typedef for a pointer to that structure. This mess tries to get around
 * that.
 */
#ifdef HAVE_RPM_4_0_2
typedef rpmDependencyConflict rpmDepConflict;
#else
typedef struct rpmDependencyConflict * rpmDepConflict;
#endif

static RpmProgress *prog;

static void * updateDisp(const Header h, const rpmCallbackType what,
			 const unsigned long amount, const unsigned long total,
			 const void * pkgKey, void * data)
{
  static FD_t fd;
  gint size;
  unsigned long *sizep;
  char *name;
  const char * filename = pkgKey;

  switch (what) {
    case RPMCALLBACK_INST_OPEN_FILE:
      name = "";
      size = 0;
      fd = fdOpen(filename, O_RDONLY, 0);
      headerGetEntry(h, RPMTAG_SIZE, NULL, (void **)&sizep, NULL);
      headerGetEntry(h, RPMTAG_NAME, NULL, (void **)&name, NULL);
      size = *sizep;
      rpm_progress_next(prog, name, size);

      return fd;

    case RPMCALLBACK_INST_CLOSE_FILE:
      fdClose(fd);
      break;

    case RPMCALLBACK_INST_PROGRESS:
      rpm_progress_update(prog, (1.0 * amount / total));
      break;

    default:
      break;
  }
  return NULL;
}

static void addDepNodes(rpmdb db, gchar *rootdir,
			GtkCTree *ctree, GtkCTreeNode *parent,
			rpmDepConflict conflicts,
			int numConflicts, GList **processed) {
  GtkCTreeNode *node;
  gint i, flags;
  guint32 offs;
  gchar buf[1024], *row[1];
  rpmDepConflict conf2;
  gint numConf2;

  row[0] = buf;
  for (i = 0; i < numConflicts; i++) {
    /* construct the text for this conflict */
    buf[0] = '\0';
    strcat(buf, conflicts[i].byName); strcat(buf, "-");
    strcat(buf, conflicts[i].byVersion); strcat(buf, "-");
    strcat(buf, conflicts[i].byRelease);
    if (conflicts[i].sense == RPMDEP_SENSE_REQUIRES)
      strcat(buf, _(" requires "));
    else
      strcat(buf, _(" conflicts with "));
    strcat(buf, conflicts[i].needsName);
    flags = conflicts[i].needsFlags;
    if (flags) {
      strcat(buf, " ");
      if (flags & RPMSENSE_LESS)
        strcat(buf, "<");
      if (flags & RPMSENSE_GREATER)
        strcat(buf, ">");
      if (flags & RPMSENSE_EQUAL)
        strcat(buf, "=");
      if (flags & RPMSENSE_SERIAL)
        strcat(buf, "S");
      strcat(buf, " ");
      strcat(buf, conflicts[i].needsVersion);
    }
    node = gtk_ctree_insert_node(ctree, parent, NULL, row, 0,
				 NULL, NULL, NULL, NULL, FALSE, FALSE);

    /* get the record offset for this package ... */
    offs = 0;
#ifdef	HAVE_RPM_4_0
  { rpmdbMatchIterator mi;
    const char * name, * version, * release;
    headerNVR(conflicts[i].byHeader, &name, &version, &release);
    mi = rpmdbInitIterator(db, RPMTAG_NAME, name, 0);
    rpmdbSetIteratorVersion(mi, version);
    rpmdbSetIteratorRelease(mi, release);
    while (rpmdbNextIterator(mi)) {
      offs = rpmdbGetIteratorOffset(mi);
      break;
    }
    rpmdbFreeIterator(mi);
  }
#else
  { dbiIndexSet matches;
    if (rpmdbFindByHeader(db, conflicts[i].byHeader, &matches))
      /* could not find header ... */
      continue;
    offs = matches.recs[0].recOffset;
    dbiFreeIndexRecord(matches);
  }
#endif
    if (!offs || g_list_find(*processed, GUINT_TO_POINTER(offs)))
	/* already processed this package */
	continue;
    *processed = g_list_prepend(*processed, GUINT_TO_POINTER(offs));

    /* get a conflicts array for this package */
    {
      rpmTransactionSet rpmdep = rpmtransCreateSet(db, rootdir);
      rpmtransRemovePackage(rpmdep, offs);
      if (rpmdepCheck(rpmdep, &conf2, &numConf2)) {
	rpmtransFree(rpmdep);
	continue;
      }
      rpmtransFree(rpmdep);
    }
    /* add nodes for packages this one depends on ... */
    addDepNodes(db, rootdir, ctree, node, conf2, numConf2, processed);
    /* free the conflicts array */
    rpmdepFreeConflicts(conf2, numConf2);
  }
}

/* returns TRUE if yes was pressed */
static gboolean displayDepProblems(rpmDepConflict conflicts,
				   int numConflicts,
				   rpmdb db, gchar *rootdir,
				   GList **dependentPkgs) {
  GtkWidget *win, *w, *ctree;
  gint ret = FALSE;
  GList *processed = NULL;

  win = gnome_dialog_new(_("Dependency Problems"), GNOME_STOCK_BUTTON_YES,
			 GNOME_STOCK_BUTTON_NO, NULL);
  gnome_dialog_close_hides(GNOME_DIALOG(win), FALSE);
  gtk_window_set_policy(GTK_WINDOW(win), FALSE, TRUE, TRUE);
  set_icon(win);
  gtk_widget_set_usize(win, 300, -1);
  gnome_dialog_set_default(GNOME_DIALOG(win), 1);
  gnome_dialog_set_close(GNOME_DIALOG(win), TRUE);

  w = gtk_label_new(_("The following dependency problems occured:"));
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(win)->vbox), w, FALSE, TRUE, 0);
  gtk_widget_show(w);

  w = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_usize(w, 400, 160);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(w), GTK_POLICY_AUTOMATIC,
				 GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(win)->vbox), w, TRUE, TRUE, 0);
  gtk_widget_show(w);

  ctree = gtk_ctree_new(1, 0);
  gtk_ctree_set_indent(GTK_CTREE(ctree), 10);
  gtk_clist_set_selection_mode(GTK_CLIST(ctree), GTK_SELECTION_BROWSE);
  gtk_clist_set_column_auto_resize(GTK_CLIST(ctree), 0, TRUE);

  addDepNodes(db, rootdir, GTK_CTREE(ctree), NULL,
	      conflicts, numConflicts, &processed);
  if (dependentPkgs == NULL)
    g_list_free(processed);
  else
    *dependentPkgs = processed;

  gtk_container_add(GTK_CONTAINER(w), ctree);
  gtk_widget_show(ctree);

  w = gtk_label_new(_("Do you want to ignore these problems?\n"
		      "(saying yes may make your system unstable)"));
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(win)->vbox), w, FALSE, TRUE, 0);
  gtk_widget_show(w);

  gtk_widget_show(win);
  ret = gnome_dialog_run_and_close(GNOME_DIALOG(win));
  return !ret;
}

static gboolean printProblems(rpmProblemSet probs)
{
  GtkWidget *win, *w, *list;
  gint i, ret = FALSE;
  GList *items = NULL;

  win = gnome_dialog_new(_("Installation Problems"), GNOME_STOCK_BUTTON_YES,
			 GNOME_STOCK_BUTTON_NO, NULL);
  gnome_dialog_close_hides(GNOME_DIALOG(win), FALSE);
  gtk_window_set_policy(GTK_WINDOW(win), FALSE, TRUE, TRUE);
  set_icon(win);
  gtk_widget_set_usize(win, 300, -1);
  gnome_dialog_set_default(GNOME_DIALOG(win), 1);
  gnome_dialog_set_close(GNOME_DIALOG(win), TRUE);

  w = gtk_label_new(_("The following installation problems occured:"));
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(win)->vbox), w, FALSE, TRUE, 0);
  gtk_widget_show(w);

  for (i = 0; i < probs->numProblems; i++) {
      if (!probs->probs[i].ignoreProblem) {
#ifdef	HAVE_RPM_4_0_1
	  const char *msg = rpmProblemString(probs->probs + i);
#else
	  const char *msg = rpmProblemString(probs->probs[i]);
#endif
	  w = gtk_list_item_new_with_label(msg);
	  gtk_widget_show(w);
	  items = g_list_append(items, w);
	  free((void *)msg);
      }
  }

  w = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_usize(w, 320, 120);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(w), GTK_POLICY_AUTOMATIC,
				 GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(win)->vbox), w, TRUE, TRUE, 0);
  gtk_widget_show(w);
  list = gtk_list_new();
  gtk_list_set_selection_mode(GTK_LIST(list), GTK_SELECTION_BROWSE);
  gtk_list_append_items(GTK_LIST(list), items);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(w), list);
  gtk_widget_show(list);

  w = gtk_label_new(_("Do you want to ignore these problems?\n"
		      "(saying yes may make your system unstable)"));
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(win)->vbox), w, FALSE, TRUE, 0);
  gtk_widget_show(w);

  gtk_widget_show(win);
  ret = gnome_dialog_run_and_close(GNOME_DIALOG(win));

  return (!ret);
}

int do_install(gchar *rootdir, GList *pkgs, gchar *location,
	       gint transFlags, gint probFilter, gint interfaceFlags,
	       rpmInstallCb cb, void *user_data) {
  gint mode, rc, i, isSource, stopInstall = 0, pkgc;
  gint numPackages, numBinaryPackages = 0;
  gint numSourcePackages = 0, numFailed = 0;
  gint numConflicts, totSize = 0;
  gchar **packages, *pkg_file;
  Header *binaryHeaders;
  rpmdb db;
  rpmTransactionSet rpmdep = NULL;
  rpmProblemSet probs = NULL, finalProbs = NULL;
  GtkWidget *win, *wid;

  FD_t fd;
  rpmDepConflict conflicts;
  gulong *sizep;

  if (transFlags & RPMTRANS_FLAG_TEST)
    mode = O_RDONLY;
  else
    mode = O_RDWR | O_CREAT;

  pkgc = g_list_length(pkgs);
  packages = g_new0(gchar *, pkgc + 1);
  binaryHeaders = g_new0(Header, pkgc + 1);
  for (numPackages = 0; pkgs != NULL; pkgs = pkgs->next) {
    pkg_file = pkgs->data;
    fd = fdOpen(pkg_file, O_RDONLY, 0644);
    if (fdFileno(fd) < 0) {
      numFailed++;
      continue;
    }
    packages[numPackages++] = pkg_file;
    rc = rpmReadPackageHeader(fd, &binaryHeaders[numBinaryPackages],
                              &isSource, NULL, NULL);
    fdClose(fd);
    if (binaryHeaders[numBinaryPackages]) {
      if (headerGetEntry(binaryHeaders[numBinaryPackages], RPMTAG_SIZE,
			 NULL, (void **) &sizep, NULL))
	totSize += *sizep;
    } else
      totSize += UNKNOWN_SIZE;
    if (rc) {
      numPackages--;
      numFailed++;
    } else if (isSource) {
      if (binaryHeaders[numBinaryPackages])
        headerFree(binaryHeaders[numBinaryPackages]);
      numSourcePackages++;
    } else
      numBinaryPackages++;
  }
  if (numBinaryPackages) {
    if (rpmdbOpen(rootdir, &db, mode, 0644)) {
      for (i=0; i < numBinaryPackages; i++)
        headerFree(binaryHeaders[i]);
      g_free(binaryHeaders);
      g_free(packages);
      return numPackages;
    }

    rpmdep = rpmtransCreateSet(db, rootdir);

    for (i =0; i < numBinaryPackages; i++)
      rpmtransAddPackage(rpmdep, binaryHeaders[i], NULL, packages[i],
			 interfaceFlags & INTER_UPGRADE, NULL);

    if (!(interfaceFlags & INTER_NODEPS)) {
      if (rpmdepCheck(rpmdep, &conflicts, &numConflicts)) {
        numFailed = numPackages;
        message_box(_("Dependency check failed."));
        stopInstall = 1;
      }
      if (!stopInstall && conflicts) {
        if (!displayDepProblems(conflicts, numConflicts, db, rootdir, NULL)) {
	  numFailed = numPackages;
	  stopInstall = 1;
	}
	rpmdepFreeConflicts(conflicts, numConflicts);
      }
    }
    if (!(interfaceFlags & INTER_NOORDER))
      if (rpmdepOrder(rpmdep)) {
        numFailed = numPackages;
        stopInstall = 1;
      }
  } else
    db = NULL;
  if (!stopInstall && rpmdep != NULL) {
    /* do the actual install */
      win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
      set_icon(win);
      gtk_widget_set_usize(win, 350, -1);
      gtk_container_set_border_width(GTK_CONTAINER(win), 10);
      if (interfaceFlags & INTER_UPGRADE)
	  gtk_window_set_title(GTK_WINDOW(win), _("Upgrading"));
      else
	  gtk_window_set_title(GTK_WINDOW(win), _("Installing"));

      wid = rpm_progress_new(numPackages, totSize);
      gtk_container_add(GTK_CONTAINER(win), wid);
      gtk_widget_show(wid);

      prog = RPM_PROGRESS(wid);
      gtk_widget_show(win);
      rc = rpmRunTransactions(rpmdep, (rpmCallbackFunction) updateDisp, NULL,
				  NULL, &probs, transFlags, probFilter);


      if (rc > 0) {
	  if (printProblems(probs)) {
	      /* ignore all the problems */
	      rc = rpmRunTransactions(rpmdep, (rpmCallbackFunction) updateDisp,
				      NULL, probs, &finalProbs,
				      transFlags, probFilter);
	      rpmProblemSetFree(finalProbs);
	  }
      }
      gtk_widget_destroy(win);
      rpmProblemSetFree(probs);
      probs = NULL;
      if (rc == -1) /* some error */
	  numFailed = numPackages;
      else
	  numFailed += rc;
  }
  for (i = 0; i < numBinaryPackages; i++)
    headerFree(binaryHeaders[i]);
  g_free(binaryHeaders);
  if (db) rpmdbClose(db);
  g_free(packages);
  if (rpmdep != NULL)
      rpmtransFree(rpmdep);

  return numFailed;
}

static gboolean continue_remove(rpmdb db, GList *indices) {
  GList *tmp, *items = NULL;
  gchar *s1, *s2, *s3, buf[512];
  GtkWidget *win, *box, *w, *list;
  gboolean ret = FALSE;

  win = gnome_dialog_new(_("Continue Removal"), GNOME_STOCK_BUTTON_YES,
			 GNOME_STOCK_BUTTON_NO, NULL);
  gnome_dialog_close_hides(GNOME_DIALOG(win), FALSE);
  gtk_window_set_policy(GTK_WINDOW(win), FALSE, TRUE, TRUE);
  set_icon(win);
  gnome_dialog_set_default(GNOME_DIALOG(win), 0);
  gnome_dialog_set_close(GNOME_DIALOG(win), TRUE);

  box = GNOME_DIALOG(win)->vbox;
  w = gtk_label_new(_("Remove the following packages?"));
  gtk_box_pack_start(GTK_BOX(box), w, FALSE, TRUE, 0);
  gtk_widget_show(w);
  for (tmp = indices; tmp; tmp = tmp->next) {
    guint32 index = GPOINTER_TO_UINT(tmp->data);
    GtkWidget *item;
    Header h;

#ifdef	HAVE_RPM_4_0
    rpmdbMatchIterator mi;

    mi = rpmdbInitIterator(db, RPMDBI_PACKAGES, &index, sizeof(index));
    h = rpmdbNextIterator(mi);
    if (h)
      h = headerLink(h);
    rpmdbFreeIterator(mi);
#else
    h = rpmdbGetRecord(db, index);
#endif

    headerGetEntry(h, RPMTAG_NAME, NULL, (void**)&s1, NULL);
    headerGetEntry(h, RPMTAG_VERSION, NULL, (void**)&s2, NULL);
    headerGetEntry(h, RPMTAG_RELEASE, NULL, (void**)&s3, NULL);
    g_snprintf(buf, 511, "%s-%s-%s", s1, s2, s3);
    item = gtk_list_item_new_with_label(buf);
    gtk_widget_show(item);
    items = g_list_append(items, item);

    headerFree(h);
  }
  w = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_usize(w, 320, 120);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(w), GTK_POLICY_AUTOMATIC,
				 GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start(GTK_BOX(box), w, TRUE, TRUE, 0);
  gtk_widget_show(w);
  list = gtk_list_new();
  gtk_list_set_selection_mode(GTK_LIST(list), GTK_SELECTION_BROWSE);
  gtk_list_append_items(GTK_LIST(list), items);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(w), list);
  gtk_widget_show(list);

  gtk_widget_show(win);
  ret = gnome_dialog_run_and_close(GNOME_DIALOG(win));
  return !ret;
}

gint do_uninstall(gchar *root, GList *indices, gint transFlags,
		  gint probFilter, gint interfaceFlags) {
  rpmdb db;
  gint mode, numFailed = 0, numConflicts, stopUninstall = 0;
  rpmTransactionSet rpmdep = NULL;
  rpmDepConflict conflicts;
  GList *tmp;
  rpmProblemSet probs;

  if (transFlags & RPMTRANS_FLAG_TEST)
    mode = O_RDONLY;
  else
    mode = O_RDWR | O_EXCL;
  if (rpmdbOpen(root, &db, mode, 0644)) {
    return g_list_length(indices);
  }
  if (!continue_remove(db, indices)) {
    rpmdbClose(db);
    return 0;
  }
  if (!(interfaceFlags & INTER_NODEPS)) {
    rpmdep = rpmtransCreateSet(db, root);

    for (tmp = indices; tmp != NULL; tmp = tmp->next)
	rpmtransRemovePackage(rpmdep, GPOINTER_TO_UINT(tmp->data));
    if (rpmdepCheck(rpmdep, &conflicts, &numConflicts)) {
      numFailed = g_list_length(indices);
      stopUninstall = 1;
    }

    if (!stopUninstall && conflicts) {
      if (!displayDepProblems(conflicts, numConflicts, db, root, NULL)) {
	numFailed += g_list_length(indices);
	stopUninstall = 1;
      }
      rpmdepFreeConflicts(conflicts, numConflicts);
    }
  }
  if (!stopUninstall && rpmdep != NULL) {
      numFailed += rpmRunTransactions(rpmdep, NULL, NULL, NULL, &probs,
				      transFlags, probFilter);
  }

  if (rpmdep != NULL)
    rpmtransFree(rpmdep);
  
  rpmdbClose(db);
  return numFailed;
}

gint install_one(gchar *root, gchar *file, gchar *location,
		 gint transFlags, gint probFilter, gint interfaceFlags,
		 rpmInstallCb cb, void *user_data) {
  GList *list;
  gint result;

  list = g_list_append(NULL, file);
  result = do_install(root, list, location, transFlags, probFilter,
		      interfaceFlags, cb, user_data);
  g_list_free(list);
  return result;
}

gint uninstall_one(gchar *root, guint index, gint transFlags,
		   gint probFilter, gint interfaceFlags) {
  GList *list;
  gint result;
  list = g_list_append(NULL, GUINT_TO_POINTER(index));
  result = do_uninstall(root, list, transFlags, probFilter, interfaceFlags);
  g_list_free(list);
  return result;
}
